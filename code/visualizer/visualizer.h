/*
File:   visualizer.h
Author: Taylor Robbins
Date:   10\23\2020
*/

#ifndef _VISUALIZER_H
#define _VISUALIZER_H

#define VIS_PEOPLE_SPACE_SIZE             NewVec2(300, 150) //px
#define VIS_MESSAGE_SPACE_HEIGHT          500 //px
#define MAX_NUM_PARTICLES                 1024
#define NUM_ALIEN_COLORS                  18
#define ALIEN_PINK_COLOR                  8
#define ALIEN_INVISIBLE_COLOR             13
#define ALIEN_RAINBOW_COLOR               17
#define PEOPLE_MSG_BUBBLE_DEPTH           0.30f
#define PEOPLE_NAME_DEPTH                 0.31f
#define PEOPLE_MIN_DEPTH                  0.50f
#define PEOPLE_MAX_DEPTH                  0.60f
#define PEOPLE_DEPTH_PADDING_SIZE         32 //px
#define PEOPLE_WALK_CHOICE_INNER_PADDING  32 //px
#define MESSAGE_SPACE_PADDING             10 //px
#define MESSAGE_USER_MSG_SPACING          5 //px
#define MESSAGE_VERT_PADDING              3 //px
#define MESSAGE_DISAPPEAR_TIME            (45*1000) //ms
#define MESSAGE_DISAPPEAR_ANIM_TIME       200 //ms
#define MESSAGE_SPAWN_ANIM_TIME           300
#define PEOPLE_EAT_DISTANCE               35 //px
#define PEOPLE_AVOID_DISTANCE             50 //px
#define PEOPLE_AVOID_RUN_DISTANCE         30 //px
#define PEOPLE_AUTO_SLEEP_TIME            (20*60*1000) //ms (10 minutes)
#define STRETCH_TIME                      (30*60*1000) //ms (30 minutes)
#define STRETCH_PAGE_AMOUNT               (5*60*1000) //ms (5 minutes)
#define FIRE_LIFE_SPAN                    15000 //ms
#define FIRE_RADIUS                       10 //px
#define FIRE_BURN_MARK_LIFE_SPAN          30000 //ms
#define FIRE_AVOID_DISTANCE               (FIRE_RADIUS*2) //px
#define FIRE_SPAWNER_AVOID_DISTANCE       (FIRE_RADIUS*6) //px
#define FIRE_SPAWNER_IMMUNE_TIME          1000 //ms
#define PEOPLE_BURN_TIME                  8000 //ms
#define POISON_ANIM_TIME                  60000 //ms

//ABD stands for Abduction
#define ABD_FLY_IN_TIME   2000 //ms
#define ABD_WAIT_TIME     15000 //ms
#define ABD_MIN_SUCK_TIME 5000 //ms
#define ABD_FLY_OUT_TIME  2000 //ms
#define ABD_TRACTOR_SPEED 1.5f //px/frame
#define ABD_FALL_SPEED    4.0f //px/frame

struct FriendlyName_t
{
	char* userStr;
	char* displayStr;
};

typedef enum
{
	PersonAnim_Idle = 0,
	PersonAnim_Spawning,
	PersonAnim_Chatting,
	PersonAnim_ChattingUnicode,
	PersonAnim_ChattingParty,
	PersonAnim_Eating,
	PersonAnim_Sleeping,
	PersonAnim_Jumping,
	PersonAnim_PlayingDead,
	PersonAnim_BeingAbducted,
	PersonAnim_BeingPoisoned,
	PersonAnim_NumAnims,
} PersonAnim_t;

typedef enum
{
	PersonFlag_FacingLeft        = 0x00000001,
	PersonFlag_IsRunning         = 0x00000002,
	PersonFlag_IsHappy           = 0x00000004,
	PersonFlag_IsSad             = 0x00000008,
	PersonFlag_IsStopped         = 0x00000010,
	PersonFlag_IsAsleep          = 0x00000020, //TODO: Recycle this flag
	PersonFlag_Big               = 0x00000040,
	PersonFlag_Smol              = 0x00000080,
	PersonFlag_AvoidAll          = 0x00000100,
	PersonFlag_AvoidRunning      = 0x00000200,
	PersonFlag_IsSmiling         = 0x00000400,
	PersonFlag_Persistant        = 0x00000800,
	PersonFlag_AvoidFires        = 0x00001000,
	PersonFlag_Poisonous         = 0x00002000,
} PersonFlag_t;

struct Person_t
{
	u32 flags;
	u32 chatterId;
	u8 alienColor;
	v2 position;
	r32 height;
	v2 velocity;
	v2 targetPos;
	r32 walkDelayTime;
	r32 animProgress;
	PersonAnim_t anim;
	FriendlyName_t* displayName;
	u32 lookChatterId;
	u32 eatChatterId;
	u32 avoidChatterId;
	r32 fireTimer;
};

struct ActiveFire_t
{
	u32 spawnChatterId;
	v2 position;
	r32 radius;
	r32 scale;
	r32 age;
	r32 lifeSpan;
	r32 burnMarkLifeSpan;
};

typedef enum
{
	ParticleFlag_Alive           = 0x01,
	ParticleFlag_ScreenSpace     = 0x02,
} ParticleFlags_t;

struct Particle_t
{
	u8 flags;
	const SpriteSheet_t* sheet;
	v2i frame;
	r32 frameTime;
	u32 numFrames;
	v2 position;
	v2 velocity;
	v2 acceleration;
	Color_t colorStart;
	Color_t colorEnd;
	r32 scaleStart;
	r32 scaleEnd;
	r32 depth;
	r32 age; //ms
	r32 lifeSpan; //ms
	u32 paletteColorIndex;
};

struct ChatMessage_t
{
	u32 chatterId;
	char* message;
	v2 spawnPos;
	FriendlyName_t* displayName;
	u64 startTime;
	
	r32 spawnAnimProgress;
	
	v2 displayNameSize;
	v2 messageSize;
	rec drawRec;
	v2 usernamePos;
	v2 messagePos;
};

struct AbductionData_t
{
	bool inProgress;
	r32 progress;
	
	r32 spaceshipScale;
	r32 spaceshipTargetLean;
	r32 spaceshipBob;
	v2 spaceshipPos;
	obb2 spaceshipDrawRec;
	rec tractorBeamRec;
};

struct VisualizerData_t
{
	bool initialized;
	
	v2i oldWindowSize;
	
	AbductionData_t abduction;
	r32 stretchTimer;
	bool stretchTimerSilenced;
	bool stretchTimerPaused;
	bool stretchTimerRunning;
	
	DynArray_t people;
	DynArray_t parts;
	DynArray_t names;
	DynArray_t messages;
	DynArray_t fires;
	
	rec peopleSpace;
	rec messageSpace;
	rec stretchAnimRec;
};

#endif //  _VISUALIZER_H
