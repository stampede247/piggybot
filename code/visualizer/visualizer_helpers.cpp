/*
File:   visualizer_helpers.cpp
Author: Taylor Robbins
Date:   11\21\2020
Description: 
	** Holds some functions that help us perform various actions in the Visualizer AppState 
*/

Person_t* GetPersonById(u32 chatterId)
{
	for (u32 pIndex = 0; pIndex < vis->people.length; pIndex++)
	{
		Person_t* person = DynArrayGet(&vis->people, Person_t, pIndex);
		NotNull(person);
		if (person->chatterId == chatterId) { return person; }
	}
	return nullptr;
}

const char* GetPersonDisplayName(Person_t* person)
{
	NotNull(person);
	Chatter_t* chatter = GetChatterById(&app->chat, person->chatterId);
	const char* result = (chatter != nullptr) ? chatter->username : "[Unknown]";
	if (person->displayName != nullptr && person->displayName->displayStr != nullptr) { result = person->displayName->displayStr; }
	return result;
}

char* ParseChatMessage(MemoryArena_t* memArena, const char* message)
{
	NotNull(memArena);
	NotNull(message);
	u32 messageLength = MyStrLength32(message);
	char* result = nullptr;
	u32 resultSize = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 outputSize = 0;
		for (u32 cIndex = 0; cIndex < messageLength; cIndex++)
		{
			char nextChar = message[cIndex];
			u8 nextCharValue = (u8)nextChar;
			if (nextCharValue >= 0x80)
			{
				if (IsFlagSet(nextCharValue, 0x40))
				{
					if (IsFlagSet(nextCharValue, 0x20))
					{
						if (IsFlagSet(nextCharValue, 0x10))
						{
							if (IsFlagSet(nextCharValue, 0x08))
							{
								SrlPrint(result, resultSize, &outputSize, "[U+?]");
							}
							else 
							{
								if (cIndex+3 < messageLength)
								{
									u8 byte1 = nextCharValue;
									u8 byte2 = message[cIndex+1];
									u8 byte3 = message[cIndex+2];
									u8 byte4 = message[cIndex+3];
									if (IsFlagSet(byte2, 0x80) && !IsFlagSet(byte2, 0x40) && IsFlagSet(byte3, 0x80) && !IsFlagSet(byte3, 0x40) && IsFlagSet(byte4, 0x80) && !IsFlagSet(byte4, 0x40))
									{
										u32 unicodePoint = ((u32)(byte1 & 0x07) << 18) | ((u32)(byte2 & 0x3F) << 12) | ((u32)(byte3 & 0x3F) << 6) | ((u32)(byte4 & 0x3F) << 0);
										SrlPrint(result, resultSize, &outputSize, "[U+%06X]", unicodePoint);
									}
									else { SrlPrint(result, resultSize, &outputSize, "[U+?]"); }
									cIndex += 3;
								}
								else { SrlPrint(result, resultSize, &outputSize, "[U+?]"); }
							}
						}
						else
						{
							if (cIndex+2 < messageLength)
							{
								u8 byte1 = nextCharValue;
								u8 byte2 = message[cIndex+1];
								u8 byte3 = message[cIndex+2];
								if (IsFlagSet(byte2, 0x80) && !IsFlagSet(byte2, 0x40) && IsFlagSet(byte3, 0x80) && !IsFlagSet(byte3, 0x40))
								{
									u32 unicodePoint = ((u32)(byte1 & 0x0F) << 12) | ((u32)(byte2 & 0x3F) << 6) | ((u32)(byte3 & 0x3F) << 0);
									SrlPrint(result, resultSize, &outputSize, "[U+%04X]", unicodePoint);
								}
								else { SrlPrint(result, resultSize, &outputSize, "[U+?]"); }
								cIndex += 2;
							}
							else { SrlPrint(result, resultSize, &outputSize, "[U+?]"); }
						}
					}
					else
					{
						if (cIndex+1 < messageLength)
						{
							u8 byte1 = nextCharValue;
							u8 byte2 = message[cIndex+1];
							if (IsFlagSet(byte2, 0x80) && !IsFlagSet(byte2, 0x40))
							{
								u32 unicodePoint = ((u32)(byte1 & 0x1F) << 6) | ((u32)(byte2 & 0x3F) << 0);
								SrlPrint(result, resultSize, &outputSize, "[U+%03X]", unicodePoint);
							}
							else { SrlPrint(result, resultSize, &outputSize, "[U+?]"); }
							cIndex += 1;
						}
						else { SrlPrint(result, resultSize, &outputSize, "[U+?]"); }
					}
				}
				else { SrlPrint(result, resultSize, &outputSize, "[U+?]"); }
			}
			else
			{
				bool foundEmoji = false;
				u32 emojiLength = 0;
				char emojiId[3];
				emojiId[2] = '\0';
				if (cIndex+10 <= messageLength && MyStrCompare(&message[cIndex], "piggybFrog", 10) == 0)
				{
					foundEmoji = true;
					emojiLength = 10;
					emojiId[0] = 'P'; emojiId[1] = 'I';
				}
				else if (cIndex+8 <= messageLength && MyStrCompare(&message[cIndex], "PogChamp", 8) == 0)
				{
					foundEmoji = true;
					emojiLength = 8;
					emojiId[0] = 'P'; emojiId[1] = 'G';
				}
				
				if (foundEmoji)
				{
					if (result != nullptr)
					{
						Assert(outputSize+3 <= resultSize);
						result[outputSize+0] = EMOJI_CHAR_VALUE;
						result[outputSize+1] = emojiId[0];
						result[outputSize+2] = emojiId[1];
					}
					outputSize += 3;
					cIndex += emojiLength-1;
				}
				else
				{
					if (result != nullptr)
					{
						Assert(outputSize+1 <= resultSize);
						result[outputSize] = nextChar;
					}
					outputSize += 1;
				}
			}
			//TODO: Handle finding and doing something with emoji's
		}
		
		if (pass == 0)
		{
			result = PushArray(memArena, char, outputSize+1);
			NotNull(result);
			MyMemSet(result, 0x00, outputSize+1);
			result[outputSize] = '\0';
			resultSize = outputSize+1; //+1 to account for vsnprintf weirndess in SrlPrint
		}
		else
		{
			NotNull(result);
			Assert(outputSize+1 == resultSize);
			result[outputSize] = '\0';
		}
	}
	return result;
}

v2 VisualizerNormalizeVec(v2 position)
{
	v2 result = position;
	while (!IsInfinite(position.x) && result.x < 0) { result.x += vis->peopleSpace.width; }
	while (!IsInfinite(position.x) && result.x >= vis->peopleSpace.width) { result.x -= vis->peopleSpace.width; }
	while (!IsInfinite(position.y) && result.y < 0) { result.y += vis->peopleSpace.height; }
	while (!IsInfinite(position.y) && result.y >= vis->peopleSpace.height) { result.y -= vis->peopleSpace.height; }
	return result;
}

v2 VisualizerVecBetween(v2 start, v2 end)
{
	v2 result = end - start;
	if (result.x < -vis->peopleSpace.width/2)  { result.x += vis->peopleSpace.width;  }
	if (result.x >  vis->peopleSpace.width/2)  { result.x -= vis->peopleSpace.width;  }
	if (result.y < -vis->peopleSpace.height/2) { result.y += vis->peopleSpace.height; }
	if (result.y >  vis->peopleSpace.height/2) { result.y -= vis->peopleSpace.height; }
	return result;
}

u32 GetPersonPaletteColorIndex(const Person_t* person)
{
	NotNull(person);
	u32 firstColorIndex = GetPaletteColorIndexByPntr(&plt->alienNormal);
	return (firstColorIndex + person->alienColor);
}

r32 GetPersonScale(const Person_t* person)
{
	NotNull(person);
	if (IsFlagSet(person->flags, PersonFlag_Big))
	{
		return 3.0f;
	}
	else if (IsFlagSet(person->flags, PersonFlag_Smol))
	{
		return 1.0f;
	}
	return 2.0f;
}

bool ShouldPersonMove(const Person_t* person)
{
	if (person->anim == PersonAnim_Sleeping) { return false; }
	if (person->anim == PersonAnim_Jumping) { return false; }
	if (person->anim == PersonAnim_PlayingDead) { return false; }
	if (person->anim == PersonAnim_BeingAbducted) { return false; }
	if (person->anim == PersonAnim_Eating) { return false; }
	if (person->anim == PersonAnim_BeingPoisoned) { return false; }
	return true;
}

bool ShouldPersonCollide(const Person_t* person)
{
	if (person->anim == PersonAnim_PlayingDead) { return false; }
	if (person->anim == PersonAnim_BeingAbducted) { return false; }
	if (person->anim == PersonAnim_BeingPoisoned) { return false; }
	return true;
}

bool IsPersonCollsionHeavy(const Person_t* person)
{
	if (IsFlagSet(person->flags, PersonFlag_IsRunning)) { return true; }
	if (person->fireTimer > 0.0f) { return true; }
	return false;
}

bool CanChangeAnimationRightNow(const Person_t* person, PersonAnim_t newAnim)
{
	if (person->anim == newAnim) { return true; }
	if (person->anim == PersonAnim_BeingPoisoned) { return false; }
	if (person->anim == PersonAnim_BeingAbducted) { return false; }
	return true;
}

v2i GetPersonEyeOffsetForFrame(v2i frame)
{
	if (frame == NewVec2i(0, 3)) { return NewVec2i(-1,  0); }
	if (frame == NewVec2i(1, 3)) { return NewVec2i(-1,  0); }
	if (frame == NewVec2i(2, 3)) { return NewVec2i( 0,  0); }
	if (frame == NewVec2i(3, 3)) { return NewVec2i( 0,  1); }
	if (frame == NewVec2i(4, 3)) { return NewVec2i(-1,  1); }
	
	if (frame == NewVec2i(0, 6)) { return NewVec2i(-4,  1); }
	if (frame == NewVec2i(1, 6)) { return NewVec2i(-4,  1); }
	if (frame == NewVec2i(2, 6)) { return NewVec2i(-4,  2); }
	if (frame == NewVec2i(3, 6)) { return NewVec2i(-3,  2); }
	if (frame == NewVec2i(4, 6)) { return NewVec2i(-3,  1); }
	
	if (frame == NewVec2i(0, 7)) { return NewVec2i( 4,  1); }
	if (frame == NewVec2i(1, 7)) { return NewVec2i( 4,  1); }
	if (frame == NewVec2i(2, 7)) { return NewVec2i( 4,  2); }
	if (frame == NewVec2i(3, 7)) { return NewVec2i( 3,  2); }
	if (frame == NewVec2i(4, 7)) { return NewVec2i( 3,  1); }
	
	if (frame == NewVec2i(0, 4)) { return NewVec2i( 0,  0); }
	if (frame == NewVec2i(1, 4)) { return NewVec2i( 0,  1); }
	if (frame == NewVec2i(2, 4)) { return NewVec2i( 0,  1); }
	if (frame == NewVec2i(3, 4)) { return NewVec2i( 0,  0); }
	
	if (frame == NewVec2i(0, 9)) { return NewVec2i( 2,  0); }
	if (frame == NewVec2i(1, 9)) { return NewVec2i( 4,  1); }
	if (frame == NewVec2i(2, 9)) { return NewVec2i( 2,  3); }
	if (frame == NewVec2i(3, 9)) { return NewVec2i( 0,  3); }
	if (frame == NewVec2i(4, 9)) { return NewVec2i(-3,  2); }
	if (frame == NewVec2i(5, 9)) { return NewVec2i(-2,  0); }
	if (frame == NewVec2i(6, 9)) { return NewVec2i( 0,  0); }
	
	if (frame == NewVec2i(0, 10)) { return NewVec2i( 2,  0); }
	if (frame == NewVec2i(1, 10)) { return NewVec2i( 4,  2); }
	if (frame == NewVec2i(2, 10)) { return NewVec2i( 6,  5); }
	return Vec2i_Zero;
}
v2i GetPersonMouthOffsetForFrame(v2i frame, bool facingLeft)
{
	if (frame == NewVec2i(0, 9)) { return NewVec2i(facingLeft ? -1 : 1,  2); }
	if (frame == NewVec2i(1, 9)) { return NewVec2i(facingLeft ? -1 : 1,  3); }
	if (frame == NewVec2i(2, 9)) { return NewVec2i(facingLeft ? -1 : 1,  3); }
	if (frame == NewVec2i(3, 9)) { return NewVec2i(facingLeft ? -1 : 1,  3); }
	if (frame == NewVec2i(4, 9)) { return NewVec2i(facingLeft ? -1 : 1,  3); }
	if (frame == NewVec2i(5, 9)) { return NewVec2i(facingLeft ? -1 : 1,  2); }
	if (frame == NewVec2i(6, 9)) { return NewVec2i(facingLeft ? -1 : 1,  1); }
	return NewVec2i(facingLeft ? -1 : 1, 3);
}

v2 NormalizeVisualizerPosition(v2 position)
{
	v2 result = position;
	if (result.x < 0) { result.x += vis->peopleSpace.width; }
	if (result.y < 0) { result.y += vis->peopleSpace.height; }
	if (result.x >= vis->peopleSpace.width) { result.x -= vis->peopleSpace.width; }
	if (result.y >= vis->peopleSpace.height) { result.y -= vis->peopleSpace.height; }
	return result;
}

Particle_t* SpawnParticle(u8 flags, const SpriteSheet_t* sheet, v2i sheetFrame, r32 frameTime, u32 numFrames, v2 position, v2 velocity, v2 acceleration, r32 scaleStart, r32 scaleEnd, Color_t colorStart, Color_t colorEnd, r32 depth, r32 lifeSpan)
{
	Particle_t* result = nullptr;
	for (u32 pIndex = 0; pIndex < vis->parts.length; pIndex++)
	{
		Particle_t* newPart = DynArrayGet(&vis->parts, Particle_t, pIndex);
		if (newPart != nullptr && !IsFlagSet(newPart->flags, ParticleFlag_Alive))
		{
			result = newPart;
			break;
		}
	}
	if (result == nullptr && vis->parts.allocLength < MAX_NUM_PARTICLES)
	{
		result = DynArrayAdd(&vis->parts, Particle_t);
	}
	if (result == nullptr) { return result; }
	
	ClearPointer(result);
	result->flags = flags;
	FlagSet(result->flags, ParticleFlag_Alive);
	result->sheet = sheet;
	result->frame = sheetFrame;
	result->frameTime = frameTime;
	result->numFrames = numFrames;
	result->position = position;
	result->velocity = velocity;
	result->acceleration = acceleration;
	result->scaleStart = scaleStart;
	result->scaleEnd = scaleEnd;
	result->colorStart = colorStart;
	result->colorEnd = colorEnd;
	result->depth = depth;
	result->lifeSpan = lifeSpan;
	result->age = 0.0f;
	result->paletteColorIndex = 0;
	
	return result;
}

FriendlyName_t* FindDisplayNameForUsername(const char* username)
{
	NotNull(username);
	for (u32 nIndex = 0; nIndex < vis->names.length; nIndex++)
	{
		FriendlyName_t* name = DynArrayGet(&vis->names, FriendlyName_t, nIndex);
		NotNull(name);
		if (StrCompareIgnoreCaseNt(username, name->userStr))
		{
			return name;
		}
	}
	return nullptr;
}
void FindDisplayNameForPerson(Person_t* person)
{
	NotNull(person);
	person->displayName = nullptr;
	Chatter_t* chatter = GetChatterById(&app->chat, person->chatterId);
	if (chatter != nullptr && chatter->username != nullptr)
	{
		person->displayName = FindDisplayNameForUsername(chatter->username);
	}
}

Person_t* SpawnPersonForChatter(Chatter_t* chatter, bool doFanfare)
{
	NotNull(chatter);
	Person_t* newPerson = DynArrayAdd(&vis->people, Person_t);
	NotNull(newPerson);
	ClearPointer(newPerson);
	newPerson->chatterId = chatter->id;
	// newPerson->position = NewVec2(GetRandR32(appRand, 0, vis->peopleSpace.width), GetRandR32(appRand, 0, vis->peopleSpace.height));
	newPerson->position = vis->peopleSpace.size/2;
	newPerson->targetPos = newPerson->position;
	newPerson->walkDelayTime = GetRandR32(appRand, 1000, 10000);
	do
	{
		newPerson->alienColor = GetRandU8(appRand, 0, NUM_ALIEN_COLORS);
	} while (newPerson->alienColor == ALIEN_INVISIBLE_COLOR || newPerson->alienColor == ALIEN_RAINBOW_COLOR);
	if (doFanfare)
	{
		newPerson->anim = PersonAnim_Spawning;
		newPerson->animProgress = 0.0f;
		StartSndInstance(&soundsList->personSpawnSound.sound);
	}
	else
	{
		newPerson->anim = PersonAnim_Idle;
		newPerson->animProgress = 0.0f;
	}
	FindDisplayNameForPerson(newPerson);
	if (chatter->username != nullptr && StrCompareIgnoreCaseNt(chatter->username, "piggybankstudios"))
	{
		newPerson->alienColor = ALIEN_PINK_COLOR;
	}
	return newPerson;
}

v2 PickWalkPositionForPerson()
{
	return NewVec2(
		GetRandR32(appRand, PEOPLE_WALK_CHOICE_INNER_PADDING, vis->peopleSpace.width-PEOPLE_WALK_CHOICE_INNER_PADDING),
		GetRandR32(appRand, PEOPLE_WALK_CHOICE_INNER_PADDING, vis->peopleSpace.height-PEOPLE_WALK_CHOICE_INNER_PADDING)
	);
}

void VisualizerReloadNames()
{
	if (vis->names.allocArena == nullptr)
	{
		CreateDynamicArray(&vis->names, mainHeap, sizeof(FriendlyName_t));
	}
	else
	{
		for (u32 nIndex = 0; nIndex < vis->names.length; nIndex++)
		{
			FriendlyName_t* name = DynArrayGet(&vis->names, FriendlyName_t, nIndex);
			NotNull(name);
			if (name->userStr != nullptr) { ArenaPop(vis->names.allocArena, name->userStr); }
			if (name->displayStr != nullptr) { ArenaPop(vis->names.allocArena, name->displayStr); }
		}
		DynArrayClear(&vis->names);
	}
	
	if (ParseNamesFile(&vis->names, NAMES_FILE_PATH))
	{
		PrintLine_I("Found %u names in %s", vis->names.length, NAMES_FILE_PATH);
	}
	else { NotifyPrint_E("Failed to parse names in %s", NAMES_FILE_PATH); }
	
	for (u32 pIndex = 0; pIndex < vis->people.length; pIndex++)
	{
		Person_t* person = DynArrayGet(&vis->people, Person_t, pIndex);
		NotNull(person);
		FindDisplayNameForPerson(person);
	}
	for (u32 mIndex = 0; mIndex < vis->messages.length; mIndex++)
	{
		ChatMessage_t* message = DynArrayGet(&vis->messages, ChatMessage_t, mIndex);
		NotNull(message);
		message->displayName = nullptr;
		Chatter_t* messageChatter = GetChatterById(&app->chat, message->chatterId);
		if (messageChatter != nullptr && messageChatter->username != nullptr)
		{
			message->displayName = FindDisplayNameForUsername(messageChatter->username);
		}
	}
}

ActiveFire_t* VisualizerSpawnFire(u32 chatterId, v2 position, r32 lifeSpan)
{
	ActiveFire_t* result = DynArrayAdd(&vis->fires, ActiveFire_t);
	NotNull(result);
	ClearPointer(result);
	result->spawnChatterId = chatterId;
	result->position = position;
	result->scale = 2.0f;
	result->radius = FIRE_RADIUS;
	result->age = 0.0f;
	result->lifeSpan = lifeSpan;
	result->burnMarkLifeSpan = lifeSpan + FIRE_BURN_MARK_LIFE_SPAN;
	return result;
}
