/*
File:   visualizer_names.cpp
Author: Taylor Robbins
Date:   11\21\2020
Description: 
	** Handles converting raw usernames into more friendly display names by parsing the names.txt
*/

bool ParseNamesFile(DynArray_t* names, const char* filePath)
{
	NotNull(filePath);
	Assert(names->allocArena != nullptr);
	Assert(names->itemSize == sizeof(FriendlyName_t));
	
	FileInfo_t namesFile = platform->ReadEntireFile(filePath);
	if (namesFile.content == nullptr || namesFile.size == 0)
	{
		PrintLine_W("Failed to open names file \"%s\"", filePath);
		return false;
	}
	
	TextParser_t parser = {};
	CreateTextParser(&parser, (const char*)namesFile.content, namesFile.size);
	while (parser.position < parser.contentLength)
	{
		ParsingToken_t token = {};
		const char* errorStr = ParserConsumeToken(&parser, &token);
		if (errorStr != nullptr)
		{
			PrintLine_E("Parsing error while parsing names file at \"%s\": %s", filePath, errorStr);
			platform->FreeFileMemory(&namesFile);
			return false;
		}
		
		if (token.type == ParsingTokenType_KeyValue)
		{
			if (token.key.length > 0 && token.value.length > 0)
			{
				FriendlyName_t* newName = DynArrayAdd(names, FriendlyName_t);
				NotNull(newName);
				ClearPointer(newName);
				newName->userStr = ArenaString(names->allocArena, token.key.pntr, token.key.length);
				newName->displayStr = ArenaString(names->allocArena, token.value.pntr, token.value.length);
			}
			else { PrintLine_W("Found Key-Value pair in names file with empty key or value: %.*s", token.length, &parser.content[token.start]); }
		}
		else
		{
			//unhandled token
		}
	}
	
	platform->FreeFileMemory(&namesFile);
	return true;
}

