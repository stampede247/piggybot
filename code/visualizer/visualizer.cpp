/*
File:   visualizer.cpp
Author: Taylor Robbins
Date:   10\23\2020
Description: 
	** Holds all of the functions that run the Visualizer App State
*/

#include "visualizer_names.cpp"
#include "visualizer_helpers.cpp"

void VisualizerUpdateChatMessageSize(ChatMessage_t* message)
{
	NotNull(message);
	RcBindFont(GetFont(debugFont));
	r32 maxWidth = vis->messageSpace.width - MESSAGE_SPACE_PADDING*2;
	const char* displayNameStr = "[Unknown]";
	if (message->displayName != nullptr && message->displayName->displayStr != nullptr) { displayNameStr = message->displayName->displayStr; }
	else if (app->chat.created)
	{
		Chatter_t* chatter = GetChatterById(&app->chat, message->chatterId);
		if (chatter != nullptr && chatter->username != nullptr) { displayNameStr = chatter->username; }
	}
	displayNameStr = TempPrint("%s:", displayNameStr);
	message->displayNameSize = RcMeasureStringNt(displayNameStr);
	r32 messageOffset = message->displayNameSize.width + MESSAGE_USER_MSG_SPACING;
	message->messageSize = RcMeasureFormattedStringNt(message->message, maxWidth - messageOffset, true, 1.0f, -messageOffset);
	if (message->messageSize.height <= RcLineHeight()) { message->messageSize.height = RcLineHeight(); }
	message->drawRec.height = message->messageSize.height + MESSAGE_SPACE_PADDING*2;
	message->drawRec.width = vis->messageSpace.width;
	message->messagePos = NewVec2(MESSAGE_SPACE_PADDING, MESSAGE_SPACE_PADDING + RcMaxExtendUp());
	message->usernamePos = message->messagePos;
	message->messagePos.x += messageOffset;
	message->messagePos = Vec2Round(message->messagePos);
}

void VisualizerUpdateRecs()
{
	vis->peopleSpace.size = VIS_PEOPLE_SPACE_SIZE;
	vis->peopleSpace.topLeft = NewVec2(0, RenderScreenSize.height - vis->peopleSpace.height);
	vis->messageSpace.topLeft = Vec2_Zero;
	vis->messageSpace.size = NewVec2(vis->peopleSpace.width, vis->peopleSpace.y);
	r32 yOffset = 0;
	for (u32 mIndex = 0; mIndex < vis->messages.length; mIndex++)
	{
		ChatMessage_t* message = DynArrayGet(&vis->messages, ChatMessage_t, mIndex);
		NotNull(message);
		if (message->drawRec.width != RenderScreenSize.width)
		{
			VisualizerUpdateChatMessageSize(message);
		}
		r32 effectiveHeight = message->drawRec.height;
		if (message->spawnAnimProgress < 1.0f)
		{
			effectiveHeight *= EaseQuadraticInOut(message->spawnAnimProgress);
		}
		message->drawRec.x = 0;
		message->drawRec.y = vis->messageSpace.height - yOffset - effectiveHeight;
		message->drawRec.topLeft = Vec2Round(message->drawRec.topLeft);
		yOffset += effectiveHeight;
	}
	
	if (vis->abduction.inProgress)
	{
		vis->abduction.spaceshipScale = 2.0f;
		vis->abduction.spaceshipDrawRec.size = GetTextureSizef(humanSpaceship) * vis->abduction.spaceshipScale;
		vis->abduction.spaceshipDrawRec.center = NewVec2(
			vis->peopleSpace.x + vis->peopleSpace.width/2,
			vis->peopleSpace.y + vis->peopleSpace.height/2 - 140*vis->abduction.spaceshipScale
		);
		v2 spaceshipStartPos = NewVec2(RenderScreenSize.width + vis->abduction.spaceshipDrawRec.width/2 + 100*vis->abduction.spaceshipScale, vis->abduction.spaceshipDrawRec.y - 50*vis->abduction.spaceshipScale);
		v2 spaceshipEndPos = NewVec2(0 - vis->abduction.spaceshipDrawRec.width/2 - 200*vis->abduction.spaceshipScale, vis->abduction.spaceshipDrawRec.y - 50*vis->abduction.spaceshipScale);
		vis->abduction.spaceshipTargetLean = 0.0f;
		if (vis->abduction.progress < 1.0f)
		{
			r32 subAnim = SubAnimAmount(vis->abduction.progress, 0.0f, 1.0f);
			vis->abduction.spaceshipDrawRec.center = Vec2Lerp(spaceshipStartPos, vis->abduction.spaceshipDrawRec.center, EaseQuarticOut(subAnim));
			vis->abduction.spaceshipTargetLean = LerpR32(Pi32/8, 0.0f, EaseQuadraticOut(subAnim));
			vis->abduction.spaceshipBob = LerpR32(0, 10, EaseQuadraticOut(subAnim));
			// vis->abduction.spaceshipDrawRec.rotation = vis->abduction.spaceshipTargetLean;
		}
		else if (vis->abduction.progress >= 3.0f)
		{
			r32 subAnim = SubAnimAmount(vis->abduction.progress, 3.0f, 4.0f);
			vis->abduction.spaceshipDrawRec.center = Vec2Lerp(vis->abduction.spaceshipDrawRec.center, spaceshipEndPos, EaseQuadraticIn(subAnim));
			vis->abduction.spaceshipTargetLean = (2*Pi32) - (Pi32/8.0f);
			vis->abduction.spaceshipBob = LerpR32(10, 0, EaseQuadraticOut(subAnim));
		}
		else
		{
			vis->abduction.spaceshipTargetLean = 0;
			vis->abduction.spaceshipBob = 10;
		}
		vis->abduction.tractorBeamRec.size = GetSheetFrameSizef(tractorBeamSheet) * vis->abduction.spaceshipScale;
		vis->abduction.tractorBeamRec.y = vis->abduction.spaceshipDrawRec.y + vis->abduction.spaceshipDrawRec.height/2 - (16 * vis->abduction.spaceshipScale);
		vis->abduction.tractorBeamRec.x = vis->abduction.spaceshipDrawRec.x - vis->abduction.tractorBeamRec.width/2;
		vis->abduction.tractorBeamRec.topLeft = Vec2Round(vis->abduction.tractorBeamRec.topLeft);
		
		vis->abduction.spaceshipDrawRec.y += SinR32(((r32)(ProgramTime%2000)/2000.0f) * Pi32*2) * vis->abduction.spaceshipBob;
		vis->abduction.spaceshipDrawRec.center = Vec2Round(vis->abduction.spaceshipDrawRec.center);
	}
	
	r32 stretchTimerScale = 2.0f;
	vis->stretchAnimRec.size = GetSheetFrameSizef(stretchAnimSheet) * stretchTimerScale;
	vis->stretchAnimRec.x = RenderScreenSize.width*0.75f - vis->stretchAnimRec.width/2;
	vis->stretchAnimRec.y = vis->messageSpace.y + vis->messageSpace.height - vis->stretchAnimRec.height;
	vis->stretchAnimRec.topLeft = Vec2Round(vis->stretchAnimRec.topLeft);
}

#include "visualizer_commands.cpp"

// +--------------------------------------------------------------+
// |                    Initialize Visualizer                     |
// +--------------------------------------------------------------+
void InitializeVisualizer()
{
	Assert(vis != nullptr);
	
	CreateDynamicArray(&vis->people, mainHeap, sizeof(Person_t));
	CreateDynamicArray(&vis->parts, mainHeap, sizeof(Particle_t));
	CreateDynamicArray(&vis->messages, mainHeap, sizeof(ChatMessage_t));
	CreateDynamicArray(&vis->fires, mainHeap, sizeof(ActiveFire_t));
	
	platform->WatchFile(NAMES_FILE_PATH);
	
	vis->initialized = true;
}

// +--------------------------------------------------------------+
// |                       Start Visualizer                       |
// +--------------------------------------------------------------+
bool StartVisualizer(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	// SoftQueueMusicChange(GetMusic(menuMusic), SoundTransition_FadeIn);
	
	vis->oldWindowSize = appInput->screenSize;
	appOutput->changeResolution = true;
	appOutput->newResolution = Vec2Roundi(VIS_PEOPLE_SPACE_SIZE + NewVec2(0, VIS_MESSAGE_SPACE_HEIGHT));
	
	VisualizerReloadNames();
	
	return true;
}

// +--------------------------------------------------------------+
// |                   Deinitialize Visualizer                    |
// +--------------------------------------------------------------+
void DeinitializeVisualizer()
{
	Assert(vis != nullptr);
	Assert(vis->initialized == true);
	
	appOutput->changeResolution = true;
	appOutput->newResolution = vis->oldWindowSize;
	DestroyDynamicArray(&vis->people);
	DestroyDynamicArray(&vis->parts);
	DestroyDynamicArray(&vis->names);
	DestroyDynamicArray(&vis->messages);
	DestroyDynamicArray(&vis->fires);
	
	platform->UnwatchFile(NAMES_FILE_PATH);
	
	vis->initialized = false;
	ClearPointer(vis);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesVisualizerCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                      Update Visualizer                       |
// +--------------------------------------------------------------+
void UpdateVisualizer(AppMenu_t appMenuAbove)
{
	Assert(vis != nullptr);
	Assert(vis->initialized);
	
	VisualizerUpdateRecs();
	
	r32 timeScale = (ElapsedMs / (r32)TARGET_FRAME_DURATION);
	
	if (appMenuAbove == AppMenu_None)
	{
		// +================================+
		// | Handle Switch App State Hotkey |
		// +================================+
		if (ButtonPressed(Button_Tilde))
		{
			HandleButton(Button_Tilde);
			PopAppState();
		}
		
		if (app->chat.created)
		{
			// +==============================+
			// |          Add People          |
			// +==============================+
			bool wereNoPeople = (vis->people.length == 0);
			for (u32 cIndex = 0; cIndex < app->chat.chatters.length; cIndex++)
			{
				Chatter_t* chatter = DynArrayGet(&app->chat.chatters, Chatter_t, cIndex);
				NotNull(chatter);
				Person_t* existingPerson = GetPersonById(chatter->id);
				if (existingPerson == nullptr)
				{
					PrintLine_I("Adding new person for chatter %u username %s", chatter->id, chatter->username);
					Person_t* newPerson = SpawnPersonForChatter(chatter, !wereNoPeople);
				}
			}
			
			// +==============================+
			// |       Debug Add People       |
			// +==============================+
			if (app->chat.created && app->chat.chatters.length >= 1 && ButtonPressed(Button_Enter))
			{
				Chatter_t* chatter = DynArrayGet(&app->chat.chatters, Chatter_t, GetRandU32(appRand, 0, app->chat.chatters.length));
				NotNull(chatter);
				PrintLine_I("Adding new person for chatter %u username %s", chatter->id, chatter->username);
				Person_t* newPerson = SpawnPersonForChatter(chatter, !wereNoPeople);
			}
			
			// +==============================+
			// |    Debug Start Abduction     |
			// +==============================+
			if (ButtonPressed(Button_Home))
			{
				HandleButton(Button_Home);
				vis->abduction.inProgress = !vis->abduction.inProgress;
				PrintLine_D("Abduction %s", vis->abduction.inProgress ? "Started" : "Stopped");
				if (vis->abduction.inProgress)
				{
					StartSndInstance(&soundsList->spaceshipAppear.sound);
					StartSndInstance(&soundsList->spaceshipLoop.sound, true);
					vis->abduction.progress = 0.0f;
					vis->abduction.spaceshipDrawRec.rotation = Pi32/8;
				}
				else
				{
					StopAllLoopingSounds();
				}
			}
			
			// +==============================+
			// |    Stretch Timer Hotkeys     |
			// +==============================+
			if (ButtonPressed(Button_Delete))
			{
				HandleButton(Button_Delete);
				if (vis->stretchTimerRunning)
				{
					if (ButtonDownNoHandling(Button_Control))
					{
						vis->stretchTimerRunning = false;
						StopSoundsBySrc(&soundsList->stretchTimerSound.sound);
					}
					else if (vis->stretchTimer <= 0.0f)
					{
						if (vis->stretchTimerSilenced)
						{
							vis->stretchTimer = (r32)STRETCH_TIME;
							vis->stretchTimerSilenced = false;
						}
						else
						{
							vis->stretchTimerSilenced = true;
							StopSoundsBySrc(&soundsList->stretchTimerSound.sound);
						}
					}
					else
					{
						vis->stretchTimerPaused = !vis->stretchTimerPaused;
					}
				}
				else
				{
					vis->stretchTimerRunning = true;
					vis->stretchTimer = (r32)STRETCH_TIME;
					vis->stretchTimerSilenced = false;
					vis->stretchTimerPaused = false;
				}
			}
			if (vis->stretchTimerRunning && vis->stretchTimer > 0.0f)
			{
				if (ButtonPressed(Button_PageUp))
				{
					HandleButton(Button_PageUp);
					vis->stretchTimer += STRETCH_PAGE_AMOUNT;
				}
				if (ButtonPressed(Button_PageDown))
				{
					HandleButton(Button_PageDown);
					vis->stretchTimer -= STRETCH_PAGE_AMOUNT;
					if (vis->stretchTimer <= 0.0f) { vis->stretchTimer = 1.0f; }
				}
			}
			
			// +==============================+
			// |     Update Stretch Timer     |
			// +==============================+
			if (vis->stretchTimerRunning)
			{
				if (vis->stretchTimer > 0.0f && !vis->stretchTimerPaused)
				{
					vis->stretchTimer -= ElapsedMs;
					if (vis->stretchTimer <= 0.0f)
					{
						vis->stretchTimer = 0.0f;
						vis->stretchTimerSilenced = false;
						StartSndInstance(&soundsList->stretchTimerSound.sound, true);
					}
				}
			}
			
			// +==============================+
			// |  Update Adbuction Progress   |
			// +==============================+
			if (vis->abduction.inProgress)
			{
				if (vis->abduction.progress < 1.0f)
				{
					vis->abduction.progress += TIME_SCALED_ANIM(ABD_FLY_IN_TIME, timeScale);
					if (vis->abduction.progress >= 1.0f) { vis->abduction.progress = 1.0f; }
				}
				else if (vis->abduction.progress < 2.0f)
				{
					vis->abduction.progress += TIME_SCALED_ANIM(ABD_WAIT_TIME, timeScale);
					if (vis->abduction.progress >= 2.0f)
					{
						StartSndInstance(&soundsList->spaceshipTractorBeam.sound, true);
						vis->abduction.progress = 2.0f;
					}
				}
				else if (vis->abduction.progress < 3.0f)
				{
					vis->abduction.progress += TIME_SCALED_ANIM(ABD_MIN_SUCK_TIME, timeScale);
					if (vis->abduction.progress >= 3.0f)
					{
						StopSoundsBySrc(&soundsList->spaceshipTractorBeam.sound);
						StopSoundsBySrc(&soundsList->spaceshipLoop.sound);
						StartSndInstance(&soundsList->spaceshipLeave.sound);
						vis->abduction.progress = 3.0f;
					}
				}
				else if (vis->abduction.progress < 4.0f)
				{
					vis->abduction.progress += TIME_SCALED_ANIM(ABD_FLY_OUT_TIME, timeScale);
					if (vis->abduction.progress >= 4.0f)
					{
						WriteLine_I("The abduction has finished");
						vis->abduction.progress = 4.0f;
						vis->abduction.inProgress = false;
					}
				}
			}
			
			// +==============================+
			// |  Update Abduction Spaceship  |
			// +==============================+
			if (vis->abduction.inProgress)
			{
				vis->abduction.spaceshipDrawRec.rotation = AngleLerp(vis->abduction.spaceshipDrawRec.rotation, vis->abduction.spaceshipTargetLean, 0.05f);
				vis->abduction.spaceshipDrawRec.rotation = AngleNormalize(vis->abduction.spaceshipDrawRec.rotation);
			}
			
			// +==============================+
			// |         Update Fires         |
			// +==============================+
			for (u32 fIndex = 0; fIndex < vis->fires.length; fIndex++)
			{
				ActiveFire_t* fire = DynArrayGet(&vis->fires, ActiveFire_t, fIndex);
				NotNull(fire);
				r32 oldAge = fire->age;
				fire->age += ElapsedMs;
				if (fire->age >= fire->burnMarkLifeSpan)
				{
					DynArrayRemove(&vis->fires, fIndex);
					fIndex--;
					continue;
				}
				else if (fire->age < fire->lifeSpan)
				{
					if (FloorR32i(oldAge / 100) != FloorR32i(fire->age / 100))
					{
						Color_t smokeColor = ColorLerp(PalBlack, PalGrey, GetRandR32(appRand, 0.0f, 1.0f));
						v2 smokeVel = NewVec2(GetRandR32(appRand, -0.2f, 0.2f), -0.3f);
						Particle_t* smokePart = SpawnParticle(0x00,
							GetSheet(partsSheet), NewVec2i(2, 1), 150, 8,
							fire->position, smokeVel, NewVec2(-smokeVel.x*0.01f, -smokeVel.y*0.005f),
							fire->scale, fire->scale, smokeColor, smokeColor,
							PEOPLE_MIN_DEPTH - GetRandR32(appRand, -0.01f, 0.01f),
							2000
						);
					}
					if (FloorR32i(oldAge / 30) != FloorR32i(fire->age / 30))
					{
						Color_t partColor = ColorLerp(PalOrange, PalOrangeDarker, GetRandR32(appRand, 0.0f, 1.0f));
						v2 fireVel = NewVec2(GetRandR32(appRand, -0.6f, 0.6f), -0.6f);
						r32 offsetDir = GetRandR32(appRand, 0, Pi32*2);
						r32 offsetDist = GetRandR32(appRand, 0, fire->radius);
						v2 partPos = fire->position + NewVec2(CosR32(offsetDir) * offsetDist, SinR32(offsetDir) * offsetDist);
						Particle_t* firePart = SpawnParticle(0x00,
							GetSheet(partsSheet), NewVec2i(0, 2), 100, 6,
							partPos, fireVel, NewVec2(-fireVel.x*0.02f, 0.0f),
							fire->scale, fire->scale, partColor, ColorTransparent(partColor, 0.0f),
							PEOPLE_MIN_DEPTH + GetRandR32(appRand, -0.01f, 0.01f),
							1200
						);
					}
				}
			}
			
			// +==============================+
			// |        Update People         |
			// +==============================+
			for (u32 pIndex = 0; pIndex < vis->people.length; pIndex++)
			{
				Person_t* person = DynArrayGet(&vis->people, Person_t, pIndex);
				NotNull(person);
				bool isAwake = (person->anim != PersonAnim_Sleeping);
				r32 personScale = GetPersonScale(person);
				if (IsFlagSet(person->flags, PersonFlag_IsStopped)) { person->targetPos = person->position; }
				
				//fall down
				if (person->anim != PersonAnim_BeingAbducted)
				{
					if (person->height > 0)
					{
						person->height -= ABD_FALL_SPEED;
						if (person->height <= 0) { person->height = 0; } //TODO: Play a sound effect
					}
				}
				
				// +==============================+
				// |    Update Person Burning     |
				// +==============================+
				if (person->fireTimer > 0.0f)
				{
					r32 oldFireTimer = person->fireTimer;
					person->fireTimer -= ElapsedMs;
					if (person->fireTimer <= 0.0f) { person->fireTimer = 0.0f; }
					else
					{
						r32 ourRadius = (GetSheetFrameWidth(personSheet) * personScale) * (3.0f/8.0f);
						// if (FloorR32i(oldFireTimer / 100) != FloorR32i(person->fireTimer / 100))
						{
							Color_t smokeColor = ColorLerp(PalBlack, PalGrey, GetRandR32(appRand, 0.0f, 1.0f));
							v2 smokeVel = NewVec2(GetRandR32(appRand, -0.2f, 0.2f), -0.3f);
							r32 offsetDir = GetRandR32(appRand, 0, Pi32*2);
							r32 offsetDist = GetRandR32(appRand, 0, ourRadius);
							v2 partPos = person->position + NewVec2(CosR32(offsetDir) * offsetDist, SinR32(offsetDir) * offsetDist);
							Particle_t* smokePart = SpawnParticle(0x00,
								GetSheet(partsSheet), NewVec2i(2, 1), 150, 8,
								partPos, smokeVel, NewVec2(-smokeVel.x*0.01f, -smokeVel.y*0.005f),
								personScale, personScale, smokeColor, smokeColor,
								PEOPLE_MIN_DEPTH - GetRandR32(appRand, -0.01f, 0.01f),
								2000
							);
						}
						if (FloorR32i(oldFireTimer / 30) != FloorR32i(person->fireTimer / 30))
						{
							Color_t partColor = ColorLerp(PalOrange, PalOrangeDarker, GetRandR32(appRand, 0.0f, 1.0f));
							v2 fireVel = NewVec2(GetRandR32(appRand, -0.6f, 0.6f), -0.6f);
							r32 offsetDir = GetRandR32(appRand, 0, Pi32*2);
							r32 offsetDist = GetRandR32(appRand, 0, ourRadius);
							v2 partPos = person->position + NewVec2(CosR32(offsetDir) * offsetDist, SinR32(offsetDir) * offsetDist);
							Particle_t* firePart = SpawnParticle(0x00,
								GetSheet(partsSheet), NewVec2i(0, 2), 100, 6,
								partPos, fireVel, NewVec2(-fireVel.x*0.02f, 0.0f),
								personScale, personScale, partColor, ColorTransparent(partColor, 0.0f),
								PEOPLE_MIN_DEPTH + GetRandR32(appRand, -0.01f, 0.01f),
								1200
							);
						}
					}
				}
				
				// +===============================+
				// | Abduction Person Interactions |
				// +===============================+
				if (vis->abduction.inProgress)
				{
					Chatter_t* chatter = GetChatterById(&app->chat, person->chatterId);
					bool isPiggy = (chatter != nullptr && StrCompareIgnoreCaseNt(chatter->username, "piggybankstudios"));
					if (vis->abduction.inProgress && vis->abduction.progress >= 1.0f && vis->abduction.progress < 2.0f)
					{
						if (!IsFlagSet(person->flags, PersonFlag_IsRunning) && isAwake && !isPiggy)
						{
							FlagSet(person->flags, PersonFlag_IsRunning);
						}
					}
					else if (vis->abduction.progress >= 2.0f && vis->abduction.progress < 3.0f)
					{
						if (person->anim != PersonAnim_PlayingDead && !isPiggy)
						{
							person->anim = PersonAnim_BeingAbducted;
							person->animProgress = 0.0f;
						}
					}
					else if (vis->abduction.inProgress && vis->abduction.progress >= 3.0f && vis->abduction.progress < 4.0f)
					{
						if (IsFlagSet(person->flags, PersonFlag_IsRunning)) { FlagUnset(person->flags, PersonFlag_IsRunning); }
						if (person->anim == PersonAnim_BeingAbducted)
						{
							person->anim = PersonAnim_Idle;
							person->animProgress = 0.0f;
						}
					}
					
					if (person->anim == PersonAnim_BeingAbducted)
					{
						r32 offsetFromCenter = person->position.x - (vis->abduction.tractorBeamRec.x + vis->abduction.tractorBeamRec.width/2);
						person->position.x = (vis->abduction.tractorBeamRec.x + vis->abduction.tractorBeamRec.width/2) + (offsetFromCenter * 0.98f);
						r32 screenPosY = vis->peopleSpace.y + person->position.y - person->height;
						r32 spaceshipHeightDiff = vis->abduction.spaceshipDrawRec.y - screenPosY;
						if (AbsR32(spaceshipHeightDiff) >= 20 * vis->abduction.spaceshipScale)
						{
							person->height += -SignOfR32(spaceshipHeightDiff) * ABD_TRACTOR_SPEED;
						}
						else
						{
							if (chatter != nullptr) { DeleteChatter(&app->chat, chatter); }
							DynArrayRemove(&vis->people, pIndex);
							pIndex--;
							continue;
						}
					}
				}
				
				// +==============================+
				// |        Movement Logic        |
				// +==============================+
				if (ShouldPersonMove(person))
				{
					Person_t* eatPerson = GetPersonById(person->eatChatterId);
					if (eatPerson == nullptr && person->eatChatterId != 0) { person->eatChatterId = 0; }
					if (eatPerson != nullptr)
					{
						person->targetPos = eatPerson->position;
					}
					bool foundAvoiding = false;
					v2 avoidPersonPos = Vec2_Zero;
					r32 avoidPersonDistance = 0;
					if (person->avoidChatterId != 0 || IsFlagSet(person->flags, PersonFlag_AvoidAll) || IsFlagSet(person->flags, PersonFlag_AvoidRunning))
					{
						//avoid people
						for (u32 pIndex2 = 0; pIndex2 < vis->people.length; pIndex2++)
						{
							Person_t* person2 = DynArrayGet(&vis->people, Person_t, pIndex2);
							NotNull(person2);
							if (pIndex2 != pIndex)
							{
								bool shouldAvoid = false;
								if (IsFlagSet(person->flags, PersonFlag_AvoidAll)) { shouldAvoid = true; }
								else if (IsFlagSet(person->flags, PersonFlag_AvoidRunning) && IsFlagSet(person2->flags, PersonFlag_IsRunning)) { shouldAvoid = true; }
								else if (person->avoidChatterId == person2->chatterId) { shouldAvoid = true; }
								if (shouldAvoid)
								{
									v2 distanceVec = VisualizerVecBetween(person2->position, person->position);
									r32 distance = Vec2Length(distanceVec);
									if (distance < PEOPLE_AVOID_DISTANCE)
									{
										if (!foundAvoiding || distance < avoidPersonDistance)
										{
											foundAvoiding = true;
											avoidPersonPos = person2->position;
											avoidPersonDistance = distance;
										}
									}
								}
							}
						}
					}
					if (!foundAvoiding)
					{
						//avoid fires
						for (u32 fIndex = 0; fIndex < vis->fires.length; fIndex++)
						{
							ActiveFire_t* fire = DynArrayGet(&vis->fires, ActiveFire_t, fIndex);
							NotNull(fire);
							bool wasSpawner = (fire->spawnChatterId == person->chatterId);
							if (fire->age < fire->lifeSpan && ((wasSpawner && fire->age < FIRE_SPAWNER_IMMUNE_TIME) || IsFlagSet(person->flags, PersonFlag_AvoidFires)))
							{
								v2 distanceVec = VisualizerVecBetween(fire->position, person->position);
								r32 distance = Vec2Length(distanceVec);
								if (distance < (wasSpawner ? FIRE_SPAWNER_AVOID_DISTANCE : FIRE_AVOID_DISTANCE))
								{
									if (!foundAvoiding || distance < avoidPersonDistance)
									{
										foundAvoiding = true;
										avoidPersonPos = fire->position;
										avoidPersonDistance = distance;
									}
								}
							}
						}
					}
					if (foundAvoiding)
					{
						v2 awayVec = VisualizerVecBetween(avoidPersonPos, person->position);
						awayVec = Vec2Normalize(awayVec);
						if (awayVec == Vec2_Zero)
						{
							r32 randomDir = GetRandR32(appRand, 0, Pi32*2);
							awayVec = NewVec2(CosR32(randomDir), SinR32(randomDir));
						}
						person->targetPos = VisualizerNormalizeVec(person->position + (awayVec * PEOPLE_AVOID_RUN_DISTANCE));
					}
					
					bool isRunning = IsFlagSet(person->flags, PersonFlag_IsRunning);
					if (person->fireTimer > 0.0f) { isRunning = true; }
					if (person->targetPos != person->position || eatPerson != nullptr || foundAvoiding)
					{
						v2 targetDelta = Vec2_Zero;
						targetDelta = VisualizerVecBetween(person->position, person->targetPos);
						r32 targetDistance = Vec2Length(targetDelta);
						
						r32 walkSpeed = isRunning ? 3.0f : 1.0f;
						if (foundAvoiding) { walkSpeed = 3.0f; }
						if (eatPerson != nullptr) { walkSpeed = 3.1f; }
						if (eatPerson != nullptr && targetDistance <= PEOPLE_EAT_DISTANCE)
						{
							r32 atePersonScale = GetPersonScale(eatPerson);
							u32 atePersonColorIndex = GetPersonPaletteColorIndex(eatPerson);
							u32 eatPersonIndex = GetDynArrayItemIndex(&vis->people, eatPerson);
							Chatter_t* atePersonChatter = GetChatterById(&app->chat, person->eatChatterId);
							StartSndInstance(&soundsList->eatSound1.sound);
							for (u32 partIndex = 0; partIndex < 6; partIndex++)
							{
								v2i partFrame = Vec2i_Zero;
								v2i partOffset = Vec2i_Zero;
								switch (partIndex)
								{
									case 0: partFrame = NewVec2i(13, 0); partOffset = NewVec2i( 3, -1); break; //eye
									case 1: partFrame = NewVec2i(14, 0); partOffset = NewVec2i( 1,  9); break; //left leg
									case 2: partFrame = NewVec2i(15, 0); partOffset = NewVec2i( 4,  9); break; //middle leg
									case 3: partFrame = NewVec2i(16, 0); partOffset = NewVec2i( 7,  8); break; //right leg
									case 4: partFrame = NewVec2i(17, 0); partOffset = NewVec2i( 4,  3); break; //neck
									case 5: partFrame = NewVec2i(18, 0); partOffset = NewVec2i( 3,  7); break; //body
								}
								r32 direction = RandR32(0, Pi32*2);
								r32 speed = RandR32(0.5f, 2);
								v2 velocity = NewVec2(CosR32(direction)*speed, SinR32(direction)*speed);
								v2 partPos = person->position;
								partPos += NewVec2(partOffset) * atePersonScale;
								Particle_t* part = SpawnParticle(0x00,
									GetSheet(partsSheet), partFrame, 100, 1,
									partPos, velocity, NewVec2(0, 0.05f),
									atePersonScale, atePersonScale, White, TransparentWhite,
									0.0f, 1200
								);
								if (part != nullptr) { part->paletteColorIndex = atePersonColorIndex; }
							}
							for (u32 partIndex = 0; partIndex < 30; partIndex++)
							{
								r32 direction = RandR32(0, Pi32*2);
								r32 speed = RandR32(0.5f, 2);
								v2 velocity = NewVec2(CosR32(direction)*speed, SinR32(direction)*speed);
								v2 partPos = person->position;
								partPos += NewVec2(GetRandR32(appRand, -6, 6), GetRandR32(appRand, -6, 6)) * atePersonScale;
								Particle_t* part = SpawnParticle(0x00,
									GetSheet(partsSheet), NewVec2i(19, 0), 100, 1,
									person->targetPos, velocity, NewVec2(0, 0.05f),
									atePersonScale, atePersonScale, White, TransparentWhite,
									0.0f, 1200
								);
								if (part != nullptr) { part->paletteColorIndex = atePersonColorIndex; }
							}
							if (eatPersonIndex == pIndex)
							{
								if (atePersonChatter != nullptr) { DeleteChatter(&app->chat, atePersonChatter); }
								DynArrayRemove(&vis->people, eatPersonIndex);
								pIndex--;
								continue;
							}
							else
							{
								person->eatChatterId = 0;
								if (IsFlagSet(eatPerson->flags, PersonFlag_Poisonous))
								{
									person->anim = PersonAnim_BeingPoisoned;
									person->animProgress = 0.0f;
								}
								else
								{
									person->anim = PersonAnim_Eating;
									person->animProgress = 0.0f;
								}
							}
							if (eatPersonIndex < pIndex) { pIndex--; }
							if (atePersonChatter != nullptr) { DeleteChatter(&app->chat, atePersonChatter); }
							DynArrayRemove(&vis->people, eatPersonIndex);
							person = DynArrayGet(&vis->people, Person_t, pIndex);
							eatPerson = nullptr;
						}
						else if (targetDistance > walkSpeed)
						{
							targetDelta = Vec2Normalize(targetDelta);
							person->velocity = targetDelta * walkSpeed;
							person->position += person->velocity;
							if (person->velocity.x < 0) { FlagSet(person->flags, PersonFlag_FacingLeft); }
							else { FlagUnset(person->flags, PersonFlag_FacingLeft); }
						}
						else
						{
							person->velocity = Vec2_Zero;
							person->position = person->targetPos;
						}
					}
					else { person->velocity = Vec2_Zero; }
					
					// +==============================+
					// |     Choose New Walk Pos      |
					// +==============================+
					if (person->walkDelayTime > 0 && person->position == person->targetPos && person->anim != PersonAnim_Chatting)
					{
						if (isRunning) { person->walkDelayTime = 0.0f; }
						person->walkDelayTime -= ElapsedMs;
						if (person->walkDelayTime < 0) { person->walkDelayTime = 0; }
					}
					if (person->walkDelayTime <= 0 && !IsFlagSet(person->flags, PersonFlag_IsStopped))
					{
						person->targetPos = PickWalkPositionForPerson();
						person->walkDelayTime = GetRandR32(appRand, 1000, 10000);
					}
				}
				else
				{
					person->targetPos = person->position;
					person->velocity = Vec2_Zero;
				}
				
				// +==============================+
				// |          Auto-Sleep          |
				// +==============================+
				if (isAwake && !IsFlagSet(person->flags, PersonFlag_Persistant))
				{
					Chatter_t* chatter = GetChatterById(&app->chat, person->chatterId);
					if (chatter != nullptr && TimeSince(chatter->lastActivityTime) >= PEOPLE_AUTO_SLEEP_TIME && person->anim != PersonAnim_BeingAbducted)
					{
						TryParseChatMessageAsCommandForPerson(person, chatter, "!sleep");
						isAwake = false;
					}
				}
				
				person->position = NormalizeVisualizerPosition(person->position);
				person->targetPos = NormalizeVisualizerPosition(person->targetPos);
				
				// +==============================+
				// |   Update People Animations   |
				// +==============================+
				if (person->anim == PersonAnim_Spawning)
				{
					person->animProgress += TIME_SCALED_ANIM(300, timeScale);
					for (u32 partIndex = 0; partIndex < 10; partIndex++)
					{
						r32 direction = RandR32(0, Pi32*2);
						r32 speed = RandR32(0, 5);
						v2 velocity = NewVec2(CosR32(direction)*speed, SinR32(direction)*speed);
						Particle_t* part = SpawnParticle(0x00,
							GetSheet(partsSheet), NewVec2i(0, 0), 100, 12,
							person->position, velocity, NewVec2(0, 0.01f),
							1.0f, 1.0f, White, White,
							0.0f, 1200
						);
					}
					if (person->animProgress >= 1.0f)
					{
						person->anim = PersonAnim_Idle;
						person->animProgress = 0.0f;
					}
				}
				else if (person->anim == PersonAnim_Chatting || person->anim == PersonAnim_ChattingUnicode || person->anim == PersonAnim_ChattingParty)
				{
					person->animProgress += TIME_SCALED_ANIM(2000, timeScale);
					if (person->animProgress >= 1.0f)
					{
						person->anim = PersonAnim_Idle;
						person->animProgress = 0.0f;
					}
				}
				else if (person->anim == PersonAnim_Eating)
				{
					person->animProgress += TIME_SCALED_ANIM(3600, timeScale);
					if (person->animProgress >= 1.0f)
					{
						person->anim = PersonAnim_Idle;
						person->animProgress = 0.0f;
					}
				}
				else if (person->anim == PersonAnim_BeingPoisoned)
				{
					r32 oldAnimProgress = person->animProgress;
					person->animProgress += TIME_SCALED_ANIM(POISON_ANIM_TIME, timeScale);
					if (person->animProgress >= 1.0f)
					{
						person->anim = PersonAnim_Idle;
						person->animProgress = 0.0f;
					}
					if (FloorR32i(oldAnimProgress / (800.0f / POISON_ANIM_TIME)) != FloorR32i(person->animProgress / (800.0f / POISON_ANIM_TIME)))
					{
						v2 partPos = person->position;
						partPos += NewVec2(GetRandR32(appRand, -6, 6), GetRandR32(appRand, 0, 6)) * personScale;
						Particle_t* poisonPart = SpawnParticle(0x00,
							GetSheet(partsSheet), NewVec2i(12, 2), 200, 4,
							partPos, NewVec2(0, -0.2f), Vec2_Zero,
							1.0f, 1.0f, PalGreen, ColorTransparent(PalGreen, 0.0f),
							0.0f, 2000
						);
					}
				}
				else if (person->anim == PersonAnim_Sleeping)
				{
					FlagUnset(person->flags, PersonFlag_FacingLeft);
					if ((ProgramTime/1200) != (PrevProgramTime/1200))
					{
						Particle_t* part = SpawnParticle(0x00,
							GetSheet(partsSheet), NewVec2i(13, 1), 100, 1,
							person->position, NewVec2(0, -0.5f), NewVec2(0, 0.008f),
							personScale, personScale, Black, TransparentBlack,
							0.0f, 1200
						);
					}
				}
				else if (person->anim == PersonAnim_Jumping)
				{
					person->animProgress += TIME_SCALED_ANIM(3500, timeScale);
					if (person->animProgress >= 1.0f)
					{
						person->anim = PersonAnim_Idle;
						person->animProgress = 0.0f;
					}
				}
				else if (person->anim == PersonAnim_PlayingDead)
				{
					person->animProgress += TIME_SCALED_ANIM(500, timeScale);
					if (person->animProgress >= 1.0f) { person->animProgress = 1.0f; }
				}
				
				// +==============================+
				// |     Handle People Events     |
				// +==============================+
				for (u32 eIndex = 0; eIndex < app->chat.events.length; eIndex++)
				{
					ChatEvent_t* event = DynArrayGet(&app->chat.events, ChatEvent_t, eIndex);
					NotNull(event);
					if (event->type == ChatEventType_Message && event->chatterId == person->chatterId)
					{
						if (person->anim == PersonAnim_Sleeping)
						{
							person->anim = PersonAnim_Idle;
							person->animProgress = 0.0f;
						}
						if (person->anim == PersonAnim_PlayingDead)
						{
							person->anim = PersonAnim_Idle;
							person->animProgress = 0.0f;
						}
						
						Chatter_t* chatter = GetChatterById(&app->chat, person->chatterId);
						bool foundCommand = TryParseChatMessageAsCommandForPerson(person, chatter, event->message);
						if (foundCommand) { FlagSet(event->flags, ChatEventFlag_Handled); }
						
						if (!foundCommand)
						{
							if (CanChangeAnimationRightNow(person, PersonAnim_Chatting))
							{
								person->anim = PersonAnim_Chatting;
								person->animProgress = 0.0f;
							}
							for (u32 cIndex = 0; event->message[cIndex] != '\0'; cIndex++)
							{
								if ((u8)event->message[cIndex] >= 0x80) { person->anim = PersonAnim_ChattingUnicode; break; }
							}
							
							// if (StrFindSubstringIgnoreCaseNt(event->message, "new year") || StrFindSubstringIgnoreCaseNt(event->message, "2021"))
							// {
							// 	person->anim = PersonAnim_ChattingParty;
							// 	u8 sndIndex = GetRandU8(appRand, 0, 6);
							// 	switch (sndIndex)
							// 	{
							// 		case 0: StartSndInstance(&soundsList->applauseSound1.sound); break;
							// 		case 1: StartSndInstance(&soundsList->applauseSound2.sound); break;
							// 		case 2: StartSndInstance(&soundsList->applauseSound3.sound); break;
							// 		case 3: StartSndInstance(&soundsList->applauseSound4.sound); break;
							// 		case 4: StartSndInstance(&soundsList->applauseSound5.sound); break;
							// 		case 5: StartSndInstance(&soundsList->applauseSound6.sound); break;
							// 		default: Assert(false); break;
							// 	}
								
							// 	//spawn confetti
							// 	for (u32 partIndex = 0; partIndex < 80; partIndex++)
							// 	{
							// 		r32 direction = RandR32(Pi32*5.0f/4.0f, Pi32*7.0f/4.0f);
							// 		r32 speed = RandR32(1.0f, 5);
							// 		v2 velocity = NewVec2(CosR32(direction)*speed, SinR32(direction)*speed);
							// 		v2 partPos = person->position + vis->peopleSpace.topLeft;
							// 		Color_t partColor = GetPredefPalColorByIndex(RandU32(0, 8));
							// 		Particle_t* part = SpawnParticle(ParticleFlag_ScreenSpace,
							// 			GetSheet(partsSheet), NewVec2i(0, 1), 100, 13,
							// 			partPos, velocity, NewVec2(0, 0.05f),
							// 			1.0f, 1.0f, partColor, partColor,
							// 			0.0f, 1300
							// 		);
							// 	}
							// }
							// else
							{
								u8 sndIndex = GetRandU8(appRand, 0, 5);
								switch (sndIndex)
								{
									case 0: StartSndInstance(&soundsList->talkingSound1.sound); break;
									case 1: StartSndInstance(&soundsList->talkingSound2.sound); break;
									case 2: StartSndInstance(&soundsList->talkingSound3.sound); break;
									case 3: StartSndInstance(&soundsList->talkingSound4.sound); break;
									case 4: StartSndInstance(&soundsList->talkingSound5.sound); break;
									default: Assert(false); break;
								}
							}
							
							ChatMessage_t* newMessage = DynArrayInsert(&vis->messages, ChatMessage_t, 0);
							NotNull(newMessage);
							ClearPointer(newMessage);
							newMessage->message = ParseChatMessage(mainHeap, event->message);
							NotNull(newMessage->message);
							newMessage->startTime = ProgramTime;
							newMessage->spawnAnimProgress = 0.0f;
							newMessage->spawnPos = person->position;
							newMessage->chatterId = person->chatterId;
							newMessage->displayName = person->displayName;
							ConsumeChatEvent(&app->chat, event);
							person->targetPos = person->position;
						}
					}
				}
				
				// +==============================+
				// |      Collision Handling      |
				// +==============================+
				if (ShouldPersonCollide(person))
				{
					personScale = GetPersonScale(person);
					r32 ourRadius = (GetSheetFrameWidth(personSheet) * personScale) * (3.0f/8.0f);
					for (u32 pIndex2 = pIndex+1; pIndex2 < vis->people.length; pIndex2++)
					{
						Person_t* person2 = DynArrayGet(&vis->people, Person_t, pIndex2);
						NotNull(person2);
						if (ShouldPersonCollide(person2))
						{
							v2 relativePos = VisualizerVecBetween(person->position, person2->position);
							r32 distance = Vec2Length(relativePos);
							r32 theirRadius = (GetSheetFrameWidth(personSheet) * GetPersonScale(person2)) * (3.0f/8.0f);
							if (distance < ourRadius + theirRadius)
							{
								bool wasPerson1Standing = (person->position == person->targetPos);
								bool wasPerson2Standing = (person2->position == person2->targetPos);
								r32 intersectionDepth = (ourRadius + theirRadius) - distance;
								if (distance != 0) { relativePos = relativePos / distance; }
								else { relativePos = NewVec2(1, 0); }
								r32 collisionWeightOffset = 0.5f;
								if (IsPersonCollsionHeavy(person) && !IsPersonCollsionHeavy(person2)) { collisionWeightOffset = 0.2f; }
								else if (!IsPersonCollsionHeavy(person) && IsPersonCollsionHeavy(person2)) { collisionWeightOffset = 0.8f; }
								person->position -= relativePos * intersectionDepth * collisionWeightOffset;
								person2->position += relativePos * intersectionDepth * (1 - collisionWeightOffset);
								person->position = NormalizeVisualizerPosition(person->position);
								person2->position = NormalizeVisualizerPosition(person2->position);
								if (wasPerson1Standing) { person->targetPos = person->position; }
								if (wasPerson2Standing) { person2->targetPos = person2->position; }
							}
						}
					}
					//collisions with active fire spots
					if (person->fireTimer == 0.0f)
					{
						for (u32 fIndex = 0; fIndex < vis->fires.length; fIndex++)
						{
							ActiveFire_t* fire = DynArrayGet(&vis->fires, ActiveFire_t, fIndex);
							NotNull(fire);
							bool wasSpawner = (fire->spawnChatterId == person->chatterId);
							if (fire->age < fire->lifeSpan && (!wasSpawner || fire->age >= FIRE_SPAWNER_IMMUNE_TIME))
							{
								v2 distanceVec = VisualizerVecBetween(fire->position, person->position);
								r32 distance = Vec2Length(distanceVec);
								if (distance < fire->radius)
								{
									person->fireTimer = PEOPLE_BURN_TIME;
									break;
								}
							}
						}
					}
				}
			}
			
			// +==============================+
			// |   Consume Old Chat Events    |
			// +==============================+
			for (u32 eIndex = 0; eIndex < app->chat.events.length; eIndex++)
			{
				ChatEvent_t* event = DynArrayGet(&app->chat.events, ChatEvent_t, eIndex);
				NotNull(event);
				if (event->type != ChatEventType_None && (TimeSince(event->time) >= 10000 || IsFlagSet(event->flags, ChatEventFlag_Handled)))
				{
					if (!IsFlagSet(event->flags, ChatEventFlag_Handled))
					{
						if (event->type == ChatEventType_Message)
						{
							NotifyPrint_D("Got message from person %u: %s", event->chatterId, event->message);
						}
						else
						{
							NotifyPrint_D("Got chat event %s", GetChatEventTypeStr(event->type));
						}
					}
					ConsumeChatEvent(&app->chat, event);
				}
			}
		}
		
		// +==============================+
		// |       Update Particles       |
		// +==============================+
		for (u32 pIndex = 0; pIndex < vis->parts.length; pIndex++)
		{
			Particle_t* part = DynArrayGet(&vis->parts, Particle_t, pIndex);
			if (IsFlagSet(part->flags, ParticleFlag_Alive))
			{
				part->velocity += part->acceleration;
				part->position += part->velocity;
				part->age += ElapsedMs;
				if (part->age >= part->lifeSpan)
				{
					FlagUnset(part->flags, ParticleFlag_Alive);
				}
			}
		}
		
		// +==============================+
		// |       Update Messages        |
		// +==============================+
		for (u32 mIndex = 0; mIndex < vis->messages.length; )
		{
			ChatMessage_t* message = DynArrayGet(&vis->messages, ChatMessage_t, mIndex);
			NotNull(message);
			if (message->spawnAnimProgress < 1.0f)
			{
				message->spawnAnimProgress += TIME_SCALED_ANIM(MESSAGE_SPAWN_ANIM_TIME, timeScale);
				if (message->spawnAnimProgress >= 1.0f) { message->spawnAnimProgress = 1.0f; }
			}
			if (TimeSince(message->startTime) >= MESSAGE_DISAPPEAR_TIME)
			{
				DynArrayRemove(&vis->messages, mIndex);
				//don't increment mIndex
			}
			else { mIndex++; }
		}
	}
}

// +--------------------------------------------------------------+
// |                      Render Visualizer                       |
// +--------------------------------------------------------------+
void RenderVisualizer(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	// +==============================+
	// |         Render Setup         |
	// +==============================+
	RcBegin(GetShader(tileShader), NewRec(Vec2_Zero, RenderScreenSize), GetFont(pixelFont), renderBuffer, true);
	RcClear(PureBlue, 1.0f);
	VisualizerUpdateRecs();
	
	RcBindFont(GetFont(pixelFont), 1);
	// RcDrawRectangle(vis->peopleSpace, PalBackground);
	RcBindTexture(GetTexture(backgroundBack));
	RcDrawTexturedRec(vis->peopleSpace, White);
	RcSetViewport(vis->peopleSpace);
	RcSetViewMatrix(Mat4Translate2(vis->peopleSpace.x, vis->peopleSpace.y));
	
	// +==============================+
	// |         Render Fires         |
	// +==============================+
	RcSetDepth(PEOPLE_MAX_DEPTH);
	for (u8 renderPass = 0; renderPass < 5; renderPass++)
	{
		v2 renderOffset = Vec2_Zero;
		if (renderPass > 0) { renderOffset = Vec2Multiply(NewVec2(Dir2ByIndex(renderPass-1)), vis->peopleSpace.size); }
		for (u32 fIndex = 0; fIndex < vis->fires.length; fIndex++)
		{
			ActiveFire_t* fire = DynArrayGet(&vis->fires, ActiveFire_t, fIndex);
			NotNull(fire);
			rec drawRec = NewRecCentered(fire->position + renderOffset, GetSheetFrameSizef(personSheet) * fire->scale);
			r32 burnSpotOpacity = SubAnimAmount(fire->age, fire->burnMarkLifeSpan, fire->burnMarkLifeSpan - 1000);
			RcDefaultReplaceColors();
			RcDrawSheetFrame(GetSheet(personSheet), NewVec2i(8, 0), drawRec, ColorTransparent(burnSpotOpacity));
		}
	}
	
	// +==============================+
	// |        Render People         |
	// +==============================+
	RcSetDepth(PEOPLE_MAX_DEPTH);
	for (u8 renderPass = 0; renderPass < 5; renderPass++)
	{
		v2 renderOffset = Vec2_Zero;
		if (renderPass > 0) { renderOffset = Vec2Multiply(NewVec2(Dir2ByIndex(renderPass-1)), vis->peopleSpace.size); }
		for (u32 pIndex = 0; pIndex < vis->people.length; pIndex++)
		{
			Person_t* person = DynArrayGet(&vis->people, Person_t, pIndex);
			NotNull(person);
			Chatter_t* chatter = nullptr;
			if (app->chat.created) { chatter = GetChatterById(&app->chat, person->chatterId); }
			if (person->height > 0 && renderPass != 0) { continue; } //don't render duplicates when being lifted up
			
			r32 personScale = GetPersonScale(person);
			rec drawRec = NewRecCentered(person->position + renderOffset, GetSheetFrameSizef(personSheet) * personScale);
			drawRec.y -= person->height;
			drawRec.topLeft = Vec2Round(drawRec.topLeft);
			r32 personDepth = (drawRec.y + drawRec.height) - (0 - PEOPLE_DEPTH_PADDING_SIZE);
			personDepth /= (vis->peopleSpace.height + PEOPLE_DEPTH_PADDING_SIZE*2);
			personDepth = LerpR32(PEOPLE_MAX_DEPTH, PEOPLE_MIN_DEPTH, personDepth);
			if (person->height > 0) { personDepth = PEOPLE_NAME_DEPTH; }
			bool facingLeft = IsFlagSet(person->flags, PersonFlag_FacingLeft);
			Color_t personColor = White;
			Color_t extrasColor = White;
			PalColor_t personPalColor = plt->colors[GetPersonPaletteColorIndex(person)];
			v2i personFrame = NewVec2i(0, 4);
			v2i mouthFrame = NewVec2i(1, 5);
			v2i mouthOffset = Vec2i_Zero; NewVec2i(facingLeft ? -1 : 1, 3);
			v2i eyeFrame = NewVec2i(0, 5);
			v2i eyeOffset = NewVec2i(0, -5);
			v2 eyeDirOffset = NewVec2((r32)(facingLeft ? -1 : 0), 0);
			v2i decorFrame = Vec2i_Zero;
			v2i decorOffset = Vec2i_Zero;
			v2i fireFrame = Vec2i_Zero;
			
			if (person->alienColor == ALIEN_INVISIBLE_COLOR) { extrasColor = ColorTransparent(extrasColor, 0.25f); }
			if (person->alienColor == ALIEN_RAINBOW_COLOR)
			{
				PalColorShiftSectionLooped(&personPalColor, PAL_PRIMARY2_INDEX, 6, (i32)((ProgramTime%1200)/200));
			}
			
			if (IsFlagSet(person->flags, PersonFlag_IsHappy)) { mouthFrame = NewVec2i(facingLeft ? 7 : 6, 5); }
			else if (IsFlagSet(person->flags, PersonFlag_IsHappy)) { mouthFrame = NewVec2i(facingLeft ? 7 : 6, 5); }
			else if (IsFlagSet(person->flags, PersonFlag_IsSad)) { mouthFrame = NewVec2i(facingLeft ? 5 : 4, 5); }
			else if (IsFlagSet(person->flags, PersonFlag_IsSmiling)) { mouthFrame = NewVec2i(facingLeft ? 11 : 10, 5); }
			
			if (person->fireTimer > 0.0f)
			{
				if (facingLeft) { fireFrame = NewVec2i(8 + (i32)((ProgramTime%500)/250), 7); }
				else { fireFrame = NewVec2i(8 + (i32)((ProgramTime%500)/250), 6); }
			}
			
			if (person->anim == PersonAnim_Spawning)
			{
				personFrame = NewVec2i(0 + (i32)(person->animProgress * 8), 0);
			}
			else if (person->position != person->targetPos && person->anim != PersonAnim_BeingAbducted)
			{
				bool isRunning = IsFlagSet(person->flags, PersonFlag_IsRunning);
				if (person->fireTimer > 0.0f) { isRunning = true; }
				if (isRunning || person->eatChatterId != 0)
				{
					if (person->velocity.x >= 0) { personFrame = NewVec2i(0 + (i32)((ProgramTime%165)/33), 6); }
					else { personFrame = NewVec2i(4 - (i32)((ProgramTime%165)/33), 7); }
				}
				else
				{
					if (person->velocity.x >= 0) { personFrame = NewVec2i(0 + (i32)((ProgramTime%500)/100), 3); }
					else { personFrame = NewVec2i(4 - (i32)((ProgramTime%500)/100), 3); }
				}
				if (person->velocity.x < 0) { eyeDirOffset.x = -1; }
				if (person->velocity.y < 0) { eyeDirOffset.y = -1; }
			}
			else
			{
				personFrame = NewVec2i(0 + (i32)((ProgramTime%800)/200), 4);
			}
			
			if (person->anim == PersonAnim_Chatting || person->anim == PersonAnim_ChattingUnicode || person->anim == PersonAnim_ChattingParty)
			{
				decorFrame = NewVec2i(facingLeft ? 7 : 6, 1);
				if (person->anim == PersonAnim_ChattingUnicode) { decorFrame = NewVec2i(facingLeft ? 7 : 6, 2); }
				if (person->anim == PersonAnim_ChattingParty)   { decorFrame = NewVec2i(facingLeft ? 7 : 6, 3); }
				decorOffset = NewVec2i(facingLeft ? -8 : 6, -1);
				mouthFrame = NewVec2i(2 + (i32)((ProgramTime%200)/100), 5);
			}
			if (person->anim == PersonAnim_Eating)
			{
				mouthFrame = NewVec2i(5 + ((i32)(person->animProgress * 12) % 3), facingLeft ? 7 : 6);
			}
			if (person->anim == PersonAnim_BeingPoisoned)
			{
				r32 eatingTime = (1000.0f / POISON_ANIM_TIME);
				r32 flopTime = eatingTime + (500.0f / POISON_ANIM_TIME);
				if (person->animProgress < eatingTime)
				{
					mouthFrame = NewVec2i(5 + ((i32)(SubAnimAmount(person->animProgress, 0, eatingTime) * 12) % 3), facingLeft ? 7 : 6);
				}
				else
				{
					r32 flopAnimProgress = SubAnimAmount(person->animProgress, eatingTime, flopTime);
					personFrame = NewVec2i(0 + (i32)(flopAnimProgress * 5), 10);
					personPalColor = LerpPalColors(personPalColor, plt->variant4, flopAnimProgress);
					if (personFrame.x > 4) { personFrame.x = 4; }
					if (personFrame.x >= 3) { eyeFrame = Vec2i_Zero; }
					if (personFrame.x >= 2) { mouthFrame = Vec2i_Zero; }
				}
			}
			if (person->anim == PersonAnim_Sleeping)
			{
				personFrame = NewVec2i(0 + (i32)((ProgramTime%800) / 200), 8);
				eyeFrame = Vec2i_Zero;
				mouthFrame = NewVec2i(8 + (i32)((ProgramTime%800)/400), 5);
			}
			if (person->anim == PersonAnim_Jumping)
			{
				if (facingLeft)
				{
					personFrame = NewVec2i(6 - (i32)((ProgramTime%700) / 100), 9);
				}
				else
				{
					personFrame = NewVec2i(0 + (i32)((ProgramTime%700) / 100), 9);
				}
			}
			if (person->anim == PersonAnim_PlayingDead)
			{
				personFrame = NewVec2i(0 + (i32)(person->animProgress * 5), 10);
				if (person->animProgress >= 1.0f) { personFrame.x = 4; }
				if (personFrame.x >= 3) { eyeFrame = Vec2i_Zero; }
				if (personFrame.x >= 2) { mouthFrame = Vec2i_Zero; }
			}
			if (person->anim == PersonAnim_BeingAbducted)
			{
				mouthFrame = NewVec2i(2 + (i32)((ProgramTime%200)/100), 5);
			}
			
			//handle look at
			if (person->lookChatterId != 0)
			{
				v2 lookTargetVec = Vec2_Zero;
				r32 lookTargetDistance = 0.0f;
				Person_t* lookTarget = nullptr;
				for (u32 pIndex2 = 0; pIndex2 < vis->people.length; pIndex2++)
				{
					Person_t* person2 = DynArrayGet(&vis->people, Person_t, pIndex2);
					NotNull(person2);
					if (pIndex != pIndex2 && person2->chatterId == person->lookChatterId)
					{
						v2 person2Vec = person2->position - person->position;
						r32 person2Distance = Vec2Length(person2Vec);
						if (lookTarget == nullptr || person2Distance < lookTargetDistance)
						{
							lookTarget = person2;
							lookTargetVec = person2Vec;
							lookTargetDistance = person2Distance;
						}
					}
				}
				
				if (lookTarget != nullptr)
				{
					r32 lookAngle = AtanR32(lookTargetVec.y, lookTargetVec.x);
					if (lookAngle < 0) { lookAngle += Pi32*2; }
					if (lookAngle >= Pi32*2) { lookAngle -= Pi32*2; }
					if      (lookAngle > Pi32*(15/8.0f) || lookAngle <= Pi32*( 1/8.0f)) { eyeDirOffset = NewVec2(    0, -0.5f); } //right
					else if (lookAngle > Pi32*(13/8.0f) && lookAngle <= Pi32*(15/8.0f)) { eyeDirOffset = NewVec2(    0,    -1); } //up-right
					else if (lookAngle > Pi32*(11/8.0f) && lookAngle <= Pi32*(13/8.0f)) { eyeDirOffset = NewVec2(-0.5f,    -1); } //up
					else if (lookAngle > Pi32*( 9/8.0f) && lookAngle <= Pi32*(11/8.0f)) { eyeDirOffset = NewVec2(   -1,    -1); } //up-left
					else if (lookAngle > Pi32*( 7/8.0f) && lookAngle <= Pi32*( 9/8.0f)) { eyeDirOffset = NewVec2(   -1, -0.5f); } //left
					else if (lookAngle > Pi32*( 5/8.0f) && lookAngle <= Pi32*( 7/8.0f)) { eyeDirOffset = NewVec2(   -1,     0); } //down-left
					else if (lookAngle > Pi32*( 3/8.0f) && lookAngle <= Pi32*( 5/8.0f)) { eyeDirOffset = NewVec2(-0.5f,     0); } //down
					else if (lookAngle > Pi32*( 1/8.0f) && lookAngle <= Pi32*( 3/8.0f)) { eyeDirOffset = NewVec2(    0,     0); } //down-right
					// if (lookTargetVec.x >= 0) { eyeDirOffset.x = 0; }
					// else { eyeDirOffset.x = -1; }
					// if (lookTargetVec.y >= 0) { eyeDirOffset.y = 0; }
					// else { eyeDirOffset.y = -1; }
				}
			}
			
			eyeOffset += GetPersonEyeOffsetForFrame(personFrame);
			mouthOffset += GetPersonMouthOffsetForFrame(personFrame, facingLeft);
			
			if (person->height > 0)
			{
				RcSetDepth(0.0f);
				RcSetViewport(NewRec(0, vis->abduction.spaceshipDrawRec.y, RenderScreenSize.width, RenderScreenSize.height - vis->abduction.spaceshipDrawRec.y));
			}
			else
			{
				RcSetDepth(personDepth);
				RcSetViewport(vis->peopleSpace);
			}
			RcSetReplaceColors(personPalColor);
			RcDrawSheetFrame(GetSheet(personSheet), personFrame, drawRec, personColor);
			if (eyeFrame != Vec2i_Zero)
			{
				rec eyeDrawRec = drawRec;
				RecOffsetPx(eyeDrawRec, NewVec2(eyeOffset) + eyeDirOffset);
				RcDrawSheetFrame(GetSheet(personSheet), eyeFrame, eyeDrawRec, White);
			}
			if (mouthFrame != Vec2i_Zero)
			{
				rec mouthDrawRec = drawRec;
				RecOffsetPxi(mouthDrawRec, mouthOffset);
				RcDrawSheetFrame(GetSheet(personSheet), mouthFrame, mouthDrawRec, extrasColor);
			}
			if (fireFrame != Vec2i_Zero)
			{
				RcDefaultReplaceColors();
				RcDrawSheetFrame(GetSheet(personSheet), fireFrame, drawRec, White);
			}
			RcSetDepth(PEOPLE_MSG_BUBBLE_DEPTH);
			if (decorFrame != Vec2i_Zero)
			{
				rec decorDrawRec = drawRec;
				RecOffsetPxi(decorDrawRec, decorOffset);
				RcDrawSheetFrame(GetSheet(personSheet), decorFrame, decorDrawRec, extrasColor);
			}
			
			RcSetDepth(PEOPLE_NAME_DEPTH);
			{
				v2 usernamePos = NewVec2(drawRec.x + drawRec.width/2, drawRec.y - 2 - RcMaxExtendDown());
				usernamePos = Vec2Round(usernamePos);
				Color_t textColor = White;
				if (person->alienColor == ALIEN_INVISIBLE_COLOR) { textColor = ColorTransparent(textColor, 0.25f); }
				const char* displayNameStr = (chatter != nullptr) ? chatter->username : "[Unknown]";
				if (person->displayName != nullptr && person->displayName->displayStr != nullptr) { displayNameStr = person->displayName->displayStr; }
				RcDrawStringNt(displayNameStr, usernamePos, textColor, 1.0f, Alignment_Center);
				// RcDrawStringNt(TempPrint("(%.0f, %.0f)", person->position.x, person->position.y), usernamePos, textColor, 1.0f, Alignment_Center);
			}
		}
	}
	
	// +==============================+
	// |       Render Particles       |
	// +==============================+
	for (u32 pIndex = 0; pIndex < vis->parts.length; pIndex++)
	{
		Particle_t* part = DynArrayGet(&vis->parts, Particle_t, pIndex);
		if (IsFlagSet(part->flags, ParticleFlag_Alive) && !IsFlagSet(part->flags, ParticleFlag_ScreenSpace))
		{
			RcSetDepth(part->depth);
			r32 lifePercent = part->age / part->lifeSpan;
			r32 scale = LerpR32(part->scaleStart, part->scaleEnd, lifePercent);
			Color_t color = ColorLerp(part->colorStart, part->colorEnd, lifePercent);
			v2i frame = part->frame + NewVec2i(FloorR32i(part->age / part->frameTime)%part->numFrames, 0);
			rec partRec = NewRecCentered(part->position, NewVec2(part->sheet->innerFrameSize) * scale);
			RcSetReplaceColors(plt->colors[part->paletteColorIndex]);
			RcDrawSheetFrame(part->sheet, frame, partRec, color);
		}
	}
	
	RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
	RcSetViewMatrix(Mat4_Identity);
	RcSetDepth(0.1f);
	// RcDrawButton(vis->peopleSpace, Transparent, PalBackgroundDarker, 2.0f);
	
	// +==============================+
	// |  Render People Space Border  |
	// +==============================+
	RcBindTexture(GetTexture(backgroundBorder));
	RcDrawTexturedRec(vis->peopleSpace, White);
	
	// +==============================+
	// |       Render Messages        |
	// +==============================+
	RcSetViewport(vis->messageSpace);
	RcBindFont(GetFont(debugFont));
	r32 maxWidth = vis->messageSpace.width - MESSAGE_SPACE_PADDING*2;
	for (u32 mIndex = 0; mIndex < vis->messages.length; mIndex++)
	{
		ChatMessage_t* message = DynArrayGet(&vis->messages, ChatMessage_t, mIndex);
		NotNull(message);
		
		r32 disappearAnimProgress = 0.0f;
		u64 messageAge = TimeSince(message->startTime);
		if (messageAge >= MESSAGE_DISAPPEAR_TIME - MESSAGE_DISAPPEAR_ANIM_TIME)
		{
			disappearAnimProgress = (r32)(messageAge - (MESSAGE_DISAPPEAR_TIME - MESSAGE_DISAPPEAR_ANIM_TIME)) / (r32)MESSAGE_DISAPPEAR_ANIM_TIME;
		}
		
		const char* displayNameStr = "[Unknown]";
		if (message->displayName != nullptr && message->displayName->displayStr != nullptr) { displayNameStr = message->displayName->displayStr; }
		else
		{
			Chatter_t* chatter = GetChatterById(&app->chat, message->chatterId);
			if (chatter != nullptr && chatter->username != nullptr) { displayNameStr = chatter->username; }
		}
		displayNameStr = TempPrint("%s:", displayNameStr);
		rec actualDrawRec = message->drawRec + vis->messageSpace.topLeft;
		actualDrawRec.x -= actualDrawRec.width * EaseQuadraticOut(disappearAnimProgress);
		actualDrawRec.topLeft = Vec2Round(actualDrawRec.topLeft);
		v2 actualUsernamePos = actualDrawRec.topLeft + message->usernamePos;
		v2 actualMessagePos = actualDrawRec.topLeft + message->messagePos;
		r32 messageOffset = (actualMessagePos.x - actualUsernamePos.x);
		
		// RcDrawRectangle(message->drawRec, Black);
		RcDrawTiledUiRec(GetSheet(messageBackSheet), Vec2i_Zero, actualDrawRec, White, 2.0f);
		// RcDrawStringNt(displayNameStr, actualUsernamePos + NewVec2(0, 1), Black);
		RcDrawStringNt(displayNameStr, actualUsernamePos, PalGrassDarker);
		// RcDrawFormattedString(message->message, actualMessagePos + NewVec2(0, 1), maxWidth - messageOffset, Black, 1.0f, Alignment_Left, true, -messageOffset);
		RcDrawFormattedString(message->message, actualMessagePos, maxWidth - messageOffset, PalBlueDarker, 1.0f, Alignment_Left, true, -messageOffset);
	}
	
	// +==============================+
	// |  Render Abduction Spaceship  |
	// +==============================+
	RcSetDepth(0.0f);
	RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
	if (vis->abduction.inProgress)
	{
		if (vis->abduction.progress >= 2.0f && vis->abduction.progress < 3.0f)
		{
			r32 subAnim = SubAnimAmount(vis->abduction.progress, 2.0f, 3.0f);
			v2i groundFrame = NewVec2i(0 + (i32)((ProgramTime%1000)/500), 0);
			v2i particlesFrame = NewVec2i(2 + (i32)((ProgramTime%600)/100), 0);
			v2i overlayFrame = NewVec2i(8, 0);
			RcSetDepth(0.99f);
			RcDrawSheetFrame(GetSheet(tractorBeamSheet), groundFrame, vis->abduction.tractorBeamRec, ColorTransparent(Oscillate(0.4f, 0.6f, 1000)));
			RcSetDepth(0.0f);
			RcDrawSheetFrame(GetSheet(tractorBeamSheet), particlesFrame, vis->abduction.tractorBeamRec, White);
			RcDrawSheetFrame(GetSheet(tractorBeamSheet), overlayFrame, vis->abduction.tractorBeamRec, ColorTransparent(Oscillate(0.3f, 0.4f, 1000)));
		}
		
		RcBindTexture(GetTexture(humanSpaceship));
		RcDrawTexturedObb2(vis->abduction.spaceshipDrawRec, White, NewRec(Vec2_Zero, GetTextureSizef(humanSpaceship)));
		
		// RcBindFont(GetFont(pixelFont), 1.0f);
		// RcPrintStringAligned(NewVec2(RenderScreenSize.width - 5, vis->peopleSpace.y - RcMaxExtendDown()), White, 1.0f, Alignment_Right, "%.1f", vis->abduction.progress);
	}
	
	// +==============================+
	// |     Render Stretch Timer     |
	// +==============================+
	if (vis->stretchTimerRunning)
	{
		RcBindFont(GetFont(pixelFontOutline), 2.0f);
		i32 timerSeconds = FloorR32i(vis->stretchTimer / 1000);
		i32 timerMinutes = timerSeconds / 60;
		timerSeconds -= timerMinutes * 60;
		v2 timerTextPos = NewVec2(RenderScreenSize.width - 5, vis->peopleSpace.y + RcCenterOffset());
		Color_t timerTextColor = White;
		Color_t timerTextOutlineColor = Black;
		if (vis->stretchTimer <= 0.0f && !vis->stretchTimerSilenced)
		{
			timerTextColor = ColorLerp(PalOrange, PalOrangeLighter, Oscillate(0, 1.0f, 1000));
			timerTextOutlineColor = ColorLerp(PalRedDarker, PalRed, Oscillate(0, 1.0f, 1000));
		}
		if (vis->stretchTimer <= 0.0f && vis->stretchTimerSilenced)
		{
			v2i stretchFrame = NewVec2i(0 + (i32)((ProgramTime%1000)/500), 0);
			RcDefaultReplaceColors();
			RcDrawSheetFrame(GetSheet(stretchAnimSheet), stretchFrame, vis->stretchAnimRec, White);
		}
		RcSetReplaceColorDark(timerTextOutlineColor);
		RcSetReplaceColorPrimary(timerTextColor);
		if (vis->stretchTimerSilenced)
		{
			RcDrawStringNt("Stretching...", timerTextPos, White, 1.0f, Alignment_Right);
		}
		else
		{
			RcPrintStringAligned(timerTextPos, White, 1.0f, Alignment_Right,
				"%s%d:%02d",
				vis->stretchTimerPaused ? "||" : "",
				timerMinutes, timerSeconds
			);
		}
	}
	
	// +==============================+
	// |     Render Special Text      |
	// +==============================+
	{
		RcBindFont(GetFont(titleFont), 1.0f);
		Color_t specialTextColor1 = PalPurple;
		Color_t specialTextColor2 = PalBlueLighter;
		Color_t specialTextEmbossColor = White;
		r32 specialTextPosY = RenderScreenSize.height/2;
		const char* specialText = nullptr;
		if (vis->abduction.inProgress && vis->abduction.progress >= 1.0f && vis->abduction.progress < 2.0f)
		{
			specialText = "Play !dead";
			specialTextPosY = vis->messageSpace.y + vis->messageSpace.height - 100;
			specialTextColor1 = PalPurpleDark;
			specialTextColor2 = Black;
		}
		if (specialText != nullptr)
		{
			v2 specialTextSize = RcMeasureStringNt(specialText);
			v2 specialTextPos = NewVec2(vis->messageSpace.x + vis->messageSpace.width/2 - specialTextSize.width/2, specialTextPosY + RcCenterOffset());
			rec specialTextRec = NewRec(specialTextPos.x, specialTextPos.y - RcCenterOffset() - specialTextSize.height/2, specialTextSize.width, specialTextSize.height);
			RcDrawStringNt(specialText, specialTextPos + NewVec2(0, 2), specialTextEmbossColor);
			RcDrawStringNt(specialText, specialTextPos + NewVec2(0, -2), specialTextEmbossColor);
			RcDrawStringNt(specialText, specialTextPos + NewVec2(2, 0), specialTextEmbossColor);
			RcDrawStringNt(specialText, specialTextPos + NewVec2(-2, 0), specialTextEmbossColor);
			RcDrawStringNt(specialText, specialTextPos, specialTextColor1);
			const Shader_t* oldShader = rc->boundShader;
			RcStartStencilDrawing();
			RcClearStencilBuffer(0);
			RcDrawStringNt(specialText, specialTextPos, White);
			RcUseStencil();
			// RcDrawRectangle(specialTextRec, PalRed);
			RcBindShader(GetShader(cineShader));
			RcSetTime((r32)(ProgramTime%(100*1000)) / 100.0f);
			// RcDrawRectangle(NewRecCentered(specialTextRec.topLeft + specialTextRec.size/2, NewVec2(200)), VisBlueGray);
			RcDrawRectangle(specialTextRec, specialTextColor2);
			RcBindShader(oldShader);
			RcDisableStencil();
		}
	}
	
	// +==============================+
	// |   Render Screen Particles    |
	// +==============================+
	RcSetDepth(0.0f);
	for (u32 pIndex = 0; pIndex < vis->parts.length; pIndex++)
	{
		Particle_t* part = DynArrayGet(&vis->parts, Particle_t, pIndex);
		if (IsFlagSet(part->flags, ParticleFlag_Alive) && IsFlagSet(part->flags, ParticleFlag_ScreenSpace))
		{
			RcSetDepth(part->depth);
			r32 lifePercent = part->age / part->lifeSpan;
			r32 scale = LerpR32(part->scaleStart, part->scaleEnd, lifePercent);
			Color_t color = ColorLerp(part->colorStart, part->colorEnd, lifePercent);
			v2i frame = part->frame + NewVec2i(FloorR32i(part->age / part->frameTime)%part->numFrames, 0);
			rec partRec = NewRecCentered(part->position, NewVec2(part->sheet->innerFrameSize) * scale);
			RcSetReplaceColors(plt->colors[part->paletteColorIndex]);
			RcDrawSheetFrame(part->sheet, frame, partRec, color);
		}
	}
	
}

