/*
File:   visualizer_commands.cpp
Author: Taylor Robbins
Date:   11\22\2020
Description: 
	** Holds the TryParseChatMessageAsCommandForPerson function
*/

Person_t* VisualizerGetPersonByStr(const char* strPntr, u32 strLength, Chatter_t** chatterOut = nullptr)
{
	Assert(strPntr != nullptr || strLength == 0);
	if (chatterOut != nullptr) { *chatterOut = nullptr; }
	for (u32 pIndex = 0; pIndex < vis->people.length; pIndex++)
	{
		Person_t* person = DynArrayGet(&vis->people, Person_t, pIndex);
		NotNull(person);
		Chatter_t* chatter = GetChatterById(&app->chat, person->chatterId);
		if (person->displayName != nullptr && person->displayName->displayStr != nullptr)
		{
			if (MyStrLength32(person->displayName->displayStr) >= strLength && StrCompareIgnoreCase(person->displayName->displayStr, strPntr, strLength))
			{
				if (chatterOut != nullptr) { *chatterOut = chatter; }
				return person;
			}
		}
	}
	for (u32 pIndex = 0; pIndex < vis->people.length; pIndex++)
	{
		Person_t* person = DynArrayGet(&vis->people, Person_t, pIndex);
		NotNull(person);
		Chatter_t* chatter = GetChatterById(&app->chat, person->chatterId);
		if (chatter != nullptr && chatter->username != nullptr)
		{
			if (MyStrLength32(chatter->username) >= strLength && StrCompareIgnoreCase(chatter->username, strPntr, strLength))
			{
				if (chatterOut != nullptr) { *chatterOut = chatter; }
				return person;
			}
		}
	}
	return nullptr;
}

//NOTE: chatter could be nullptr here
bool TryParseChatMessageAsCommandForPerson(Person_t* person, Chatter_t* chatter, const char* message)
{
	u32 numPieces = 0;
	StrSplitPiece_t* pieces = SplitString(TempArena, message, MyStrLength32(message), " ", 1, &numPieces);
	Assert(pieces != nullptr || numPieces == 0);
	if (numPieces == 0) { return false; }
	char* cmdStr = ArenaString(TempArena, pieces[0].pntr, pieces[0].length);
	NotNull(cmdStr);
	
	if (StrCompareIgnoreCaseNt(cmdStr, "!run"))
	{
		if (!IsFlagSet(person->flags, PersonFlag_IsRunning))
		{
			FlagSet(person->flags, PersonFlag_IsRunning);
			FlagUnset(person->flags, PersonFlag_IsStopped);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!walk"))
	{
		if (IsFlagSet(person->flags, PersonFlag_IsRunning) || IsFlagSet(person->flags, PersonFlag_IsStopped))
		{
			FlagUnset(person->flags, PersonFlag_IsRunning);
			FlagUnset(person->flags, PersonFlag_IsStopped);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!happy"))
	{
		if (!IsFlagSet(person->flags, PersonFlag_IsHappy))
		{
			FlagSet(person->flags, PersonFlag_IsHappy);
			FlagUnset(person->flags, PersonFlag_IsSad);
			FlagUnset(person->flags, PersonFlag_IsSmiling);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!persist"))
	{
		if (!IsFlagSet(person->flags, PersonFlag_Persistant))
		{
			FlagSet(person->flags, PersonFlag_Persistant);
		}
		else
		{
			FlagUnset(person->flags, PersonFlag_Persistant);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!sad"))
	{
		if (!IsFlagSet(person->flags, PersonFlag_IsSad))
		{
			FlagSet(person->flags, PersonFlag_IsSad);
			FlagUnset(person->flags, PersonFlag_IsHappy);
			FlagUnset(person->flags, PersonFlag_IsSmiling);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!smile"))
	{
		if (!IsFlagSet(person->flags, PersonFlag_IsSmiling))
		{
			FlagSet(person->flags, PersonFlag_IsSmiling);
			FlagUnset(person->flags, PersonFlag_IsSad);
			FlagUnset(person->flags, PersonFlag_IsHappy);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!normal"))
	{
		if (IsFlagSet(person->flags, PersonFlag_IsSad) || IsFlagSet(person->flags, PersonFlag_IsHappy))
		{
			FlagUnset(person->flags, PersonFlag_IsSad);
			FlagUnset(person->flags, PersonFlag_IsHappy);
			FlagUnset(person->flags, PersonFlag_IsSmiling);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!stop"))
	{
		if (!IsFlagSet(person->flags, PersonFlag_IsStopped))
		{
			FlagSet(person->flags, PersonFlag_IsStopped);
			FlagUnset(person->flags, PersonFlag_IsRunning);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!color"))
	{
		if (numPieces >= 2)
		{
			if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "normal")         || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "0"))  { person->alienColor = 0;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "green")     || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "1"))  { person->alienColor = 1;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "blue")      || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "2"))  { person->alienColor = 2;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "yellow")    || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "3"))  { person->alienColor = 3;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "orange")    || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "4"))  { person->alienColor = 4;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "red")       || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "5"))  { person->alienColor = 5;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "white")     || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "6"))  { person->alienColor = 6;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "cyan")      || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "7"))  { person->alienColor = 7;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "pink")      || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "8"))  { person->alienColor = 8;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "brown")     || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "9"))  { person->alienColor = 9;  return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "lime")      || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "10")) { person->alienColor = 10; return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "purple")    || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "11")) { person->alienColor = 11; return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "sky")       || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "12")) { person->alienColor = 12; return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "invisible") || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "13")) { person->alienColor = 13; return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "grey")      || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "14")) { person->alienColor = 14; return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "black")     || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "15")) { person->alienColor = 15; return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "slate")     || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "16")) { person->alienColor = 16; return true; }
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "rainbow")   || DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "17")) { person->alienColor = 17; return true; }
		}
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!pink"))
	{
		person->alienColor = NUM_ALIEN_COLORS;
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!look"))
	{
		if (numPieces >= 2)
		{
			Person_t* target = VisualizerGetPersonByStr(pieces[1].pntr, MyStrLength32(pieces[1].pntr));
			if (target != nullptr)
			{
				PrintLine_I("%s is now looking at %s", GetPersonDisplayName(person), GetPersonDisplayName(target));
				person->lookChatterId = target->chatterId;
				return true;
			}
			else if (person->lookChatterId != 0)
			{
				person->lookChatterId = 0;
				return true;
			}
		}
		else if (person->lookChatterId != 0)
		{
			person->lookChatterId = 0;
			return true;
		}
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!eat"))
	{
		if (numPieces >= 2)
		{
			Person_t* target = VisualizerGetPersonByStr(pieces[1].pntr, MyStrLength32(pieces[1].pntr));
			if (target != nullptr)
			{
				PrintLine_I("%s is going to eat %s", GetPersonDisplayName(person), GetPersonDisplayName(target));
				person->eatChatterId = target->chatterId;
				return true;
			}
			else if (person->eatChatterId != 0)
			{
				person->eatChatterId = 0;
				return true;
			}
		}
		else if (person->eatChatterId != 0)
		{
			person->eatChatterId = 0;
			return true;
		}
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!leave") || StrCompareIgnoreCaseNt(cmdStr, "!exit"))
	{
		person->eatChatterId = person->chatterId;
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!big"))
	{
		if (!IsFlagSet(person->flags, PersonFlag_Big))
		{
			FlagSet(person->flags, PersonFlag_Big);
			FlagUnset(person->flags, PersonFlag_Smol);
		}
		else
		{
			FlagUnset(person->flags, PersonFlag_Big);
			FlagUnset(person->flags, PersonFlag_Smol);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!small") || StrCompareIgnoreCaseNt(cmdStr, "!smol"))
	{
		if (!IsFlagSet(person->flags, PersonFlag_Smol))
		{
			FlagSet(person->flags, PersonFlag_Smol);
			FlagUnset(person->flags, PersonFlag_Big);
		}
		else
		{
			FlagUnset(person->flags, PersonFlag_Big);
			FlagUnset(person->flags, PersonFlag_Smol);
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!avoid"))
	{
		if (numPieces >= 2)
		{
			if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "all"))
			{
				if (!IsFlagSet(person->flags, PersonFlag_AvoidAll))
				{
					PrintLine_I("%s is avoiding everyone", GetPersonDisplayName(person));
					FlagSet(person->flags, PersonFlag_AvoidAll);
					FlagUnset(person->flags, PersonFlag_AvoidRunning);
					person->avoidChatterId = 0;
				}
				else
				{
					PrintLine_I("%s is no longer avoiding", GetPersonDisplayName(person));
					FlagUnset(person->flags, PersonFlag_AvoidAll);
					FlagUnset(person->flags, PersonFlag_AvoidRunning);
					person->avoidChatterId = 0;
				}
				return true;
			}
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "running"))
			{
				if (!IsFlagSet(person->flags, PersonFlag_AvoidRunning))
				{
					PrintLine_I("%s is avoiding running people", GetPersonDisplayName(person));
					FlagSet(person->flags, PersonFlag_AvoidRunning);
					FlagUnset(person->flags, PersonFlag_AvoidAll);
					person->avoidChatterId = 0;
				}
				else
				{
					PrintLine_I("%s is no longer avoiding", GetPersonDisplayName(person));
					FlagUnset(person->flags, PersonFlag_AvoidRunning);
					FlagUnset(person->flags, PersonFlag_AvoidAll);
					person->avoidChatterId = 0;
				}
				return true;
			}
			else if (DoesSplitPieceEqualIgnoreCaseNt(&pieces[1], "fire"))
			{
				if (!IsFlagSet(person->flags, PersonFlag_AvoidFires))
				{
					PrintLine_I("%s is avoiding fires", GetPersonDisplayName(person));
					FlagSet(person->flags, PersonFlag_AvoidFires);
				}
				else
				{
					PrintLine_I("%s is no longer avoiding fire", GetPersonDisplayName(person));
					FlagUnset(person->flags, PersonFlag_AvoidFires);
				}
				return true;
			}
			else
			{
				Person_t* target = VisualizerGetPersonByStr(pieces[1].pntr, MyStrLength32(pieces[1].pntr));
				if (target != nullptr)
				{
					PrintLine_I("%s is avoiding %s", GetPersonDisplayName(person), GetPersonDisplayName(target));
					person->avoidChatterId = target->chatterId;
					FlagUnset(person->flags, PersonFlag_AvoidAll);
					FlagUnset(person->flags, PersonFlag_AvoidRunning);
					return true;
				}
				else if (person->avoidChatterId != 0 || IsFlagSet(person->flags, PersonFlag_AvoidAll) || IsFlagSet(person->flags, PersonFlag_AvoidRunning))
				{
					person->avoidChatterId = 0;
					FlagUnset(person->flags, PersonFlag_AvoidAll);
					FlagUnset(person->flags, PersonFlag_AvoidRunning);
					return true;
				}
			}
		}
		else if (person->avoidChatterId != 0 || IsFlagSet(person->flags, PersonFlag_AvoidAll) || IsFlagSet(person->flags, PersonFlag_AvoidRunning))
		{
			person->avoidChatterId = 0;
			FlagUnset(person->flags, PersonFlag_AvoidAll);
			FlagUnset(person->flags, PersonFlag_AvoidRunning);
			return true;
		}
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!sleep"))
	{
		if (CanChangeAnimationRightNow(person, PersonAnim_Sleeping))
		{
			if (person->anim != PersonAnim_Sleeping)
			{
				person->anim = PersonAnim_Sleeping;
				person->animProgress = 0.0f;
				
				FlagUnset(person->flags, PersonFlag_IsRunning);
				FlagUnset(person->flags, PersonFlag_AvoidAll);
				FlagUnset(person->flags, PersonFlag_AvoidRunning);
				person->eatChatterId = 0;
				person->lookChatterId = 0;
				person->avoidChatterId = 0;
			}
			return true;
		}
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!fire") || StrCompareIgnoreCaseNt(cmdStr, "!arson"))
	{
		ActiveFire_t* newFire = VisualizerSpawnFire(person->chatterId, person->position, FIRE_LIFE_SPAN);
		if (newFire != nullptr)
		{
			newFire->spawnChatterId = person->chatterId;
		}
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!burn"))
	{
		person->fireTimer = PEOPLE_BURN_TIME;
		return true;
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!jump"))
	{
		if (CanChangeAnimationRightNow(person, PersonAnim_Jumping))
		{
			if (person->anim != PersonAnim_Jumping)
			{
				person->anim = PersonAnim_Jumping;
				person->animProgress = 0.0f;
				u8 sndIndex = GetRandU8(appRand, 0, 4);
				switch (sndIndex)
				{
					case 0: StartSndInstance(&soundsList->yahooSound1.sound); break;
					case 1: StartSndInstance(&soundsList->yahooSound2.sound); break;
					case 2: StartSndInstance(&soundsList->yahooSound3.sound); break;
					case 3: StartSndInstance(&soundsList->yahooSound4.sound); break;
					default: Assert(false); break;
				}
			}
			return true;
		}
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!dead") || StrCompareIgnoreCaseNt(cmdStr, "!ded") || StrCompareIgnoreCaseNt(cmdStr, "!playdead"))
	{
		if (CanChangeAnimationRightNow(person, PersonAnim_PlayingDead))
		{
			if (person->anim != PersonAnim_PlayingDead)
			{
				person->anim = PersonAnim_PlayingDead;
				person->animProgress = 0.0f;
			}
			return true;
		}
	}
	else if (StrCompareIgnoreCaseNt(cmdStr, "!poison"))
	{
		if (!IsFlagSet(person->flags, PersonFlag_Poisonous))
		{
			FlagSet(person->flags, PersonFlag_Poisonous);
		}
		else
		{
			FlagUnset(person->flags, PersonFlag_Poisonous);
		}
		return true;
	}
	else
	{
		// if (StrFindSubstringIgnoreCaseNt(message, "new year") || StrFindSubstringIgnoreCaseNt(message, "2021"))
		// {
		// 	u8 sndIndex = GetRandU8(appRand, 0, 6);
		// 	switch (sndIndex)
		// 	{
		// 		case 0: StartSndInstance(&soundsList->applauseSound1.sound); break;
		// 		case 1: StartSndInstance(&soundsList->applauseSound2.sound); break;
		// 		case 2: StartSndInstance(&soundsList->applauseSound3.sound); break;
		// 		case 3: StartSndInstance(&soundsList->applauseSound4.sound); break;
		// 		case 4: StartSndInstance(&soundsList->applauseSound5.sound); break;
		// 		case 5: StartSndInstance(&soundsList->applauseSound6.sound); break;
		// 		default: Assert(false); break;
		// 	}
		// }
	}
	
	return false;
}

