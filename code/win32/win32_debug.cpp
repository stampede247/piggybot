/*
File:   win32_debug.cpp
Author: Taylor Robbins
Date:   11\04\2017
Description: 
	** Holds functions that help the platform layer (and the application) output debug text to
	** the console, stdout, and anywhere else we would like to send the data
	** This file also holds the Win32_CrashDump function used by the AssertFailure function in win32_main.cpp
*/

char* Win32_CrashDump(u32* resultSizeOut, const char* crashFilename, int crashLineNumber, const char* crashFunction, const char* crashExpressionStr, const RealTime_t* realTime, u64 programTime)
{
	char* result = nullptr;
	u32 resultSize = 0;
	if (resultSizeOut != nullptr) { *resultSizeOut = 0; }
	
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 numChars = 0;
		
		TwoPassPrint(&numChars, result, resultSize, "+==============================+\n");
		TwoPassPrint(&numChars, result, resultSize, "| Plat Crash Dump v%u.%02u(%5u) |\n", PLATFORM_VERSION_MAJOR, PLATFORM_VERSION_MINOR, PLATFORM_VERSION_BUILD);
		TwoPassPrint(&numChars, result, resultSize, "+==============================+\n");
		TwoPassPrint(&numChars, result, resultSize, "Time: %s %s %s %u at %u:%u:%u%s\n", GetDayOfWeekStr((DayOfWeek_t)realTime->dayOfWeek), GetMonthStr((Month_t)realTime->month), GetDayOfMonthString(realTime->day), realTime->year, Convert24HourTo12Hour(realTime->hour), realTime->minute, realTime->second, IsPostMeridian(realTime->hour) ? "pm" : "am");
		TwoPassPrint(&numChars, result, resultSize, "Failed Assertion in %s:%u function %s: (%s) is false\n\n", crashFilename, crashLineNumber, crashFunction, crashExpressionStr);
		
		//TODO: Try and get this working sometime http://www.rioki.org/2017/01/09/windows_stacktrace.html
		#if 0
		if (platform.dbgHelpInitialized)
		{
			TwoPassPrint(&numChars, result, resultSize, "Stack Trace:\n");
			
			DWORD machine = IMAGE_FILE_MACHINE_I386; //TODO: Is there a way to find this out?
			CONTEXT context = {};
			context.ContextFlags = CONTEXT_FULL;
			RtlCaptureContext(&context);
			
			STACKFRAME64 frame = {};
			frame.AddrPC.Offset    = context.Eip;
			frame.AddrPC.Mode      = AddrModeFlat;
			frame.AddrFrame.Offset = context.Ebp;
			frame.AddrFrame.Mode   = AddrModeFlat;
			frame.AddrStack.Offset = context.Esp;
			frame.AddrStack.Mode   = AddrModeFlat;
			
			u32 walkDepth = 0;
			while (StackWalk(machine, platform.processHandle, GetCurrentThread(), &frame, &context, NULL, SymFunctionTableAccess, SymGetModuleBase, NULL))
			{
				DWORD64 functionAddress = frame.AddrPC.Offset;
				
				TwoPassPrint(&numChars, result, resultSize, "  [%u] { 0x%08X ", walkDepth, functionAddress);
				
				DWORD lineOffset = 0;
				IMAGEHLP_LINE lineInfo;
				lineInfo.SizeOfStruct = sizeof(IMAGEHLP_LINE);
				if (SymGetLineFromAddr(platform.processHandle, frame.AddrPC.Offset, &lineOffset, &lineInfo))
				{
					TwoPassPrint(&numChars, result, resultSize, "%s:%u ", lineInfo.FileName, lineInfo.LineNumber);
				}
				
				char symbolBuffer[sizeof(IMAGEHLP_SYMBOL) + 255];
				PIMAGEHLP_SYMBOL symbolPntr = (PIMAGEHLP_SYMBOL)symbolBuffer;
				symbolPntr->SizeOfStruct = sizeof(IMAGEHLP_SYMBOL) + 255;
				symbolPntr->MaxNameLength = 254;
				if (SymGetSymFromAddr(platform.processHandle, frame.AddrPC.Offset, NULL, symbolPntr))
				{
					symbolPntr->Name[255-1] = '\0'; //just to make sure it's null-terminated
					TwoPassPrint(&numChars, result, resultSize, "%s ", symbolPntr->Name);
				}
				
				DWORD moduleBase = SymGetModuleBase(platform.processHandle, frame.AddrPC.Offset);
				char moduleNameBuffer[MAX_PATH];
				if (moduleBase && GetModuleFileNameA((HINSTANCE)moduleBase, moduleNameBuffer, ArrayCount(moduleNameBuffer)))
				{
					moduleNameBuffer[ArrayCount(moduleNameBuffer)-1] = '\0';//just to make sure it's null-terminated
					TwoPassPrint(&numChars, result, resultSize, "%s ", &moduleNameBuffer[0]);
				}
				
				TwoPassPrint(&numChars, result, resultSize, "}\n");
				walkDepth++;
			}
		}
		#endif
		
		//TODO: Can we retrieve any of the debug output
		
		if (pass == 0)
		{
			if (numChars == 0) { return nullptr; }
			resultSize = numChars;
			result = (char*)malloc(numChars+1);
			if (result == nullptr) { return nullptr; }
		}
		else
		{
			if (numChars != resultSize) { free(result); return nullptr; }
			result[resultSize] = '\0';
		}
	}
	if (resultSizeOut != nullptr) { *resultSizeOut = resultSize; }
	return result;
}

#if PLAT_DEBUG_OUTPUT_ENABLED
void _Win32_PushDebugChars(const char* fileName, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, const char* strPntr, u32 strLength)
{
	Assert_(strPntr != nullptr || strLength == 0);
	if (strLength == 0) { return; }
	
	if (!Win32_LockMutex(&platform.debugOutputMutex, INFINITE)) { return; }
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		char newChar = strPntr[cIndex];
		if (platform.lineBuffer.length == 0)
		{
			platform.workingLine.level = dbgLevel;
			platform.workingLine.lineNumber = lineNumber;
			
			platform.workingLine.functionNameLength = MyStrLength32(functionStr);
			platform.workingLine.functionNamePntr = DynArrayAddRange(&platform.lineBuffer, char, platform.workingLine.functionNameLength+1);
			Assert_(platform.workingLine.functionNamePntr != nullptr);
			MyMemCopy(platform.workingLine.functionNamePntr, functionStr, platform.workingLine.functionNameLength);
			platform.workingLine.functionNamePntr[platform.workingLine.functionNameLength] = '\0';
			
			platform.workingLine.fileNameLength = MyStrLength32(fileName);
			platform.workingLine.fileNamePntr = DynArrayAddRange(&platform.lineBuffer, char, platform.workingLine.fileNameLength+1);
			Assert_(platform.workingLine.fileNamePntr != nullptr);
			MyMemCopy(platform.workingLine.fileNamePntr, fileName, platform.workingLine.fileNameLength);
			platform.workingLine.fileNamePntr[platform.workingLine.fileNameLength] = '\0';
			
			platform.workingLine.lineLength = 0;
			platform.workingLine.linePntr = DynArrayAddRange(&platform.lineBuffer, char, 1);
			Assert_(platform.workingLine.linePntr != nullptr);
			platform.workingLine.linePntr[platform.workingLine.lineLength] = '\0';
		}
		
		if (newChar == '\n')
		{
			PlatDebugLine_t* newLine = DynArrayAdd(&platform.debugLines, PlatDebugLine_t);
			Assert_(newLine != nullptr);
			ClearPointer(newLine);
			newLine->level = platform.workingLine.level;
			newLine->lineNumber = platform.workingLine.lineNumber;
			u32 charSpaceSize = platform.workingLine.functionNameLength+1 + platform.workingLine.fileNameLength+1 + platform.workingLine.lineLength+1;
			char* charSpace = PushArray(&platform.mainHeap, char, charSpaceSize);
			Assert_(charSpace != nullptr);
			
			newLine->functionNameLength = platform.workingLine.functionNameLength;
			newLine->functionNamePntr = &charSpace[0];
			MyMemCopy(newLine->functionNamePntr, platform.workingLine.functionNamePntr, newLine->functionNameLength+1);
			
			newLine->fileNameLength = platform.workingLine.fileNameLength;
			newLine->fileNamePntr = &charSpace[newLine->functionNameLength+1];
			MyMemCopy(newLine->fileNamePntr, platform.workingLine.fileNamePntr, newLine->fileNameLength+1);
			
			newLine->lineLength = platform.workingLine.lineLength;
			newLine->linePntr = &charSpace[newLine->functionNameLength+1 + newLine->fileNameLength+1];
			MyMemCopy(newLine->linePntr, platform.workingLine.linePntr, newLine->lineLength+1);
			
			DynArrayClear(&platform.lineBuffer);
		}
		else
		{
			DynArrayAdd(&platform.lineBuffer, char);
			Assert_(platform.lineBuffer.length >= 2);
			
			char* newCharSpace = DynArrayGet(&platform.lineBuffer, char, platform.lineBuffer.length-2);
			Assert_(newCharSpace != nullptr);
			*newCharSpace = newChar;
			
			char* newNullTermSpace = DynArrayGet(&platform.lineBuffer, char, platform.lineBuffer.length-1);
			Assert_(newNullTermSpace != nullptr);
			*newNullTermSpace = '\0';
			
			platform.workingLine.lineLength++;
			
			//we have to reaquire our pointers in case the dynamic array reallocated
			platform.workingLine.functionNamePntr = DynArrayGet(&platform.lineBuffer, char, 0);
			platform.workingLine.fileNamePntr = DynArrayGet(&platform.lineBuffer, char, platform.workingLine.functionNameLength+1);
			platform.workingLine.linePntr = DynArrayGet(&platform.lineBuffer, char, platform.workingLine.functionNameLength+1 + platform.workingLine.fileNameLength+1);
		}
	}
	Win32_UnlockMutex(&platform.debugOutputMutex);
}

PlatDebugLine_t* _Win32_PopDebugLines(u32* numLinesOut)
{
	PlatDebugLine_t* result = nullptr;
	*numLinesOut = 0;
	if (platform.debugLines.length > 0)
	{
		*numLinesOut = platform.debugLines.length;
		result = PushArray(&platform.mainHeap, PlatDebugLine_t, *numLinesOut);
		Assert(result != nullptr);
		PlatDebugLine_t* newLines = DynArrayGet(&platform.debugLines, PlatDebugLine_t, 0);
		Assert(newLines != nullptr);
		MyMemCopy(result, newLines, sizeof(PlatDebugLine_t) * (*numLinesOut));
		DynArrayClear(&platform.debugLines);
	}
	return result;
}

void _Win32_DeallocatePoppedDebugLines(PlatDebugLine_t* lines, u32 numLines)
{
	if (numLines > 0)
	{
		Assert(lines != nullptr);
		for (u32 lIndex = 0; lIndex < numLines; lIndex++)
		{
			PlatDebugLine_t* linePntr = &lines[lIndex];
			ArenaPop(&platform.mainHeap, linePntr->functionNamePntr);
		}
		ArenaPop(&platform.mainHeap, lines);
	}
}

//These are the internal versions which gets routed to the application so it can know about and display platform layer debug output
void _Win32_DebugOutput(u8 flags, const char* filePath, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, bool newLine, const char* message)
{
	//TODO: Loop over the contents and do prefixes after line-breaks
	OutputDebugStringA(message);
	if (newLine) { OutputDebugStringA("\n"); }
	
	printf(message);
	if (newLine) { printf("\n"); }
	
	_Win32_PushDebugChars(filePath, lineNumber, functionStr, dbgLevel, message, MyStrLength32(message));
	if (newLine) { _Win32_PushDebugChars(filePath, lineNumber, functionStr, dbgLevel, "\n", 1); }
}
void _Win32_DebugPrint(u8 flags, const char* filePath, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, bool newLine, const char* formatString, ...)
{
	MemoryArena_t* tempArena = GetThreadTempArena();
	if (tempArena == nullptr) { _Win32_DebugOutput(flags, filePath, lineNumber, functionStr, dbgLevel, newLine, "[NO_TEMP_ARENA_TO_PRINT]"); return; }
	ArenaPushMark(tempArena);
	ArenaPrintVa(tempArena, printBuffer, printResult, formatString);
	if (printResult >= 0)
	{
		_Win32_DebugOutput(flags, filePath, lineNumber, functionStr, dbgLevel, newLine, printBuffer);
	}
	else
	{
		_Win32_DebugOutput(flags, filePath, lineNumber, functionStr, dbgLevel, newLine, "[DEBUG_OUTPUT_OVERFLOW]");
	}
	ArenaPopMark(tempArena);
}
void _Win32_DebugPrintLocal(void* bufferPntr, u32 bufferSize, u8 flags, const char* filePath, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, bool newLine, const char* formatString, ...)
{
	va_list args;
	va_start(args, formatString);
	i32 printResult = vsnprintf((char*)bufferPntr, bufferSize, formatString, args);
	va_end(args);
	((char*)bufferPntr)[bufferSize-1] = '\0';
	if (printResult >= 0)
	{
		va_start(args, formatString);
		_Win32_DebugOutput(flags, filePath, lineNumber, functionStr, dbgLevel, newLine, (const char*)bufferPntr);
		va_end(args);
	}
	else
	{
		_Win32_DebugOutput(flags, filePath, lineNumber, functionStr, dbgLevel, newLine, formatString);
	}
}

// +==============================+
// |      Win32_DebugOutput       |
// +==============================+
// void DebugOutput(const char* filePath, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, bool newLine, const char* message)
DebugOutput_DEFINITION(Win32_DebugOutput)
{
	//TODO: Loop over the contents and do prefixes after line-breaks?
	OutputDebugStringA(message);
	if (newLine) { OutputDebugStringA("\n"); }
	
	//TODO: Do we have to use printf here? Can we do something that doesn't have to handle formatting and sends data to stdout
	printf(message);
	if (newLine) { printf("\n"); }
}

// +==============================+
// |      Win32_DebugPrintVa      |
// +==============================+
// void DebugPrintVa(const char* filePath, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, bool newLine, const char* formatString, va_list args, u32 printSize)
DebugPrintVa_DEFINITION(Win32_DebugPrintVa)
{
	if (printSize > 0)
	{
		char* printSpace = (char*)MyMalloc(printSize+1);
		Assert_(printSpace != nullptr);
		i32 printResult = vsnprintf(printSpace, printSize+1, formatString, args);
		Assert_(printResult == (i32)printSize);
		printSpace[printSize] = '\0';
		Win32_DebugOutput(filePath, lineNumber, functionStr, dbgLevel, newLine, printSpace);
		MyFree(printSpace);
	}
	else
	{
		Win32_DebugOutput(filePath, lineNumber, functionStr, dbgLevel, newLine, "");
	}
}

// +==============================+
// |       Win32_DebugPrint       |
// +==============================+
// void DebugPrint(const char* filePath, u32 lineNumber, const char* functionStr, DbgLevel_t dbgLevel, bool newLine, const char* formatString, ...)
DebugPrint_DEFINITION(Win32_DebugPrint)
{
	Assert_(formatString != nullptr);
	va_list args;
	va_start(args, formatString);
	i32 printResult = vsnprintf(nullptr, 0, formatString, args);
	va_end(args);
	if (printResult >= 0)
	{
		va_start(args, formatString);
		Win32_DebugPrintVa(filePath, lineNumber, functionStr, dbgLevel, newLine, formatString, args, (u32)printResult);
		va_end(args);
	}
	else
	{
		Win32_DebugOutput(filePath, lineNumber, functionStr, dbgLevel, newLine, formatString);
	}
}

// +==============================+
// |     Win32_ShowMessageBox     |
// +==============================+
// void ShowMessageBox(bool isAssertion, const char* titleStr, const char* messageStr)
ShowMessageBox_DEFINITION(Win32_ShowMessageBox)
{
	MessageBoxA(NULL, messageStr, titleStr, MB_OK);
}

#define LocalPrintAt(buffer, dbgLevel, formatString, ...)     _Win32_DebugPrintLocal(buffer, sizeof(buffer), 0x00, __FILE__, __LINE__, __func__, DbgLevel_Debug, false, formatString, ##__VA_ARGS__)
#define LocalPrintLineAt(buffer, dbgLevel, formatString, ...) _Win32_DebugPrintLocal(buffer, sizeof(buffer), 0x00, __FILE__, __LINE__, __func__, DbgLevel_Debug, true, formatString, ##__VA_ARGS__)

#define WriteAt(dbgLevel, message)               _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, dbgLevel, false, message)
#define WriteLineAt(dbgLevel, message)           _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, dbgLevel, true,  message)
#define PrintAt(dbgLevel, formatString, ...)     _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, dbgLevel, false, formatString, ##__VA_ARGS__)
#define PrintLineAt(dbgLevel, formatString, ...) _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, dbgLevel, true,  formatString, ##__VA_ARGS__)

#define Write_D(message)                         _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Debug, false, message)
#define WriteLine_D(message)                     _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  message)
#define Print_D(formatString, ...)               _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Debug, false, formatString, ##__VA_ARGS__)
#define PrintLine_D(formatString, ...)           _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Debug, true,  formatString, ##__VA_ARGS__)

#define Write_R(message)                         _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Regular, false, message)
#define WriteLine_R(message)                     _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  message)
#define Print_R(formatString, ...)               _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Regular, false, formatString, ##__VA_ARGS__)
#define PrintLine_R(formatString, ...)           _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Regular, true,  formatString, ##__VA_ARGS__)

#define Write_I(message)                         _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Info, false, message)
#define WriteLine_I(message)                     _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Info, true,  message)
#define Print_I(formatString, ...)               _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Info, false, formatString, ##__VA_ARGS__)
#define PrintLine_I(formatString, ...)           _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Info, true,  formatString, ##__VA_ARGS__)

#define Write_N(message)                         _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Notify, false, message)
#define WriteLine_N(message)                     _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  message)
#define Print_N(formatString, ...)               _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Notify, false, formatString, ##__VA_ARGS__)
#define PrintLine_N(formatString, ...)           _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Notify, true,  formatString, ##__VA_ARGS__)

#define Write_O(message)                         _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Other, false, message)
#define WriteLine_O(message)                     _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Other, true,  message)
#define Print_O(formatString, ...)               _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Other, false, formatString, ##__VA_ARGS__)
#define PrintLine_O(formatString, ...)           _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Other, true,  formatString, ##__VA_ARGS__)

#define Write_W(message)                         _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Warning, false, message)
#define WriteLine_W(message)                     _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  message)
#define Print_W(formatString, ...)               _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Warning, false, formatString, ##__VA_ARGS__)
#define PrintLine_W(formatString, ...)           _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Warning, true,  formatString, ##__VA_ARGS__)

#define Write_E(message)                         _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Error, false, message)
#define WriteLine_E(message)                     _Win32_DebugOutput(0x00, __FILE__, __LINE__, __func__, DbgLevel_Error, true,  message)
#define Print_E(formatString, ...)               _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Error, false, formatString, ##__VA_ARGS__)
#define PrintLine_E(formatString, ...)           _Win32_DebugPrint (0x00, __FILE__, __LINE__, __func__, DbgLevel_Error, true,  formatString, ##__VA_ARGS__)

#else //!PLAT_DEBUG_OUTPUT_ENABLED

void Win32_DebugOutput(const char* functionName, const char* fileName, u32 lineNumber, DbgLevel_t dbgLevel, bool newLine, const char* message) {}
void Win32_DebugPrintVa(const char* functionName, const char* fileName, u32 lineNumber, DbgLevel_t dbgLevel, bool newLine, const char* formatString, va_list args) {}
void Win32_DebugPrint(const char* functionName, const char* fileName, u32 lineNumber, DbgLevel_t dbgLevel, bool newLine, const char* formatString, ...) {}

#define WriteAt(dbgLevel, message)
#define WriteLineAt(dbgLevel, message)
#define PrintAt(dbgLevel, formatString, ...)
#define PrintLineAt(dbgLevel, formatString, ...)

#define Write_D(message)
#define WriteLine_D(message)
#define Print_D(formatString, ...)
#define PrintLine_D(formatString, ...)

#define Write_R(message)
#define WriteLine_R(message)
#define Print_R(formatString, ...)
#define PrintLine_R(formatString, ...)

#define Write_I(message)
#define WriteLine_I(message)
#define Print_I(formatString, ...)
#define PrintLine_I(formatString, ...)

#define Write_N(message)
#define WriteLine_N(message)
#define Print_N(formatString, ...)
#define PrintLine_N(formatString, ...)

#define Write_O(message)
#define WriteLine_O(message)
#define Print_O(formatString, ...)
#define PrintLine_O(formatString, ...)

#define Write_W(message)
#define WriteLine_W(message)
#define Print_W(formatString, ...)
#define PrintLine_W(formatString, ...)

#define Write_E(message)
#define WriteLine_E(message)
#define Print_E(formatString, ...)
#define PrintLine_E(formatString, ...)

#endif
