/*
File:   win32_app_loading.h
Author: Taylor Robbins
Date:   07\01\2019
*/

#ifndef _WIN_32_APP_LOADING_H
#define _WIN_32_APP_LOADING_H

struct LoadedApp_t
{
	bool isValid;
	Version_t version;
	HMODULE module;
	FILETIME lastWriteTime;
	
	AppGetVersion_f*        AppGetVersion;
	AppGetStartupOptions_f* AppGetStartupOptions;
	AppReloaded_f*          AppReloaded;
	AppInitialize_f*        AppInitialize;
	AppTaskFinished_f*      AppTaskFinished;
	AppUpdate_f*            AppUpdate;
	AppPerformTask_f*       AppPerformTask;
	AppClosing_f*           AppClosing;
};

struct DllWatchingInfo_t
{
	Mutex_t mutex;
	bool appChanged;
	char* dllFilePath;
	FILETIME lastWriteTime;
};

#endif //  _WIN_32_APP_LOADING_H
