/*
File:   win32_irc.cpp
Author: Taylor Robbins
Date:   10\23\2020
Description: 
	** Holds functions that allow the application to open and read and write to and from a socket (encrypted or not) that is using the IRC protocol
*/

const char* GetWsaErrorCodeStr(int errorCode)
{
	switch (errorCode)
	{
		case WSAEINTR:                   return "WSAEINTR";
		case WSAEBADF:                   return "WSAEBADF";
		case WSAEACCES:                  return "WSAEACCES";
		case WSAEFAULT:                  return "WSAEFAULT";
		case WSAEINVAL:                  return "WSAEINVAL";
		case WSAEMFILE:                  return "WSAEMFILE";
		case WSAEWOULDBLOCK:             return "WSAEWOULDBLOCK";
		case WSAEINPROGRESS:             return "WSAEINPROGRESS";
		case WSAEALREADY:                return "WSAEALREADY";
		case WSAENOTSOCK:                return "WSAENOTSOCK";
		case WSAEDESTADDRREQ:            return "WSAEDESTADDRREQ";
		case WSAEMSGSIZE:                return "WSAEMSGSIZE";
		case WSAEPROTOTYPE:              return "WSAEPROTOTYPE";
		case WSAENOPROTOOPT:             return "WSAENOPROTOOPT";
		case WSAEPROTONOSUPPORT:         return "WSAEPROTONOSUPPORT";
		case WSAESOCKTNOSUPPORT:         return "WSAESOCKTNOSUPPORT";
		case WSAEOPNOTSUPP:              return "WSAEOPNOTSUPP";
		case WSAEPFNOSUPPORT:            return "WSAEPFNOSUPPORT";
		case WSAEAFNOSUPPORT:            return "WSAEAFNOSUPPORT";
		case WSAEADDRINUSE:              return "WSAEADDRINUSE";
		case WSAEADDRNOTAVAIL:           return "WSAEADDRNOTAVAIL";
		case WSAENETDOWN:                return "WSAENETDOWN";
		case WSAENETUNREACH:             return "WSAENETUNREACH";
		case WSAENETRESET:               return "WSAENETRESET";
		case WSAECONNABORTED:            return "WSAECONNABORTED";
		case WSAECONNRESET:              return "WSAECONNRESET";
		case WSAENOBUFS:                 return "WSAENOBUFS";
		case WSAEISCONN:                 return "WSAEISCONN";
		case WSAENOTCONN:                return "WSAENOTCONN";
		case WSAESHUTDOWN:               return "WSAESHUTDOWN";
		case WSAETOOMANYREFS:            return "WSAETOOMANYREFS";
		case WSAETIMEDOUT:               return "WSAETIMEDOUT";
		case WSAECONNREFUSED:            return "WSAECONNREFUSED";
		case WSAELOOP:                   return "WSAELOOP";
		case WSAENAMETOOLONG:            return "WSAENAMETOOLONG";
		case WSAEHOSTDOWN:               return "WSAEHOSTDOWN";
		case WSAEHOSTUNREACH:            return "WSAEHOSTUNREACH";
		case WSAENOTEMPTY:               return "WSAENOTEMPTY";
		case WSAEPROCLIM:                return "WSAEPROCLIM";
		case WSAEUSERS:                  return "WSAEUSERS";
		case WSAEDQUOT:                  return "WSAEDQUOT";
		case WSAESTALE:                  return "WSAESTALE";
		case WSAEREMOTE:                 return "WSAEREMOTE";
		case WSASYSNOTREADY:             return "WSASYSNOTREADY";
		case WSAVERNOTSUPPORTED:         return "WSAVERNOTSUPPORTED";
		case WSANOTINITIALISED:          return "WSANOTINITIALISED";
		case WSAEDISCON:                 return "WSAEDISCON";
		case WSAENOMORE:                 return "WSAENOMORE";
		case WSAECANCELLED:              return "WSAECANCELLED";
		case WSAEINVALIDPROCTABLE:       return "WSAEINVALIDPROCTABLE";
		case WSAEINVALIDPROVIDER:        return "WSAEINVALIDPROVIDER";
		case WSAEPROVIDERFAILEDINIT:     return "WSAEPROVIDERFAILEDINIT";
		case WSASYSCALLFAILURE:          return "WSASYSCALLFAILURE";
		case WSASERVICE_NOT_FOUND:       return "WSASERVICE_NOT_FOUND";
		case WSATYPE_NOT_FOUND:          return "WSATYPE_NOT_FOUND";
		case WSAEREFUSED:                return "WSAEREFUSED";
		case WSAHOST_NOT_FOUND:          return "WSAHOST_NOT_FOUND";
		case WSATRY_AGAIN:               return "WSATRY_AGAIN";
		case WSANO_RECOVERY:             return "WSANO_RECOVERY";
		case WSANO_DATA:                 return "WSANO_DATA";
		case WSA_INVALID_HANDLE:         return "WSA_INVALID_HANDLE";
		case WSA_NOT_ENOUGH_MEMORY:      return "WSA_NOT_ENOUGH_MEMORY";
		case WSA_INVALID_PARAMETER:      return "WSA_INVALID_PARAMETER";
		case WSA_OPERATION_ABORTED:      return "WSA_OPERATION_ABORTED";
		case WSA_IO_INCOMPLETE:          return "WSA_IO_INCOMPLETE";
		case WSA_IO_PENDING:             return "WSA_IO_PENDING";
		case WSA_E_NO_MORE:              return "WSA_E_NO_MORE";
		case WSA_E_CANCELLED:            return "WSA_E_CANCELLED";
		case WSA_QOS_RECEIVERS:          return "WSA_QOS_RECEIVERS";
		case WSA_QOS_SENDERS:            return "WSA_QOS_SENDERS";
		case WSA_QOS_NO_SENDERS:         return "WSA_QOS_NO_SENDERS";
		case WSA_QOS_NO_RECEIVERS:       return "WSA_QOS_NO_RECEIVERS";
		case WSA_QOS_REQUEST_CONFIRMED:  return "WSA_QOS_REQUEST_CONFIRMED";
		case WSA_QOS_ADMISSION_FAILURE:  return "WSA_QOS_ADMISSION_FAILURE";
		case WSA_QOS_POLICY_FAILURE:     return "WSA_QOS_POLICY_FAILURE";
		case WSA_QOS_BAD_STYLE:          return "WSA_QOS_BAD_STYLE";
		case WSA_QOS_BAD_OBJECT:         return "WSA_QOS_BAD_OBJECT";
		case WSA_QOS_TRAFFIC_CTRL_ERROR: return "WSA_QOS_TRAFFIC_CTRL_ERROR";
		case WSA_QOS_GENERIC_ERROR:      return "WSA_QOS_GENERIC_ERROR";
		case WSA_QOS_ESERVICETYPE:       return "WSA_QOS_ESERVICETYPE";
		case WSA_QOS_EFLOWSPEC:          return "WSA_QOS_EFLOWSPEC";
		case WSA_QOS_EPROVSPECBUF:       return "WSA_QOS_EPROVSPECBUF";
		case WSA_QOS_EFILTERSTYLE:       return "WSA_QOS_EFILTERSTYLE";
		case WSA_QOS_EFILTERTYPE:        return "WSA_QOS_EFILTERTYPE";
		case WSA_QOS_EFILTERCOUNT:       return "WSA_QOS_EFILTERCOUNT";
		case WSA_QOS_EOBJLENGTH:         return "WSA_QOS_EOBJLENGTH";
		case WSA_QOS_EFLOWCOUNT:         return "WSA_QOS_EFLOWCOUNT";
		case WSA_QOS_EUNKOWNPSOBJ:       return "WSA_QOS_EUNKOWNPSOBJ";
		case WSA_QOS_EPOLICYOBJ:         return "WSA_QOS_EPOLICYOBJ";
		case WSA_QOS_EFLOWDESC:          return "WSA_QOS_EFLOWDESC";
		case WSA_QOS_EPSFLOWSPEC:        return "WSA_QOS_EPSFLOWSPEC";
		case WSA_QOS_EPSFILTERSPEC:      return "WSA_QOS_EPSFILTERSPEC";
		case WSA_QOS_ESDMODEOBJ:         return "WSA_QOS_ESDMODEOBJ";
		case WSA_QOS_ESHAPERATEOBJ:      return "WSA_QOS_ESHAPERATEOBJ";
		case WSA_QOS_RESERVED_PETYPE:    return "WSA_QOS_RESERVED_PETYPE";
		default: return "Unknown";
	}
}

void Win32_InitializeWinSock()
{
	WSADATA winsockData;
	int startupResult = WSAStartup(MAKEWORD(2, 2), &winsockData);
	if (startupResult != 0)
	{
		PrintLine_E("WSAStartup Result: %d", startupResult);
		Win32_ExitOnError("Failed to initialize WinSock 2");
	}
	platform.winsockInitialized = true;
}

void _Win32_DestroyIrcConnection(IrcConnection_t* connection)
{
	NotNull(connection);
	if (connection->isValid)
	{
		Assert(connection->socket != INVALID_SOCKET);
		int closeResult = closesocket(connection->socket);
		Assert(closeResult == 0);
		connection->socket = INVALID_SOCKET;
		connection->isValid = false;
	}
	if (connection->url != nullptr)
	{
		NotNull(connection->allocArena);
		ArenaPop(connection->allocArena, connection->url);
		connection->url = nullptr;
	}
	DeleteFifo(&connection->readFifo);
	DeleteFifo(&connection->writeFifo);
	ClearPointer(connection);
}

// +==============================+
// |   Win32_OpenIrcConnection    |
// +==============================+
// bool OpenIrcConnection(const char* url, u32 port, IrcConnection_t* connectionOut, MemoryArena_t* memArena, u32 writeFifoSize)
OpenIrcConnection_DEFINITION(Win32_OpenIrcConnection)
{
	NotNull(url);
	NotNull(connectionOut);
	NotNull(memArena);
	
	ClearPointer(connectionOut);
	connectionOut->port = port;
	connectionOut->socket = INVALID_SOCKET;
	
	char* portNumStr = TempPrint("%u", port);
	NotNull(portNumStr);
	
	addrinfo dnsHints = {};
	dnsHints.ai_family = AF_UNSPEC; //AF_INET6, or AF_INET
	dnsHints.ai_socktype = SOCK_STREAM;
	dnsHints.ai_protocol = IPPROTO_TCP;
	addrinfo* dnsResults = nullptr;
	
	int getAddrInfoResult = getaddrinfo(url, portNumStr, &dnsHints, &dnsResults);
	if (getAddrInfoResult != 0)
	{
		PrintLine_E("getaddrinfo error: %s", GetWsaErrorCodeStr(WSAGetLastError()));
		return false;
	}
	if (dnsResults == nullptr)
	{
		WriteLine_E("getaddrinfo returned no results");
		return false;
	}
	connectionOut->state = IrcConnectionState_DnsResolve;
	
	if (dnsResults->ai_family == AF_INET6)
	{
		connectionOut->isIpv6 = true;
		connectionOut->ipAddress6.value0 = ntohs(*(u16*)(&dnsResults->ai_addr->sa_data[0]));
		connectionOut->ipAddress6.value1 = ntohs(*(u16*)(&dnsResults->ai_addr->sa_data[2]));
		connectionOut->ipAddress6.value2 = ntohs(*(u16*)(&dnsResults->ai_addr->sa_data[4]));
		connectionOut->ipAddress6.value3 = ntohs(*(u16*)(&dnsResults->ai_addr->sa_data[6]));
		PrintLine_D("Ipv6 Address: %04x:%04x:%04x:%04x", connectionOut->ipAddress6.value0, connectionOut->ipAddress6.value1, connectionOut->ipAddress6.value2, connectionOut->ipAddress6.value3);
	}
	else if (dnsResults->ai_family == AF_INET)
	{
		connectionOut->isIpv6 = false;
		connectionOut->ipAddress4.value0 = dnsResults->ai_addr->sa_data[0];
		connectionOut->ipAddress4.value1 = dnsResults->ai_addr->sa_data[1];
		connectionOut->ipAddress4.value2 = dnsResults->ai_addr->sa_data[2];
		connectionOut->ipAddress4.value3 = dnsResults->ai_addr->sa_data[3];
		PrintLine_D("Ipv4 Address: %u.%u.%u.%u", connectionOut->ipAddress4.value0, connectionOut->ipAddress4.value1, connectionOut->ipAddress4.value2, connectionOut->ipAddress4.value3);
	}
	else { Assert(false); }
	
	
	connectionOut->socket = socket(dnsResults->ai_family, dnsResults->ai_socktype, dnsResults->ai_protocol);
	if (connectionOut->socket == INVALID_SOCKET)
	{
		PrintLine_E("Socket open error: %s", GetWsaErrorCodeStr(WSAGetLastError()));
		freeaddrinfo(dnsResults);
		return false;
	}
	
	u_long socketBlockingMode = 1; //non-blocking
	ioctlsocket(connectionOut->socket, FIONBIO, &socketBlockingMode);
	
	int connectResult = connect(connectionOut->socket, dnsResults->ai_addr, (int)dnsResults->ai_addrlen);
	if (connectResult != 0)
	{
		int errorCode = WSAGetLastError();
		if (errorCode != WSAEWOULDBLOCK)
		{
			PrintLine_E("Socket connection failed to start");
			closesocket(connectionOut->socket);
			connectionOut->socket = INVALID_SOCKET;
			freeaddrinfo(dnsResults);
			return false;
		}
	}
	else
	{
		PrintLine_E("Socket connection immediately succeeded despite non-blocking mode enabled");
		Assert(false);
	}
	
	connectionOut->state = IrcConnectionState_Connecting;
	connectionOut->isValid = true;
	connectionOut->allocArena = memArena;
	connectionOut->url = ArenaNtString(memArena, url);
	CreateFifo(&connectionOut->readFifo, memArena, 1024, FifoType_Dynamic);
	CreateFifo(&connectionOut->writeFifo, memArena, writeFifoSize, FifoType_StaticDrop);
	NotNull(connectionOut->url);
	
	return true;
}

// +==============================+
// |  Win32_UpdateIrcConnection   |
// +==============================+
// void UpdateIrcConnection(IrcConnection_t* connection)
UpdateIrcConnection_DEFINITION(Win32_UpdateIrcConnection)
{
	NotNull(connection);
	if (connection->isValid)
	{
		if (connection->state == IrcConnectionState_Connecting)
		{
			timeval timeout = {};
			timeout.tv_sec = 0;
			timeout.tv_usec = 0;
			fd_set socketsSet = {};
			socketsSet.fd_count = 1;
			socketsSet.fd_array[0] = connection->socket;
			
			int selectResult = select(1, nullptr, &socketsSet, nullptr, &timeout);
			if (selectResult == SOCKET_ERROR)
			{
				int errorCode = WSAGetLastError();
				PrintLine_E("Socket select error: %s:%u Error %s", connection->url, connection->port, GetWsaErrorCodeStr(errorCode));
				connection->state = IrcConnectionState_Error;
				return;
			}
			else if (selectResult > 0)
			{
				// PrintLine_I("selectResult = %d", selectResult);
				connection->state = IrcConnectionState_Connected;
			}
			
			fd_set socketsSet2 = {};
			socketsSet2.fd_count = 1;
			socketsSet2.fd_array[0] = connection->socket;
			selectResult = select(1, nullptr, nullptr, &socketsSet2, &timeout);
			if (selectResult == SOCKET_ERROR)
			{
				int errorCode = WSAGetLastError();
				PrintLine_E("Socket select error 2: %s:%u Error %s", connection->url, connection->port, GetWsaErrorCodeStr(errorCode));
				connection->state = IrcConnectionState_Error;
				return;
			}
			else if (selectResult > 0)
			{
				int errorCode = 0;
				int errorCodeSize = sizeof(errorCode);
				int getSockOptResult = getsockopt(connection->socket, SOL_SOCKET, SO_ERROR, (char*)&errorCode, &errorCodeSize);
				if (getSockOptResult != 0) { errorCode = WSAGetLastError(); }
				PrintLine_E("Socket connection failed: %s:%u Error %s", connection->url, connection->port, GetWsaErrorCodeStr(errorCode));
				connection->state = IrcConnectionState_Error;
				return;
			}
			
		}
		else if (connection->state == IrcConnectionState_Connected)
		{
			bool readMoreData = false;
			u8 readWriteBuffer[512];
			do
			{
				readMoreData = false;
				int recvResult = recv(connection->socket, (char*)&readWriteBuffer[0], sizeof(readWriteBuffer), 0);
				if (recvResult == SOCKET_ERROR)
				{
					int errorCode = WSAGetLastError();
					if (errorCode == WSAEWOULDBLOCK)
					{
						//do nothing, this is fine
					}
					else
					{
						PrintLine_E("Socket error while reading: %s", GetWsaErrorCodeStr(errorCode));
						connection->state = IrcConnectionState_Error;
						return;
					}
				}
				else if (recvResult > 0)
				{
					Assert(recvResult <= sizeof(readWriteBuffer));
					bool pushSucceeded = FifoPushArray(&connection->readFifo, &readWriteBuffer[0], (u32)recvResult);
					Assert(pushSucceeded);
					if (recvResult == sizeof(readWriteBuffer)) { readMoreData = true; }
				}
			} while(readMoreData);
			
			if (FifoLength(&connection->writeFifo) > 0)
			{
				u32 numBytesToSend = FifoLength(&connection->writeFifo);
				if (numBytesToSend >= sizeof(readWriteBuffer)) { numBytesToSend = sizeof(readWriteBuffer); }
				bool getResult = FifoGetArray(&connection->writeFifo, &readWriteBuffer[0], numBytesToSend);
				Assert(getResult == true);
				
				int sendResult = send(connection->socket, (const char*)&readWriteBuffer[0], numBytesToSend, 0);
				if (sendResult == SOCKET_ERROR)
				{
					int errorCode = WSAGetLastError();
					if (errorCode == WSAEWOULDBLOCK)
					{
						//do nothing, this is fine
						PrintLine_D("No space to send %u bytes", numBytesToSend);
					}
					else
					{
						PrintLine_E("Socket error while reading: %s", GetWsaErrorCodeStr(WSAGetLastError()));
						connection->state = IrcConnectionState_Error;
						return;
					}
				}
				else if (sendResult > 0)
				{
					Assert((u32)sendResult <= numBytesToSend);
					// PrintLine_D("Sent %u/%u bytes", numBytesToSend, FifoLength(&connection->writeFifo));
					for (u32 bIndex = 0; bIndex < (u32)sendResult; bIndex++) { FifoPop(&connection->writeFifo); }
				}
			}
		}
	}
}

// +==============================+
// |   Win32_CloseIrcConnection   |
// +==============================+
// void CloseIrcConnection(IrcConnection_t* connection)
CloseIrcConnection_DEFINITION(Win32_CloseIrcConnection)
{
	_Win32_DestroyIrcConnection(connection);
}