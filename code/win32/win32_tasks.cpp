/*
File:   win32_tasks.cpp
Author: Taylor Robbins
Date:   01\26\2020
Description: 
	** This file handles keeping a bunch of threads alive and ready to do various tasks for the application as well as the platform layer
*/

WIN32_THREAD_FUNCTION(WaitForTaskFunction, userPntr);

void _Win32_StartTaskThreads()
{
	Assert(platform.taskThreads.items == nullptr);
	Assert(IsThisMainThread());
	
	u32 numThreadsToSpinUp = 4; //TODO: This should be dynamic based off the CPU core count maybe?
	if (numThreadsToSpinUp > MAX_TASK_THREADS) { numThreadsToSpinUp = MAX_TASK_THREADS; }
	
	_Win32_CreateSemaphore(&platform.taskThreadsSemaphore, 0, (LONG)numThreadsToSpinUp);
	
	CreateDynamicArray(&platform.taskThreads, &platform.mainHeap, sizeof(Thread_t*), numThreadsToSpinUp, numThreadsToSpinUp);
	CreateDynamicArray(&platform.taskThreadInfos, &platform.mainHeap, sizeof(TaskThreadInfo_t*), numThreadsToSpinUp, numThreadsToSpinUp);
	for (u32 tIndex = 0; tIndex < numThreadsToSpinUp; tIndex++)
	{
		TaskThreadInfo_t* newThreadInfo = PushStruct(&platform.mainHeap, TaskThreadInfo_t);
		Assert(newThreadInfo != nullptr);
		ClearPointer(newThreadInfo);
		newThreadInfo->index = tIndex;
		newThreadInfo->workingMemSize = TASK_THREAD_TEMP_MEM_SIZE;
		newThreadInfo->workingMemPntr = malloc(newThreadInfo->workingMemSize);
		Assert(newThreadInfo->workingMemPntr != nullptr);
		InitializeMemoryArenaTemp(&newThreadInfo->tempArena, newThreadInfo->workingMemPntr, newThreadInfo->workingMemSize, TASK_THREAD_TEMP_MAX_NUM_MARKS);
		
		Thread_t* newThread = _Win32_CreateThread(WaitForTaskFunction, newThreadInfo);
		if (newThread == nullptr) { PrintLine_E("Failed to spin up task thread %u/%u. Only %u task threads available", tIndex+1, numThreadsToSpinUp, platform.taskThreads.length); break; }
		
		Thread_t** newThreadSpace = DynArrayAdd(&platform.taskThreads, Thread_t*);
		Assert(newThreadSpace != nullptr);
		*newThreadSpace = newThread;
		
		TaskThreadInfo_t** newThreadInfoSpace = DynArrayAdd(&platform.taskThreadInfos, TaskThreadInfo_t*);
		Assert(newThreadInfoSpace != nullptr);
		*newThreadInfoSpace = newThreadInfo;
	}
	
	ClearArray(platform.taskBuffer);
	platform.nextTaskId = 1;
}

//TODO: We should probably have some sort of maximum wait time here
void _Win32_WaitForThreadedTasksToFinish()
{
	Assert(IsThisMainThread());
	for (u32 tIndex = 0; tIndex < MAX_QUEUED_TASKS; tIndex++)
	{
		ThreadedTask_t* taskPntr = &platform.taskBuffer[tIndex];
		if (taskPntr->filled && !taskPntr->finished)
		{
			PrintLine_I("Waiting for task %u to finish%s", taskPntr->id, taskPntr->started ? "" : " (Hasn't started)");
			if (!taskPntr->started)
			{
				while (!taskPntr->started) { }
				PrintLine_I("Task %u started", taskPntr->id);
			}
			while (!taskPntr->finished) { }
			PrintLine_I("Task %u finished!", taskPntr->id);
		}
	}
}

void _Win32_ClearOutFinishedTasks()
{
	Assert(IsThisMainThread());
	for (u32 tIndex = 0; tIndex < platform.taskThreads.length; tIndex++)
	{
		Thread_t** threadPntrPntr = DynArrayGet(&platform.taskThreads, Thread_t*, tIndex);
		Assert(threadPntrPntr != nullptr);
		Thread_t* threadPntr = *threadPntrPntr;
		Assert(threadPntr != nullptr);
		
		TaskThreadInfo_t** threadInfoPntrPntr = DynArrayGet(&platform.taskThreadInfos, TaskThreadInfo_t*, tIndex);
		Assert(threadInfoPntrPntr != nullptr);
		TaskThreadInfo_t* threadInfoPntr = *threadInfoPntrPntr;
		Assert(threadInfoPntr != nullptr);
		
		if (!_Win32_IsThreadAlive(threadPntr))
		{
			PrintLine_E("Task Thread %u exited! Restarting", tIndex);
			Assert(threadInfoPntr->workingMemPntr != nullptr);
			InitializeMemoryArenaTemp(&threadInfoPntr->tempArena, threadInfoPntr->workingMemPntr, threadInfoPntr->workingMemSize, TASK_THREAD_TEMP_MAX_NUM_MARKS);
			Thread_t* newThread = _Win32_CreateThread(WaitForTaskFunction, threadInfoPntr);
			if (newThread == nullptr) { WriteLine_E("Failed to restart task thread"); continue; }
			*threadPntrPntr = newThread;
		}
	}
	for (u32 tIndex = 0; tIndex < MAX_QUEUED_TASKS; tIndex++)
	{
		ThreadedTask_t* taskPntr = &platform.taskBuffer[tIndex];
		if (taskPntr->filled && taskPntr->finished)
		{
			taskPntr->endTime = Win32_GetPerfTime();
			if (taskPntr->isApplicationTask)
			{
				app.AppTaskFinished(&platform.info, &platform.appMemory, taskPntr);
			}
			taskPntr->filled = false;
		}
	}
}

void _Win32_StopTaskThreads()
{
	Assert(IsThisMainThread());
	for (u32 tIndex = 0; tIndex < platform.taskThreads.length; tIndex++)
	{
		Thread_t** threadPntrPntr = DynArrayGet(&platform.taskThreads, Thread_t*, tIndex);
		Assert(threadPntrPntr != nullptr);
		Thread_t* threadPntr = *threadPntrPntr;
		Assert(threadPntr != nullptr);
		_Win32_DestroyThread(threadPntr);
	}
	for (u32 tIndex = 0; tIndex < platform.taskThreadInfos.length; tIndex++)
	{
		TaskThreadInfo_t** threadInfoPntrPntr = DynArrayGet(&platform.taskThreadInfos, TaskThreadInfo_t*, tIndex);
		Assert(threadInfoPntrPntr != nullptr);
		TaskThreadInfo_t* threadInfoPntr = *threadInfoPntrPntr;
		Assert(threadInfoPntr != nullptr);
		if (threadInfoPntr->workingMemPntr != nullptr) { free(threadInfoPntr->workingMemPntr); }
		ArenaPop(&platform.mainHeap, threadInfoPntr);
	}
	DestroyDynamicArray(&platform.taskThreads);
	DestroyDynamicArray(&platform.taskThreadInfos);
}

bool _Win32_AreAllApplicationTasksFinished()
{
	for (u32 tIndex = 0; tIndex < MAX_QUEUED_TASKS; tIndex++)
	{
		ThreadedTask_t* taskPntr = &platform.taskBuffer[tIndex];
		if (taskPntr->isApplicationTask && !taskPntr->finished)
		{
			return false;
		}
	}
	return true;
}

void _Win32_UpdateAppInputTaskInfo(AppInput_t* appInput)
{
	Assert(appInput != nullptr);
	appInput->numAvailableTaskThreads = platform.taskThreads.length;
	appInput->numActiveThreadedTasks = 0;
	appInput->numQueuedThreadedTasks = 0;
	appInput->numTotalThreadedTasks = 0;
	for (u32 tIndex = 0; tIndex < MAX_QUEUED_TASKS; tIndex++)
	{
		ThreadedTask_t* taskPntr = &platform.taskBuffer[tIndex];
		if (taskPntr->isApplicationTask && !taskPntr->finished)
		{
			if (taskPntr->started) { appInput->numActiveThreadedTasks++; }
			else { appInput->numQueuedThreadedTasks++; }
			appInput->numTotalThreadedTasks++;
		}
	}
}

u32 _Win32_QueueThreadedTask(const ThreadedTask_t* task, bool isApplicationTask)
{
	Assert_(IsThisMainThread());
	Assert_(task != nullptr);
	PerfTime_t requestStartTime = Win32_GetPerfTime();
	ThreadedTask_t* newTask = nullptr;
	for (u32 tIndex = 0; tIndex < MAX_QUEUED_TASKS; tIndex++)
	{
		if (!platform.taskBuffer[tIndex].filled) { newTask = &platform.taskBuffer[tIndex]; break; }
	}
	if (newTask == nullptr) { return 0; }
	ClearPointer(newTask);
	newTask->id = platform.nextTaskId;
	platform.nextTaskId++;
	newTask->isApplicationTask = isApplicationTask;
	newTask->started = false;
	newTask->finished = false;
	newTask->thread = nullptr;
	newTask->startTime = requestStartTime;
	newTask->taskNumber   = task->taskNumber;
	newTask->taskNumber64 = task->taskNumber64;
	newTask->taskType     = task->taskType;
	newTask->resultCode   = task->resultCode;
	newTask->callback     = task->callback;
	newTask->taskSize1    = task->taskSize1;
	newTask->taskPntr1    = task->taskPntr1;
	newTask->taskSize2    = task->taskSize2;
	newTask->taskPntr2    = task->taskPntr2;
	newTask->resultSize1  = task->resultSize1;
	newTask->resultPntr1  = task->resultPntr1;
	newTask->resultSize2  = task->resultSize2;
	newTask->resultPntr2  = task->resultPntr2;
	ThreadingWriteBarrier();
	newTask->filled = true;
	_Win32_TriggerSemaphore(&platform.taskThreadsSemaphore);
	return newTask->id;
}

// +==============================+
// |   Win32_QueueThreadedTask    |
// +==============================+
// u32 QueueThreadedTask(const ThreadedTask_t* task)
QueueThreadedTask_DEFINITION(Win32_QueueThreadedTask)
{
	return _Win32_QueueThreadedTask(task, true);
}

// +==============================+
// |     Win32_GetThreadInfo      |
// +==============================+
// TaskThreadInfo_t* GetThreadInfo(ThreadId_t threadId)
GetThreadInfo_DEFINITION(Win32_GetThreadInfo) //pre-declared in win32_main.cpp
{
	for (u32 tIndex = 0; tIndex < platform.taskThreads.length; tIndex++)
	{
		Thread_t** taskThreadPntrPntr = DynArrayGet(&platform.taskThreads, Thread_t*, tIndex);
		Assert_(taskThreadPntrPntr != nullptr);
		Thread_t* taskThreadPntr = *taskThreadPntrPntr;
		Assert_(taskThreadPntr != nullptr);
		if (taskThreadPntr->id == threadId)
		{
			TaskThreadInfo_t** threadInfoPntrPntr = DynArrayGet(&platform.taskThreadInfos, TaskThreadInfo_t*, tIndex);
			Assert_(threadInfoPntrPntr != nullptr);
			TaskThreadInfo_t* threadInfoPntr = *threadInfoPntrPntr;
			Assert_(threadInfoPntr != nullptr);
			return threadInfoPntr;
		}
	}
	return nullptr;
}

bool IsThisMainThread() //pre-declared in win32_main.cpp
{
	ThreadId_t thisThreadId = Win32_GetThisThreadId();
	return Win32_IsMainThread(thisThreadId);
}
MemoryArena_t* GetThreadTempArena() //pre-declared in win32_main.cpp
{
	ThreadId_t thisThreadId = Win32_GetThisThreadId();
	if (Win32_IsMainThread(thisThreadId)) { return TempArena; }
	else
	{
		TaskThreadInfo_t* threadInfo = Win32_GetThreadInfo(thisThreadId);
		if (threadInfo == nullptr) { return nullptr; }
		if (threadInfo->tempArena.size == 0 || threadInfo->tempArena.base == nullptr) { return nullptr; }
		return &threadInfo->tempArena;
	}
}

// +--------------------------------------------------------------+
// |                     Task Thread Function                     |
// +--------------------------------------------------------------+
WIN32_THREAD_FUNCTION(WaitForTaskFunction, userPntr) //pre-declared above
{
	Assert_(userPntr != nullptr);
	TaskThreadInfo_t* threadInfo = (TaskThreadInfo_t*)userPntr;
	DWORD threadId = GetCurrentThreadId();
	Thread_t* thisThread = _Win32_GetThreadById(threadId);
	PrintLine_I("Thread %u(0x%X) started", threadInfo->index, threadId);
	for (;;)
	{
		ArenaPushMark(&threadInfo->tempArena);
		bool anyWorkToDo = false;
		for (u32 tIndex = 0; tIndex < MAX_QUEUED_TASKS; tIndex++)
		{
			ThreadedTask_t* task = &platform.taskBuffer[tIndex];
			if (task->filled && ThreadSafeClaimByBool(&task->started))
			{
				// PrintLine_I("Thread %u(0x%X) claims task[%u] %u", threadInfo->index, threadId, tIndex, task->id);
				threadInfo->currentTaskId = task->id;
				threadInfo->currentTaskPntr = task;
				task->thread = thisThread;
				anyWorkToDo = true;
				
				if (task->isApplicationTask)
				{
					//TODO: Keep track of how long it takes to do this task?
					app.AppPerformTask(&platform.info, &platform.appMemory, task, threadInfo);
				}
				else 
				{
					//TODO: Perform the task ourselves
					Sleep(1000);
				}
				
				PrintLine_I("Thread %u(0x%X) finished task[%u] %u", threadInfo->index, threadId, tIndex, task->id);
				threadInfo->currentTaskId = 0;
				threadInfo->currentTaskPntr = nullptr;
				task->finished = true;
			}
		}
		if (!anyWorkToDo)
		{
			_Win32_WaitOnSemaphore(&platform.taskThreadsSemaphore);
		}
		ArenaPopMark(&threadInfo->tempArena);
		Assert(ArenaNumMarks(&threadInfo->tempArena) == 0);
	}
	PrintLine_W("Thread %u(0x%X) exiting!", threadInfo->index, threadId);
	return 0;
}
