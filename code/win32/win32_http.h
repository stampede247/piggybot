/*
File:   win32_http.h
Author: Taylor Robbins
Date:   10\04\2019
*/

#ifndef _WIN_32_HTTP_H
#define _WIN_32_HTTP_H

#define MAX_HTTP_URL_LENGTH  255
#define MAX_HTTP_MAX_HEADER_KEY_LENGTH  255
#define MAX_HTTP_MAX_HEADER_VALUE_LENGTH  512
#define MAX_HTTP_HEADERS 16
#define MAX_HTTP_POST_FIELDS 8

typedef enum
{
	HttpRequestType_Get = 0,
	HttpRequestType_Post,
} HttpRequestType_t;

typedef void HttpCallback_f(bool success, const struct HttpRequest_t* request, const char* responseData, u32 responseLength);

struct HttpRequest_t
{
	const char* url;
	HttpRequestType_t requestType;
	u32 numHeaders;
	const char* headerKeys[MAX_HTTP_HEADERS];
	const char* headerValues[MAX_HTTP_HEADERS];
	u32 numPostFields;
	const char* postKeys[MAX_HTTP_POST_FIELDS];
	const char* postValues[MAX_HTTP_POST_FIELDS];
	u32 rawPayloadLength;
	const char* rawPayload;
	HttpCallback_f* callback;
};

struct Win32_HttpData
{
	HINTERNET sessionHandle;
	bool performingRequest;
	volatile bool requestFailed;
	volatile bool requestSucceeded;
	u32 numRequestsMade;
	HttpRequest_t request;
	HINTERNET connectionHandle;
	HINTERNET requestHandle;
	bool requestWriting;
	u32 encodedDataLength;
	char* encodedData;
	bool requestReading;
	u32 responseDataLength;
	char* responseData;
};

#endif //  _WIN_32_HTTP_H
