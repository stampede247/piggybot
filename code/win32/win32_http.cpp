/*
File:   win32_http.cpp
Author: Taylor Robbins
Date:   10\04\2019
Description: 
	** Provides some functions that help the application make HTTP requests using WinHTTP
*/

const char* GetHttpErrorString(DWORD errorCode)
{
	switch (errorCode)
	{
		case ERROR_WINHTTP_AUTO_PROXY_SERVICE_ERROR:              return "ERROR_WINHTTP_AUTO_PROXY_SERVICE_ERROR";
		case ERROR_WINHTTP_AUTODETECTION_FAILED:                  return "ERROR_WINHTTP_AUTODETECTION_FAILED";
		case ERROR_WINHTTP_BAD_AUTO_PROXY_SCRIPT:                 return "ERROR_WINHTTP_BAD_AUTO_PROXY_SCRIPT";
		case ERROR_WINHTTP_CANNOT_CALL_AFTER_OPEN:                return "ERROR_WINHTTP_CANNOT_CALL_AFTER_OPEN";
		case ERROR_WINHTTP_CANNOT_CALL_AFTER_SEND:                return "ERROR_WINHTTP_CANNOT_CALL_AFTER_SEND";
		case ERROR_WINHTTP_CANNOT_CALL_BEFORE_OPEN:               return "ERROR_WINHTTP_CANNOT_CALL_BEFORE_OPEN";
		case ERROR_WINHTTP_CANNOT_CALL_BEFORE_SEND:               return "ERROR_WINHTTP_CANNOT_CALL_BEFORE_SEND";
		case ERROR_WINHTTP_CANNOT_CONNECT:                        return "ERROR_WINHTTP_CANNOT_CONNECT";
		case ERROR_WINHTTP_CLIENT_AUTH_CERT_NEEDED:               return "ERROR_WINHTTP_CLIENT_AUTH_CERT_NEEDED";
		case ERROR_WINHTTP_CLIENT_CERT_NO_ACCESS_PRIVATE_KEY:     return "ERROR_WINHTTP_CLIENT_CERT_NO_ACCESS_PRIVATE_KEY";
		case ERROR_WINHTTP_CLIENT_CERT_NO_PRIVATE_KEY:            return "ERROR_WINHTTP_CLIENT_CERT_NO_PRIVATE_KEY";
		case ERROR_WINHTTP_CHUNKED_ENCODING_HEADER_SIZE_OVERFLOW: return "ERROR_WINHTTP_CHUNKED_ENCODING_HEADER_SIZE_OVERFLOW";
		case ERROR_WINHTTP_CONNECTION_ERROR:                      return "ERROR_WINHTTP_CONNECTION_ERROR";
		case ERROR_WINHTTP_HEADER_ALREADY_EXISTS:                 return "ERROR_WINHTTP_HEADER_ALREADY_EXISTS";
		case ERROR_WINHTTP_HEADER_COUNT_EXCEEDED:                 return "ERROR_WINHTTP_HEADER_COUNT_EXCEEDED";
		case ERROR_WINHTTP_HEADER_NOT_FOUND:                      return "ERROR_WINHTTP_HEADER_NOT_FOUND";
		case ERROR_WINHTTP_HEADER_SIZE_OVERFLOW:                  return "ERROR_WINHTTP_HEADER_SIZE_OVERFLOW";
		case ERROR_WINHTTP_INCORRECT_HANDLE_STATE:                return "ERROR_WINHTTP_INCORRECT_HANDLE_STATE";
		case ERROR_WINHTTP_INCORRECT_HANDLE_TYPE:                 return "ERROR_WINHTTP_INCORRECT_HANDLE_TYPE";
		case ERROR_WINHTTP_INTERNAL_ERROR:                        return "ERROR_WINHTTP_INTERNAL_ERROR";
		case ERROR_WINHTTP_INVALID_OPTION:                        return "ERROR_WINHTTP_INVALID_OPTION";
		case ERROR_WINHTTP_INVALID_QUERY_REQUEST:                 return "ERROR_WINHTTP_INVALID_QUERY_REQUEST";
		case ERROR_WINHTTP_INVALID_SERVER_RESPONSE:               return "ERROR_WINHTTP_INVALID_SERVER_RESPONSE";
		case ERROR_WINHTTP_INVALID_URL:                           return "ERROR_WINHTTP_INVALID_URL";
		case ERROR_WINHTTP_LOGIN_FAILURE:                         return "ERROR_WINHTTP_LOGIN_FAILURE";
		case ERROR_WINHTTP_NAME_NOT_RESOLVED:                     return "ERROR_WINHTTP_NAME_NOT_RESOLVED";
		case ERROR_WINHTTP_NOT_INITIALIZED:                       return "ERROR_WINHTTP_NOT_INITIALIZED";
		case ERROR_WINHTTP_OPERATION_CANCELLED:                   return "ERROR_WINHTTP_OPERATION_CANCELLED";
		case ERROR_WINHTTP_OPTION_NOT_SETTABLE:                   return "ERROR_WINHTTP_OPTION_NOT_SETTABLE";
		case ERROR_WINHTTP_OUT_OF_HANDLES:                        return "ERROR_WINHTTP_OUT_OF_HANDLES";
		case ERROR_WINHTTP_REDIRECT_FAILED:                       return "ERROR_WINHTTP_REDIRECT_FAILED";
		case ERROR_WINHTTP_RESEND_REQUEST:                        return "ERROR_WINHTTP_RESEND_REQUEST";
		case ERROR_WINHTTP_RESPONSE_DRAIN_OVERFLOW:               return "ERROR_WINHTTP_RESPONSE_DRAIN_OVERFLOW";
		case ERROR_WINHTTP_SCRIPT_EXECUTION_ERROR:                return "ERROR_WINHTTP_SCRIPT_EXECUTION_ERROR";
		case ERROR_WINHTTP_SECURE_CERT_CN_INVALID:                return "ERROR_WINHTTP_SECURE_CERT_CN_INVALID";
		case ERROR_WINHTTP_SECURE_CERT_DATE_INVALID:              return "ERROR_WINHTTP_SECURE_CERT_DATE_INVALID";
		case ERROR_WINHTTP_SECURE_CERT_REV_FAILED:                return "ERROR_WINHTTP_SECURE_CERT_REV_FAILED";
		case ERROR_WINHTTP_SECURE_CERT_REVOKED:                   return "ERROR_WINHTTP_SECURE_CERT_REVOKED";
		case ERROR_WINHTTP_SECURE_CERT_WRONG_USAGE:               return "ERROR_WINHTTP_SECURE_CERT_WRONG_USAGE";
		case ERROR_WINHTTP_SECURE_CHANNEL_ERROR:                  return "ERROR_WINHTTP_SECURE_CHANNEL_ERROR";
		case ERROR_WINHTTP_SECURE_FAILURE:                        return "ERROR_WINHTTP_SECURE_FAILURE";
		case ERROR_WINHTTP_SECURE_INVALID_CA:                     return "ERROR_WINHTTP_SECURE_INVALID_CA";
		case ERROR_WINHTTP_SECURE_INVALID_CERT:                   return "ERROR_WINHTTP_SECURE_INVALID_CERT";
		case ERROR_WINHTTP_SHUTDOWN:                              return "ERROR_WINHTTP_SHUTDOWN";
		case ERROR_WINHTTP_TIMEOUT:                               return "ERROR_WINHTTP_TIMEOUT";
		case ERROR_WINHTTP_UNABLE_TO_DOWNLOAD_SCRIPT:             return "ERROR_WINHTTP_UNABLE_TO_DOWNLOAD_SCRIPT";
		case ERROR_WINHTTP_UNHANDLED_SCRIPT_TYPE:                 return "ERROR_WINHTTP_UNHANDLED_SCRIPT_TYPE";
		case ERROR_WINHTTP_UNRECOGNIZED_SCHEME:                   return "ERROR_WINHTTP_UNRECOGNIZED_SCHEME";
		case ERROR_NOT_ENOUGH_MEMORY:                             return "ERROR_NOT_ENOUGH_MEMORY";
		case ERROR_INSUFFICIENT_BUFFER:                           return "ERROR_INSUFFICIENT_BUFFER";
		case ERROR_INVALID_HANDLE:                                return "ERROR_INVALID_HANDLE";
		case ERROR_NO_MORE_FILES:                                 return "ERROR_NO_MORE_FILES";
		case ERROR_NO_MORE_ITEMS:                                 return "ERROR_NO_MORE_ITEMS";
		case ERROR_NOT_SUPPORTED:                                 return "ERROR_NOT_SUPPORTED";
		default: return "UNKNOWN";
	};
}

u32 GetUrlHostNameLength(const char* fullUrl, u32 fullUrlLength)
{
	for (u32 cIndex = 0; cIndex < fullUrlLength; cIndex++)
	{
		if (fullUrl[cIndex] == '/' || fullUrl[cIndex] == '\\')
		{
			return cIndex;
		}
	}
	return fullUrlLength;
}

const char* GetUrlPathPart(const char* fullUrl, u32 fullUrlLength)
{
	for (u32 cIndex = 0; cIndex < fullUrlLength; cIndex++)
	{
		if (fullUrl[cIndex] == '/' || fullUrl[cIndex] == '\\')
		{
			return &fullUrl[cIndex];
		}
	}
	return nullptr;
}

char* EncodePostFields(MemoryArena_t* arenaPntr, u32 numFields, const char* const* keys, const char* const* values, u32* dataLengthOut)
{
	if (numFields == 0)
	{
		char* result = PushArray(arenaPntr, char, 1);
		result[0] = '\0';
		if (dataLengthOut != nullptr) { *dataLengthOut = 0; }
		return result;
	}
	
	u32 encodedSize = 0;
	for (u32 pIndex = 0; pIndex < numFields; pIndex++)
	{
		Assert(keys[pIndex] != nullptr);
		Assert(values[pIndex] != nullptr);
		
		u32 keyLength = MyStrLength32(keys[pIndex]);
		u32 valueLength = MyStrLength32(values[pIndex]);
		
		//TODO: Sanatize these strings!
		
		if (pIndex > 0) { encodedSize += 1; } //+1 for '&' between fields
		
		encodedSize += keyLength + 1 + valueLength; //+1 for '='
	}
	
	Assert(encodedSize > 0);
	
	char* result = PushArray(arenaPntr, char, encodedSize+1);
	u32 cIndex = 0;
	for (u32 pIndex = 0; pIndex < numFields; pIndex++)
	{
		Assert(keys[pIndex] != nullptr);
		Assert(values[pIndex] != nullptr);
		
		u32 keyLength = MyStrLength32(keys[pIndex]);
		u32 valueLength = MyStrLength32(values[pIndex]);
		
		//TODO: Sanatize these strings!
		
		if (pIndex > 0)
		{
			result[cIndex] = '&'; cIndex++;
		}
		
		MyMemCopy(&result[cIndex], keys[pIndex], keyLength); cIndex += keyLength;
		result[cIndex] = '='; cIndex++;
		MyMemCopy(&result[cIndex], values[pIndex], valueLength); cIndex += valueLength;
	}
	result[encodedSize] = '\0';
	
	if (dataLengthOut != nullptr) { *dataLengthOut = encodedSize; }
	return result;
}

void _Win32_CopyHttpRequest(MemoryArena_t* memArena, HttpRequest_t* target, const HttpRequest_t* source)
{
	ClearPointer(target);
	MyMemCopy(target, source, sizeof(HttpRequest_t));
	target->url = nullptr;
	ClearArray(target->postKeys);
	ClearArray(target->postValues);
	
	u32 urlLength = MyStrLength32(source->url);
	target->url = PushArray(memArena, char, urlLength+1);
	MyMemCopy((char*)target->url, source->url, urlLength);
	((char*)target->url)[urlLength] = '\0';
	
	for (u32 pIndex = 0; pIndex < source->numPostFields; pIndex++)
	{
		u32 keyLength = MyStrLength32(source->postKeys[pIndex]);
		target->postKeys[pIndex] = PushArray(memArena, char, keyLength+1);
		MyMemCopy((char*)target->postKeys[pIndex], source->postKeys[pIndex], keyLength);
		((char*)target->postKeys[pIndex])[keyLength] = '\0';
		
		u32 valueLength = MyStrLength32(source->postValues[pIndex]);
		target->postValues[pIndex] = PushArray(memArena, char, valueLength+1);
		MyMemCopy((char*)target->postValues[pIndex], source->postValues[pIndex], valueLength);
		((char*)target->postValues[pIndex])[valueLength] = '\0';
	}
	
	if (source->rawPayloadLength > 0)
	{
		target->rawPayload = PushArray(memArena, char, source->rawPayloadLength);
		MyMemCopy((char*)target->rawPayload, source->rawPayload, source->rawPayloadLength);
		target->rawPayloadLength = source->rawPayloadLength;
	}
}

void _Win32_FreeHttpRequest(MemoryArena_t* memArena, HttpRequest_t* httpRequest)
{
	Assert(httpRequest != nullptr);
	
	if (httpRequest->url != nullptr)
	{
		ArenaPop(memArena, (void*)httpRequest->url);
		httpRequest->url = nullptr;
	}
	
	for (u32 pIndex = 0; pIndex < httpRequest->numPostFields; pIndex++)
	{
		if (httpRequest->postKeys[pIndex] != nullptr)
		{
			ArenaPop(memArena, (void*)httpRequest->postKeys[pIndex]);
			httpRequest->postKeys[pIndex] = nullptr;
		}
		if (httpRequest->postValues[pIndex] != nullptr)
		{
			ArenaPop(memArena, (void*)httpRequest->postValues[pIndex]);
			httpRequest->postValues[pIndex] = nullptr;
		}
	}
	
	if (httpRequest->rawPayload != nullptr)
	{
		ArenaPop(memArena, (void*)httpRequest->rawPayload);
		httpRequest->rawPayload = nullptr;
	}
}

const char* GetWinHttpStatusStr(DWORD status)
{
	switch (status)
	{
		case WINHTTP_CALLBACK_STATUS_CLOSING_CONNECTION:      return "CLOSING_CONNECTION";
		case WINHTTP_CALLBACK_STATUS_CONNECTED_TO_SERVER:     return "CONNECTED_TO_SERVER";
		case WINHTTP_CALLBACK_STATUS_CONNECTING_TO_SERVER:    return "CONNECTING_TO_SERVER";
		case WINHTTP_CALLBACK_STATUS_CONNECTION_CLOSED:       return "CONNECTION_CLOSED";
		case WINHTTP_CALLBACK_STATUS_DATA_AVAILABLE:          return "DATA_AVAILABLE";
		case WINHTTP_CALLBACK_STATUS_HANDLE_CREATED:          return "HANDLE_CREATED";
		case WINHTTP_CALLBACK_STATUS_HANDLE_CLOSING:          return "HANDLE_CLOSING";
		case WINHTTP_CALLBACK_STATUS_HEADERS_AVAILABLE:       return "HEADERS_AVAILABLE";
		case WINHTTP_CALLBACK_STATUS_INTERMEDIATE_RESPONSE:   return "INTERMEDIATE_RESPONSE";
		case WINHTTP_CALLBACK_STATUS_NAME_RESOLVED:           return "NAME_RESOLVED";
		case WINHTTP_CALLBACK_STATUS_READ_COMPLETE:           return "READ_COMPLETE";
		case WINHTTP_CALLBACK_STATUS_RECEIVING_RESPONSE:      return "RECEIVING_RESPONSE";
		case WINHTTP_CALLBACK_STATUS_REDIRECT:                return "REDIRECT";
		case WINHTTP_CALLBACK_STATUS_REQUEST_ERROR:           return "REQUEST_ERROR";
		case WINHTTP_CALLBACK_STATUS_REQUEST_SENT:            return "REQUEST_SENT";
		case WINHTTP_CALLBACK_STATUS_RESOLVING_NAME:          return "RESOLVING_NAME";
		case WINHTTP_CALLBACK_STATUS_RESPONSE_RECEIVED:       return "RESPONSE_RECEIVED";
		case WINHTTP_CALLBACK_STATUS_SECURE_FAILURE:          return "SECURE_FAILURE";
		case WINHTTP_CALLBACK_STATUS_SENDING_REQUEST:         return "SENDING_REQUEST";
		case WINHTTP_CALLBACK_STATUS_SENDREQUEST_COMPLETE:    return "SENDREQUEST_COMPLETE";
		case WINHTTP_CALLBACK_STATUS_WRITE_COMPLETE:          return "WRITE_COMPLETE";
		case WINHTTP_CALLBACK_STATUS_GETPROXYFORURL_COMPLETE: return "GETPROXYFORURL_COMPLETE";
		case WINHTTP_CALLBACK_STATUS_CLOSE_COMPLETE:          return "CLOSE_COMPLETE";
		case WINHTTP_CALLBACK_STATUS_SHUTDOWN_COMPLETE:       return "SHUTDOWN_COMPLETE";
		default: return "UNKNOWN";
	};
}

void GenericHandleHttpEvent(HINTERNET handle, DWORD_PTR context, DWORD status, LPVOID infoPntr, DWORD infoLength)
{
	//NOTE: This could be happening on a alternate thread
	//TODO: Make these work largely without any Print statements so they work even if this is happening on an alternate unmanaged thread
	PrintLine_D("HttpStatus %s(%04X): %p[%u] (%p,%p)", GetWinHttpStatusStr(status), status, infoPntr, infoLength, handle, context);
	
	switch (status)
	{
		case WINHTTP_CALLBACK_STATUS_REQUEST_ERROR:
		{
			Assert(infoLength == sizeof(WINHTTP_ASYNC_RESULT));
			WINHTTP_ASYNC_RESULT* asyncResultPntr = (WINHTTP_ASYNC_RESULT*)infoPntr;
			const char* functionName = "UnknownFunction";
			switch (asyncResultPntr->dwResult)
			{
				case 1: functionName = "WinHttpReceiveResponse"; break;
				case 2: functionName = "WinHttpQueryDataAvailable"; break;
				case 3: functionName = "WinHttpReadData"; break;
				case 4: functionName = "WinHttpWriteData"; break;
				case 5: functionName = "WinHttpSendRequest"; break;
			};
			PrintLine_D("\tFunction %s: 0x%04X %s", functionName, asyncResultPntr->dwError, GetHttpErrorString(asyncResultPntr->dwError));
		} break;
		case WINHTTP_CALLBACK_STATUS_RESOLVING_NAME:
		{
			PrintLine_D("\tName: \"%.*S\"", infoLength, (const char*)infoPntr);
		} break;
	};
}

void Win32_HttpStatusCallback(HINTERNET handle, DWORD_PTR context, DWORD status, LPVOID infoPntr, DWORD infoLength)
{
	// GenericHandleHttpEvent(handle, context, status, infoPntr, infoLength);
	char printBuffer[256];
	
	ThreadId_t threadId = GetCurrentThreadId();
	bool isMainThread = (threadId == MainThreadId);
	MemoryArena_t* tempArena = GetThreadTempArena();
	// if (!isMainThread)
	// {
	// 	Write_W("Status event is happening on alternate thread:");
	// 	Write_W(GetWinHttpStatusStr(status));
	// 	Write_W("\n");
	// }
	
	LocalPrintLineAt(printBuffer, DbgLevel_Error, "#%u Status event on thread %u%s%s: %s", platform.http.numRequestsMade, threadId, isMainThread ? " (Main)" : "", (tempArena != nullptr) ? " (Managed)" : "", GetWinHttpStatusStr(status));
	
	if (status == WINHTTP_CALLBACK_STATUS_SENDREQUEST_COMPLETE) //alternate thread
	{
		WriteLine_D("HTTP Request sent!");
		platform.http.requestWriting = false;
		bool32 receiveResult = WinHttpReceiveResponse(platform.http.requestHandle, NULL);
		if (!receiveResult)
		{
			WriteLine_D("WinHttpReceiveResponse Failed!");
			ThreadingWriteBarrier();
			platform.http.requestFailed = true;
		}
	}
	else if (status == WINHTTP_CALLBACK_STATUS_REQUEST_ERROR) //alternate thread
	{
		WriteLine_E("HTTP Request failed!");
		Assert(infoPntr != nullptr);
		WINHTTP_ASYNC_RESULT* errorPntr = (WINHTTP_ASYNC_RESULT*)infoPntr;
		
		const char* functionName = nullptr;
		switch (errorPntr->dwResult)
		{
			case API_RECEIVE_RESPONSE:     functionName = "WinHttpReceiveResponse";    break;
			case API_QUERY_DATA_AVAILABLE: functionName = "WinHttpQueryDataAvailable"; break;
			case API_READ_DATA:            functionName = "WinHttpReadData";           break;
			case API_WRITE_DATA:           functionName = "WinHttpWriteData";          break;
			case API_SEND_REQUEST:         functionName = "WinHttpSendRequest";        break;
			default: functionName = "Unknown"; break;
		}
		LocalPrintLineAt(printBuffer, DbgLevel_Error, "Called Function: %s Return Value: %s", functionName, GetHttpErrorString(errorPntr->dwError));
		platform.http.requestWriting = false;
		ThreadingWriteBarrier();
		platform.http.requestFailed = true;
	}
	else if (status == WINHTTP_CALLBACK_STATUS_RECEIVING_RESPONSE) //alternate thread
	{
		WriteLine_D("Getting HTTP response...");
		ThreadingWriteBarrier();
		platform.http.requestReading = true;
	}
	else if (status == WINHTTP_CALLBACK_STATUS_DATA_AVAILABLE) //alternate thread
	{
		Assert(infoLength == sizeof(DWORD));
		
		u32 numBytesAvail = *((DWORD*)infoPntr);
		if (platform.http.responseData == nullptr)
		{
			Assert(platform.http.responseDataLength == 0);
			platform.http.responseData = PushArray(&platform.stdHeap, char, numBytesAvail + 1);
		}
		else
		{
			char* newSpace = PushArray(&platform.stdHeap, char, platform.http.responseDataLength + numBytesAvail + 1);
			Assert(newSpace != nullptr);
			MyMemCopy(newSpace, platform.http.responseData, platform.http.responseDataLength);
			ArenaPop(&platform.stdHeap, platform.http.responseData);
			platform.http.responseData = newSpace;
		}
		Assert(platform.http.responseData != nullptr);
		
		char* newSpacePntr = platform.http.responseData + platform.http.responseDataLength;
		MyMemSet(newSpacePntr, 0x00, numBytesAvail+1);
		platform.http.responseDataLength += numBytesAvail;
		
		//NOTE: A READ_COMPLETE status event can happen on this thread INSIDE the WinHttpReadData function
		LocalPrintLineAt(printBuffer, DbgLevel_Debug, "Downloading %u bytes...", numBytesAvail);
		if (WinHttpReadData(platform.http.requestHandle, (LPVOID)newSpacePntr, numBytesAvail, NULL))
		{
			LocalPrintLineAt(printBuffer, DbgLevel_Debug, "Downloaded %u bytes (%u total)", numBytesAvail, platform.http.responseDataLength);
		}
		else
		{
			LocalPrintLineAt(printBuffer, DbgLevel_Warning, "WinHttpReadData error %u", GetLastError());
		}
	}
	else if (status == WINHTTP_CALLBACK_STATUS_READ_COMPLETE) //alternate thread
	{
		//NOTE: This event can happen inside the DATA_AVAILABLE status event when calling WinHttpReadData
		WriteLine_D("Finished getting HTTP response");
		ThreadingWriteBarrier();
		platform.http.requestSucceeded = true;
	}
	else
	{
		if (status != WINHTTP_CALLBACK_STATUS_HANDLE_CLOSING &&
			status != WINHTTP_CALLBACK_STATUS_HANDLE_CREATED &&
			status != WINHTTP_CALLBACK_STATUS_SENDING_REQUEST &&
			status != WINHTTP_CALLBACK_STATUS_REQUEST_SENT &&
			status != WINHTTP_CALLBACK_STATUS_RESPONSE_RECEIVED &&
			status != WINHTTP_CALLBACK_STATUS_HEADERS_AVAILABLE &&
			status != WINHTTP_CALLBACK_STATUS_RESOLVING_NAME &&
			status != WINHTTP_CALLBACK_STATUS_NAME_RESOLVED &&
			status != WINHTTP_CALLBACK_STATUS_CONNECTING_TO_SERVER &&
			status != WINHTTP_CALLBACK_STATUS_CONNECTED_TO_SERVER)
		{
			GenericHandleHttpEvent(handle, context, status, infoPntr, infoLength);
		}
	}
}

void Win32_InitHttp()
{
	WriteLine_R("Opening WinHTTP session...");
	platform.http.sessionHandle = WinHttpOpen(L"Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0", //TODO: Make this a different client string?
		WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
		WINHTTP_NO_PROXY_NAME,
		WINHTTP_NO_PROXY_BYPASS,
		WINHTTP_FLAG_ASYNC
	);
	
	DWORD callbackMask = WINHTTP_CALLBACK_FLAG_ALL_COMPLETIONS |
		WINHTTP_CALLBACK_FLAG_ALL_NOTIFICATIONS |
		WINHTTP_CALLBACK_FLAG_RESOLVE_NAME |
		WINHTTP_CALLBACK_FLAG_CONNECT_TO_SERVER |
		WINHTTP_CALLBACK_FLAG_DETECTING_PROXY |
		WINHTTP_CALLBACK_FLAG_DATA_AVAILABLE |
		WINHTTP_CALLBACK_FLAG_HEADERS_AVAILABLE |
		WINHTTP_CALLBACK_FLAG_READ_COMPLETE |
		WINHTTP_CALLBACK_FLAG_REQUEST_ERROR |
		WINHTTP_CALLBACK_FLAG_SEND_REQUEST |
		WINHTTP_CALLBACK_FLAG_SENDREQUEST_COMPLETE |
		WINHTTP_CALLBACK_FLAG_WRITE_COMPLETE |
		WINHTTP_CALLBACK_FLAG_RECEIVE_RESPONSE |
		WINHTTP_CALLBACK_FLAG_CLOSE_CONNECTION |
		WINHTTP_CALLBACK_FLAG_HANDLES |
		WINHTTP_CALLBACK_FLAG_REDIRECT |
		WINHTTP_CALLBACK_FLAG_INTERMEDIATE_RESPONSE |
		WINHTTP_CALLBACK_FLAG_SECURE_FAILURE;
	
	if (WinHttpSetStatusCallback(platform.http.sessionHandle, Win32_HttpStatusCallback, callbackMask, NULL) == WINHTTP_INVALID_STATUS_CALLBACK)
	{
		Win32_ExitOnError("Couldn't set WinHTTP callback");
	}
	
	unsigned long securitySetting = 0;
	WinHttpSetOption(platform.http.sessionHandle, WINHTTP_OPTION_SECURITY_FLAGS, &securitySetting, sizeof(securitySetting));
	
	if (!platform.http.sessionHandle)
	{
		Win32_ExitOnError("Couldn't open WinHTTP session handle");
	}
	
	WriteLine_R("Session created!");
}

void _Win32_UpdateHttpRequest()
{
	// +==============================+
	// |     Update Http Request      |
	// +==============================+
	if (platform.http.performingRequest && platform.http.requestReading)
	{
		if (!WinHttpQueryDataAvailable(platform.http.requestHandle, NULL))
		{
			PrintLine_E("WinHttpQueryDataAvailable error %u", GetLastError());
		}
	}
	
	// +==============================+
	// |    HTTP Request Callback     |
	// +==============================+
	if (platform.http.performingRequest && (platform.http.requestFailed || platform.http.requestSucceeded))
	{
		WinHttpCloseHandle(platform.http.requestHandle);
		WinHttpCloseHandle(platform.http.connectionHandle);
		if (platform.http.request.callback != nullptr)
		{
			platform.http.request.callback(platform.http.requestSucceeded, &platform.http.request, platform.http.responseData, platform.http.responseDataLength);
		}
		if (platform.http.responseData != nullptr) { ArenaPop(&platform.stdHeap, platform.http.responseData); platform.http.responseData = nullptr; }
		if (platform.http.encodedData != nullptr) { ArenaPop(&platform.mainHeap, platform.http.encodedData); platform.http.encodedData = nullptr; }
		_Win32_FreeHttpRequest(&platform.mainHeap, &platform.http.request);
		platform.http.performingRequest = false;
	}
}

// +==============================+
// |    Win32_HttpRequestStart    |
// +==============================+
// bool HttpRequestStart(const HttpRequest_t* request)
HttpRequestStart_DEFINITION(Win32_HttpRequestStart)
{
	if (request == nullptr) { WriteLine_E("request is nullptr"); return false; }
	if (request->url[0] == '\0') { WriteLine_E("URL is empty!"); return false; }
	if (request->requestType != HttpRequestType_Get && request->requestType != HttpRequestType_Post) { WriteLine_E("Unknown request type!"); return false; }
	//TODO: Check to make sure the url is null-terminated
	if (!platform.http.sessionHandle) { WriteLine_E("No HTTP session!"); return false; }
	if (platform.http.performingRequest) { WriteLine_E("Already performing!"); return false; }
	if (MyStrLength32(request->url) > MAX_HTTP_URL_LENGTH) { WriteLine_E("URL is too long"); return false; }
	if (request->numPostFields > MAX_HTTP_POST_FIELDS) { WriteLine_E("Too many POST fields"); return false; }
	
	// PrintLine_D("Making an HTTP request to \"%s\"...", request->url);
	TempPushMark();
	platform.http.numRequestsMade++;
	
	u32 hostNameLength = GetUrlHostNameLength(request->url, MyStrLength32(request->url));
	const char* urlPath = GetUrlPathPart(request->url, MyStrLength32(request->url));
	if (urlPath == nullptr) { urlPath = &request->url[MyStrLength(request->url)]; }
	u32 urlPathLength = urlPathLength = MyStrLength32(urlPath);
	Assert(hostNameLength <= MAX_HTTP_URL_LENGTH);
	Assert(urlPathLength <= MAX_HTTP_URL_LENGTH);
	
	// PrintLine_D("Host: \"%.*s\" Path: \"%s\"", hostNameLength, request->url, urlPath);
	
	wchar_t hostNameWide[MAX_HTTP_URL_LENGTH+1];
	MyMemSet(hostNameWide, 0x00, sizeof(hostNameWide));
	mbstowcs(hostNameWide, request->url, hostNameLength);
	hostNameWide[hostNameLength] = L'\0';
	
	wchar_t urlPathWide[MAX_HTTP_URL_LENGTH+1];
	MyMemSet(urlPathWide, 0x00, sizeof(urlPathWide));
	if (urlPathLength > 0)
	{
		mbstowcs(urlPathWide, urlPath, urlPathLength);
		urlPathWide[urlPathLength] = L'\0';
	}
	
	platform.http.encodedDataLength = 0;
	platform.http.encodedData = nullptr;
	if (request->rawPayloadLength > 0)
	{
		Assert(request->rawPayload != nullptr);
		platform.http.encodedDataLength = request->rawPayloadLength;
		platform.http.encodedData = ArenaString(&platform.mainHeap, request->rawPayload, request->rawPayloadLength);
	}
	if (request->numPostFields > 0)
	{
		platform.http.encodedData = EncodePostFields(&platform.mainHeap, request->numPostFields, &request->postKeys[0], &request->postValues[0], &platform.http.encodedDataLength);
	}
	
	platform.http.connectionHandle = WinHttpConnect(platform.http.sessionHandle, hostNameWide, INTERNET_DEFAULT_HTTPS_PORT, 0);
	if (platform.http.connectionHandle)
	{
		wchar_t* actionWideStr = L"GET";
		if (request->requestType == HttpRequestType_Post) { actionWideStr = L"POST"; }
		
		platform.http.requestHandle = WinHttpOpenRequest(platform.http.connectionHandle,
			actionWideStr,
			urlPathWide,
			NULL,
			WINHTTP_NO_REFERER,
			WINHTTP_DEFAULT_ACCEPT_TYPES,
			WINHTTP_FLAG_SECURE
		);
		
		if (platform.http.requestHandle)
		{
			wchar_t* headersWideStr = L"Content-Type: application/x-www-form-urlencoded\r\n";
			if (request->rawPayloadLength > 0)
			{
				headersWideStr = L"Content-Type: application/octet-stream\r\n";
			}
			
			WinHttpSetOption(platform.http.requestHandle, WINHTTP_OPTION_CONTEXT_VALUE, (void*)&platform.http.request, sizeof(void*));
			
			wchar_t headerWideBuffer[MAX_HTTP_MAX_HEADER_KEY_LENGTH+1+MAX_HTTP_MAX_HEADER_VALUE_LENGTH+1];
			for (u32 hIndex = 0; hIndex < request->numHeaders; hIndex++)
			{
				const char* headerKeyPntr = request->headerKeys[hIndex];
				u32 headerKeyLength = MyStrLength32(headerKeyPntr);
				Assert(headerKeyLength <= MAX_HTTP_MAX_HEADER_KEY_LENGTH);
				const char* headerValuePntr = request->headerValues[hIndex];
				u32 headerValueLength = MyStrLength32(headerValuePntr);
				Assert(headerValueLength <= MAX_HTTP_MAX_HEADER_VALUE_LENGTH);
				NotNull(headerKeyPntr);
				NotNull(headerValuePntr);
				size_t headerBufferIndex = 0;
				//TODO: Handle a response of -1 from mbstowcs
				headerBufferIndex += mbstowcs(&headerWideBuffer[headerBufferIndex], headerKeyPntr, headerKeyLength);
				Assert(headerBufferIndex <= ArrayCount(headerWideBuffer));
				headerWideBuffer[headerBufferIndex] = L':'; headerBufferIndex++;
				Assert(headerBufferIndex <= ArrayCount(headerWideBuffer));
				headerBufferIndex += mbstowcs(&headerWideBuffer[headerBufferIndex], headerValuePntr, headerValueLength);
				Assert(headerBufferIndex <= ArrayCount(headerWideBuffer));
				wprintf(L"Header[%u]: %.*s\n", hIndex, (int)headerBufferIndex, headerWideBuffer);
				WinHttpAddRequestHeaders(platform.http.requestHandle, headerWideBuffer, (DWORD)headerBufferIndex, WINHTTP_ADDREQ_FLAG_ADD);
			}
			
			platform.http.performingRequest = true;
			_Win32_CopyHttpRequest(&platform.mainHeap, &platform.http.request, request);
			platform.http.responseDataLength = 0;
			platform.http.responseData = nullptr;
			
			platform.http.requestWriting = true;
			platform.http.requestReading = false;
			platform.http.requestFailed = false;
			platform.http.requestSucceeded = false;
			ThreadingWriteBarrier();
			
			bool32 requestResult = WinHttpSendRequest(platform.http.requestHandle,
				headersWideStr, (DWORD)-1,
				platform.http.encodedData, platform.http.encodedDataLength, platform.http.encodedDataLength,
				0
			);
			
			if (requestResult)
			{
				TempPopMark();
				return true;
			}
			else
			{
				WriteLine_E("WinHttpSendRequest Failed!");
				platform.http.requestWriting = false;
			}
			
			WinHttpCloseHandle(platform.http.requestHandle);
		}
		else
		{
			WriteLine_E("WinHttpOpenRequest Failed!");
		}
		
		WinHttpCloseHandle(platform.http.connectionHandle);
	}
	else
	{
		DWORD errorCode = GetLastError();
		switch (errorCode)
		{
			case ERROR_WINHTTP_INCORRECT_HANDLE_TYPE: WriteLine_E("WinHttpConnect Failed: WINHTTP_INCORRECT_HANDLE_TYPE"); break;
			case ERROR_WINHTTP_INTERNAL_ERROR: WriteLine_E("WinHttpConnect Failed: WINHTTP_INTERNAL_ERROR"); break;
			case ERROR_WINHTTP_INVALID_URL: WriteLine_E("WinHttpConnect Failed: WINHTTP_INVALID_URL"); break;
			case ERROR_WINHTTP_OPERATION_CANCELLED: WriteLine_E("WinHttpConnect Failed: WINHTTP_OPERATION_CANCELLED"); break;
			case ERROR_WINHTTP_UNRECOGNIZED_SCHEME: WriteLine_E("WinHttpConnect Failed: WINHTTP_UNRECOGNIZED_SCHEME"); break;
			case ERROR_WINHTTP_SHUTDOWN: WriteLine_E("WinHttpConnect Failed: WINHTTP_SHUTDOWN"); break;
			case ERROR_NOT_ENOUGH_MEMORY: WriteLine_E("WinHttpConnect Failed: NOT_ENOUGH_MEMORY"); break;
			default: PrintLine_E("WinHttpConnect Failed: error %u", errorCode); break;
		};
	}
	
	//NOTE: If we get to this point the connection/request failed for some reason
	if (request->callback != nullptr)
	{
		request->callback(false, request, nullptr, 0);
	}
	TempPopMark();
	platform.http.numRequestsMade--;
	return false;
}

// +==============================+
// | Win32_HttpRequestInProgress  |
// +==============================+
// bool HttpRequestInProgress()
HttpRequestInProgress_DEFINITION(Win32_HttpRequestInProgress)
{
	return platform.http.performingRequest;
}
