/*
File:   win32_defines.h
Author: Taylor Robbins
Date:   07\01\2019
*/

#ifndef _WIN_32_DEFINES_H
#define _WIN_32_DEFINES_H

// +--------------------------------------------------------------+
// |                           Defines                            |
// +--------------------------------------------------------------+
#define WINDOW_CLASS_NAME           "PiggyBot"
#define WINDOW_TITLE                "Piggy Bot"
#define APPLICATION_DLL_NAME        "PiggyBot.dll"
#define APPLICATION_DLL_TEMP_NAME   "PiggyBot_TEMP.dll"
#define OPEN_CONSOLE_WINDOW         (true && DEBUG)
#define PLAT_DEBUG_OUTPUT_ENABLED   true
// #define APP_PERM_MEMORY_SIZE        Megabytes(1)
// #define APP_TEMP_MEMORY_SIZE        Megabytes(16)
#define WINDOW_SWAP_INTERVAL        1
#define MAX_QUEUED_TASKS            32

#define CRASH_ON_AUDIO_INIT_FAILED       (true && DEBUG)
#define AUDIO_OUTPUT_SAMPLE_RATE         48000 //48 kHz
#define AUDIO_OUTPUT_CHANNEL_COUNT       2 //channels
#define AUDIO_OUTPUT_BIT_DEPTH           16 //bit
#define AUDIO_OUTPUT_BUFFER_LENGTH       1 //second
#define AUDIO_BUFFER_DISPLAY_CHUNK_COUNT 480 //chunks
#define AUDIO_WRITE_AHEAD_LENGTH         4000 //samples or 1/12th of a second or 83.3 ms

#define PLAT_MIN_FRAME_WAIT_TIME         1.0f //ms
#define PLAT_FLIP_SYNC_DETECT_THRESHOLD  0.5f //ms
#define PLAT_MANUAL_WAIT_KICK_IN_TIME    10 //frames

#define MIN_OPENGL_VERSION_MAJOR     3
#define MIN_OPENGL_VERSION_MINOR     0
#define OPENGL_REQUEST_VERSION_MAJOR 1
#define OPENGL_REQUEST_VERSION_MINOR 0
//NOTE: Setting this to true causes weird texture problems when we run with the Steam Overlay on top.
//      This setting is supposed to make sure that all deprecated functionality in version 3.0 of OpenGL
//      is unsupported. Maybe we are using some deprecated features that we shouldn't be?
#define OPENGL_FORCE_FORWARD_COMPAT  false
#define OPENGL_DEBUG_CONTEXT         DEBUG

#define NUM_FRAMERATE_AVGS             5 //frames
#define PLATFORM_HEAP_SIZE             Kilobytes(128)
#define PLATFORM_TEMP_SIZE             Kilobytes(16)
#define PLATFORM_TEMP_MAX_NUM_MARKS    16 //marks
#define TASK_THREAD_TEMP_MEM_SIZE      Kilobytes(16)
#define TASK_THREAD_TEMP_MAX_NUM_MARKS 16
#define MAX_NUM_THREADS                8 //threads
#define MAX_CRASH_DUMPS                4 //dumps

//NOTE: This must match resource.h in build directory!
#define WIN32_EXE_ICON_ID           101

#define EXPECTED_STEAM_APPID             1103550

#endif //  _WIN_32_DEFINES_H
