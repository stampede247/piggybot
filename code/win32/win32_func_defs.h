/*
File:   win32_func_defs.h
Author: Taylor Robbins
Date:   05\14\2020
*/

#ifndef _WIN_32_FUNC_DEFS_H
#define _WIN_32_FUNC_DEFS_H

//plat_input.h
InputEvent_t* Plat_PushInputEvent(AppInput_t* currentInput);
InputEvent_t* Plat_PushInputEventSimple(AppInput_t* currentInput, InputEventType_t eventType);

#endif //  _WIN_32_FUNC_DEFS_H
