/*
File:   win32_email.cpp
Author: Taylor Robbins
Date:   10\04\2019
Description: 
	** Holds some functions that help the application open an email for the user to send feedback or other information
*/

// +==============================+
// |    Win32_OpenEmailPrompt     |
// +==============================+
// bool OpenEmailPrompt(const char* recipientStr, const char* subjectStr, const char* bodyStr)
OpenEmailPrompt_DEFINITION(Win32_OpenEmailPrompt)
{
	Assert(recipientStr != nullptr);
	Assert(subjectStr != nullptr);
	Assert(bodyStr != nullptr);
	bool result = false;
	
	TempPushMark();
	char* escapedSubject = UrlEscape(TempArena, subjectStr, MyStrLength32(subjectStr)); Assert(escapedSubject != nullptr);
	char* escapedBody = UrlEscape(TempArena, bodyStr, MyStrLength32(bodyStr)); Assert(escapedBody != nullptr);
	char* commandStr = TempPrint("mailto:%s?subject=%s&body=%s", recipientStr, escapedSubject, escapedBody); Assert(commandStr != nullptr);
	
	u64 executeResult = (u64)ShellExecuteA(
		NULL,   //No parent window
		"open", //The action verb
		commandStr, //The target file
		NULL, //No parameters
		NULL, //Use default working directory
		SW_SHOWNORMAL //Show command is normal
	);
	
	if (executeResult > 32)
	{
		result = true;
	}
	else
	{
		switch (executeResult)
		{
			case ERROR_FILE_NOT_FOUND:   { WriteLine_E("ShellExecute returned ERROR_FILE_NOT_FOUND"); } break;
			case ERROR_PATH_NOT_FOUND:   { WriteLine_E("ShellExecute returned ERROR_PATH_NOT_FOUND"); } break;
			case ERROR_BAD_FORMAT:       { WriteLine_E("ShellExecute returned ERROR_BAD_FORMAT"); } break;
			case SE_ERR_ACCESSDENIED:    { WriteLine_E("ShellExecute returned SE_ERR_ACCESSDENIED"); } break;
			case SE_ERR_ASSOCINCOMPLETE: { WriteLine_E("ShellExecute returned SE_ERR_ASSOCINCOMPLETE"); } break;
			case SE_ERR_DDEBUSY:         { WriteLine_E("ShellExecute returned SE_ERR_DDEBUSY"); } break;
			case SE_ERR_DDEFAIL:         { WriteLine_E("ShellExecute returned SE_ERR_DDEFAIL"); } break;
			case SE_ERR_DDETIMEOUT:      { WriteLine_E("ShellExecute returned SE_ERR_DDETIMEOUT"); } break;
			case SE_ERR_DLLNOTFOUND:     { WriteLine_E("ShellExecute returned SE_ERR_DLLNOTFOUND"); } break;
			// case SE_ERR_FNF:             { WriteLine_E("ShellExecute returned SE_ERR_FNF"); } break;
			case SE_ERR_NOASSOC:         { WriteLine_E("ShellExecute returned SE_ERR_NOASSOC"); } break;
			case SE_ERR_OOM:             { WriteLine_E("ShellExecute returned SE_ERR_OOM"); } break;
			// case SE_ERR_PNF:             { WriteLine_E("ShellExecute returned SE_ERR_PNF"); } break;
			case SE_ERR_SHARE:           { WriteLine_E("ShellExecute returned SE_ERR_SHARE"); } break;
			
			default:
			{
				PrintLine_E("ShellExecute failed with result 0x%02X for mailto command", executeResult);
			} break;
		};
	}
	
	TempPopMark();
	
	return result;
}
