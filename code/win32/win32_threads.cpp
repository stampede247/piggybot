/*
File:   win32_threads.cpp
Author: Taylor Robbins
Date:   03\21\2019
Description: 
	** Holds a bunch of functions that make creating, monitoring, and destroying various threads easier for the platform layer
*/

// +==============================+
// |      Win32_CreateMutex       |
// +==============================+
// void CreateMutex(Mutex_t* mutex)
CreateMutex_DEFINITION(Win32_CreateMutex)
{
	Assert_(mutex != nullptr);
	ClearPointer(mutex);
	mutex->handle = CreateMutex(nullptr, false, nullptr); //default security, no ownership, no name
	Assert_(mutex->handle != NULL);
}
// +==============================+
// |       Win32_LockMutex        |
// +==============================+
// bool LockMutex(Mutex_t* mutex, u32 timeoutMs)
LockMutex_DEFINITION(Win32_LockMutex) //pre-declared in win32_main.cpp
{
	Assert_(mutex != nullptr);
	DWORD realTimeout = (DWORD)timeoutMs;
	if (timeoutMs == 0xFFFFFFFFUL) { realTimeout = INFINITE; }
	DWORD mutexResult = WaitForSingleObject(mutex->handle, realTimeout);
	if (realTimeout == INFINITE && mutexResult != WAIT_OBJECT_0) { Assert_(false); } //Failed to lock mutex!
	return (mutexResult == WAIT_OBJECT_0);
}
// +==============================+
// |      Win32_UnlockMutex       |
// +==============================+
// void UnlockMutex(Mutex_t* mutex)
UnlockMutex_DEFINITION(Win32_UnlockMutex) //pre-declared in win32_main.cpp
{
	BOOL mutexResult = ReleaseMutex(mutex->handle);
	Assert_(mutexResult != 0);
}

void _Win32_CreateSemaphore(Win32Semaphore_t* semaphore, u32 startCount, u32 maxCount)
{
	Assert(semaphore != nullptr);
	ClearPointer(semaphore);
	semaphore->handle = CreateSemaphoreExA(nullptr, (LONG)startCount, (LONG)maxCount, NULL, 0, SEMAPHORE_ALL_ACCESS); //default attributes, no name, 
	Assert(semaphore->handle != NULL);
}
bool _Win32_WaitOnSemaphore(Win32Semaphore_t* semaphore, DWORD timeout = INFINITE)
{
	Assert_(semaphore != nullptr && semaphore->handle != NULL);
	DWORD waitResult = WaitForSingleObjectEx(semaphore->handle, timeout, false);
	if (waitResult == WAIT_OBJECT_0) { return true; }
	else { return false; }
}
bool _Win32_TriggerSemaphore(Win32Semaphore_t* semaphore, u32 count = 1, u32* previousCountOut = nullptr)
{
	Assert_(semaphore != nullptr && semaphore->handle != NULL);
	Assert_(count > 0);
	LONG previousCount = 0;
	BOOL releaseResult = ReleaseSemaphore(semaphore->handle, (LONG)count, &previousCount);
	if (previousCountOut != nullptr) { *previousCountOut = (u32)previousCount; }
	return (releaseResult != 0);
}

void _Win32_ThreadsInitialize()
{
	MainThreadId = GetCurrentThreadId();
	if (MainThreadId == 0)
	{
		Win32_ExitOnError("Failed to get the ID of the main thread");
	}
	Win32_CreateMutex(&platform.threadsMutex);
	ClearArray(platform.threads);
}

Thread_t* _Win32_CreateThread(Win32ThreadFunction_f* function, void* userPntr)
{
	Assert(function != nullptr);
	if (!Win32_LockMutex(&platform.threadsMutex, 10)) { return nullptr; }
	
	Thread_t* openThread = nullptr;
	for (u32 tIndex = 0; tIndex < MAX_NUM_THREADS; tIndex++)
	{
		Thread_t* threadPntr = &platform.threads[tIndex];
		if (threadPntr->handle == NULL)
		{
			openThread = threadPntr;
			ClearPointer(openThread);
			openThread->function = function;
			openThread->userPntr = userPntr;
			break;
		}
	}
	if (openThread == nullptr)
	{
		Win32_UnlockMutex(&platform.threadsMutex);
		return nullptr; //Cannot create any more threads. All the thread slots are being used
	}
	
	ClearPointer(openThread);
	openThread->handle = CreateThread(
		NULL, //default security attributes
		0, //default stack size
		function,
		userPntr,
		0, //default creation flags
		&openThread->id
	);
	if (openThread->handle == NULL) //Failed to create thread for unknown reason
	{
		Win32_UnlockMutex(&platform.threadsMutex);
		return nullptr;
	}
	
	Win32_UnlockMutex(&platform.threadsMutex);
	return openThread;
}

bool _Win32_DestroyThread(Thread_t* thread)
{
	Assert(thread != nullptr);
	
	if (!Win32_LockMutex(&platform.threadsMutex, 10)) { return false; }
	
	if (thread->id == NULL) //Already stopped
	{
		Win32_UnlockMutex(&platform.threadsMutex);
		return true;
	}
	
	bool result = false;
	if (TerminateThread(thread->handle, 0x00) != 0)
	{
		ClearPointer(thread);
		result = true;
	}
	else
	{
		Assert(false); //Failed to terminate thread, not sure why
	}
	
	Win32_UnlockMutex(&platform.threadsMutex);
	return result;
}

void _Win32_TerminateAllThreads()
{
	for (u32 tIndex = 0; tIndex < ArrayCount(platform.threads); tIndex++)
	{
		Thread_t* threadPntr = &platform.threads[tIndex];
		if (threadPntr->handle != NULL)
		{
			_Win32_DestroyThread(threadPntr);
		}
	}
}

bool _Win32_IsThreadAlive(Thread_t* thread)
{
	Assert(thread != nullptr);
	
	if (!Win32_LockMutex(&platform.threadsMutex, 10)) { Assert(false); return true; }
	
	if (thread->id == NULL)
	{
		Win32_UnlockMutex(&platform.threadsMutex);
		return false;
	}
	
	DWORD waitResult = WaitForSingleObject(thread->handle, 0);
	bool result = (waitResult != WAIT_OBJECT_0);
	
	Win32_UnlockMutex(&platform.threadsMutex);
	return result;
}

Thread_t* _Win32_GetThreadById(DWORD threadId)
{
	for (u32 tIndex = 0; tIndex < MAX_NUM_THREADS; tIndex++)
	{
		Thread_t* threadPntr = &platform.threads[tIndex];
		if (threadPntr->id == threadId)
		{
			return threadPntr;
		}
	}
	return nullptr;
}

// +==============================+
// |    Win32_GetThisThreadId     |
// +==============================+
// ThreadId_t GetThisThreadId()
GetThisThreadId_DEFINITION(Win32_GetThisThreadId)
{
	DWORD result = GetCurrentThreadId();
	return (ThreadId_t)result;
}

// +==============================+
// |      Win32_IsMainThread      |
// +==============================+
// bool IsMainThread(ThreadId_t threadId)
IsMainThread_DEFINITION(Win32_IsMainThread) //pre-declared in win32_main.cpp
{
	return (threadId == MainThreadId);
}

// +==============================+
// |       Win32_SleepForMs       |
// +==============================+
// void SleepForMs(u32 numMs)
SleepForMs_DEFINITION(Win32_SleepForMs)
{
	Sleep(numMs);
}
