/*
File:   win32_files.cpp
Author: Taylor Robbins
Date:   11\04\2017
Description: 
	** Contains a bunch of functions that help the platform layer and the
	** application interact with the operating system's file system.
*/

// +==============================+
// |  Win32_CompleteRelativePath  |
// +==============================+
// char* CompleteRelativePath(MemoryArena_t* memArena, const char* path, u32 pathLength)
CompleteRelativePath_DEFINITION(Win32_CompleteRelativePath)
{
	Assert(memArena != nullptr);
	Assert(path != nullptr);
	
	bool isRelative = true;
	if (pathLength > 0 && (path[0] == '/' || path[0] == '\\')) { isRelative = false; }
	if (pathLength > 0 && (path[pathLength-1] == '/' || path[pathLength-1] == '\\')) { pathLength -= 1; } //cut trailing /
	if (pathLength >= 2 && IsCharClassAlphabet(path[0]) && path[1] == ':') { isRelative = false; }
	
	char* result = nullptr;
	if (isRelative)
	{
		result = MyStrCat(memArena, platform.baseResourceDirectory, MyStrLength32(platform.baseResourceDirectory), path, pathLength);
	}
	else
	{
		result = ArenaString(memArena, path, pathLength);
	}
	Assert(result != nullptr);
	StrReplaceInPlace(result, MyStrLength32(result), "/", "\\", 1);
	
	return result;
}

// +==============================+
// |    Win32_DoesFolderExist     |
// +==============================+
// bool DoesFolderExist(const char* folderPath)
DoesFolderExist_DEFINITION(Win32_DoesFolderExist)
{
	Assert(folderPath != nullptr);
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, MyStrLength32(folderPath));
	DWORD fileType = GetFileAttributesA(absoluteFolderPath);
	TempPopMark();
	if (fileType == INVALID_FILE_ATTRIBUTES)
	{
		return false; //doesn't exist
	}
	else if (fileType & FILE_ATTRIBUTE_DIRECTORY)
	{
		return true; //does exist
	}
	else
	{
		return false; //it's a file path
	}
}

// +==============================+
// |      Win32_CreateFolder      |
// +==============================+
// bool CreateFolder(const char* folderPath)
CreateFolder_DEFINITION(Win32_CreateFolder)
{
	Assert(folderPath != nullptr);
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, MyStrLength32(folderPath));
	if (CreateDirectoryA(absoluteFolderPath, NULL))
	{
		TempPopMark();
		return true;
	}
	else
	{
		PrintLine_E("Failed to create directory at \"%s\"", absoluteFolderPath);
		TempPopMark();
		return false;
	}
}

// +==============================+
// |     Win32_DoesFileExist      |
// +==============================+
// bool DoesFileExist(const char* filePath)
DoesFileExist_DEFINITION(Win32_DoesFileExist)
{
	Assert(filePath != nullptr);
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, MyStrLength32(filePath));
	DWORD fileType = GetFileAttributesA(absoluteFilePath);
	TempPopMark();
	if (fileType == INVALID_FILE_ATTRIBUTES)
	{
		return false;
	}
	else
	{
		return true;
	}
}

// +==============================+
// |     Win32_FreeFileMemory     |
// +==============================+
// void FreeFileMemory(FileInfo_t* fileInfo)
FreeFileMemory_DEFINITION(Win32_FreeFileMemory)
{
	Assert(fileInfo != nullptr);
	if (fileInfo->content != nullptr)
	{
		VirtualFree(fileInfo->content, 0, MEM_RELEASE);
		fileInfo->content = nullptr;
	}
}

// +==============================+
// |     Win32_ReadEntireFile     |
// +==============================+
// FileInfo_t ReadEntireFile(const char* filePath)
ReadEntireFile_DEFINITION(Win32_ReadEntireFile)
{
	Assert(filePath != nullptr);
	FileInfo_t result = {};
	
	MemoryArena_t* tempArena = GetThreadTempArena();
	Assert(tempArena != nullptr);
	ArenaPushMark(tempArena);
	char* absoluteFilePath = Win32_CompleteRelativePath(tempArena, filePath, MyStrLength32(filePath));
	HANDLE fileHandle = CreateFileA(absoluteFilePath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if (fileHandle != INVALID_HANDLE_VALUE)
	{
		LARGE_INTEGER fileSize;
		if (GetFileSizeEx(fileHandle, &fileSize))
		{
			//TODO: Define and use SafeTruncateUInt64 
			u32 fileSize32 = (u32)(fileSize.QuadPart);
			result.content = VirtualAlloc(0, fileSize32+1, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
			if (result.content)
			{
				DWORD bytesRead;
				if (ReadFile(fileHandle, result.content, fileSize32, &bytesRead, 0) &&
				   (fileSize32 == bytesRead))
				{
					// NOTE: File read successfully
					result.size = fileSize32;
					((u8*)result.content)[fileSize32] = '\0';
				}
				else
				{
					Win32_FreeFileMemory(&result);
					result.content = 0;
				}
			}
			else
			{
				PrintLine_E("Failed allocate space for %u byte file at \"%s\"", fileSize32, absoluteFilePath);
			}
		}
		else
		{
			PrintLine_E("Failed to get file size of file at \"%s\"", absoluteFilePath);
		}
		CloseHandle(fileHandle);
	}
	else
	{
		// PrintLine_E("Failed to access file for reading at \"%s\"", absoluteFilePath);
	}
	ArenaPopMark(tempArena);
	
	return result;
}

// +==============================+
// |    Win32_WriteEntireFile     |
// +==============================+
// bool WriteEntireFile(const char* filePath, const void* memory, u32 memorySize)
WriteEntireFile_DEFINITION(Win32_WriteEntireFile)
{
	Assert(filePath != nullptr);
	Assert(memory != nullptr);
	Assert(memorySize > 0);
	bool result = false;
	
	MemoryArena_t* tempArena = GetThreadTempArena();
	Assert(tempArena != nullptr);
	ArenaPushMark(tempArena);
	char* absoluteFilePath = Win32_CompleteRelativePath(tempArena, filePath, MyStrLength32(filePath));
	HANDLE fileHandle = CreateFileA(
		absoluteFilePath,      //Name of the file
		GENERIC_WRITE,         //Open for writing
		0,                     //Do not share
		NULL,                  //Default security
		CREATE_ALWAYS,         //Always overwrite
		FILE_ATTRIBUTE_NORMAL, //Default file attributes
		0                      //No Template File
	);
	if (fileHandle != INVALID_HANDLE_VALUE)
	{
		DWORD bytesWritten;
		if (WriteFile(fileHandle, memory, memorySize, &bytesWritten, 0))
		{
			if (bytesWritten == memorySize)
			{
				result = true;
			}
			else
			{
				PrintLine_W("Only wrote %u/%u bytes to file at \"%s\"", bytesWritten, memorySize, absoluteFilePath);
			}
		}
		else
		{
			PrintLine_E("Failed to write %u bytes to file at \"%s\"", memorySize, absoluteFilePath);
		}
		CloseHandle(fileHandle);
	}
	else
	{
		PrintLine_E("Failed to open file for writing at \"%s\"", absoluteFilePath);
	}
	ArenaPopMark(tempArena);
	
	return result;
}

// +==============================+
// |       Win32_DeleteFile       |
// +==============================+
// bool DeleteFile(const char* filePath)
DeleteFile_DEFINITION(Win32_DeleteFile)
{
	Assert(filePath != nullptr);
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, MyStrLength32(filePath));
	if (DeleteFile(absoluteFilePath))
	{
		TempPopMark();
		return true;
	}
	else
	{
		PrintLine_E("Failed to delete \"%s\"", absoluteFilePath);
		TempPopMark();
		return false;
	}
}

// +==============================+
// |        Win32_OpenFile        |
// +==============================+
// bool OpenFile(const char* filePath, bool openForWriting, OpenFile_t* openFileOut)
OpenFile_DEFINITION(Win32_OpenFile)
{
	Assert(openFileOut != nullptr);
	Assert(filePath != nullptr);
	
	MemoryArena_t* tempArena = GetThreadTempArena();
	Assert(tempArena != nullptr);
	ArenaPushMark(tempArena);
	char* absoluteFilePath = Win32_CompleteRelativePath(tempArena, filePath, MyStrLength32(filePath));
	HANDLE fileHandle = CreateFileA(
		absoluteFilePath, //Name of the file
		openForWriting ? GENERIC_WRITE : GENERIC_READ, //Open for reading or writing
		openForWriting ? 0 : FILE_SHARE_READ, //Do not share for writing but share for reading
		NULL, //Default security
		openForWriting ? CREATE_ALWAYS : OPEN_ALWAYS, //Create always or open/create
		FILE_ATTRIBUTE_NORMAL, //Default file attributes
		NULL //No Template File
	);
	
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		PrintLine_E("Couldn't open/create file: \"%s\"", absoluteFilePath);
		ArenaPopMark(tempArena);
		return false;
	}
	
	LONG newCursorPosHighOrder = 0;
	DWORD newCursorPos = SetFilePointer(fileHandle, 0, &newCursorPosHighOrder, FILE_END); //move to end
	if (newCursorPos == INVALID_SET_FILE_POINTER)
	{
		DWORD errorCode = GetLastError();
		PrintLine_E("Failed to seek to the end of the file when opened for writing \"%s\"!", absoluteFilePath);
		CloseHandle(fileHandle);
		ArenaPopMark(tempArena);
		return false;
	}
	u64 fileSize = (((u64)newCursorPosHighOrder << 32) | (u64)newCursorPos);
	u64 cursorIndex = fileSize;
	if (!openForWriting)
	{
		DWORD beginMove = SetFilePointer(fileHandle, 0, NULL, FILE_BEGIN); //move to beginning
		Assert(beginMove != INVALID_SET_FILE_POINTER);
		cursorIndex = 0;
	}
	
	openFileOut->isOpen = true;
	openFileOut->handle = fileHandle;
	openFileOut->openedForWriting = openForWriting;
	openFileOut->cursorIndex = cursorIndex;
	openFileOut->fileSize = fileSize;
	ArenaPopMark(tempArena);
	return true;
}

// +==============================+
// |      Win32_WriteToFile       |
// +==============================+
// bool WriteToFile(OpenFile_t* filePntr, const void* newData, u32 newDataSize)
WriteToFile_DEFINITION(Win32_WriteToFile)
{
	Assert(filePntr != nullptr);
	Assert(filePntr->isOpen);
	Assert(filePntr->handle != INVALID_HANDLE_VALUE);
	Assert(filePntr->openedForWriting);
	
	if (newDataSize == 0) return true;
	Assert(newData != nullptr);
	
	DWORD bytesWritten = 0;
	if (WriteFile(filePntr->handle, newData, newDataSize, &bytesWritten, 0))
	{
		// NOTE: File read successfully
		filePntr->cursorIndex += bytesWritten;
		filePntr->fileSize += bytesWritten;
		if (bytesWritten == newDataSize)
		{
			return true;
		}
		else
		{
			PrintLine_E("Not all bytes appended to file. %u/%u written", bytesWritten, newDataSize);
			return false;
		}
	}
	else
	{
		PrintLine_E("WriteFile failed. %u/%u written", bytesWritten, newDataSize);
		return false;
	}
}

// +==============================+
// |     Win32_MoveFileCursor     |
// +==============================+
// bool MoveFileCursor(OpenFile_t* filePntr, i64 moveAmount)
MoveFileCursor_DEFINITION(Win32_MoveFileCursor)
{
	Assert(filePntr != nullptr);
	Assert(filePntr->isOpen);
	Assert(filePntr->handle != INVALID_HANDLE_VALUE);
	if (moveAmount == 0) { return true; }
	if (moveAmount < 0 && (u64)(-moveAmount) > filePntr->cursorIndex) { return false; } //seek past beginning
	if (moveAmount > 0 && filePntr->cursorIndex + (u64)moveAmount > filePntr->fileSize) { return false; } //seek past end
	
	LARGE_INTEGER distanceToMoveLarge = {};
	MyMemCopy(&distanceToMoveLarge.LowPart, ((u8*)&moveAmount) + 0, 4);
	MyMemCopy(&distanceToMoveLarge.HighPart, ((u8*)&moveAmount) + 4, 4);
	
	LARGE_INTEGER newCursorPosLarge = {};
	BOOL setFilePointerResult = SetFilePointerEx(
		filePntr->handle,
		distanceToMoveLarge,
		&newCursorPosLarge,
		FILE_CURRENT
	);
	
	if (setFilePointerResult == 0)
	{
		DWORD errorCode = GetLastError();
		PrintLine_E("Couldn't seek %s%ld bytes in file from cursorIndex %ld", (moveAmount > 0) ? "+" : "", moveAmount, filePntr->cursorIndex);
		return false;
	}
	
	filePntr->cursorIndex += moveAmount;
	return true;
}

// +==============================+
// |   Win32_SeekToOffsetInFile   |
// +==============================+
// bool SeekToOffsetInFile(OpenFile_t* filePntr, u64 offset)
SeekToOffsetInFile_DEFINITION(Win32_SeekToOffsetInFile)
{
	Assert(filePntr != nullptr);
	Assert(filePntr->isOpen);
	Assert(filePntr->handle != INVALID_HANDLE_VALUE);
	i64 moveAmount = 0;
	if (filePntr->cursorIndex >= offset) { moveAmount = -(i64)(filePntr->cursorIndex - offset); }
	else { moveAmount = (i64)(offset - filePntr->cursorIndex); }
	return Win32_MoveFileCursor(filePntr, moveAmount);
}

// +==============================+
// |      Win32_ReadFromFile      |
// +==============================+
// bool ReadFromFile(OpenFile_t* filePntr, void* bytesOut, u32 numBytes, bool moveCursor)
ReadFromFile_DEFINITION(Win32_ReadFromFile)
{
	Assert(filePntr != nullptr);
	Assert(filePntr->isOpen);
	Assert(filePntr->handle != INVALID_HANDLE_VALUE);
	Assert(numBytes != 0);
	Assert(bytesOut != nullptr);
	
	if (filePntr->cursorIndex + numBytes > filePntr->fileSize) { return false; } //not enough bytes left in file
	
	DWORD numBytesRead = 0;
	BOOL readFileResult = ReadFile(
		filePntr->handle,
		bytesOut,
		numBytes,
		&numBytesRead,
		NULL
	);
	if (readFileResult == 0)
	{
		DWORD errorCode = GetLastError();
		PrintLine_E("Failed to read %u bytes from %u byte file at cursorIndex %u", numBytes, filePntr->fileSize, filePntr->cursorIndex);
		return false;
	}
	Assert(numBytesRead <= numBytes);
	if (numBytesRead < numBytes)
	{
		PrintLine_E("Read only %u/%u bytes from %u byte file at cursorIndex %u", numBytesRead, numBytes, filePntr->fileSize, filePntr->cursorIndex);
		return false;
	}
	
	filePntr->cursorIndex += numBytes;
	if (!moveCursor)
	{
		bool seekSuccess = Win32_MoveFileCursor(filePntr, -((i64)numBytes));
		Assert(seekSuccess);
	}
	return true;
}

// +==============================+
// |       Win32_CloseFile        |
// +==============================+
// void CloseFile(OpenFile_t* filePntr)
CloseFile_DEFINITION(Win32_CloseFile)
{
	if (filePntr == nullptr) return;
	if (filePntr->handle == INVALID_HANDLE_VALUE) return;
	if (filePntr->isOpen == false) return;
	
	CloseHandle(filePntr->handle);
	filePntr->handle = INVALID_HANDLE_VALUE;
	filePntr->isOpen = false;
}

// +==============================+
// |        Win32_MoveFile        |
// +==============================+
// bool MoveFile(const char* srcFilePath, const char* destFilePath, bool allowOverwrite, bool keepOriginal)
MoveFile_DEFINITION(Win32_MoveFile)
{
	Assert(srcFilePath != nullptr);
	Assert(destFilePath != nullptr);
	if (!Win32_DoesFileExist(srcFilePath)) { return false; }
	if (Win32_DoesFileExist(destFilePath))
	{
		if (!allowOverwrite) { return false; }
		bool deleteSuccess = Win32_DeleteFile(destFilePath);
		if (!deleteSuccess) { return false; }
	}
	FileInfo_t srcFile = Win32_ReadEntireFile(srcFilePath);
	if (srcFile.content == nullptr || srcFile.size == 0) { return false; }
	bool writeSuccess = Win32_WriteEntireFile(destFilePath, srcFile.content, srcFile.size);
	Win32_FreeFileMemory(&srcFile);
	if (!writeSuccess) { return false; }
	if (!keepOriginal)
	{
		//TODO: What should we do if this deletion fails? Can we somehow backtrack and bring back the old file? Should we do this deletion before the WriteEntireFile call?
		bool deleteSuccess = Win32_DeleteFile(srcFilePath);
		Assert(deleteSuccess); //right now we just crash because returning false would imply that the whole move failed but most of it actually succeeded
	}
	return true;
}

// +==============================+
// |       Win32_LaunchFile       |
// +==============================+
// bool LaunchFile(const char* filePath)
LaunchFile_DEFINITION(Win32_LaunchFile)
{
	Assert(filePath != nullptr);
	bool result = false;
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, MyStrLength32(filePath));
	
	u64 executeResult = (u64)ShellExecute(
		NULL,   //No parent window
		"open", //The action verb
		absoluteFilePath, //The target file
		NULL, //No parameters
		NULL, //Use default working directory
		SW_SHOWNORMAL //Show command is normal
	);
	
	if (executeResult > 32)
	{
		result = true;
	}
	else
	{
		switch (executeResult)
		{
			case ERROR_FILE_NOT_FOUND: { PrintLine_E("ShellExecute returned ERROR_FILE_NOT_FOUND for file at \"%s\"", absoluteFilePath); } break;
			default: { PrintLine_E("ShellExecute failed with result 0x%02X for file at \"%s\"", executeResult, absoluteFilePath); } break;
		};
	}
	
	TempPopMark();
	return result;
}

// +==============================+
// | Win32_GetNumFilesInDirectory |
// +==============================+
// u32 GetNumFilesInDirectory(const char* folderPath)
GetNumFilesInDirectory_DEFINITION(Win32_GetNumFilesInDirectory)
{
	Assert(folderPath != nullptr);
	
	u32 result = 0;
	
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, MyStrLength32(folderPath));
	u32 absoluteFolderPathLength = MyStrLength32(absoluteFolderPath);
	absoluteFolderPath = MyStrCat(TempArena, absoluteFolderPath, absoluteFolderPathLength, "\\*", 2); //add "\*" at the end
	absoluteFolderPathLength += 2;
	// PrintLine_D("Searching through folder \"%.*s\"", absoluteFolderPathLength, absoluteFolderPath);
	
	// PrintLine_D("Counting files in \"%s\"", absoluteFolderPath);
	
	WIN32_FIND_DATA findData = {};
	HANDLE findHandle = FindFirstFile(absoluteFolderPath, &findData);
	if (findHandle == INVALID_HANDLE_VALUE)
	{
		DWORD error = GetLastError();
		if (error != ERROR_PATH_NOT_FOUND) { PrintLine_E("FindFirstFile error: %u", error); }
		TempPopMark();
		return 0;
	}
	
	while (true)
	{
		if (!IsFlagSet(findData.dwFileAttributes, FILE_ATTRIBUTE_DIRECTORY))
		{
			// PrintLine_D("Found file \"%s\"", findData.cFileName);
			result++;
		}
		if (FindNextFile(findHandle, &findData) == 0)
		{
			// WriteLine_D("Found end of files");
			break;
		}
	}
	FindClose(findHandle);
	
	TempPopMark();
	return result;
}

// +==============================+
// |   Win32_GetFileInDirectory   |
// +==============================+
// char* GetFileInDirectory(MemoryArena_t* arenaPntr, const char* folderPath, u32 index)
GetFileInDirectory_DEFINITION(Win32_GetFileInDirectory)
{
	Assert(arenaPntr != nullptr);
	Assert(folderPath != nullptr);
	
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, MyStrLength32(folderPath));
	u32 absoluteFolderPathLength = MyStrLength32(absoluteFolderPath);
	absoluteFolderPath = MyStrCat(TempArena, absoluteFolderPath, absoluteFolderPathLength, "\\*", 2); //add "\*" at the end
	absoluteFolderPathLength += 2;
	// PrintLine_D("Getting from folder \"%.*s\"", absoluteFolderPathLength, absoluteFolderPath);
	
	// PrintLine_D("Finding file %u in \"%s\"", index, absoluteFolderPath);
	
	WIN32_FIND_DATA findData = {};
	HANDLE findHandle = FindFirstFile(absoluteFolderPath, &findData);
	if (findHandle == INVALID_HANDLE_VALUE)
	{
		DWORD error = GetLastError();
		if (error != ERROR_PATH_NOT_FOUND) { PrintLine_E("FindFirstFile error: %u", error); }
		TempPopMark();
		return 0;
	}
	
	char* result = nullptr;
	u32 fIndex = 0;
	while (true)
	{
		if (!IsFlagSet(findData.dwFileAttributes, FILE_ATTRIBUTE_DIRECTORY))
		{
			// PrintLine_D("Found file \"%s\"", findData.cFileName);
			if (fIndex == index)
			{
				result = ArenaNtString(arenaPntr, findData.cFileName);
				break;
			}
			fIndex++;
		}
		if (FindNextFile(findHandle, &findData) == 0)
		{
			// WriteLine_D("Found end of files");
			break;
		}
	}
	FindClose(findHandle);
	
	TempPopMark();
	return result;
}

// +================================+
// | Win32_GetNumFoldersInDirectory |
// +================================+
// u32 GetNumFoldersInDirectory(const char* folderPath)
GetNumFoldersInDirectory_DEFINITION(Win32_GetNumFoldersInDirectory)
{
	Assert(folderPath != nullptr);
	
	u32 result = 0;
	
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, MyStrLength32(folderPath));
	u32 absoluteFolderPathLength = MyStrLength32(absoluteFolderPath);
	absoluteFolderPath = MyStrCat(TempArena, absoluteFolderPath, absoluteFolderPathLength, "\\*", 2); //add "\*" at the end
	absoluteFolderPathLength += 2;
	// PrintLine_D("Searching through folder \"%.*s\"", absoluteFolderPathLength, absoluteFolderPath);
	
	// PrintLine_D("Counting files in \"%s\"", absoluteFolderPath);
	
	WIN32_FIND_DATA findData = {};
	HANDLE findHandle = FindFirstFile(absoluteFolderPath, &findData);
	if (findHandle == INVALID_HANDLE_VALUE)
	{
		DWORD error = GetLastError();
		if (error != ERROR_PATH_NOT_FOUND && error != ERROR_FILE_NOT_FOUND) { PrintLine_E("FindFirstFile error: %u for path \"%s\"", error, absoluteFolderPath); }
		TempPopMark();
		return 0;
	}
	
	while (true)
	{
		if (IsFlagSet(findData.dwFileAttributes, FILE_ATTRIBUTE_DIRECTORY) && MyStrCompareNt(findData.cFileName, ".") != 0 && MyStrCompareNt(findData.cFileName, "..") != 0)
		{
			// PrintLine_D("Found folder \"%s\"", findData.cFileName);
			result++;
		}
		if (FindNextFile(findHandle, &findData) == 0)
		{
			// WriteLine_D("Found end of folders");
			break;
		}
	}
	FindClose(findHandle);
	
	TempPopMark();
	return result;
}

// +==============================+
// |  Win32_GetFolderInDirectory  |
// +==============================+
// char* GetFolderInDirectory(MemoryArena_t* arenaPntr, const char* folderPath, u32 index)
GetFolderInDirectory_DEFINITION(Win32_GetFolderInDirectory)
{
	Assert(arenaPntr != nullptr);
	Assert(folderPath != nullptr);
	
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, MyStrLength32(folderPath));
	u32 absoluteFolderPathLength = MyStrLength32(absoluteFolderPath);
	absoluteFolderPath = MyStrCat(TempArena, absoluteFolderPath, absoluteFolderPathLength, "\\*", 2); //add "\*" at the end
	absoluteFolderPathLength += 2;
	// PrintLine_D("Getting from folder \"%.*s\"", absoluteFolderPathLength, absoluteFolderPath);
	
	// PrintLine_D("Finding file %u in \"%s\"", index, absoluteFolderPath);
	
	WIN32_FIND_DATA findData = {};
	HANDLE findHandle = FindFirstFile(absoluteFolderPath, &findData);
	if (findHandle == INVALID_HANDLE_VALUE)
	{
		DWORD error = GetLastError();
		if (error != ERROR_PATH_NOT_FOUND) { PrintLine_E("FindFirstFile error: %u", error); }
		TempPopMark();
		return 0;
	}
	
	char* result = nullptr;
	u32 fIndex = 0;
	while (true)
	{
		if (IsFlagSet(findData.dwFileAttributes, FILE_ATTRIBUTE_DIRECTORY) && MyStrCompareNt(findData.cFileName, ".") != 0 && MyStrCompareNt(findData.cFileName, "..") != 0)
		{
			// PrintLine_D("Found folder \"%s\"", findData.cFileName);
			if (fIndex == index)
			{
				result = ArenaNtString(arenaPntr, findData.cFileName);
				break;
			}
			fIndex++;
		}
		if (FindNextFile(findHandle, &findData) == 0)
		{
			// WriteLine_D("Found end of folders");
			break;
		}
	}
	FindClose(findHandle);
	
	TempPopMark();
	return result;
}

// +==============================+
// |     Win32_ShowSourceFile     |
// +==============================+
// bool ShowSourceFile(const char* filePath, u32 lineNumber)
ShowSourceFile_DEFINITION(Win32_ShowSourceFile)
{
	Assert(filePath != nullptr);
	bool result = false;
	if (MyStrLength32(filePath) == 0) { return false; }
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, MyStrLength32(filePath));
	char* commandStr = TempPrint("%s:%u", absoluteFilePath, lineNumber);
	
	u64 executeResult = (u64)ShellExecute(
		NULL,   //No parent window
		"open", //The action verb
		"\"C:\\Program Files\\Sublime Text 3\\subl.exe\"", //The target file
		commandStr, //No parameters
		NULL, //Use default working directory
		SW_SHOWNORMAL //Show command is normal
	);
	
	if (executeResult > 32)
	{
		result = true;
	}
	else
	{
		switch (executeResult)
		{
			case ERROR_FILE_NOT_FOUND:   { WriteLine_E("ShellExecute returned ERROR_FILE_NOT_FOUND"); } break;
			case ERROR_PATH_NOT_FOUND:   { WriteLine_E("ShellExecute returned ERROR_PATH_NOT_FOUND"); } break;
			case ERROR_BAD_FORMAT:       { WriteLine_E("ShellExecute returned ERROR_BAD_FORMAT"); } break;
			case SE_ERR_ACCESSDENIED:    { WriteLine_E("ShellExecute returned SE_ERR_ACCESSDENIED"); } break;
			case SE_ERR_ASSOCINCOMPLETE: { WriteLine_E("ShellExecute returned SE_ERR_ASSOCINCOMPLETE"); } break;
			case SE_ERR_DDEBUSY:         { WriteLine_E("ShellExecute returned SE_ERR_DDEBUSY"); } break;
			case SE_ERR_DDEFAIL:         { WriteLine_E("ShellExecute returned SE_ERR_DDEFAIL"); } break;
			case SE_ERR_DDETIMEOUT:      { WriteLine_E("ShellExecute returned SE_ERR_DDETIMEOUT"); } break;
			case SE_ERR_DLLNOTFOUND:     { WriteLine_E("ShellExecute returned SE_ERR_DLLNOTFOUND"); } break;
			// case SE_ERR_FNF:             { WriteLine_E("ShellExecute returned SE_ERR_FNF"); } break;
			case SE_ERR_NOASSOC:         { WriteLine_E("ShellExecute returned SE_ERR_NOASSOC"); } break;
			case SE_ERR_OOM:             { WriteLine_E("ShellExecute returned SE_ERR_OOM"); } break;
			// case SE_ERR_PNF:             { WriteLine_E("ShellExecute returned SE_ERR_PNF"); } break;
			case SE_ERR_SHARE:           { WriteLine_E("ShellExecute returned SE_ERR_SHARE"); } break;
			
			default:
			{
				PrintLine_E("ShellExecute failed with result 0x%02X for file at \"%s\"", executeResult, absoluteFilePath);
			} break;
		};
	}
	
	TempPopMark();
	return result;
}

// +==============================+
// |        Win32_ShowFile        |
// +==============================+
// bool ShowFile(const char* filePath)
ShowFile_DEFINITION(Win32_ShowFile)
{
	Assert(filePath != nullptr);
	//TODO: Add proper support for relative path completion
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, MyStrLength32(filePath));
	char* commandStr = TempPrint("C:\\Windows\\explorer.exe /select,\"%s\"", absoluteFilePath);
	
	int systemResult = system(commandStr);
	
	if (systemResult >= 0)
	{
		TempPopMark();
		return true;
	}
	else
	{
		PrintLine_D("Command \"%s\" returned an error: %d", commandStr, errno);
		TempPopMark();
		return false;
	}
}

// +==============================+
// |  Win32_GetSpecialFolderPath  |
// +==============================+
// char* GetSpecialFolderPath(MemoryArena_t* arenaPntr, SpecialFolder_t folder, const char* applicationName)
GetSpecialFolderPath_DEFINITION(Win32_GetSpecialFolderPath)
{
	Assert(arenaPntr != nullptr);
	char pathBuffer[MAX_PATH+1];
	MyMemSet(&pathBuffer[0], 0xFF, MAX_PATH);
	pathBuffer[MAX_PATH] = '\0';
	switch (folder)
	{
		case SpecialFolder_Saves:
		{
			BOOL getFolderPathResult = SHGetSpecialFolderPathA(
				NULL, //hwnd
				&pathBuffer[0], //pszPath
				CSIDL_APPDATA, //csidl
				false //fCreate
			);
			if (getFolderPathResult)
			{
				Assert(BufferIsNullTerminated(pathBuffer, ArrayCount(pathBuffer)));
				return ArenaPrint(arenaPntr, "%s\\%s\\Saves\\", pathBuffer, applicationName);
			}
			else
			{
				WriteLine_E("Couldn't get the path to the AppData directory");
				return nullptr;
			}
		} break;
		
		case SpecialFolder_Settings:
		{
			BOOL getFolderPathResult = SHGetSpecialFolderPathA(
				NULL, //hwnd
				&pathBuffer[0], //pszPath
				CSIDL_APPDATA, //csidl
				false //fCreate
			);
			if (getFolderPathResult)
			{
				Assert(BufferIsNullTerminated(pathBuffer, ArrayCount(pathBuffer)));
				return ArenaPrint(arenaPntr, "%s\\%s\\", pathBuffer, applicationName);
			}
			else
			{
				WriteLine_E("Couldn't get the path to the AppData directory");
				return nullptr;
			}
		} break;
		
		case SpecialFolder_Screenshots:
		{
			BOOL getFolderPathResult = SHGetSpecialFolderPathA(
				NULL, //hwnd
				&pathBuffer[0], //pszPath
				CSIDL_MYPICTURES, //csidl
				false //fCreate
			);
			if (getFolderPathResult)
			{
				Assert(BufferIsNullTerminated(pathBuffer, ArrayCount(pathBuffer)));
				return ArenaPrint(arenaPntr, "%s\\%s\\", pathBuffer, applicationName);
			}
			else
			{
				WriteLine_E("Couldn't get the path to the MyPictures directory");
				return nullptr;
			}
		} break;
		
		case SpecialFolder_CustomLevels:
		{
			BOOL getFolderPathResult = SHGetSpecialFolderPathA(
				NULL, //hwnd
				&pathBuffer[0], //pszPath
				CSIDL_APPDATA, //csidl
				false //fCreate
			);
			if (getFolderPathResult)
			{
				Assert(BufferIsNullTerminated(pathBuffer, ArrayCount(pathBuffer)));
				return ArenaPrint(arenaPntr, "%s\\%s\\Levels\\", pathBuffer, applicationName);
			}
			else
			{
				WriteLine_E("Couldn't get the path to the AppData directory");
				return nullptr;
			}
		} break;
		
		case SpecialFolder_Share:
		{
			BOOL getFolderPathResult = SHGetSpecialFolderPathA(
				NULL, //hwnd
				&pathBuffer[0], //pszPath
				CSIDL_MYDOCUMENTS, //csidl
				false //fCreate
			);
			if (getFolderPathResult)
			{
				Assert(BufferIsNullTerminated(pathBuffer, ArrayCount(pathBuffer)));
				return ArenaPrint(arenaPntr, "%s\\%s\\", pathBuffer, applicationName);
			}
			else
			{
				WriteLine_E("Couldn't get the path to the MyDocuments directory");
				return nullptr;
			}
		} break;
		
		default:
		{
			Assert(false);
			return nullptr;
		} break;
	}
}
