/*
File:   win32_tasks.h
Author: Taylor Robbins
Date:   01\26\2020
*/

#ifndef _WIN_32_TASKS_H
#define _WIN_32_TASKS_H

struct ThreadedTask_t
{
	bool filled;
	u32 id;
	bool isApplicationTask;
	volatile bool started;
	Thread_t* thread;
	volatile bool finished;
	PerfTime_t startTime;
	PerfTime_t endTime;
	
	//Application Filled
	u32 taskNumber;
	u64 taskNumber64;
	u8 taskType;
	u8 resultCode;
	void* callback;
	
	u32 taskSize1;
	void* taskPntr1;
	
	u32 taskSize2;
	void* taskPntr2;
	
	u32 resultSize1;
	void* resultPntr1;
	
	u32 resultSize2;
	void* resultPntr2;
};

struct TaskThreadInfo_t
{
	u32 index;
	MemoryArena_t tempArena;
	u32 workingMemSize;
	void* workingMemPntr;
	u32 currentTaskId;
	ThreadedTask_t* currentTaskPntr;
};

#endif //  _WIN_32_TASKS_H
