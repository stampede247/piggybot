/*
File:   win32_irc.h
Author: Taylor Robbins
Date:   10\23\2020
*/

#ifndef _WIN_32_IRC_H
#define _WIN_32_IRC_H

union Ipv4Address_t
{
	u8 values[4];
	struct
	{
		u8 value0;
		u8 value1;
		u8 value2;
		u8 value3;
	};
};
union Ipv6Address_t
{
	u16 values[4];
	struct
	{
		u16 value0;
		u16 value1;
		u16 value2;
		u16 value3;
	};
};

typedef enum
{
	IrcConnectionState_None = 0,
	IrcConnectionState_DnsResolve,
	IrcConnectionState_Connecting,
	IrcConnectionState_Connected,
	IrcConnectionState_Error,
	IrcConnectionState_NumStates,
} IrcConnectionState_t;

struct IrcConnection_t
{
	bool isValid;
	MemoryArena_t* allocArena;
	char* url;
	u32 port;
	
	IrcConnectionState_t state;
	
	bool isIpv6;
	union
	{
		Ipv4Address_t ipAddress4;
		Ipv6Address_t ipAddress6;
	};
	
	SOCKET socket;
	
	Fifo_t readFifo;
	Fifo_t writeFifo;
};

const char* GetIrcConnectionStateStr(IrcConnectionState_t state)
{
	switch (state)
	{
		case IrcConnectionState_None:       return "None";
		case IrcConnectionState_DnsResolve: return "DnsResolve";
		case IrcConnectionState_Connecting: return "Connecting";
		case IrcConnectionState_Connected:  return "Connected";
		case IrcConnectionState_Error:      return "Error";
		default:                            return "Unknown";
	}
}

#endif //  _WIN_32_IRC_H
