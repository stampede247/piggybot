/*
File:   app_console_arena.h
Author: Taylor Robbins
Date:   06\23\2020
*/

#ifndef _APP_CONSOLE_ARENA_H
#define _APP_CONSOLE_ARENA_H

struct ConsoleLine_t
{
	ConsoleLine_t* prev;
	ConsoleLine_t* next;
	
	u32 filePathLength;
	u32 funcNameLength;
	u32 messageLength;
	
	u8 flags;
	u64 time;
	DbgLevel_t dbgLevel;
	u32 fileLineNumber;
	u32 gutterNumber;
	ThreadId_t threadId;
	u32 threadIndex;
	u32 taskId;
	
	rec drawRec;
	r32 gutterNumWidth;
	r32 fileNameWidth;
	r32 lineNumWidth;
	r32 funcNameWidth;
	r32 taskNumWidth;
	r32 messageOffsetX;
};

typedef void ConsoleLineRemovedCallback_f(const struct ConsoleArena_t* arena, const ConsoleLine_t* removedLine, void* userPntr);

struct ConsoleArena_t
{
	bool isExternallyAllocated;
	MemoryArena_t* allocArena;
	
	bool saveFilePaths;
	bool saveFunctionNames;
	
	u32 fifoSize;
	u32 fifoUsage;
	u8* fifoSpace;
	
	u32 lineBuildSize;
	u32 lineBuildUsage;
	ConsoleLine_t lineBuildInfo;
	char* lineBuildSpace;
	
	u32 printBufferSize;
	char* printBuffer;
	MemoryArena_t* printArena;
	
	u32 numLines;
	u32 nextGutterNumber;
	ConsoleLine_t* firstLine;
	ConsoleLine_t* lastLine;
	
	ConsoleLineRemovedCallback_f* removeLineCallback;
	void* removeLineCallbackUserPntr;
};

ConsoleLine_t* ConsoleArenaProcessChars(ConsoleArena_t* arena, u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, bool addNewLine, const char* message);
ConsoleLine_t* ConsoleArenaPrintAndProcessVa(ConsoleArena_t* arena, u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, bool addNewLine, const char* formatStr, va_list args, u32 printSize);
ConsoleLine_t* ConsoleArenaPrintAndProcess(ConsoleArena_t* arena, u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, bool addNewLine, const char* formatStr, ...);

#endif //  _APP_CONSOLE_ARENA_H
