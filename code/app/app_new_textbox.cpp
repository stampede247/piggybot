/*
File:   app_new_textbox.cpp
Author: Taylor Robbins
Date:   08\12\2020
Description: 
	** Just a standard text box that handles character input and editing with scrolling
	** capabilities and an option for multiline version
*/

void DestroyNewTb(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	if (tb->text != nullptr)
	{
		Assert(tb->allocArena != nullptr);
		ArenaPop(tb->allocArena, tb->text);
	}
	ClearPointer(tb);
}

void InitNewTb(NewTextbox_t* tb, MemoryArena_t* memArena, u32 textLength = 0, bool growingTextAllocation = true, u32 allocChunkSize = NEW_TB_DEF_ALLOC_CHUNK_SIZE)
{
	Assert(tb != nullptr);
	Assert(allocChunkSize > 0 || !growingTextAllocation);
	ClearPointer(tb);
	
	tb->allocArena = memArena;
	tb->growingTextAllocation = growingTextAllocation;
	tb->allocChunkSize = allocChunkSize;
	tb->mouseHoverId = app->nextTextboxId;
	app->nextTextboxId++;
	
	Assert(textLength > 0 || growingTextAllocation);
	u32 allocSize = textLength+1;
	if (growingTextAllocation) { allocSize = CeilToU32(allocSize, allocChunkSize); Assert(allocSize >= textLength); }
	tb->text = PushArray(memArena, char, allocSize);
	Assert(tb->text != nullptr);
	MyMemSet(tb->text, 0x00, allocSize);
	tb->allocLength = allocSize;
	tb->textLength = 0;
	
	tb->type = TextboxType_Default;
	tb->minValueEnabled = false;
	tb->maxValueEnabled = false;
	tb->validateTextEveryStroke = false;
	tb->autoCorrectTextFormat = true;
	tb->deselectOnEnter = true;
	
	tb->fontIndex = GetFontIndexByName(debugFont);
	tb->fontScale = 1.0f;
	tb->textPadding = NewVec2(5, 10);
	tb->scrollLag = NEW_TB_DEF_SCROLL_LAG;
	r32 properHeight = RoundR32(GetFont_(tb->fontIndex, false)->lineHeight*tb->fontScale + tb->textPadding.y*2);
	tb->drawRec.height = properHeight;
	
	//TODO: Tune these default colors
	tb->backColorInactive = PalBackground;
	tb->textColorInactive = White;
	tb->borderColorInactive = PalBackgroundLighter;
	tb->selectionColorInactive = ColorTransparent(White, 0.5f);
	
	tb->backColorActive = PalBackground;
	tb->textColorActive = White;
	tb->borderColorActive = White;
	tb->selectionColorActive = White;
	
	tb->backColorInvalid = PalBackground;
	tb->textColorInvalid = PalRed;
	tb->borderColorInvalid = PalRed;
	tb->selectionColorInvalid = PalRed;
}
void InitNewTbNt(NewTextbox_t* tb, MemoryArena_t* memArena, const char* initialText, bool growingTextAllocation = true, u32 allocChunkSize = NEW_TB_DEF_ALLOC_CHUNK_SIZE)
{
	Assert(tb != nullptr);
	Assert(initialText != nullptr);
	u32 initialTextLength = MyStrLength32(initialText);
	
	InitNewTb(tb, memArena, initialTextLength, growingTextAllocation, allocChunkSize);
	
	Assert(tb->text != nullptr);
	Assert(tb->allocLength >= initialTextLength+1);
	
	MyMemCopy(tb->text, initialText, initialTextLength+1);
	tb->textLength = initialTextLength;
	tb->remeasureText = true;
	tb->cursorStart = initialTextLength;
	tb->cursorEnd = initialTextLength;
	tb->scrollToCursor = true;
}

bool IsCharAllowedInTextboxType(TextboxType_t type, char c)
{
	if (type == TextboxType_Default) { return true; }
	if (type == TextboxType_Integer)
	{
		if (IsCharClassNumeric(c)) { return true; }
		if (c == '-') { return true; }
		return false;
	}
	if (type == TextboxType_UnsignedInteger)
	{
		if (IsCharClassNumeric(c)) { return true; }
		return false;
	}
	if (type == TextboxType_FloatingPoint)
	{
		if (IsCharClassNumeric(c)) { return true; }
		if (c == '.' || c == ',' || c == '-') { return true; }
		return false;
	}
	if (type == TextboxType_UnsignedFloatingPoint)
	{
		if (IsCharClassNumeric(c)) { return true; }
		if (c == '.' || c == ',') { return true; }
		return false;
	}
	return true;
}

void SetNewTbFont_(NewTextbox_t* tb, u32 fontIndex, r32 fontScale, bool setTbHeight)
{
	Assert(tb != nullptr);
	if (tb->fontIndex != fontIndex || tb->fontScale != fontScale)
	{
		tb->fontIndex = fontIndex;
		tb->fontScale = fontScale;
		tb->remeasureText = true;
	}
	if (setTbHeight)
	{
		r32 properHeight = RoundR32(GetFont_(tb->fontIndex, false)->lineHeight*tb->fontScale + tb->textPadding.y*2);
		if (tb->drawRec.height != properHeight)
		{
			tb->drawRec.height = properHeight;
			tb->remeasureText = true;
		}
	}
}
#define SetNewTbFont(tb, fontName, fontScale, setTbHeight) SetNewTbFont_((tb), GetFontIndexByName(fontName), (fontScale), (setTbHeight));

void SetNewTbBackColor(NewTextbox_t* tb, Color_t inactive, Color_t active)
{
	Assert(tb != nullptr);
	tb->backColorInactive = inactive;
	tb->backColorActive = active;
}
void SetNewTbTextColor(NewTextbox_t* tb, Color_t inactive, Color_t active, bool useAsSelectionColor = true)
{
	Assert(tb != nullptr);
	tb->textColorInactive = inactive;
	tb->textColorActive = active;
	if (useAsSelectionColor)
	{
		tb->selectionColorInactive = ColorTransparent(inactive, 0.5f);
		tb->selectionColorActive = ColorTransparent(active, 0.5f);
	}
}
void SetNewTbBorderColor(NewTextbox_t* tb, Color_t inactive, Color_t active)
{
	Assert(tb != nullptr);
	tb->borderColorInactive = inactive;
	tb->borderColorActive = active;
}
void SetNewTbSelectionColor(NewTextbox_t* tb, Color_t inactive, Color_t active)
{
	Assert(tb != nullptr);
	tb->selectionColorInactive = inactive;
	tb->selectionColorActive = active;
}

Color_t GetCurrentNewTbBackColor(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	return (tb->textIsInvalid ? tb->backColorInvalid : (tb->isSelected ? tb->backColorActive : tb->backColorInactive));
}
Color_t GetCurrentNewTbTextColor(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	return (tb->textIsInvalid ? tb->textColorInvalid : (tb->isSelected ? tb->textColorActive : tb->textColorInactive));
}
Color_t GetCurrentNewTbBorderColor(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	return (tb->textIsInvalid ? tb->borderColorInvalid : (tb->isSelected ? tb->borderColorActive : tb->borderColorInactive));
}
Color_t GetCurrentNewTbSelectionColor(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	return (tb->textIsInvalid ? tb->selectionColorInvalid : (tb->isSelected ? tb->selectionColorActive : tb->selectionColorInactive));
}

void SetNewTbDrawRec(NewTextbox_t* tb, rec drawRec, bool dontChangeHeight = false)
{
	Assert(tb != nullptr);
	if (dontChangeHeight) { drawRec.height = tb->drawRec.height; }
	if (tb->drawRec != drawRec)
	{
		bool firstLocation = (tb->drawRec.size == Vec2_Zero);
		tb->drawRec = drawRec;
		if (tb->multiline) { tb->remeasureText = true; }
		tb->scrollToCursor = firstLocation;
	}
}

void NewTbExpandForTextLength(NewTextbox_t* tb, u32 newTextLength)
{
	Assert(tb != nullptr);
	Assert(tb->growingTextAllocation);
	Assert(tb->allocArena != nullptr);
	Assert(tb->allocChunkSize > 0);
	if (newTextLength+1 <= tb->allocLength) { return; } //nothing to do
	u32 newAllocSize = newTextLength+1;
	newAllocSize = CeilToU32(newAllocSize, tb->allocChunkSize);
	Assert(newAllocSize >= newTextLength);
	char* newTextPntr = PushArray(tb->allocArena, char, newAllocSize);
	Assert(newTextPntr != nullptr);
	MyMemSet(newTextPntr, 0x00, newAllocSize);
	if (tb->text != nullptr)
	{
		if (tb->textLength > 0)
		{
			Assert(tb->textLength+1 <= newAllocSize);
			MyMemCopy(newTextPntr, tb->text, tb->textLength+1);
		}
		ArenaPop(tb->allocArena, tb->text);
	}
	tb->text = newTextPntr;
	tb->allocLength = newAllocSize;
}

void SetNewTbText(NewTextbox_t* tb, const char* text, u32 textLength)
{
	Assert(tb != nullptr);
	Assert(text != nullptr || textLength == 0);
	Assert((tb->allocLength > 0 && tb->text != nullptr) || (tb->growingTextAllocation && tb->allocArena != nullptr));
	
	if (textLength == 0)
	{
		if (tb->textLength == 0) { return; } //nothing to do
		tb->textLength = 0;
		Assert(tb->text != nullptr && tb->allocLength >= 1);
		tb->text[0] = '\0';
		tb->remeasureText = true;
		tb->scrollToCursor = false;
		tb->selectionChanged = false;
		tb->scroll = Vec2_Zero;
		tb->scrollGoto = tb->scroll;
	}
	else
	{
		if (textLength == tb->textLength && MyStrCompare(tb->text, text, textLength) == 0) { return; } //nothing to do
		
		if (tb->growingTextAllocation)
		{
			NewTbExpandForTextLength(tb, textLength);
		}
		Assert(tb->allocLength > 0 && tb->text != nullptr);
		
		if (textLength > tb->allocLength-1) { textLength = tb->allocLength-1; }
		Assert(textLength+1 <= tb->allocLength);
		
		MyMemCopy(tb->text, text, textLength);
		tb->text[textLength] = '\0';
		tb->textLength = textLength;
		tb->cursorStart = textLength;
		tb->cursorEnd = textLength;
		tb->remeasureText = true;
		tb->scrollToCursor = true;
		tb->selectionChanged = false;
	}
}
void SetNewTbTextNt(NewTextbox_t* tb, const char* nullTermString)
{
	Assert(tb != nullptr);
	Assert(nullTermString != nullptr);
	SetNewTbText(tb, nullTermString, MyStrLength32(nullTermString));
}
void ClearNewTb(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	SetNewTbText(tb, "", 0);
}

void NewTbRemeasureText(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	tb->maxWidth = (tb->drawRec.width - tb->textPadding.x*2);
	if (tb->textLength > 0)
	{
		if (tb->multiline)
		{
			tb->textSize = MeasureFormattedString(GetFont_(tb->fontIndex, false), tb->text, tb->textLength, tb->maxWidth, true);
		}
		else
		{
			tb->textSize = MeasureString(GetFont_(tb->fontIndex, false), tb->text, tb->textLength) * tb->fontScale;
		}
	}
	else { tb->textSize = Vec2_Zero; }
	tb->remeasureText = false;
}

void NewTbCaptureMouse(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	if (IsMouseInside(tb->drawRec) || tb->clickIsSelectingText) { CaptureMousePrint("Textbox%u", tb->mouseHoverId); }
}

bool NewTbRemoveInvalidChars(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	bool result = false;
	for (u32 cIndex = 0; cIndex < tb->textLength; )
	{
		if (!IsCharAllowedInTextboxType(tb->type, tb->text[cIndex]))
		{
			RemoveStringRegionInPlace(tb->text, tb->textLength, cIndex, cIndex+1);
			if (tb->cursorEnd > cIndex) { tb->cursorEnd--; }
			if (tb->cursorStart > cIndex) { tb->cursorStart--; }
			tb->remeasureText = true;
			tb->scrollToCursor = true;
			result = true;
			tb->textLength--;
			//don't increment cIndex
		}
		else { cIndex++; }
	}
	return result;
}
bool NewTbHasInvalidChars(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	for (u32 cIndex = 0; cIndex < tb->textLength; cIndex++)
	{
		if (!IsCharAllowedInTextboxType(tb->type, tb->text[cIndex])) { return true; }
	}
	return false;
}

//Returns true when the text is invalid
bool NewTbValidateText(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	tb->textIsInvalid = false;
	bool result = false;
	bool corrected = false;
	if (tb->type == TextboxType_Default) { return result; }
	
	WriteLine_D("Validating");
	
	if (tb->autoCorrectTextFormat) { if (NewTbRemoveInvalidChars(tb)) { result = true; } }
	else { if (NewTbHasInvalidChars(tb)) { result = true; } }
	
	if (tb->type == TextboxType_UnsignedInteger)
	{
		u32 currentValue;
		if (TryParseU32(tb->text, tb->textLength, &currentValue))
		{
			PrintLine_D("Valid U32: %u", currentValue);
			if (tb->minValueEnabled && currentValue < tb->minValueU32)
			{
				WriteLine_D("Less than min");
				result = true;
				if (tb->autoCorrectTextFormat)
				{
					char* minValueText = TempPrint("%u", tb->minValueU32);
					Assert(minValueText != nullptr);
					SetNewTbText(tb, minValueText, MyStrLength32(minValueText));
					corrected = true;
				}
			}
			else if (tb->maxValueEnabled && currentValue > tb->maxValueU32)
			{
				WriteLine_D("More than max");
				result = true;
				if (tb->autoCorrectTextFormat)
				{
					char* maxValueText = TempPrint("%u", tb->maxValueU32);
					Assert(maxValueText != nullptr);
					SetNewTbText(tb, maxValueText, MyStrLength32(maxValueText));
					corrected = true;
				}
			}
			else
			{
				if (tb->minValueEnabled) { PrintLine_D("> min: %u", tb->minValueU32); }
				if (tb->maxValueEnabled) { PrintLine_D("< max: %u", tb->maxValueU32); }
			}
		}
		else
		{
			WriteLine_D("Invalid U32");
			result = true;
			if (tb->autoCorrectTextFormat)
			{
				//NOTE: The most likely scenario for invalid (assuming the contents only contain numberic characters) is
				//      that there are too many numbers present for a valid U32 value. So we will make the box contain the maximum value in this case
				if (tb->maxValueEnabled)
				{
					WriteLine_D("Correcting to max value");
					char* maxValueText = TempPrint("%u", tb->maxValueU32);
					Assert(maxValueText != nullptr);
					SetNewTbText(tb, maxValueText, MyStrLength32(maxValueText));
					corrected = true;
				}
				else
				{
					WriteLine_D("Correcting to max u32");
					char* maxValueText = TempPrint("%u", 0xFFFFFFFFUL);
					Assert(maxValueText != nullptr);
					SetNewTbText(tb, maxValueText, MyStrLength32(maxValueText));
					corrected = true;
				}
			}
		}
	}
	else if (tb->type == TextboxType_Integer)
	{
		//Remove/find - characters that are in the middle of the number
		bool foundDashChar = false;
		for (u32 cIndex = 0; cIndex < tb->textLength; )
		{
			char nextChar = tb->text[cIndex];
			if (cIndex > 0 && nextChar == '-')
			{
				PrintLine_D("Found dash at %u", cIndex);
				if (tb->autoCorrectTextFormat)
				{
					RemoveStringRegionInPlace(tb->text, tb->textLength, cIndex, cIndex+1);
					if (tb->cursorEnd > cIndex) { tb->cursorEnd--; }
					if (tb->cursorStart > cIndex) { tb->cursorStart--; }
					tb->remeasureText = true;
					tb->scrollToCursor = true;
					result = true;
					corrected = true;
					tb->textLength--;
					//don't increment cIndex
				}
				else
				{
					result = true;
					break;
				}
			}
			else
			{
				if (nextChar == '-') { foundDashChar = true; }
				cIndex++;
			}
		}
		
		PrintLine_D("After dash removal: \"%s\"", tb->text);
		
		if (!result || tb->autoCorrectTextFormat)
		{
			WriteLine_D("Attempting to parse I32...");
			i32 currentValue;
			if (TryParseI32(tb->text, tb->textLength, &currentValue))
			{
				WriteLine_D("Valid I32");
				if (tb->minValueEnabled && currentValue < tb->minValueI32)
				{
					WriteLine_D("Less than min");
					result = true;
					if (tb->autoCorrectTextFormat)
					{
						char* minValueText = TempPrint("%d", tb->minValueI32);
						Assert(minValueText != nullptr);
						SetNewTbText(tb, minValueText, MyStrLength32(minValueText));
						corrected = true;
					}
				}
				else if (tb->maxValueEnabled && currentValue > tb->maxValueI32)
				{
					WriteLine_D("More than max");
					result = true;
					if (tb->autoCorrectTextFormat)
					{
						char* maxValueText = TempPrint("%d", tb->maxValueI32);
						Assert(maxValueText != nullptr);
						SetNewTbText(tb, maxValueText, MyStrLength32(maxValueText));
						corrected = true;
					}
				}
			}
			else
			{
				WriteLine_D("Invalid I32");
				result = true;
				if (tb->autoCorrectTextFormat)
				{
					//NOTE: The most likely scenario for invalid (assuming the contents contain only numbers and only '-' at the beginning)
					//      is that there are too many numbers present for a valid I32 value. So we will make the box contain either the minimum or
					//      maximum value or "0" depending on the contents of the text
					if (tb->textLength == 0 || (tb->textLength == 1 && !IsCharClassNumeric(tb->text[0])))
					{
						WriteLine_D("Making it 0");
						SetNewTbText(tb, "0", 1);
						corrected = true;
					}
					else if (foundDashChar)
					{
						if (tb->minValueEnabled)
						{
							WriteLine_D("Making it min value");
							char* minValueText = TempPrint("%d", tb->minValueI32);
							Assert(minValueText != nullptr);
							SetNewTbText(tb, minValueText, MyStrLength32(minValueText));
							corrected = true;
						}
						else
						{
							WriteLine_D("Making it min i32");
							char* maxValueText = TempPrint("%d", (i32)0x80000000);
							Assert(maxValueText != nullptr);
							SetNewTbText(tb, maxValueText, MyStrLength32(maxValueText));
							corrected = true;
						}
					}
					else
					{
						if (tb->maxValueEnabled)
						{
							WriteLine_D("Making it max value");
							char* maxValueText = TempPrint("%d", tb->maxValueI32);
							Assert(maxValueText != nullptr);
							SetNewTbText(tb, maxValueText, MyStrLength32(maxValueText));
							corrected = true;
						}
						else
						{
							WriteLine_D("Making it max i32");
							char* maxValueText = TempPrint("%d", (i32)0x7FFFFFFF);
							Assert(maxValueText != nullptr);
							SetNewTbText(tb, maxValueText, MyStrLength32(maxValueText));
							corrected = true;
						}
					}
				}
			}
		}
	}
	else if (tb->type == TextboxType_FloatingPoint)
	{
		//Remove/find - characters that are in the middle of the number
		bool foundDashChar = false;
		bool foundPeriod = false;
		for (u32 cIndex = 0; cIndex < tb->textLength; )
		{
			char nextChar = tb->text[cIndex];
			if ((cIndex > 0 && nextChar == '-') || (foundPeriod && nextChar == '.'))
			{
				PrintLine_D("Found \'%c\' at %u", cIndex, nextChar);
				if (tb->autoCorrectTextFormat)
				{
					RemoveStringRegionInPlace(tb->text, tb->textLength, cIndex, cIndex+1);
					if (tb->cursorEnd > cIndex) { tb->cursorEnd--; }
					if (tb->cursorStart > cIndex) { tb->cursorStart--; }
					tb->remeasureText = true;
					tb->scrollToCursor = true;
					result = true;
					corrected = true;
					tb->textLength--;
					//don't increment cIndex
				}
				else
				{
					result = true;
					break;
				}
			}
			else
			{
				if (nextChar == '-') { foundDashChar = true; }
				if (nextChar == '.') { foundPeriod = true; }
				cIndex++;
			}
		}
		
		PrintLine_D("After dash/period removal: \"%s\"", tb->text);
		
		if (!result || tb->autoCorrectTextFormat)
		{
			r32 currentValue = 0;
			if (TryParseR32(tb->text, &currentValue))
			{
				PrintLine_D("Valid R32: %f", currentValue);
				if (tb->minValueEnabled && currentValue < tb->minValueR32)
				{
					WriteLine_D("Less than min");
					result = true;
					if (tb->autoCorrectTextFormat)
					{
						WriteLine_D("Making it min value");
						char* minValueText = TempPrint("%f", tb->minValueR32);
						Assert(minValueText != nullptr);
						SetNewTbText(tb, minValueText, MyStrLength32(minValueText));
						corrected = true;
					}
				}
				else if (tb->maxValueEnabled && currentValue > tb->maxValueR32)
				{
					WriteLine_D("More than max");
					result = true;
					if (tb->autoCorrectTextFormat)
					{
						WriteLine_D("Making it max value");
						char* maxValueText = TempPrint("%f", tb->maxValueR32);
						Assert(maxValueText != nullptr);
						SetNewTbText(tb, maxValueText, MyStrLength32(maxValueText));
						corrected = true;
					}
				}
				else if (tb->autoCorrectTextFormat)
				{
					// char* currentValueText = TempPrint("%f", currentValue);
					// Assert(currentValueText != nullptr);
					// if (MyStrCompareNt(currentValueText, tb->text) != 0)
					// {
					// 	WriteLine_D("Making it proper looking");
					// 	SetNewTbText(tb, currentValueText, MyStrLength32(currentValueText));
					// }
				}
			}
			else
			{
				WriteLine_D("Invalid R32");
				result = true;
				if (tb->autoCorrectTextFormat)
				{
					if (tb->minValueEnabled)
					{
						WriteLine_D("Making it min value");
						char* minValueText = TempPrint("%f", tb->minValueR32);
						Assert(minValueText != nullptr);
						SetNewTbText(tb, minValueText, MyStrLength32(minValueText));
						corrected = true;
					}
					else if (tb->maxValueEnabled && tb->maxValueR32 < 0.0f)
					{
						WriteLine_D("Making it max value");
						char* maxValueText = TempPrint("%f", tb->maxValueR32);
						Assert(maxValueText != nullptr);
						SetNewTbText(tb, maxValueText, MyStrLength32(maxValueText));
						corrected = true;
					}
					else
					{
						WriteLine_D("Making it 0");
						SetNewTbText(tb, "0", 1);
						corrected = true;
					}
				}
			}
		}
	}
	
	tb->textIsInvalid = (result && !corrected);
	return result;
}

void SelectNewTb(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	if (!tb->isSelected)
	{
		tb->isSelected = true;
		tb->clickIsSelectingText = false;
		//TODO: Should we scroll to the cursor position? Should we select all of the text?
	}
}
void DeselectNewTb(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	if (tb->isSelected)
	{
		tb->isSelected = false;
		tb->clickIsSelectingText = false;
		
		if (tb->type != TextboxType_Default)
		{
			NewTbValidateText(tb);
		}
	}
}

void NewTbCalculateHoverMousePos(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	tb->mouseHoverIndex = -1;
	char* boxHoverName = TempPrint("Textbox%u", tb->mouseHoverId);
	Font_t* tbFont = GetFont_(tb->fontIndex, false);
	Assert(tbFont != nullptr);
	if (IsMouseOver(boxHoverName) || tb->clickIsSelectingText)
	{
		appOutput->cursorType = Cursor_Text;
		if (tb->textLength > 0)
		{
			v2 mouseOffset = RenderMousePos - tb->textPos;
			FindStringIndexInfo_t findInfo = {};
			if (FindStringIndexForLocationFlow(&findInfo, tbFont, tb->text, tb->textLength, mouseOffset, tb->fontScale, (tb->multiline ? tb->maxWidth : 0), FfsFlag_MeasureWhitespace))
			{
				Assert(findInfo.resultIndex <= tb->textLength);
				tb->mouseHoverIndex = (i32)findInfo.resultIndex;
				tb->mouseHoverCursorPos = findInfo.resultPos;
			}
		}
		else
		{
			tb->mouseHoverIndex = 0;
			tb->mouseHoverCursorPos = Vec2_Zero;
		}
	}
}

//NOTE: Returns true when this text box gets selected. The calling party should deselect any other textboxes/UI elements when this happens
bool UpdateNewTbMouseSelection(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	bool result = false;
	char* boxHoverName = TempPrint("Textbox%u", tb->mouseHoverId);
	if (!tb->isSelected)
	{
		if (IsMouseOver(boxHoverName))
		{
			appOutput->cursorType = Cursor_Pointer;
			if (ButtonPressedNoHandling(MouseButton_Left))
			{
				HandleButton(MouseButton_Left);
				SelectNewTb(tb);
				result = true;
				NewTbCalculateHoverMousePos(tb);
				tb->cursorEnd = (u32)tb->mouseHoverIndex;
				tb->cursorStart = tb->cursorEnd;
				tb->clickIsSelectingText = true;
				tb->lastSelectClickTime = ProgramTime;
				tb->justDoubleClicked = false;
			}
		}
	}
	else
	{
		if (!IsMouseOver(boxHoverName) && ButtonPressedNoHandling(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			DeselectNewTb(tb);
			tb->clickIsDeselecting = true;
		}
		if (tb->clickIsDeselecting)
		{
			if (ButtonDownNoHandling(MouseButton_Left)) { HandleButton(MouseButton_Left); }
			else { tb->clickIsDeselecting = false; }
		}
	}
	return result;
}

void UpdateNewTbScrollingClamps(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	v2 usableAreaSize = tb->drawRec.size - tb->textPadding*2;
	if (usableAreaSize.width < 0) { usableAreaSize.width = 0; }
	if (usableAreaSize.height < 0) { usableAreaSize.height = 0; }
	v2 scrollOverAmount = NewVec2(NEW_TB_SCROLL_OVER_AMOUNT, NEW_TB_SCROLL_OVER_AMOUNT);
	if (scrollOverAmount.x > usableAreaSize.width * (3.0f/4.0f)) { scrollOverAmount.x = usableAreaSize.width * (3.0f/4.0f); }
	if (scrollOverAmount.y > usableAreaSize.height * (3.0f/4.0f)) { scrollOverAmount.y = usableAreaSize.height * (3.0f/4.0f); }
	tb->maxScroll = tb->textSize - usableAreaSize;
	if (tb->maxScroll.x < 0) { tb->maxScroll.x = 0; }
	else {tb->maxScroll.x += scrollOverAmount.x; }
	if (tb->maxScroll.y < 0) { tb->maxScroll.y = 0; }
	else {tb->maxScroll.y += scrollOverAmount.y; }
	if (tb->scroll.x > tb->maxScroll.x) { tb->scroll.x = tb->maxScroll.x; }
	if (tb->scroll.y > tb->maxScroll.y) { tb->scroll.y = tb->maxScroll.y; }
	if (tb->scroll.x < 0) { tb->scroll.x = 0; }
	if (tb->scroll.y < 0) { tb->scroll.y = 0; }
	if (tb->scrollGoto.x > tb->maxScroll.x) { tb->scrollGoto.x = tb->maxScroll.x; }
	if (tb->scrollGoto.y > tb->maxScroll.y) { tb->scrollGoto.y = tb->maxScroll.y; }
	if (tb->scrollGoto.x < 0) { tb->scrollGoto.x = 0; }
	if (tb->scrollGoto.y < 0) { tb->scrollGoto.y = 0; }
}

void UpdateNewTbSizingAndPositioning(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	Font_t* tbFont = GetFont_(tb->fontIndex, false);
	Assert(tbFont != nullptr);
	tb->maxWidth = (tb->drawRec.width - tb->textPadding.x*2);
	if (tb->multiline)
	{
		tb->textPos = NewVec2(tb->drawRec.x + tb->textPadding.x, tb->drawRec.y + tb->textPadding.y + (tbFont->maxExtendUp * tb->fontScale));
		tb->textPos -= tb->scroll;
		tb->textPos = Vec2Round(tb->textPos);
	}
	else
	{
		tb->textPos = NewVec2(tb->drawRec.x + tb->textPadding.x, tb->drawRec.y + tb->drawRec.height/2 + ((-tbFont->lineHeight/2 + tbFont->maxExtendUp) * tb->fontScale));
		tb->textPos -= tb->scroll;
		tb->textPos = Vec2Round(tb->textPos);
	}
}

void NewTbDeleteSelectedText(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	if (tb->cursorStart == tb->cursorEnd) { return; } //nothing to delete
	Assert(tb->cursorStart <= tb->textLength);
	Assert(tb->cursorEnd <= tb->textLength);
	u32 selectionMin = MinU32(tb->cursorStart, tb->cursorEnd);
	u32 selectionMax = MaxU32(tb->cursorStart, tb->cursorEnd);
	u32 selectionLength = selectionMax - selectionMin;
	for (u32 cIndex = 0; selectionMax + cIndex < tb->textLength; cIndex++)
	{
		tb->text[selectionMin + cIndex] = tb->text[selectionMax + cIndex];
	}
	tb->textLength -= selectionLength;
	tb->text[tb->textLength] = '\0';
	tb->cursorEnd = selectionMin;
	tb->cursorStart = tb->cursorEnd;
	tb->remeasureText = true;
}

void UpdateNewTb(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	if (tb->remeasureText) { NewTbRemeasureText(tb); }
	
	r32 timeScale = (ElapsedMs / (r32)TARGET_FRAME_DURATION);
	char* boxHoverName = TempPrint("Textbox%u", tb->mouseHoverId);
	Font_t* tbFont = GetFont_(tb->fontIndex, false);
	Assert(tbFont != nullptr);
	
	// +==============================+
	// | Scroll on Selection Changed  |
	// +==============================+
	if (tb->selectionChanged)
	{
		v2 usableAreaSize = tb->drawRec.size - tb->textPadding*2;
		if (usableAreaSize.width < 0) { usableAreaSize.width = 0; }
		if (usableAreaSize.height < 0) { usableAreaSize.height = 0; }
		v2 overshootAmount = NewVec2(NEW_TB_SCROLL_OVERSHOOT_AMOUNT, NEW_TB_SCROLL_OVERSHOOT_AMOUNT);
		if (overshootAmount.x > usableAreaSize.width * (3.0f /4.0f)) { overshootAmount.x = usableAreaSize.width * (3.0f /4.0f); }
		if (overshootAmount.y > usableAreaSize.height * (3.0f /4.0f)) { overshootAmount.y = usableAreaSize.height * (3.0f /4.0f); }
		if (tb->scrollGoto.x + usableAreaSize.width < tb->cursorEndPos.x)
		{
			tb->scrollGoto.x = tb->cursorEndPos.x + overshootAmount.x - usableAreaSize.width;
		}
		if (tb->scrollGoto.x > tb->cursorEndPos.x)
		{
			tb->scrollGoto.x = tb->cursorEndPos.x - overshootAmount.x;
		}
		if (tb->scrollGoto.y + usableAreaSize.height < tb->cursorEndPos.y)
		{
			tb->scrollGoto.y = tb->cursorEndPos.y + overshootAmount.y - usableAreaSize.height;
		}
		if (tb->scrollGoto.y > tb->cursorEndPos.y)
		{
			tb->scrollGoto.y = tb->cursorEndPos.y - overshootAmount.y;
		}
		tb->selectionChanged = false;
	}
	
	// +==============================+
	// |    Mouse Wheel Scrolling     |
	// +==============================+
	r32 scrollSpeed = (tbFont->lineHeight * tb->fontScale) * 2.0f;
	if (ScrollWheelMovedY() && IsMouseOver(boxHoverName))
	{
		HandleScrollWheelY();
		if (tb->multiline && !ButtonDownNoHandling(Button_Shift))
		{
			tb->scrollGoto.y -= appInput->scrollDelta.y * scrollSpeed;
		}
		else
		{
			tb->scrollGoto.x -= appInput->scrollDelta.y * scrollSpeed;
		}
	}
	if (ScrollWheelMovedX() && IsMouseOver(boxHoverName))
	{
		HandleScrollWheelX();
		tb->scrollGoto.x += appInput->scrollDelta.x * scrollSpeed;
	}
	
	// +==============================+
	// |       Update Scrolling       |
	// +==============================+
	if (tb->scrollLag > 0 && Vec2Length(tb->scrollGoto - tb->scroll) >= 1.0f)
	{
		tb->scroll += (tb->scrollGoto - tb->scroll) * (1 - PowR32(1 - (1 / tb->scrollLag), timeScale));
	}
	else
	{
		tb->scroll = tb->scrollGoto;
	}
	
	UpdateNewTbScrollingClamps(tb);
	UpdateNewTbSizingAndPositioning(tb);
	NewTbCalculateHoverMousePos(tb);
	
	if (tb->isSelected)
	{
		u32 oldCursorEnd = tb->cursorEnd;
		u32 oldCursorStart = tb->cursorStart;
		
		// +================================+
		// | Handle Left/Right Key Movement |
		// +================================+
		if (ButtonRepeatedEvery(Button_Right, 50, 250))
		{
			HandleButton(Button_Right);
			if (ButtonDownNoHandling(Button_Control))
			{
				if (tb->cursorStart != tb->cursorEnd && !ButtonDownNoHandling(Button_Shift))
				{
					tb->cursorEnd = MaxU32(tb->cursorStart, tb->cursorEnd);
					tb->cursorStart = tb->cursorEnd;
				}
				else
				{
					tb->cursorEnd = FindWordBound(tb->text, tb->textLength, tb->cursorEnd, true);
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
			}
			else
			{
				if (tb->cursorStart != tb->cursorEnd && !ButtonDownNoHandling(Button_Shift))
				{
					tb->cursorEnd = MaxU32(tb->cursorStart, tb->cursorEnd);
					tb->cursorStart = tb->cursorEnd;
				}
				else if (tb->cursorEnd < tb->textLength)
				{
					tb->cursorEnd++;
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
			}
		}
		if (ButtonRepeatedEvery(Button_Left, 50, 250))
		{
			HandleButton(Button_Left);
			if (ButtonDownNoHandling(Button_Control))
			{
				if (tb->cursorStart != tb->cursorEnd && !ButtonDownNoHandling(Button_Shift))
				{
					tb->cursorEnd = MinU32(tb->cursorStart, tb->cursorEnd);
					tb->cursorStart = tb->cursorEnd;
				}
				else
				{
					tb->cursorEnd = FindWordBound(tb->text, tb->textLength, tb->cursorEnd, false);
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
			}
			else
			{
				if (tb->cursorStart != tb->cursorEnd && !ButtonDownNoHandling(Button_Shift))
				{
					tb->cursorEnd = MinU32(tb->cursorStart, tb->cursorEnd);
					tb->cursorStart = tb->cursorEnd;
				}
				else if (tb->cursorEnd > 0)
				{
					tb->cursorEnd--;
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
			}
		}
		
		// +==============================+
		// | Handle Up/Down Key Movement  |
		// +==============================+
		if (ButtonRepeatedEvery(Button_Down, 100, 250))
		{
			HandleButton(Button_Down);
			FindStringIndexInfo_t findInfo = {};
			v2 searchPos = tb->cursorEndPos + NewVec2(0, tbFont->lineHeight * tb->fontScale);
			if (FindStringIndexForLocationFlow(&findInfo, tbFont, tb->text, tb->textLength, searchPos, tb->fontScale, (tb->multiline ? tb->maxWidth : 0), FfsFlag_MeasureWhitespace))
			{
				Assert(findInfo.resultIndex <= tb->textLength);
				if (findInfo.resultIndex != tb->cursorEnd)
				{
					tb->cursorEnd = findInfo.resultIndex;
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
			}
		}
		if (ButtonRepeatedEvery(Button_Up, 100, 250))
		{
			HandleButton(Button_Up);
			FindStringIndexInfo_t findInfo = {};
			v2 searchPos = tb->cursorEndPos - NewVec2(0, tbFont->lineHeight * tb->fontScale);
			if (FindStringIndexForLocationFlow(&findInfo, tbFont, tb->text, tb->textLength, searchPos, tb->fontScale, (tb->multiline ? tb->maxWidth : 0), FfsFlag_MeasureWhitespace))
			{
				Assert(findInfo.resultIndex <= tb->textLength);
				if (findInfo.resultIndex != tb->cursorEnd)
				{
					tb->cursorEnd = findInfo.resultIndex;
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
			}
		}
		
		// +==============================+
		// |   Handle Home/End Movement   |
		// +==============================+
		if (ButtonPressed(Button_End))
		{
			HandleButton(Button_End);
			if (ButtonDownNoHandling(Button_Control))
			{
				tb->cursorEnd = tb->textLength;
				if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
			}
			else
			{
				if (tb->multiline)
				{
					u32 lineEnd = FindLineEnd(tb->text, tb->textLength, tb->cursorEnd);
					// if (lineEnd == tb->cursorEnd && tb->cursorEnd < tb->textLength)
					// {
					// 	lineEnd = FindLineEnd(tb->text, tb->textLength, tb->cursorEnd+1);
					// }
					tb->cursorEnd = lineEnd;
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
				else
				{
					tb->cursorEnd = tb->textLength;
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
			}
		}
		if (ButtonPressed(Button_Home))
		{
			HandleButton(Button_Home);
			if (ButtonDownNoHandling(Button_Control))
			{
				tb->cursorEnd = 0;
				if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
			}
			else
			{
				if (tb->multiline)
				{
					u32 lineStart = FindLineStart(tb->text, tb->textLength, tb->cursorEnd);
					// if (lineStart == tb->cursorEnd && tb->cursorEnd > 0)
					// {
					// 	lineStart = FindLineStart(tb->text, tb->textLength, tb->cursorEnd-1);
					// }
					tb->cursorEnd = lineStart;
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
				else
				{
					tb->cursorEnd = 0;
					if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				}
			}
		}
		
		// +==============================+
		// |      Handle Mouse Click      |
		// +==============================+
		if (ButtonPressed(MouseButton_Left) && tb->mouseHoverIndex >= 0)
		{
			HandleButton(MouseButton_Left);
			Assert((u32)tb->mouseHoverIndex <= tb->textLength);
			u32 selectionMin = MinU32(tb->cursorStart, tb->cursorEnd);
			u32 selectionMax = MaxU32(tb->cursorStart, tb->cursorEnd);
			
			bool startSelection = true;
			if (TimeSince(tb->lastSelectClickTime) < NEW_TB_DOUBLE_CLICK_TIME)
			{
				if (tb->justDoubleClicked && (u32)tb->mouseHoverIndex >= selectionMin && (u32)tb->mouseHoverIndex <= selectionMax)
				{
					tb->cursorStart = 0;
					tb->cursorEnd = tb->textLength;
					oldCursorStart = tb->cursorStart;
					oldCursorEnd = tb->cursorEnd;
					startSelection = false;
					tb->justDoubleClicked = false;
					tb->lastSelectClickTime = 0;
				}
				else if ((u32)tb->mouseHoverIndex == tb->cursorEnd && tb->cursorEnd == tb->cursorStart)
				{
					u32 wordLeft = FindWordBound(tb->text, tb->textLength, (tb->cursorEnd < tb->textLength ? tb->cursorEnd+1 : tb->cursorEnd), false);
					u32 wordRight = FindWordBound(tb->text, tb->textLength, (tb->cursorEnd > 0 ? tb->cursorEnd-1 : tb->cursorEnd), true);
					tb->cursorStart = wordLeft;
					tb->cursorEnd = wordRight;
					oldCursorStart = tb->cursorStart;
					oldCursorEnd = tb->cursorEnd;
					tb->justDoubleClicked = true;
					tb->lastSelectClickTime = ProgramTime;
					startSelection = false;
				}
			}
			
			if (startSelection)
			{
				tb->cursorEnd = (u32)tb->mouseHoverIndex;
				if (!ButtonDownNoHandling(Button_Shift)) { tb->cursorStart = tb->cursorEnd; }
				tb->clickIsSelectingText = true;
				tb->lastSelectClickTime = ProgramTime;
				tb->justDoubleClicked = false;
			}
		}
		if (ButtonDownNoHandling(MouseButton_Left) && tb->clickIsSelectingText && tb->mouseHoverIndex >= 0)
		{
			HandleButton(MouseButton_Left);
			tb->cursorEnd = (u32)tb->mouseHoverIndex;
		}
		if (!ButtonDownNoHandling(MouseButton_Left) && tb->clickIsSelectingText)
		{
			tb->clickIsSelectingText = false;
		}
		
		// +==============================+
		// |     Handle Backspace Key     |
		// +==============================+
		if (ButtonPressed(Button_Backspace))
		{
			HandleButton(Button_Backspace);
			if (tb->cursorStart != tb->cursorEnd)
			{
				NewTbDeleteSelectedText(tb);
			}
			else
			{
				if (tb->cursorEnd > 0)
				{
					tb->cursorStart = tb->cursorEnd-1;
					NewTbDeleteSelectedText(tb);
				}
			}
		}
		
		// +==============================+
		// |      Handle Delete Key       |
		// +==============================+
		if (ButtonPressed(Button_Delete))
		{
			HandleButton(Button_Delete);
			if (tb->cursorStart != tb->cursorEnd)
			{
				NewTbDeleteSelectedText(tb);
			}
			else
			{
				if (tb->cursorEnd < tb->textLength)
				{
					tb->cursorStart = tb->cursorEnd+1;
					NewTbDeleteSelectedText(tb);
				}
			}
		}
		
		// +==============================+
		// |      Select All Hotkey       |
		// +==============================+
		if (ButtonPressed(Button_A) && ButtonDownNoHandling(Button_Control))
		{
			HandleButton(Button_A);
			tb->cursorStart = 0;
			tb->cursorEnd = tb->textLength;
			oldCursorStart = tb->cursorStart;
			oldCursorEnd = tb->cursorEnd;
		}
		
		// +==============================+
		// |      Copy to Clipboard       |
		// +==============================+
		if (ButtonPressed(Button_C) && ButtonDownNoHandling(Button_Control))
		{
			HandleButton(Button_C);
			if (tb->cursorStart != tb->cursorEnd)
			{
				u32 selectionMin = MinU32(tb->cursorStart, tb->cursorEnd);
				u32 selectionMax = MaxU32(tb->cursorStart, tb->cursorEnd);
				u32 selectionLength = selectionMax - selectionMin;
				char* tempString = TempString(selectionLength+1);
				MyMemCopy(tempString, &tb->text[selectionMin], selectionLength);
				tempString[selectionLength] = '\0';
				platform->CopyToClipboard(tempString, selectionLength+1);
				PrintLine_D("Copied %u bytes to clipboard", selectionLength);
			}
		}
		
		// +==============================+
		// |     Paste from Clipboard     |
		// +==============================+
		if (ButtonPressed(Button_V) && ButtonDownNoHandling(Button_Control))
		{
			HandleButton(Button_V);
			TempPushMark();
			u32 clipboardDataSize = 0;
			char* clipbardData = (char*)platform->CopyFromClipboard(TempArena, &clipboardDataSize);
			if (clipbardData != nullptr)
			{
				if (tb->cursorStart != tb->cursorEnd)
				{
					NewTbDeleteSelectedText(tb);
				}
				
				char* sanatized = SanatizeStringAdvanced(clipbardData, clipboardDataSize, TempArena, true, false, true);
				u32 sanatizedLength = MyStrLength32(sanatized);
				
				if (tb->growingTextAllocation)
				{
					NewTbExpandForTextLength(tb, tb->textLength + sanatizedLength);
				}
				if (tb->textLength + sanatizedLength + 1 > tb->allocLength)
				{
					PrintLine_W("Tried to paste %u bytes but only space for %u bytes in textbox", sanatizedLength, tb->allocLength-1 - tb->textLength);
					sanatizedLength = tb->allocLength-1 - tb->textLength;
				}
				
				if (sanatizedLength > 0)
				{
					tb->textLength = InsertStringInPlace(tb->text, tb->textLength, tb->allocLength-1, tb->cursorEnd, sanatized, sanatizedLength);
					tb->cursorEnd += sanatizedLength;
					tb->cursorStart = tb->cursorEnd;
					tb->remeasureText = true;
				}
			}
			TempPopMark();
		}
		
		// +==============================+
		// |   Handle Enter to Deselect   |
		// +==============================+
		if (ButtonPressed(Button_Enter) && !tb->multiline && tb->deselectOnEnter)
		{
			HandleButton(Button_Enter);
			DeselectNewTb(tb);
		}
		
		// +==============================+
		// |   Handle Typed Characters    |
		// +==============================+
		for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
		{
			const InputEvent_t* event = &appInput->inputEvents[eIndex];
			if (event->type == InputEventType_ButtonTransition && IsFlagSet(event->flags, BtnTransFlag_Char) && (IsFlagSet(event->flags, BtnTransFlag_Pressed) || IsFlagSet(event->flags, BtnTransFlag_Repeat)))
			{
				if (!app->buttonHandled[event->button] && IsCharAllowedInTextboxType(tb->type, event->character))
				{
					HandleButton(event->button);
					const Font_t* font = GetFont_(tb->fontIndex, false);
					Assert(font != nullptr);
					if ((event->character == '\n' && tb->multiline) || (event->character == '\t') || ((u8)event->character >= font->firstChar && (u8)event->character < font->firstChar + font->numChars))
					{
						if (tb->cursorEnd != tb->cursorStart)
						{
							NewTbDeleteSelectedText(tb);
						}
						if (tb->growingTextAllocation)
						{
							NewTbExpandForTextLength(tb, tb->textLength+1);
						}
						if (tb->textLength + 1 <= tb->allocLength-1)
						{
							tb->textLength = InsertStringInPlace(tb->text, tb->textLength, tb->allocLength, tb->cursorEnd, &event->character, 1);
							tb->cursorEnd += 1;
							tb->cursorStart = tb->cursorEnd;
							tb->remeasureText = true;
						}
						else
						{
							PrintLine_W("Textbox is full of %u chars", tb->textLength);
						}
					}
				}
			}
		}
		
		// +==============================+
		// |  Constantly Handle Buttons   |
		// +==============================+
		HandleButton(Button_Right);
		HandleButton(Button_Left);
		HandleButton(Button_Down);
		HandleButton(Button_Up);
		HandleButton(Button_End);
		HandleButton(Button_Home);
		HandleButton(MouseButton_Left);
		HandleButton(Button_Backspace);
		HandleButton(Button_Delete);
		if ((tb->multiline && IsCharAllowedInTextboxType(tb->type, '\n')) || tb->deselectOnEnter) { HandleButton(Button_Enter); }
		HandleButton(Button_Control);
		HandleButton(Button_Shift);
		HandleButton(Button_CapsLock);
		for (u8 bIndex = 0; bIndex < 26; bIndex++) { if (IsCharAllowedInTextboxType(tb->type, 'a' + bIndex) || IsCharAllowedInTextboxType(tb->type, 'A' + bIndex)) { HandleButton((Buttons_t)(Button_A + bIndex)); } }
		for (u8 bIndex = 0; bIndex < 10; bIndex++) { if (IsCharAllowedInTextboxType(tb->type, '0' + bIndex)) { HandleButton((Buttons_t)(Button_0 + bIndex)); HandleButton((Buttons_t)(Button_Num0 + bIndex)); } }
		if (IsCharAllowedInTextboxType(tb->type, ' ')) { HandleButton(Button_Space); }
		if (IsCharAllowedInTextboxType(tb->type, '\t')) { HandleButton(Button_Tab); }
		if (IsCharAllowedInTextboxType(tb->type, '[') || IsCharAllowedInTextboxType(tb->type, '{')) { HandleButton(Button_OpenBracket); }
		if (IsCharAllowedInTextboxType(tb->type, ']') || IsCharAllowedInTextboxType(tb->type, '}')) { HandleButton(Button_CloseBracket); }
		if (IsCharAllowedInTextboxType(tb->type, '|') || IsCharAllowedInTextboxType(tb->type, '\\')) { HandleButton(Button_Pipe); }
		if (IsCharAllowedInTextboxType(tb->type, ';') || IsCharAllowedInTextboxType(tb->type, ':')) { HandleButton(Button_Colon); }
		if (IsCharAllowedInTextboxType(tb->type, '\'') || IsCharAllowedInTextboxType(tb->type, '"')) { HandleButton(Button_Quote); }
		if (IsCharAllowedInTextboxType(tb->type, ',') || IsCharAllowedInTextboxType(tb->type, '<')) { HandleButton(Button_Comma); }
		if (IsCharAllowedInTextboxType(tb->type, '.') || IsCharAllowedInTextboxType(tb->type, '>')) { HandleButton(Button_Period); HandleButton(Button_NumPeriod); }
		if (IsCharAllowedInTextboxType(tb->type, '/') || IsCharAllowedInTextboxType(tb->type, '?')) { HandleButton(Button_QuestionMark); }
		if (IsCharAllowedInTextboxType(tb->type, '`') || IsCharAllowedInTextboxType(tb->type, '~')) { HandleButton(Button_Tilde); }
		if (IsCharAllowedInTextboxType(tb->type, '=') || IsCharAllowedInTextboxType(tb->type, '+')) { HandleButton(Button_Plus); HandleButton(Button_NumAdd); }
		if (IsCharAllowedInTextboxType(tb->type, '-') || IsCharAllowedInTextboxType(tb->type, '_')) { HandleButton(Button_Minus); HandleButton(Button_NumSubtract); }
		if (IsCharAllowedInTextboxType(tb->type, '*')) { HandleButton(Button_NumMultiply); }
		if (IsCharAllowedInTextboxType(tb->type, '/')) { HandleButton(Button_NumDivide); }
		
		if (oldCursorEnd != tb->cursorEnd || oldCursorStart != tb->cursorStart)
		{
			tb->selectionChanged = true;
		}
	}
}

struct NewTbRenderUserInfo_t
{
	NewTextbox_t* tb;
	rec lineBounds;
	u32 lineStart;
	u32 lineLength;
	v2 selectionMinPos;
	v2 selectionMaxPos;
	bool enteredSelection;
};

// +==============================+
// |      NewTbCharCallback       |
// +==============================+
// rec NewTbCharCallback(FontFlowState_t* flowState, bool measureOnly, u32 charIndex, char c, v2 position, const Font_t* font, r32 fontScale, r32* advanceXOut, bool* wasUnprintable, bool* wasWhitespace)
FlowCharCallback_FUNCTION(NewTbCharCallback)
{
	Assert(flowState != nullptr);
	Assert(advanceXOut != nullptr);
	Assert(flowState->userPntr != nullptr);
	NewTbRenderUserInfo_t* info = (NewTbRenderUserInfo_t*)flowState->userPntr;
	NewTextbox_t* tb = info->tb;
	u32 selectionMin = MinU32(tb->cursorStart, tb->cursorEnd);
	u32 selectionMax = MaxU32(tb->cursorStart, tb->cursorEnd);
	
	if (charIndex == selectionMin) { info->selectionMinPos = position - flowState->startPos; }
	if (charIndex == selectionMax) { info->selectionMaxPos = position - flowState->startPos; }
	
	rec result = DefaultFontFlowCharCallback(flowState, measureOnly, charIndex, c, position, font, fontScale, advanceXOut, wasUnprintable, wasWhitespace);
	
	if (charIndex+1 == selectionMin) { info->selectionMinPos = (position + NewVec2(*advanceXOut, 0)) - flowState->startPos; }
	if (charIndex+1 == selectionMax) { info->selectionMaxPos = (position + NewVec2(*advanceXOut, 0)) - flowState->startPos; }
	
	return result;
}

// +==============================+
// |   NewTbBeforeLineCallback    |
// +==============================+
// void NewTbBeforeLineCallback(FontFlowState_t* flowState, u32 lineIndex, u32 startIndex, u32 lineLength, rec lineBounds)
FlowBeforeLineCallback_FUNCTION(NewTbBeforeLineCallback)
{
	Assert(flowState != nullptr);
	Assert(flowState->userPntr != nullptr);
	NewTbRenderUserInfo_t* info = (NewTbRenderUserInfo_t*)flowState->userPntr;
	NewTextbox_t* tb = info->tb;
	u32 selectionMin = MinU32(tb->cursorStart, tb->cursorEnd);
	u32 selectionMax = MaxU32(tb->cursorStart, tb->cursorEnd);
	info->lineBounds = lineBounds;
	info->lineStart = startIndex;
	info->lineLength = lineLength;
	if (startIndex > selectionMin && startIndex <= selectionMax)
	{
		if (selectionMax < startIndex + lineLength)
		{
			rec selectionRec = NewRec(lineBounds.x, lineBounds.y, flowState->startPos.x + info->selectionMaxPos.x - lineBounds.x, lineBounds.height);
			RcDrawRectangle(selectionRec, GetCurrentNewTbSelectionColor(tb));
		}
		else
		{
			RcDrawRectangle(lineBounds, GetCurrentNewTbSelectionColor(tb));
			if (selectionMax > startIndex + lineLength)
			{
				//TODO: Draw something extra at the end to indicate the selection has gone on to the next line
			}
		}
	}
	// RcDrawRectangle(lineBounds, GetPredefPalColorByIndex(lineIndex));
	// RcDrawRectangle(NewRec(lineBounds.x, lineBounds.y, lineBounds.width, GetFont_(tb->fontIndex, false)->maxExtendUp * tb->fontScale), ColorTransparent(White, 0.5f));
}

// +==============================+
// |    NewTbIndexPosCallback     |
// +==============================+
// void NewTbIndexPosCallback(FontFlowState_t* flowState, u32 charIndex, v2 position)
FlowIndexPosCallback_FUNCTION(NewTbIndexPosCallback)
{
	Assert(flowState != nullptr);
	Assert(flowState->userPntr != nullptr);
	NewTbRenderUserInfo_t* info = (NewTbRenderUserInfo_t*)flowState->userPntr;
	NewTextbox_t* tb = info->tb;
	if (charIndex == tb->cursorStart) { tb->cursorStartPos = position - flowState->startPos; }
	if (charIndex == tb->cursorEnd) { tb->cursorEndPos = position - flowState->startPos; }
}

// +==============================+
// |   NewTbBeforeCharCallback    |
// +==============================+
// void NewTbBeforeCharCallback(FontFlowState_t* flowState, u32 charIndex, char c, v2 position)
FlowBeforeCharCallback_FUNCTION(NewTbBeforeCharCallback)
{
	Assert(flowState != nullptr);
	Assert(flowState->userPntr != nullptr);
	NewTbRenderUserInfo_t* info = (NewTbRenderUserInfo_t*)flowState->userPntr;
	NewTextbox_t* tb = info->tb;
	u32 selectionMin = MinU32(tb->cursorStart, tb->cursorEnd);
	u32 selectionMax = MaxU32(tb->cursorStart, tb->cursorEnd);
	if (flowState->renderState != nullptr && selectionMax > selectionMin)
	{
		if (charIndex >= selectionMin && charIndex < selectionMax && !info->enteredSelection)
		{
			info->enteredSelection = true;
			flowState->renderState->color = (tb->isSelected ? GetCurrentNewTbBackColor(tb) : (tb->textIsInvalid ? tb->textColorInvalid : tb->textColorActive));
			if (selectionMax >= info->lineStart + info->lineLength)
			{
				rec selectionRec = NewRec(position.x, info->lineBounds.y, info->lineBounds.x + info->lineBounds.width - position.x, info->lineBounds.height);
				RcDrawRectangle(selectionRec, GetCurrentNewTbSelectionColor(tb));
				if (selectionMax > info->lineStart + info->lineLength)
				{
					//TODO: Draw something extra at the end to indicate the selection has gone on to the next line
				}
			}
			else
			{
				rec selectionRec = NewRec(position.x, info->lineBounds.y, flowState->startPos.x + info->selectionMaxPos.x - position.x, info->lineBounds.height);
				RcDrawRectangle(selectionRec, GetCurrentNewTbSelectionColor(tb));
			}
		}
		if (charIndex >= selectionMax && info->enteredSelection)
		{
			flowState->renderState->color = GetCurrentNewTbTextColor(tb);
			info->enteredSelection = false;
		}
	}
}
// +==============================+
// |    NewTbAfterCharCallback    |
// +==============================+
// void NewTbAfterCharCallback(FontFlowState_t* flowState, u32 charIndex, char c, rec charRec, v2 afterPos)
FlowAfterCharCallback_FUNCTION(NewTbAfterCharCallback)
{
	Assert(flowState != nullptr);
	Assert(flowState->userPntr != nullptr);
	
	// r32 lineCenterY = afterPos.y + ((-flowState->font->maxExtendUp + flowState->font->lineHeight/2) * flowState->fontScale);
	// RcDrawRectangle(NewRec(charRec.x, lineCenterY, charRec.width, 1), PalRed);
	
	NewTbRenderUserInfo_t* info = (NewTbRenderUserInfo_t*)flowState->userPntr;
	NewTextbox_t* tb = info->tb;
	u32 selectionMin = MinU32(tb->cursorStart, tb->cursorEnd);
	u32 selectionMax = MaxU32(tb->cursorStart, tb->cursorEnd);
	if (flowState->renderState != nullptr && selectionMax > selectionMin)
	{
		if (charIndex+1 >= selectionMax && info->enteredSelection)
		{
			flowState->renderState->color = (tb->isSelected ? tb->textColorActive : tb->textColorInactive);
			info->enteredSelection = false;
		}
	}
}

void RenderNewTb(NewTextbox_t* tb)
{
	Assert(tb != nullptr);
	
	if (tb->remeasureText) { NewTbRemeasureText(tb); }
	UpdateNewTbScrollingClamps(tb);
	UpdateNewTbSizingAndPositioning(tb);
	
	Color_t backColor = GetCurrentNewTbBackColor(tb);
	Color_t textColor = GetCurrentNewTbTextColor(tb);
	Color_t borderColor = GetCurrentNewTbBorderColor(tb);
	
	// RcDrawTiledUiButton(GetSheet(buttonBacks), NewVec2i(18, tb->isSelected ? 4 : 3), tb->drawRec, White, 1.0f);
	RcDrawButton(tb->drawRec, backColor, borderColor, 1);
	
	RcBindFont(GetFont_(tb->fontIndex, false), tb->fontScale);
	rec oldViewport = rc->viewport;
	rec tbViewport = RecDeflate(tb->drawRec, 1);
	if (tbViewport.width > 0 && tbViewport.height > 0)
	{
		RcSetViewport(tbViewport);
		FontFlowState_t flowState = {};
		FontFlowRenderState_t renderState = {};
		flowState.renderState = &renderState;
		NewTbRenderUserInfo_t info = {};
		
		info.tb = tb;
		tb->cursorStartPos = Vec2_Zero;
		tb->cursorEndPos = Vec2_Zero;
		flowState.flags = FfsFlag_MeasureWhitespace;
		// if (ButtonPressedNoHandling(Button_3)) { flowState.flags |= FfsFlag_PrintDebug; }
		flowState.textPntr = tb->text;
		flowState.textLength = tb->textLength;
		flowState.startPos = tb->textPos;
		flowState.maxWidth = (tb->multiline ? tb->maxWidth : 0);
		flowState.font = GetFont_(tb->fontIndex, false);
		flowState.fontScale = tb->fontScale;
		flowState.charCallback = NewTbCharCallback;
		flowState.beforeLineCallback = NewTbBeforeLineCallback;
		flowState.indexPosCallback = NewTbIndexPosCallback;
		flowState.beforeCharCallback = NewTbBeforeCharCallback;
		flowState.afterCharCallback = NewTbAfterCharCallback;
		flowState.renderState = &renderState;
		flowState.userPntr = &info;
		renderState.flags = FfrFlag_None;
		renderState.color = textColor;
		
		PerformFontFlow(&flowState);
		
		// +==============================+
		// |         Draw Cursor          |
		// +==============================+
		if (tb->isSelected)
		{
			rec cursorRec = NewRec(tb->textPos + tb->cursorEndPos, NewVec2(2, RcLineHeight()));
			cursorRec.x -= 1;
			cursorRec.y -= RcMaxExtendUp();
			cursorRec.topLeft = Vec2Round(cursorRec.topLeft);
			RcDrawRectangle(cursorRec, ColorTransparent(White, Oscillate(0.25f, 1, 1250)));
		}
		
		// RcDrawRectangle(NewRecCentered(tb->textPos, NewVec2(4)), PalYellow);
		// RcDrawRectangle(NewRecCentered(tb->textPos + info.selectionMinPos, NewVec2(4)), PalPurpleLight);
		// RcDrawRectangle(NewRecCentered(tb->textPos + info.selectionMaxPos, NewVec2(4)), PalBlueLight);
		
		RcSetViewport(oldViewport);
		
		// if (tb->isSelected)
		// {
		// 	if (tb->mouseHoverIndex >= 0)
		// 	{
		// 		Assert((u32)tb->mouseHoverIndex <= tb->textLength);
		// 		RcDrawRectangle(NewRecCentered(tb->textPos + tb->mouseHoverCursorPos, NewVec2(4, 4)), PalRed);
		// 		RcBindFont(GetFont(pixelFont), 1.0f);
		// 		RcPrintStringAligned(tb->textPos + tb->mouseHoverCursorPos + NewVec2(0, -50 - RcMaxExtendDown()), PalRed, 1.0f, Alignment_Center, "%d", tb->mouseHoverIndex);
		// 	}
		// 	else
		// 	{
		// 		RcDrawRectangle(NewRec(tb->drawRec.topLeft, NewVec2(4, 4)), PalOrange);
		// 	}
		// }
	}
}
