/*
File:   app_command_parser.cpp
Author: Taylor Robbins
Date:   10\06\2020
Description: 
	** Holds a bunch of functions and types that help us parse the commands entered into the debug console window
*/

typedef enum 
{
	CommandArgType_String = 0x00,
	CommandArgType_Bool,
	CommandArgType_U32,
	CommandArgType_I32,
	CommandArgType_R32,
} CommandArgType_t;

const char* GetCommandArgTypeStr(CommandArgType_t type)
{
	switch (type)
	{
		case CommandArgType_String: return "string";
		case CommandArgType_Bool:   return "bool";
		case CommandArgType_U32:    return "u32";
		case CommandArgType_I32:    return "i32";
		case CommandArgType_R32:    return "r32";
		default: return "[unknown]";
	}
}

struct CommandArg_t
{
	CommandArgType_t type;
	const char* name;
	StrSplitPiece_t rawStr;
	bool missing;
	bool parseFailure;
	bool relativeValue;
	union
	{
		StrSplitPiece_t valueStr;
		bool valueBool;
		u32 valueU32;
		i32 valueI32;
		r32 valueR32;
	};
};

struct CommandArgDef_t
{
	CommandArgType_t type;
	const char* name;
	bool required;
	bool allowParseErrors;
	bool enforceMin;
	union
	{
		u32 minValueU32;
		i32 minValueI32;
		r32 minValueR32;
	};
	bool enforceMax;
	union
	{
		u32 maxValueU32;
		i32 maxValueI32;
		r32 maxValueR32;
	};
	bool allowRelativeValues;
};

CommandArgDef_t* _DefCmdArg(DynArray_t* argDefs, CommandArgType_t type, const char* name, bool required)
{
	Assert(argDefs != nullptr);
	CommandArgDef_t* result = DynArrayAdd(argDefs, CommandArgDef_t);
	Assert(result != nullptr);
	ClearPointer(result);
	result->type = type;
	result->name = name;
	result->required = required;
	return result;
}

#define StartCmdArgs(numArgs) CommandArgDef_t* argDef = nullptr; DynArray_t argDefs; CreateDynamicArray(&argDefs, TempArena, sizeof(CommandArgDef_t), (numArgs), (numArgs))

#define DefCmdArgI32(name, required)                            argDef = _DefCmdArg(&argDefs, CommandArgType_I32, (name), (required))
#define DefCmdArgI32Min(name, required, minValue)               argDef = _DefCmdArg(&argDefs, CommandArgType_I32, (name), (required)); argDef->enforceMin = true; argDef->minValueI32 = (minValue)
#define DefCmdArgI32Max(name, required, maxValue)               argDef = _DefCmdArg(&argDefs, CommandArgType_I32, (name), (required)); argDef->enforceMax = true; argDef->maxValueI32 = (maxValue)
#define DefCmdArgI32MinMax(name, required, minValue, maxValue)  argDef = _DefCmdArg(&argDefs, CommandArgType_I32, (name), (required)); argDef->enforceMin = true; argDef->minValueI32 = (minValue); argDef->enforceMax = true; argDef->maxValueI32 = (maxValue)

#define DefCmdArgU32(name, required)                            argDef = _DefCmdArg(&argDefs, CommandArgType_U32, (name), (required))
#define DefCmdArgU32Min(name, required, minValue)               argDef = _DefCmdArg(&argDefs, CommandArgType_U32, (name), (required)); argDef->enforceMin = true; argDef->minValueU32 = (minValue)
#define DefCmdArgU32Max(name, required, maxValue)               argDef = _DefCmdArg(&argDefs, CommandArgType_U32, (name), (required)); argDef->enforceMax = true; argDef->maxValueU32 = (maxValue)
#define DefCmdArgU32MinMax(name, required, minValue, maxValue)  argDef = _DefCmdArg(&argDefs, CommandArgType_U32, (name), (required)); argDef->enforceMin = true; argDef->minValueU32 = (minValue); argDef->enforceMax = true; argDef->maxValueU32 = (maxValue)

#define DefCmdArgR32(name, required)                            argDef = _DefCmdArg(&argDefs, CommandArgType_R32, (name), (required))
#define DefCmdArgR32Min(name, required, minValue)               argDef = _DefCmdArg(&argDefs, CommandArgType_R32, (name), (required)); argDef->enforceMin = true; argDef->minValueR32 = (minValue)
#define DefCmdArgR32Max(name, required, maxValue)               argDef = _DefCmdArg(&argDefs, CommandArgType_R32, (name), (required)); argDef->enforceMax = true; argDef->maxValueR32 = (maxValue)
#define DefCmdArgR32MinMax(name, required, minValue, maxValue)  argDef = _DefCmdArg(&argDefs, CommandArgType_R32, (name), (required)); argDef->enforceMin = true; argDef->minValueR32 = (minValue); argDef->enforceMax = true; argDef->maxValueR32 = (maxValue)

#define DefCmdArgString(name, required)                         argDef = _DefCmdArg(&argDefs, CommandArgType_String, (name), (required));

#define DefCmdArgBool(name, required)                           argDef = _DefCmdArg(&argDefs, CommandArgType_Bool, (name), (required));

bool _ParseCmdArgs(DynArray_t* argDefs, const char* argsStr, DynArray_t* argsOut, u32* numArgsOut, char** errorStrOut)
{
	Assert(argDefs != nullptr);
	Assert(argsStr != nullptr);
	Assert(argsOut != nullptr);
	Assert(numArgsOut != nullptr);
	
	u32 numPieces = 0;
	StrSplitPiece_t* pieces = SplitStringWithQuotes(TempArena, argsStr, MyStrLength32(argsStr), " ", 1, &numPieces);
	Assert(pieces != nullptr || numPieces == 0);
	
	CreateDynamicArray(argsOut, TempArena, sizeof(CommandArg_t), argDefs->length, argDefs->length);
	*numArgsOut = 0;
	
	bool result = true;
	for (u32 aIndex = 0; aIndex < argDefs->length; aIndex++)
	{
		CommandArgDef_t* argDef = DynArrayGet(argDefs, CommandArgDef_t, aIndex);
		Assert(argDef != nullptr);
		CommandArg_t* arg = DynArrayAdd(argsOut, CommandArg_t);
		Assert(arg != nullptr);
		ClearPointer(arg);
		StrSplitPiece_t* piece = &pieces[aIndex];
		
		arg->type = argDef->type;
		arg->name = argDef->name;
		if (aIndex < numPieces && piece->length != 0)
		{
			*numArgsOut += 1;
			arg->rawStr.pntr = piece->pntr;
			arg->rawStr.length = piece->length;
			if (argDef->type == CommandArgType_String)
			{
				arg->valueStr.pntr = piece->pntr;
				arg->valueStr.length = piece->length;
			}
			else if (argDef->type == CommandArgType_Bool)
			{
				bool parsedValue = 0;
				if (!TryParseBool(piece->pntr, piece->length, &parsedValue))
				{
					arg->parseFailure = true;
					if (!argDef->allowParseErrors)
					{
						result = false;
						*errorStrOut = TempPrint("Can't parse argument[%u] %s as bool: \"%.*s\"", aIndex, arg->name, piece->length, piece->pntr);
						break;
					}
				}
				else
				{
					arg->valueBool = parsedValue;
				}
			}
			else if (argDef->type == CommandArgType_U32)
			{
				u32 parsedValue = 0;
				if (!TryParseU32(piece->pntr, piece->length, &parsedValue))
				{
					arg->parseFailure = true;
					if (!argDef->allowParseErrors)
					{
						result = false;
						*errorStrOut = TempPrint("Can't parse argument[%u] %s as u32: \"%.*s\"", aIndex, arg->name, piece->length, piece->pntr);
						break;
					}
				}
				else if (argDef->enforceMin && parsedValue < argDef->minValueU32)
				{
					result = false;
					*errorStrOut = TempPrint("%s must be more than %u: given=%u", argDef->name, argDef->minValueU32, parsedValue);
					break;
				}
				else if (argDef->enforceMax && parsedValue > argDef->maxValueU32)
				{
					result = false;
					*errorStrOut = TempPrint("%s must be less than %u: given=%u", argDef->name, argDef->maxValueU32, parsedValue);
					break;
				}
				else
				{
					arg->valueU32 = parsedValue;
				}
			}
			else if (argDef->type == CommandArgType_I32)
			{
				bool isRelative = false;
				i32 relativeSign = 1;
				if (argDef->allowRelativeValues)
				{
					if (piece->pntr[0] == '-') { isRelative = true; relativeSign = -1; piece->pntr++; piece->length--; }
					else if (piece->pntr[0] == '+') { isRelative = true; relativeSign = 1; piece->pntr++; piece->length--; }
				}
				i32 parsedValue = 0;
				if (!TryParseI32(piece->pntr, piece->length, &parsedValue))
				{
					arg->parseFailure = true;
					if (!argDef->allowParseErrors)
					{
						result = false;
						*errorStrOut = TempPrint("Can't parse argument[%u] %s as i32: \"%.*s\"", aIndex, arg->name, piece->length, piece->pntr);
						break;
					}
				}
				else if (argDef->enforceMin && parsedValue < argDef->minValueI32)
				{
					result = false;
					*errorStrOut = TempPrint("%s must be more than %d: given=%d", argDef->name, argDef->minValueI32, parsedValue);
					break;
				}
				else if (argDef->enforceMax && parsedValue > argDef->maxValueI32)
				{
					result = false;
					*errorStrOut = TempPrint("%s must be less than %d: given=%d", argDef->name, argDef->maxValueI32, parsedValue);
					break;
				}
				else
				{
					arg->valueI32 = parsedValue;
					if (isRelative)
					{
						arg->valueI32 *= relativeSign;
						arg->relativeValue = true;
					}
				}
			}
			else if (argDef->type == CommandArgType_R32)
			{
				r32 parsedValue = 0;
				char* pieceNulltermTemp = ArenaString(TempArena, piece->pntr, piece->length);
				Assert(pieceNulltermTemp != nullptr);
				if (!TryParseR32(pieceNulltermTemp, &parsedValue))
				{
					arg->parseFailure = true;
					if (!argDef->allowParseErrors)
					{
						result = false;
						*errorStrOut = TempPrint("Can't parse argument[%u] %s as r32: \"%.*s\"", aIndex, arg->name, piece->length, piece->pntr);
						break;
					}
				}
				else if (argDef->enforceMin && parsedValue < argDef->minValueR32)
				{
					result = false;
					*errorStrOut = TempPrint("%s must be more than %f: given=%f", argDef->name, argDef->minValueR32, parsedValue);
					break;
				}
				else if (argDef->enforceMax && parsedValue > argDef->maxValueR32)
				{
					result = false;
					*errorStrOut = TempPrint("%s must be less than %f: given=%f", argDef->name, argDef->maxValueR32, parsedValue);
					break;
				}
				else
				{
					arg->valueR32 = parsedValue;
				}
			}
		}
		else
		{
			arg->missing = true;
			if (argDef->required)
			{
				result = false;
				*errorStrOut = TempPrint("Required Argument[%u] missing: %s %s", aIndex, GetCommandArgTypeStr(argDef->type), argDef->name);
				break;
			}
		}
	}
	
	return result;
}

#define ParseCmdArgs(argsStr, argsArrayName, argsCountName) DynArray_t argsArrayName = {}; u32 argsCountName = 0; do \
{                                                                                                                    \
	char* errorStr = nullptr;                                                                                        \
	if (!_ParseCmdArgs(&argDefs, argsStr, &argsArrayName, &argsCountName, &errorStr))                                \
	{                                                                                                                \
		WriteLine_E(errorStr);                                                                                       \
		return;                                                                                                      \
	}                                                                                                                \
} while(0)

#define HandleFirstCommand(cmdStr) if (MyStrCompareNt(baseCmd, (cmdStr)) == 0)
#define HandleCommand(cmdStr) else if (MyStrCompareNt(baseCmd, (cmdStr)) == 0)

bool WasCmdArgGiven(const DynArray_t* args, const char* argName)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			return !arg->missing;
		}
	}
	return false;
}
bool WasCmdArgValid(const DynArray_t* args, const char* argName)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			return (!arg->missing && !arg->parseFailure);
		}
	}
	return false;
}
bool WasCmdArgRelative(const DynArray_t* args, const char* argName)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			return (!arg->missing && !arg->parseFailure && arg->relativeValue);
		}
	}
	return false;
}
bool WasCmdArgEqual(const DynArray_t* args, const char* argName, const char* compareStr)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			return DoesSplitPieceEqual(&arg->rawStr, compareStr);
		}
	}
	Assert(false);
	return false;
}
bool WasCmdArgEqualIgnoreCase(const DynArray_t* args, const char* argName, const char* compareStr)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			return DoesSplitPieceEqualIgnoreCaseNt(&arg->rawStr, compareStr);
		}
	}
	Assert(false);
	return false;
}

const StrSplitPiece_t* GetCmdArgRaw(const DynArray_t* args, const char* argName)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			return &arg->rawStr;
		}
	}
	Assert(false);
	return false;
}
const StrSplitPiece_t* GetCmdArgString(const DynArray_t* args, const char* argName)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_String);
			Assert(!arg->missing && !arg->parseFailure);
			return &arg->valueStr;
		}
	}
	Assert(false);
	return false;
}
const StrSplitPiece_t* TryGetCmdArgString(const DynArray_t* args, const char* argName, const StrSplitPiece_t* defaultValue)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_String);
			if (arg->missing || arg->parseFailure) { return defaultValue; }
			else { return &arg->valueStr; }
		}
	}
	Assert(false);
	return false;
}
bool GetCmdArgBool(const DynArray_t* args, const char* argName)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_Bool);
			Assert(!arg->missing && !arg->parseFailure);
			return arg->valueBool;
		}
	}
	Assert(false);
	return false;
}
bool TryGetCmdArgBool(const DynArray_t* args, const char* argName, bool defaultValue)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_Bool);
			if (arg->missing || arg->parseFailure) { return defaultValue; }
			else { return arg->valueBool; }
		}
	}
	Assert(false);
	return false;
}
u32 GetCmdArgU32(const DynArray_t* args, const char* argName)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_U32);
			Assert(!arg->missing && !arg->parseFailure);
			return arg->valueU32;
		}
	}
	Assert(false);
	return false;
}
u32 TryGetCmdArgU32(const DynArray_t* args, const char* argName, u32 defaultValue)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_U32);
			if (arg->missing || arg->parseFailure) { return defaultValue; }
			else { return arg->valueU32; }
		}
	}
	Assert(false);
	return false;
}
i32 GetCmdArgI32(const DynArray_t* args, const char* argName)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_I32);
			Assert(!arg->missing && !arg->parseFailure);
			return arg->valueI32;
		}
	}
	Assert(false);
	return false;
}
i32 TryGetCmdArgI32(const DynArray_t* args, const char* argName, i32 defaultValue)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_I32);
			if (arg->missing || arg->parseFailure) { return defaultValue; }
			else { return arg->valueI32; }
		}
	}
	Assert(false);
	return false;
}
r32 GetCmdArgR32(const DynArray_t* args, const char* argName)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_R32);
			Assert(!arg->missing && !arg->parseFailure);
			return arg->valueR32;
		}
	}
	Assert(false);
	return false;
}
r32 TryGetCmdArgR32(const DynArray_t* args, const char* argName, r32 defaultValue)
{
	Assert(args != nullptr);
	Assert(argName != nullptr);
	for (u32 aIndex = 0; aIndex < args->length; aIndex++)
	{
		const CommandArg_t* arg = DynArrayGet(args, CommandArg_t, aIndex);
		Assert(arg != nullptr);
		if (arg->name != nullptr && StrCompareIgnoreCaseNt(arg->name, argName))
		{
			Assert(arg->type == CommandArgType_R32);
			if (arg->missing || arg->parseFailure) { return defaultValue; }
			else { return arg->valueR32; }
		}
	}
	Assert(false);
	return false;
}