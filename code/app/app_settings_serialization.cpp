/*
File:   app_settings_serialization.cpp
Author: Taylor Robbins
Date:   06\17\2019
Description: 
	** Holds the functions that help us serialize and deserialize the AppSettings_t structure to be saved or loaded from a file 
*/

#define TEXT_SETTINGS_TITLE "PiggyBot Settings :O"
#define TEXT_SETTINGS_TITLE_LENGTH 20

#define TEXT_SETTINGS_VERS_MAJOR 1
#define TEXT_SETTINGS_VERS_MINOR 0

// +--------------------------------------------------------------+
// |                     Settings Save Format                     |
// +--------------------------------------------------------------+
char* SerializeSettingsText(MemoryArena_t* arenaPntr, const AppSettings_t* settingsPntr, u32* numBytesOut)
{
	Assert(settingsPntr != nullptr);
	
	char* result = nullptr;
	u32 resultSize = 0;
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 offset = 0;
		
		SrlPrint(result, resultSize, &offset, "%s\n", TEXT_SETTINGS_TITLE);
		SrlPrint(result, resultSize, &offset, "// Saved on %s %s %u\n", GetMonthStr((Month_t)LocalTime.month), GetDayOfMonthString(LocalTime.day), LocalTime.year);
		SrlPrint(result, resultSize, &offset, "Version: %u.%u\n", TEXT_SETTINGS_VERS_MAJOR, TEXT_SETTINGS_VERS_MINOR);
		
		u32 numSettings = GetNumAppSettings();
		for (u32 sIndex = 0; sIndex < numSettings; sIndex++)
		{
			const char* settingName = nullptr;
			const void* settingValuePntr = nullptr;
			SettingType_t settingType = GetSettingInfo(settingsPntr, sIndex, &settingName, &settingValuePntr);
			Assert(settingType != SettingType_None);
			
			if (settingType == SettingType_Boolean) { bool boolValue = *((bool*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %s\n", settingName, boolValue ? "True" : "False"); }
			if (settingType == SettingType_u8)      { u8  u8Value  = *((u8*)settingValuePntr);  SrlPrint(result, resultSize, &offset, "%s: %u\n",  settingName, u8Value);  }
			if (settingType == SettingType_u32)     { u32 u32Value = *((u32*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %u\n",  settingName, u32Value); }
			if (settingType == SettingType_i32)     { i32 i32Value = *((i32*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %d\n",  settingName, i32Value); }
			if (settingType == SettingType_r32)     { r32 r32Value = *((r32*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %f\n",  settingName, r32Value); }
			if (settingType == SettingType_Version) { Version_t versionValue = *((Version_t*)settingValuePntr); SrlPrint(result, resultSize, &offset, "%s: %u.%u(%u)\n",  settingName, versionValue.major, versionValue.minor, versionValue.build); }
		}
		
		if (pass == 0)
		{
			Assert(result == nullptr);
			resultSize = offset + 1; //add 1 byte to compensate for vsnprintf wierdness in SrlPrint
			if (numBytesOut != nullptr) { *numBytesOut = resultSize-1; }
			if (arenaPntr == nullptr) { return nullptr; }
			if (resultSize == 0) { return nullptr; }
			result = PushArray(arenaPntr, char, resultSize);
		}
		else
		{
			Assert(offset+1 == resultSize);
		}
	}
	Assert(result != nullptr);
	return result;
}

//NOTE: This function needs to support using the TempArena which might be the platform layer's temparena when called in App_GetStartupOptions
bool DeserializeSettingsText(MemoryArena_t* arenaPntr, AppSettings_t* settings, const void* fileData, u32 fileLength, u8* versionMajorOut = nullptr, u8* versionMinorOut = nullptr)
{
	Assert(arenaPntr != nullptr);
	Assert(settings != nullptr);
	const char* charPntr = (const char*)fileData;
	u32 numCharsLeft = fileLength;
	
	if (fileData == nullptr) { return false; }
	if (numCharsLeft < TEXT_SETTINGS_TITLE_LENGTH) { return false; }
	if (MyMemCompare(charPntr, TEXT_SETTINGS_TITLE, TEXT_SETTINGS_TITLE_LENGTH) != 0) { return false; }
	charPntr += TEXT_SETTINGS_TITLE_LENGTH;
	numCharsLeft -= TEXT_SETTINGS_TITLE_LENGTH;
	
	InitAppSettingsStruct(arenaPntr, settings);
	FillDefaultAppSettings(settings);
	
	TextParser_t parser;
	CreateTextParser(&parser, charPntr, numCharsLeft);
	
	bool foundVersion     = false;
	u8   versionMajor     = 0;
	u8   versionMinor     = 0;
	bool isOldVersion     = false;
	
	// +--------------------------------------------------------------+
	// |                      Main Parsing Loop                       |
	// +--------------------------------------------------------------+
	while (parser.position < parser.contentLength)
	{
		ParsingToken_t token;
		const char* errorStr = ParserConsumeToken(&parser, &token);
		if (errorStr != nullptr)
		{
			PrintLine_W("Error while parsing settings line %u: %s", parser.lineNumber, errorStr);
			DestroyAppSettingsStruct(settings);
			return false;
		}
		if (!parser.foundToken)
		{
			Assert(parser.position >= parser.contentLength);
			break;
		}
		
		// +==============================+
		// |          Key-Value           |
		// +==============================+
		if (token.type == ParsingTokenType_KeyValue)
		{
			// +==============================+
			// |           Version            |
			// +==============================+
			if (StrCompareIgnoreCase(token.key.pntr, "Version", token.key.length))
			{
				v2i versionV2i = {};
				errorStr = TryDeserializeVec2i(token.value.pntr, token.value.length, &versionV2i);
				if (errorStr != nullptr)
				{
					PrintLine_W("Error while parsing settings version number: %s", errorStr);
					DestroyAppSettingsStruct(settings);
					return false;
				}
				if (versionV2i.x < 0 || versionV2i.x > 255)
				{
					PrintLine_W("Invalid settings major version number: %d", versionV2i.x);
					DestroyAppSettingsStruct(settings);
					return false;
				}
				if (versionV2i.y < 0 || versionV2i.y > 255)
				{
					PrintLine_W("Invalid settings minor version number: %d", versionV2i.y);
					DestroyAppSettingsStruct(settings);
					return false;
				}
				
				versionMajor = (u8)versionV2i.x;
				versionMinor = (u8)versionV2i.y;
				if (versionMajorOut != nullptr) { *versionMajorOut = versionMajor; }
				if (versionMinorOut != nullptr) { *versionMinorOut = versionMinor; }
				
				if (!(versionMajor == TEXT_SETTINGS_VERS_MAJOR && versionMinor == TEXT_SETTINGS_VERS_MINOR) &&
					!(versionMajor == TEXT_SETTINGS_VERS_MAJOR && versionMinor == TEXT_SETTINGS_VERS_MINOR-1))
				{
					PrintLine_W("Settings is version %u.%u not %u.%u", versionMajor, versionMinor, TEXT_SETTINGS_VERS_MAJOR, TEXT_SETTINGS_VERS_MINOR);
					DestroyAppSettingsStruct(settings);
					return false; //deal breaker
				}
				isOldVersion = (versionMajor < TEXT_SETTINGS_VERS_MAJOR || (versionMajor == TEXT_SETTINGS_VERS_MAJOR && versionMinor < TEXT_SETTINGS_VERS_MINOR));
				foundVersion = true;
			}
			else
			{
				u32 settingIndex = GetSettingIndexByName(token.key.pntr, token.key.length);
				if (settingIndex < GetNumAppSettings())
				{
					const char* settingName = nullptr;
					void* settingValuePntr = nullptr;
					SettingType_t settingType = GetSettingInfo(settings, settingIndex, &settingName, &settingValuePntr);
					Assert(settingType != SettingType_None);
					Assert(settingName != nullptr);
					Assert(settingValuePntr != nullptr);
					
					if (settingType == SettingType_Boolean)
					{
						bool* boolPntr = (bool*)settingValuePntr;
						bool boolValue = false;
						errorStr = TryDeserializeBool(token.value.pntr, token.value.length, &boolValue);
						if (errorStr == nullptr)
						{
							*boolPntr = boolValue;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as boolean for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_u8)
					{
						u8* u8Pntr = (u8*)settingValuePntr;
						u8 u8Value = 0;
						errorStr = TryDeserializeU8(token.value.pntr, token.value.length, &u8Value);
						if (errorStr == nullptr)
						{
							*u8Pntr = u8Value;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as u8 for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_u32)
					{
						u32* u32Pntr = (u32*)settingValuePntr;
						u32 u32Value = 0;
						errorStr = TryDeserializeU32(token.value.pntr, token.value.length, &u32Value);
						if (errorStr == nullptr)
						{
							*u32Pntr = u32Value;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as u32 for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_i32)
					{
						i32* i32Pntr = (i32*)settingValuePntr;
						i32 i32Value = 0;
						errorStr = TryDeserializeI32(token.value.pntr, token.value.length, &i32Value);
						if (errorStr == nullptr)
						{
							*i32Pntr = i32Value;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as i32 for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_r32)
					{
						r32* r32Pntr = (r32*)settingValuePntr;
						r32 r32Value = 0;
						errorStr = TryDeserializeR32(token.value.pntr, token.value.length, &r32Value);
						if (errorStr == nullptr)
						{
							*r32Pntr = r32Value;
						}
						else
						{
							PrintLine_E("Couldn't parse \"%.*s\" as r32 for setting \"%s\": %s", token.value.length, token.value.pntr, settingName, errorStr);
						}
					}
					else if (settingType == SettingType_Version)
					{
						Version_t* versionPntr = (Version_t*)settingValuePntr;
						u32 numNumbers = 0;
						u32* numbers = ParseNumbersInString(TempArena, token.value.pntr, token.value.length, &numNumbers);
						if (numNumbers == 3)
						{
							if (numbers[0] > 255)
							{
								PrintLine_E("Major version number is too high for setting \"%s\": %u > 255", settingName, numbers[0]);
							}
							else if (numbers[1] > 255)
							{
								PrintLine_E("Minor version number is too high for setting \"%s\": %u > 255", settingName, numbers[1]);
							}
							else if (numbers[2] > 65535)
							{
								PrintLine_E("Build version number is too high for setting \"%s\": %u > 65535", settingName, numbers[2]);
							}
							else
							{
								versionPntr->major = (u8)numbers[0];
								versionPntr->minor = (u8)numbers[1];
								versionPntr->build = (u16)numbers[2];
							}
						}
						else
						{
							PrintLine_E("Found %u/3 numbers in Version setting \"%s\": %.*s", numNumbers, settingName, token.value.length, token.value.pntr);
						}
					}
					else
					{
						PrintLine_E("Unhandled setting type in deserialization: %u", settingType);
					}
				}
				else
				{
					PrintLine_E("Unknown setting found: \"%.*s\"", token.key.length, token.key.pntr);
				}
			}
		}
		// +==============================+
		// |           Comment            |
		// +==============================+
		else if (token.type == ParsingTokenType_Comment)
		{
			//Ignore the comment
		}
		// +==============================+
		// |       Unhandled Token        |
		// +==============================+
		else
		{
			if (token.pieces[0].length >= 7 && MyStrCompare(token.pieces[0].pntr, "# Saved", 7) == 0)
			{
				//This was the old format for the "# Saved On ..." comment
			}
			else
			{
				PrintLine_W("Found unsupported token in settings line %u: \"%.*s\"", parser.lineNumber, token.pieces[0].length, token.pieces[0].pntr);
				DestroyAppSettingsStruct(settings);
				return false;
			}
		}
	}
	
	// +==============================+
	// | Check Everything was defined |
	// +==============================+
	if (!foundVersion)
	{
		PrintLine_W("Settings contained no Version!");
		DestroyAppSettingsStruct(settings);
		return false;
	}
	
	return true;
}

bool SaveAppSettingsTo(const AppSettings_t* settings, const char* filePath) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	Assert(filePath != nullptr);
	
	TempPushMark();
	u32 serializedSize = 0;
	char* serializedContents = SerializeSettingsText(TempArena, settings, &serializedSize);
	if (serializedContents == nullptr || serializedSize == 0)
	{
		WriteLine_E("Failed to serialize the settings structure");
		TempPopMark();
		return false;
	}
	
	if (!platform->WriteEntireFile(filePath, serializedContents, serializedSize))
	{
		PrintLine_E("Failed to write %u byte settings file to \"%s\"", serializedSize, filePath);
		TempPopMark();
		return false;
	}
	
	TempPopMark();
	return true;
}

bool SaveAppSettings(const AppSettings_t* settings) //pre-declared in app_settings.h
{
	char* settingsFolder = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Settings, GAME_NAME_FOR_FOLDERS);
	if (settingsFolder == nullptr)
	{
		WriteLine_E("Failed to get path to settings folder location");
		return false;
	}
	if (!platform->DoesFolderExist(settingsFolder))
	{
		// bool createSuccess = platform->CreateFolder(settingsFolder);
		bool createSuccess = CreateDirectories(settingsFolder);
		if (!createSuccess)
		{
			PrintLine_E("Failed to create directory for settings file at \"%s\"", settingsFolder);
			return false;
		}
	}
	char* settingsFilePath = TempPrint("%s%s", settingsFolder, SETTINGS_FILE_NAME);
	Assert(settingsFilePath != nullptr);
	return SaveAppSettingsTo(settings, settingsFilePath);
}

bool LoadAppSettingsFrom(AppSettings_t* settings, const char* filePath) //pre-declared in app_settings.h
{
	Assert(settings != nullptr);
	Assert(filePath != nullptr);
	
	FileInfo_t settingsFile = platform->ReadEntireFile(filePath);
	if (settingsFile.content == nullptr || settingsFile.size == 0)
	{
		PrintLine_E("Couldn't open settings file at \"%s\"", filePath);
		return false;
	}
	
	TempPushMark();
	AppSettings_t newSettings = {};
	if (!DeserializeSettingsText(TempArena, &newSettings, (const char*)settingsFile.content, settingsFile.size))
	{
		PrintLine_E("Settings file is corrupt at \"%s\"", filePath);
		TempPopMark();
		platform->FreeFileMemory(&settingsFile);
		return false;
	}
	DestroyAppSettingsStruct(settings);
	CopyAppSettingsStruct(mainHeap, settings, &newSettings);
	TempPopMark();
	
	return true;
}

bool LoadAppSettings(AppSettings_t* settings)
{
	char* settingsFolder = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Settings, GAME_NAME_FOR_FOLDERS);
	if (settingsFolder == nullptr)
	{
		WriteLine_E("Failed to get path to settings folder location");
		return false;
	}
	char* settingsFilePath = TempPrint("%s%s", settingsFolder, SETTINGS_FILE_NAME);
	Assert(settingsFilePath != nullptr);
	return LoadAppSettingsFrom(settings, settingsFilePath);
}

bool DeleteSettingsFile()
{
	char* settingsFolder = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Settings, GAME_NAME_FOR_FOLDERS);
	if (settingsFolder == nullptr)
	{
		WriteLine_E("Failed to get path to settings folder location");
		return true; //I guess we can just treat this as success since we aren't sure the file exists
	}
	char* settingsFilePath = TempPrint("%s%s", settingsFolder, SETTINGS_FILE_NAME);
	if (platform->DoesFileExist(settingsFilePath))
	{
		if (platform->DeleteFile(settingsFilePath))
		{
			return true;
		}
		else
		{
			PrintLine_E("Failed to delete settings file at \"%s\"", settingsFilePath);
			return false;
		}
	}
	else { return true; } //file already doesn't exist. That's success
}
