/*
File:   app_ease_mover.h
Author: Taylor Robbins
Date:   07\22\2020
*/

#ifndef _APP_EASE_MOVER_H
#define _APP_EASE_MOVER_H

struct EaseMover_t
{
	v2 oldPos;
	v2 newPos;
	r32 currentTime;
	r32 moveTime;
	EasingStyle_t easingStyle;
};

void InitEaseMover(EaseMover_t* mover, v2 startingPos, EasingStyle_t easingStyle = EasingStyle_QuadraticInOut)
{
	Assert(mover != nullptr);
	Assert(easingStyle > EasingStyle_None);
	Assert(easingStyle < EasingStyle_NumStyles);
	ClearPointer(mover);
	mover->oldPos = startingPos;
	mover->newPos = startingPos;
	mover->currentTime = 0.0f;
	mover->moveTime = 0.0f;
	mover->easingStyle = easingStyle;
}

v2 GetEaseMoverPos(const EaseMover_t* mover)
{
	Assert(mover != nullptr);
	Assert(mover->easingStyle > EasingStyle_None);
	Assert(mover->easingStyle < EasingStyle_NumStyles);
	if (mover->moveTime <= 0.0f) { return mover->newPos; }
	return mover->oldPos + ((mover->newPos - mover->oldPos) * Ease(mover->easingStyle, mover->currentTime / mover->moveTime));
}

void UpdateEaseMover(EaseMover_t* mover, r32 elapsedMs)
{
	Assert(mover != nullptr);
	Assert(mover->easingStyle > EasingStyle_None);
	Assert(mover->easingStyle < EasingStyle_NumStyles);
	if (mover->currentTime < mover->moveTime)
	{
		mover->currentTime += elapsedMs;
		if (mover->currentTime >= mover->moveTime)
		{
			mover->currentTime = mover->moveTime;
		}
	}
	else
	{
		mover->currentTime = mover->moveTime;
	}
}

void EaseMoverMoveTo(EaseMover_t* mover, v2 newPos, r32 moveTime = 500, EasingStyle_t easingStyle = EasingStyle_None)
{
	Assert(mover != nullptr);
	Assert(mover->easingStyle > EasingStyle_None);
	Assert(mover->easingStyle < EasingStyle_NumStyles);
	Assert(easingStyle < EasingStyle_NumStyles);
	Assert(moveTime >= 0);
	if (moveTime == 0)
	{
		//instant move
		mover->oldPos = newPos;
		mover->newPos = newPos;
		mover->currentTime = 0.0f;
		mover->moveTime = 0.0f;
		if (easingStyle != EasingStyle_None) { mover->easingStyle = easingStyle; }
		return;
	}
	if (mover->currentTime < mover->moveTime && mover->newPos != mover->oldPos)
	{
		// r32 oldTotalDist = Vec2Length(mover->newPos - mover->oldPos);
		// r32 oldTotalTime = mover->moveTime;
		v2 currentPos = GetEaseMoverPos(mover);
		v2 newHeadingDir = Vec2Normalize(newPos - currentPos);
		r32 currentPercent = mover->currentTime / mover->moveTime;
		r32 currentMovePercent = Ease(mover->easingStyle, currentPercent);
		r32 newDist = Vec2Length(newPos - currentPos);
		if (newDist == 0 || currentMovePercent >= 1.0f) { EaseMoverMoveTo(mover, newPos, 0, easingStyle); return; } //degenerate case, stop movement instantly
		r32 extendBackDist = newDist * (currentMovePercent / (1 - currentMovePercent));
		// r32 newTotalDist = newDist + extendBackDist;
		// r32 newTotalTime = (newTotalDist / oldTotalDist) * oldTotalTime;
		// r32 newTime = newTotalTime * currentPercent;
		v2 newStartPos = currentPos - (newHeadingDir * extendBackDist);
		mover->oldPos = newStartPos;
		mover->newPos = newPos;
		// mover->currentTime = newTime;
		// mover->moveTime = newTotalTime;
		if (easingStyle != EasingStyle_None) { mover->easingStyle = easingStyle; }
	}
	else
	{
		mover->oldPos = GetEaseMoverPos(mover);
		mover->newPos = newPos;
		mover->currentTime = 0;
		mover->moveTime = moveTime;
		if (easingStyle != EasingStyle_None) { mover->easingStyle = easingStyle; }
	}
}
void EaseMoverSetPos(EaseMover_t* mover, v2 newPos)
{
	EaseMoverMoveTo(mover, newPos, 0, EasingStyle_None);
}

#endif //  _APP_EASE_MOVER_H
