/*
File:   app_dynamic_bitmap.h
Author: Taylor Robbins
Date:   07\28\2020
Description: 
*/

#ifndef _APP_DYNAMIC_BITMAP_H
#define _APP_DYNAMIC_BITMAP_H

struct DynBitmap_t
{
	MemoryArena_t* allocArena;
	union
	{
		reci bounds;
		struct
		{
			union
			{
				v2i offset;
				struct { i32 x, y; };
			};
			union
			{
				v2i size;
				struct { i32 width, height; };
			};
		};
	};
	u32 area;
	u32 bitmapSize;
	u8* bitmapData;
	bool debugOutput;
};

#endif //_APP_DYNAMIC_BITMAP_H

