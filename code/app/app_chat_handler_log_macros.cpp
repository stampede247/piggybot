/*
File:   app_chat_handler_log_macros.cpp
Author: Taylor Robbins
Date:   11\07\2020
Description: 
	** Just a way to make it easier to write to the chat ConsoleArena_t log
*/

#define ChatWrite_D(chat, message)                       CslArenaWrite_D(&chat->log, message)
#define ChatPrint_D(chat, formatString, ...)             CslArenaPrint_D(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWriteLine_D(chat, message)                   CslArenaWriteLine_D(&chat->log, message)
#define ChatPrintLine_D(chat, formatString, ...)         CslArenaPrintLine_D(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWrite_Dx(chat, flags, message)               CslArenaWrite_Dx(&chat->log, flags, message)
#define ChatPrint_Dx(chat, flags, formatString, ...)     CslArenaPrint_Dx(&chat->log, flags, formatString, ##__VA_ARGS__)
#define ChatWriteLine_Dx(chat, flags, message)           CslArenaWriteLine_Dx(&chat->log, flags, message)
#define ChatPrintLine_Dx(chat, flags, formatString, ...) CslArenaPrintLine_Dx(&chat->log, flags, formatString, ##__VA_ARGS__)

#define ChatWrite_R(chat, message)                       CslArenaWrite_R(&chat->log, message)
#define ChatPrint_R(chat, formatString, ...)             CslArenaPrint_R(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWriteLine_R(chat, message)                   CslArenaWriteLine_R(&chat->log, message)
#define ChatPrintLine_R(chat, formatString, ...)         CslArenaPrintLine_R(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWrite_Rx(chat, flags, message)               CslArenaWrite_Rx(&chat->log, flags, message)
#define ChatPrint_Rx(chat, flags, formatString, ...)     CslArenaPrint_Rx(&chat->log, flags, formatString, ##__VA_ARGS__)
#define ChatWriteLine_Rx(chat, flags, message)           CslArenaWriteLine_Rx(&chat->log, flags, message)
#define ChatPrintLine_Rx(chat, flags, formatString, ...) CslArenaPrintLine_Rx(&chat->log, flags, formatString, ##__VA_ARGS__)

#define ChatWrite_I(chat, message)                       CslArenaWrite_I(&chat->log, message)
#define ChatPrint_I(chat, formatString, ...)             CslArenaPrint_I(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWriteLine_I(chat, message)                   CslArenaWriteLine_I(&chat->log, message)
#define ChatPrintLine_I(chat, formatString, ...)         CslArenaPrintLine_I(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWrite_Ix(chat, flags, message)               CslArenaWrite_Ix(&chat->log, flags, message)
#define ChatPrint_Ix(chat, flags, formatString, ...)     CslArenaPrint_Ix(&chat->log, flags, formatString, ##__VA_ARGS__)
#define ChatWriteLine_Ix(chat, flags, message)           CslArenaWriteLine_Ix(&chat->log, flags, message)
#define ChatPrintLine_Ix(chat, flags, formatString, ...) CslArenaPrintLine_Ix(&chat->log, flags, formatString, ##__VA_ARGS__)

#define ChatWrite_N(chat, message)                       CslArenaWrite_N(&chat->log, message)
#define ChatPrint_N(chat, formatString, ...)             CslArenaPrint_N(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWriteLine_N(chat, message)                   CslArenaWriteLine_N(&chat->log, message)
#define ChatPrintLine_N(chat, formatString, ...)         CslArenaPrintLine_N(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWrite_Nx(chat, flags, message)               CslArenaWrite_Nx(&chat->log, flags, message)
#define ChatPrint_Nx(chat, flags, formatString, ...)     CslArenaPrint_Nx(&chat->log, flags, formatString, ##__VA_ARGS__)
#define ChatWriteLine_Nx(chat, flags, message)           CslArenaWriteLine_Nx(&chat->log, flags, message)
#define ChatPrintLine_Nx(chat, flags, formatString, ...) CslArenaPrintLine_Nx(&chat->log, flags, formatString, ##__VA_ARGS__)

#define ChatWrite_O(chat, message)                       CslArenaWrite_O(&chat->log, message)
#define ChatPrint_O(chat, formatString, ...)             CslArenaPrint_O(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWriteLine_O(chat, message)                   CslArenaWriteLine_O(&chat->log, message)
#define ChatPrintLine_O(chat, formatString, ...)         CslArenaPrintLine_O(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWrite_Ox(chat, flags, message)               CslArenaWrite_Ox(&chat->log, flags, message)
#define ChatPrint_Ox(chat, flags, formatString, ...)     CslArenaPrint_Ox(&chat->log, flags, formatString, ##__VA_ARGS__)
#define ChatWriteLine_Ox(chat, flags, message)           CslArenaWriteLine_Ox(&chat->log, flags, message)
#define ChatPrintLine_Ox(chat, flags, formatString, ...) CslArenaPrintLine_Ox(&chat->log, flags, formatString, ##__VA_ARGS__)

#define ChatWrite_W(chat, message)                       CslArenaWrite_W(&chat->log, message)
#define ChatPrint_W(chat, formatString, ...)             CslArenaPrint_W(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWriteLine_W(chat, message)                   CslArenaWriteLine_W(&chat->log, message)
#define ChatPrintLine_W(chat, formatString, ...)         CslArenaPrintLine_W(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWrite_Wx(chat, flags, message)               CslArenaWrite_Wx(&chat->log, flags, message)
#define ChatPrint_Wx(chat, flags, formatString, ...)     CslArenaPrint_Wx(&chat->log, flags, formatString, ##__VA_ARGS__)
#define ChatWriteLine_Wx(chat, flags, message)           CslArenaWriteLine_Wx(&chat->log, flags, message)
#define ChatPrintLine_Wx(chat, flags, formatString, ...) CslArenaPrintLine_Wx(&chat->log, flags, formatString, ##__VA_ARGS__)

#define ChatWrite_E(chat, message)                       CslArenaWrite_E(&chat->log, message)
#define ChatPrint_E(chat, formatString, ...)             CslArenaPrint_E(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWriteLine_E(chat, message)                   CslArenaWriteLine_E(&chat->log, message)
#define ChatPrintLine_E(chat, formatString, ...)         CslArenaPrintLine_E(&chat->log, formatString, ##__VA_ARGS__)
#define ChatWrite_Ex(chat, flags, message)               CslArenaWrite_Ex(&chat->log, flags, message)
#define ChatPrint_Ex(chat, flags, formatString, ...)     CslArenaPrint_Ex(&chat->log, flags, formatString, ##__VA_ARGS__)
#define ChatWriteLine_Ex(chat, flags, message)           CslArenaWriteLine_Ex(&chat->log, flags, message)
#define ChatPrintLine_Ex(chat, flags, formatString, ...) CslArenaPrintLine_Ex(&chat->log, flags, formatString, ##__VA_ARGS__)


