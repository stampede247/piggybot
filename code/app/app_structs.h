/*
File:   app_structs.h
Author: Taylor Robbins
Date:   11\04\2017
*/

#ifndef _APP_STRUCTS_H
#define _APP_STRUCTS_H

typedef enum
{
	SoundTransition_Immediate = 0x00,
	SoundTransition_FadeOut,
	SoundTransition_FadeIn,
	SoundTransition_FadeOutFadeIn,
	SoundTransition_CrossFade,
	SoundTransition_NumOptions,
} SoundTransition_t;

struct Texture_t
{
	GLuint id;
	bool isValid;
	
	union
	{
		v2i size;
		struct { i32 width, height; };
	};
};

struct MapAnim_t
{
	u32 index;
	Texture_t texture;
	v2i frameSize;
	u32 numFrames;
};

struct Sound_t
{
	bool isValid;
	bool isLoading;
	ALuint id;
	ALenum format;
	ALsizei frequency;
	u32 channelCount;
	u32 sampleCount;
};

struct SoundInstance_t
{
	bool playing;
	bool isLoopingSound;
	bool loopContinueFlag;
	r32 distVolume;
	v2i tilePos;
	u64 startTime;
	ALuint id;
	ALuint soundId;
};

struct BufferedSoundInstance_t
{
	MemoryArena_t* allocArena;
	bool playing;
	ALuint id;
	u32 numBuffers;
	ALuint* bufferIds;
};

struct SpriteSheet_t
{
	MemoryArena_t* allocArena;
	Texture_t texture;
	bool* frameFilledFlags;
	
	union
	{
		v2i padding;
		struct { i32 padX, padY; };
	};
	union
	{
		v2i frameSize;
		struct { i32 frameX, frameY; };
	};
	union
	{
		v2i innerFrameSize;
		struct { i32 innerX, innerY; };
	};
	union
	{
		v2i sheetSize;
		v2i numFrames;
		struct { i32 framesX, framesY; };
	};
};

struct FontCharInfo_t
{
	union
	{
		v2i position;
		struct { i32 x, y; };
	};
	union
	{
		v2i size;
		struct { i32 width, height; };
	};
	v2 offset;
	r32 advanceX;
};

struct Font_t
{
	MemoryArena_t* allocArena;
	bool isValid;
	
	Texture_t bitmap;
	r32 fontSize;
	r32 maxCharWidth;
	r32 maxCharHeight;
	r32 maxExtendUp;
	r32 maxExtendDown;
	r32 lineHeight;
	
	bool isBitmapFont;
	u8 firstChar;
	u32 numChars;
	u8 unknownCharIndex;
	FontCharInfo_t* charInfos;
};

struct Shader_t
{
	MemoryArena_t* allocArena;
	bool isValid;
	bool usesSheetFrameVerts;
	
	GLuint vertId;
	GLuint fragId;
	GLuint programId;
	
	GLuint vertexArray;
	
	char* vertLog;
	char* fragLog;
	char* progLog;
	
	struct
	{
		//Attributes
		GLint positionAttrib;
		GLint colorAttrib;
		GLint texCoordAttrib;
		GLint replaceColorAttribs[NUM_REPLACE_COLORS];
		
		//Vertex Shader
		GLint worldMatrix;
		GLint viewMatrix;
		GLint projectionMatrix;
		
		//Both
		GLint texture;
		GLint textureSize;
		GLint altTexture;
		GLint altTextureSize;
		GLint sourceRectangle;
		GLint altSourceRectangle;
		GLint maskRectangle;
		GLint shiftVec;
		
		//Fragment Shader
		GLint primaryColor;
		GLint secondaryColor;
		GLint replaceColors;
		GLint gradientEnabled;
		GLint circleRadius;
		GLint circleInnerRadius;
		GLint saturation;
		GLint brightness;
		GLint time;
		GLint value0;
		GLint value1;
		GLint value2;
		GLint value3;
		GLint value4;
		GLint mousePos;
		GLint screenSize;
	} locations;
};

struct VertexBuffer_t
{
	bool isDynamic;
	bool isSheetVertices;
	GLuint id;
	u32 numVertices;
};

struct Vertex_t
{
	union
	{
		v3 position;
		struct { r32 x, y, z; };
	};
	union
	{
		v4 color;
		struct { r32 r, g, b, a; };
	};
	union
	{
		v2 texCoord;
		struct { r32 tX, tY; };
	};
};
struct SheetFrameVertex_t
{
	v3 position;
	v4 color;
	v2 texCoord;
	v4 replaceColors[NUM_REPLACE_COLORS-4];
};

struct FontBatchVertex_t
{
	v3 position; //includes depth
	v4 color;
	rec sourceRec;
	r32 scale;
};

struct SheetBatchVertex_t
{
	v3 position; //includes depth
	v2 size;
	v4 color;
	v4 replaceColors[NUM_REPLACE_COLORS];
	v2i frame;
	u8 rotationAndFlags;
};

struct FrameBuffer_t
{
	GLuint id;
	GLuint depthBuffId;
	Texture_t texture;
	bool hasTransparency;
	bool hasStencil;
};

typedef enum
{
	Alignment_Left = 0x00,
	Alignment_Center,
	Alignment_Right,
	Alignment_NumOptions,
} Alignment_t;

const char* GetAlignmentStr(Alignment_t alignment)
{
	switch (alignment)
	{
		case Alignment_Left:   return "Left";
		case Alignment_Center: return "Center";
		case Alignment_Right:  return "Right";
		default: return "Unknown";
	}
}

union TextLocation_t
{
	v2i vec;
	struct
	{
		i32 lineIndex;
		i32 charIndex;
	};
};

inline TextLocation_t NewTextLocation(i32 lineIndex, i32 charIndex)
{
	TextLocation_t result = {};
	result.lineIndex = lineIndex;
	result.charIndex = charIndex;
	return result;
}

typedef enum
{
	DisplayBtnShape_Rectangle = 0x00,
	DisplayBtnShape_Circle,
	DisplayBtnShape_Texture,
	DisplayBtnShape_AnalogStick,
} DisplayBtnShape_t;

struct DisplayBtn_t
{
	bool isGamepad;
	bool isAlt;
	Buttons_t btn;
	GamepadButtons_t gamepadBtn;
	rec drawRec;
	DisplayBtnShape_t shape;
};

struct SampleBuffer_t
{
	u32 numSamples;
	u32 numSamplesAlloc;
	i16* samples;
};

typedef enum
{
	DisplayButtonPurpose_Default = 0x00,
	DisplayButtonPurpose_Positive,
	DisplayButtonPurpose_Negative,
	DisplayButtonPurpose_Subtle,
} DisplayButtonPurpose_t;

struct DelayedMusicQueue_t
{
	bool filled;
	Sound_t* newSong;
	SoundTransition_t transitionType;
	bool repeating;
	u64 transitionTime;
	u32 startOffset;
};

#define KEY_TRACKER_LENGTH 80
struct KeyTrackerButton_t
{
	Buttons_t button;
	u8 history[KEY_TRACKER_LENGTH];
};
enum
{
	KeyTrackerFlag_Down     = 0x01,
	KeyTrackerFlag_Pressed  = 0x02,
	KeyTrackerFlag_Released = 0x04,
	KeyTrackerFlag_Handled  = 0x08,
};

#endif //  _APP_STRUCTS_H
