/*
File:   app_api_handler.cpp
Author: Taylor Robbins
Date:   11\07\2020
Description: 
	** Holds functions that handle logging in and interacting with the Twitch HTTP(S) APIs to get information about various things
*/

void DestroyApiHandler(ApiHandler_t* handler)
{
	NotNull(handler);
	if (handler->accessToken != nullptr) { ArenaPop(mainHeap, handler->accessToken); }
	if (handler->accessTokenType != nullptr) { ArenaPop(mainHeap, handler->accessTokenType); }
	ClearPointer(handler);
}

void CreateApiHandler(ApiHandler_t* handler)
{
	NotNull(handler);
	ClearPointer(handler);
	//TODO: Anything to do right at the beginning?
	handler->created = true;
}

// +--------------------------------------------------------------+
// |                          Callbacks                           |
// +--------------------------------------------------------------+
void TwitchLoginApiCallback(bool success, const HttpRequest_t* request, const char* responseData, u32 responseLength)
{
	NotNull(request);
	ApiHandler_t* handler = &app->api; //TODO: Should this be passed somehow?
	handler->loginAttemptTime = ProgramTime;
	if (success)
	{
		if (responseData != nullptr && responseLength > 0)
		{
			bool printOutJson = true;
			JsonData_t json = {};
			if (ParseJsonString(TempArena, responseData, responseLength, &json))
			{
				jsmntok_t* accessToken = JsonFindChildValueByName(&json, "access_token");
				jsmntok_t* expiresIn = JsonFindChildValueByName(&json, "expires_in");
				jsmntok_t* tokenType = JsonFindChildValueByName(&json, "token_type");
				if (accessToken == nullptr) { WriteLine_E("Login Callback: Missing response key \"access_token\""); }
				else if (expiresIn == nullptr) { WriteLine_E("Login Callback: Missing response key \"expires_in\""); }
				else if (tokenType == nullptr) { WriteLine_E("Login Callback: Missing response key \"token_type\""); }
				else
				{
					StrSplitPiece_t accessTokenStr = JsonGetTokenStr(&json, accessToken);
					StrSplitPiece_t expiresInStr = JsonGetTokenStr(&json, expiresIn);
					StrSplitPiece_t tokenTypeStr = JsonGetTokenStr(&json, tokenType);
					if (handler->accessToken != nullptr) { ArenaPop(mainHeap, handler->accessToken); }
					if (handler->accessTokenType != nullptr) { ArenaPop(mainHeap, handler->accessTokenType); }
					handler->accessToken = ArenaString(mainHeap, accessTokenStr.pntr, accessTokenStr.length);
					NotNull(handler->accessToken);
					handler->accessTokenType = ArenaString(mainHeap, tokenTypeStr.pntr, tokenTypeStr.length);
					NotNull(handler->accessTokenType);
					// WriteLine_I("Logged In Successfully!");
					PrintLine_I("Logged In Successfully: Access Token: \"%s\" Type: \"%s\"", handler->accessToken, handler->accessTokenType); //TODO: Don't print this out for security reasons
					handler->loggedIn = true;
					handler->loginSuccessTime = ProgramTime;
				}
			}
			else
			{
				PrintLine_E("Login API response couldn't be parsed as JSON (error %d):\n%.*s", json.errorCode, responseLength, responseData);
			}
			
			if (printOutJson) { PrintLine_D("Json: %.*s", responseLength, responseData); }
		}
		else { WriteLine_E("Login API request returned no response"); }
	}
	else { WriteLine_E("Login API Request Failed!"); }
}
void GetStreamsApiCallback(bool success, const HttpRequest_t* request, const char* responseData, u32 responseLength)
{
	NotNull(request);
	ApiHandler_t* handler = &app->api; //TODO: Should this be passed somehow?
	handler->streamsAttemptTime = ProgramTime;
	if (success)
	{
		bool printOutJson = false;
		JsonData_t json = {};
		if (ParseJsonString(TempArena, responseData, responseLength, &json))
		{
			jsmntok_t* dataArrayToken = JsonFindChildValueByName(&json, "data");
			if (dataArrayToken == nullptr) { WriteLine_E("View count callback: Missing response key \"data\""); printOutJson = true; }
			else
			{
				i32 dataArrayTokenIndex = JsonGetTokenIndex(&json, dataArrayToken);
				bool foundOurChannel = false;
				u32 numChannelsFound = false;
				for (u32 tIndex = 0; tIndex < json.numTokens; tIndex++)
				{
					jsmntok_t* channelObjToken = &json.tokens[tIndex];
					if (channelObjToken->parent == dataArrayTokenIndex)
					{
						jsmntok_t* userNameToken = JsonFindChildValueByName(&json, "user_name", channelObjToken);
						jsmntok_t* viewerCountToken = JsonFindChildValueByName(&json, "viewer_count", channelObjToken);
						if (userNameToken == nullptr) { PrintLine_W("View count callback: channel[%u] doesn't contain user_name token", numChannelsFound); printOutJson = true; }
						else if (viewerCountToken == nullptr) { PrintLine_W("View count callback: channel[%u] doesn't contain viewer_count token", numChannelsFound); printOutJson = true; }
						else
						{
							StrSplitPiece_t userNameStr = JsonGetTokenStr(&json, userNameToken);
							StrSplitPiece_t viewerCountStr = JsonGetTokenStr(&json, viewerCountToken);
							if (DoesSplitPieceEqualIgnoreCaseNt(&userNameStr, BOT_CHANNEL))
							{
								foundOurChannel = true;
								u32 viewerCount = 0;
								if (!TryParseU32(viewerCountStr.pntr, viewerCountStr.length, &viewerCount)) { PrintLine_E("View count callback: Couldn't parse viewer_count as u32: \"%.*s\"", viewerCountStr.length, viewerCountStr.pntr); }
								else
								{
									PrintLine_D("View Count: %u", viewerCount);
									handler->currentViewCount = viewerCount;
									handler->streamsSuccessTime = ProgramTime;
								}
								break;
							}
						}
						numChannelsFound++;
					}
				}
				if (!foundOurChannel) { PrintLine_E("Couldn't find our channel in the list of %u channels", numChannelsFound); printOutJson = true; }
			}
		}
		else { PrintLine_E("Login API response couldn't be parsed as JSON (error %d)", json.errorCode); printOutJson = true; }
		
		if (printOutJson) { PrintLine_D("Json: %.*s", responseLength, responseData); }
	}
	else { WriteLine_E("View count API request failed"); }
}

// +--------------------------------------------------------------+
// |                            Update                            |
// +--------------------------------------------------------------+
void UpdateApiHandler(ApiHandler_t* handler)
{
	NotNull(handler);
	if (handler->created)
	{
		if (!platform->HttpRequestInProgress())
		{
			// +==============================+
			// |        Attempt Login         |
			// +==============================+
			if (!handler->loggedIn && TimeSince(handler->loginAttemptTime) >= TWITCH_API_LOGIN_RETRY_WAIT_TIME)
			{
				handler->loginAttemptTime = ProgramTime;
				
				char* clientIdStr = nullptr;
				char* clientSecretStr = nullptr;
				FileInfo_t clientInfoFile = platform->ReadEntireFile(TWITCH_API_CLIENT_INFO_FILE_PATH);
				if (clientInfoFile.content != nullptr)
				{
					// i32 FindChar(const char* str, u32 strLength, char targetChar)
					const char* fileStr = (const char*)clientInfoFile.content;
					i32 colonIndex = FindChar(fileStr, clientInfoFile.size, ':');
					if (colonIndex > 0 && (u32)colonIndex+1 < clientInfoFile.size)
					{
						clientIdStr = ArenaString(TempArena, &fileStr[0], colonIndex);
						clientSecretStr = ArenaString(TempArena, &fileStr[colonIndex+1], clientInfoFile.size - (u32)(colonIndex+1));
						NotNull(clientIdStr);
						NotNull(clientSecretStr);
					}
					else { PrintLine_E("Colon in client information file %s: %.*s", (colonIndex >= 0) ? "is in invalid place" : "was not found", clientInfoFile.size, fileStr); }
				}
				else { PrintLine_E("Failed to find the client information file at \"%s\"", TWITCH_API_CLIENT_INFO_FILE_PATH); }
				
				if (clientIdStr != nullptr && clientSecretStr != nullptr)
				{
					HttpRequest_t loginRequest = {};
					loginRequest.url = TWITCH_API_LOGIN_URL;
					loginRequest.requestType = HttpRequestType_Post;
					loginRequest.numPostFields = 3;
					loginRequest.postKeys[0] = "client_id";     loginRequest.postValues[0] = clientIdStr;
					loginRequest.postKeys[1] = "client_secret"; loginRequest.postValues[1] = clientSecretStr;
					loginRequest.postKeys[2] = "grant_type";    loginRequest.postValues[2] = "client_credentials";
					loginRequest.callback = TwitchLoginApiCallback;
					if (platform->HttpRequestStart(&loginRequest))
					{
						WriteLine_I("Logging in to Twitch API...");
					}
					else { WriteLine_E("Failed to start Twitch Login API request"); }
				}
			}
			
			// +==============================+
			// | Attempt To Get Stream Count  |
			// +==============================+
			if (handler->loggedIn && (handler->streamsAttemptTime == 0 || TimeSince(handler->streamsAttemptTime) >= TWITCH_API_STREAMS_RETRY_WAIT_TIME) && (handler->streamsSuccessTime == 0 || TimeSince(handler->streamsSuccessTime) >= TWITCH_API_STREAMS_PING_TIME))
			{
				char* clientIdStr = nullptr;
				char* clientSecretStr = nullptr;
				FileInfo_t clientInfoFile = platform->ReadEntireFile(TWITCH_API_CLIENT_INFO_FILE_PATH);
				if (clientInfoFile.content != nullptr)
				{
					// i32 FindChar(const char* str, u32 strLength, char targetChar)
					const char* fileStr = (const char*)clientInfoFile.content;
					i32 colonIndex = FindChar(fileStr, clientInfoFile.size, ':');
					if (colonIndex > 0 && (u32)colonIndex+1 < clientInfoFile.size)
					{
						clientIdStr = ArenaString(TempArena, &fileStr[0], colonIndex);
						clientSecretStr = ArenaString(TempArena, &fileStr[colonIndex+1], clientInfoFile.size - (u32)(colonIndex+1));
						NotNull(clientIdStr);
						NotNull(clientSecretStr);
					}
					else { PrintLine_E("Colon in client information file %s: %.*s", (colonIndex >= 0) ? "is in invalid place" : "was not found", clientInfoFile.size, fileStr); }
				}
				else { PrintLine_E("Failed to find the client information file at \"%s\"", TWITCH_API_CLIENT_INFO_FILE_PATH); }
				
				if (clientIdStr != nullptr && clientSecretStr != nullptr)
				{
					HttpRequest_t streamsRequest = {};
					streamsRequest.url = TWITCH_API_STREAMS_URL "?channel=" BOT_CHANNEL;
					streamsRequest.requestType = HttpRequestType_Get;
					streamsRequest.numHeaders = 2;
					streamsRequest.headerKeys[0] = "Authorization"; streamsRequest.headerValues[0] = TempPrint("%c%s %s", GetUppercaseChar(handler->accessTokenType[0]), &handler->accessTokenType[1], handler->accessToken);
					streamsRequest.headerKeys[1] = "client-id";     streamsRequest.headerValues[1] = clientIdStr;
					streamsRequest.callback = GetStreamsApiCallback;
					if (platform->HttpRequestStart(&streamsRequest))
					{
						WriteLine_I("Getting stream view count...");
					}
					else { WriteLine_E("Failed to start stream view count request"); }
				}
				platform->FreeFileMemory(&clientInfoFile);
			}
		}
	}
}

