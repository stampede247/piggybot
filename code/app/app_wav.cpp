/*
File:   app_wav.cpp
Author: Taylor Robbins
Date:   11\11\2017
Description: 
	** Handles parsing .wav files and loading the sound data into memory 
*/

#if AUDIO_ENABLED

#pragma pack(push, 1)
struct WAV_Header_t
{
	u32 RIFFID;
	u32 size;
	u32 WAVEID;
};

#define RIFF_CODE(a, b, c, d) (((u32)(a) << 0) | ((u32)(b) << 8) | ((u32)(c) << 16) | ((u32)(d) << 24))

enum
{
	WAV_ChunkID_RIFF = RIFF_CODE('R', 'I', 'F', 'F'),
	WAV_ChunkID_WAVE = RIFF_CODE('W', 'A', 'V', 'E'),
	WAV_ChunkID_fmt =  RIFF_CODE('f', 'm', 't', ' '),
	WAV_ChunkID_data = RIFF_CODE('d', 'a', 't', 'a'),
};

struct WAV_Chunk_t
{
	u32 id;
	u32 size;
};

struct WAV_FormatChunk_t
{
	u16 formatTag;
	u16 numChannels;
	u32 numSamplesPerSecond;
	u32 avgBytesPerSec;
	u16 nBlockAlign;
	u16 bitsPerSample;
	u16 cbSize;
	u16 validBitsPerSample;
	u32 dwChannelMask;
	u8 subFormat[16];
};
#pragma pack(pop)

struct RiffIterator_t
{
	u8* pntr;
	u8* stop;
};

inline RiffIterator_t ParseChunkAt(void* dataPntr, void* stop)
{
	RiffIterator_t result;
	result.pntr = (u8*)dataPntr;
	result.stop = (u8*)stop; 
	
	return result;
}

inline RiffIterator_t NextChunk(RiffIterator_t iter)
{
	WAV_Chunk_t* chunk = (WAV_Chunk_t*)iter.pntr;
	
	u32 stepSize = chunk->size;
	if ((stepSize%2) == 1) stepSize++;
	
	iter.pntr += sizeof(WAV_Chunk_t) + stepSize;
	
	return iter;
}

inline bool32 IsValid(RiffIterator_t iter)
{
	bool32 result = (iter.pntr < iter.stop);
	return result;
}

inline void* GetChunkData(RiffIterator_t iter)
{
	void* result = (iter.pntr + sizeof(WAV_Chunk_t));
	
	return result;
}

inline u32 GetType(RiffIterator_t iter)
{
	WAV_Chunk_t* chunk = (WAV_Chunk_t*)iter.pntr;
	u32 result = chunk->id;
	
	return result;
}

inline u32 GetChunkDataSize(RiffIterator_t iter)
{
	WAV_Chunk_t* chunk = (WAV_Chunk_t*)iter.pntr;
	u32 result = chunk->size;
	
	return result;
}

bool ParseWavFile(FileInfo_t* file, Sound_t* soundOut, SampleBuffer_t* sampleBufferOut = nullptr)
{
	WAV_Header_t* wavHeader = (WAV_Header_t*)file->content;
	Assert(wavHeader->RIFFID == WAV_ChunkID_RIFF);
	Assert(wavHeader->WAVEID == WAV_ChunkID_WAVE);
	
	u32 sampleDataSize = 0;
	i16* sampleData = nullptr;
	bool foundFormat = false;
	
	for (RiffIterator_t iter = ParseChunkAt(wavHeader + 1, (u8*)(wavHeader+1) + wavHeader->size-4); 
		IsValid(iter);
		iter = NextChunk(iter))
	{
		switch (GetType(iter))
		{
			case WAV_ChunkID_fmt:
			{
				WAV_FormatChunk_t* format = (WAV_FormatChunk_t*)GetChunkData(iter);
				// Assert(format->nBlockAlign == sizeof(i16)*format->numChannels);
				
				if (format->formatTag != 0x01)
				{
					WriteLine_E("Not a PCM (non-compressed) WAV file!");
					return false;
				}
				
				soundOut->frequency = format->numSamplesPerSecond;
				
				if (format->bitsPerSample == 16)
				{
					if (format->numChannels == 1)
					{
						soundOut->format = AL_FORMAT_MONO16;
						soundOut->channelCount = 1;
					}
					else if (format->numChannels == 2)
					{
						soundOut->format = AL_FORMAT_STEREO16;
						soundOut->channelCount = 2;
					}
					else
					{
						PrintLine_E("We don't support %u channel wav files!", format->numChannels);
						return false;
					}
				}
				else
				{
					PrintLine_E("We don't support %ubit wav files!", format->bitsPerSample);
					return false;
				}
				
				foundFormat = true;
			} break;
			
			case WAV_ChunkID_data:
			{
				sampleData = (i16*)GetChunkData(iter);
				sampleDataSize = GetChunkDataSize(iter);
			} break;
			
			default:
			{
				// PrintLine_W("Unknown chunk ID in wav file: \"%.4s\"", (char*)iter.pntr);
			};
		}
	}
	
	if (!foundFormat)
	{
		WriteLine_E("We did not find the format chunk in the WAV file");
		return false;
	}
	if (sampleData == nullptr || sampleDataSize == 0)
	{
		WriteLine_E("We did not find the data chunk in the WAV file");
		return false;
	}
	
	soundOut->sampleCount = sampleDataSize / (soundOut->channelCount*sizeof(i16));
	
	if (sampleBufferOut != nullptr)
	{
		Assert((sampleDataSize % sizeof(i16)) == 0);
		u32 numSamples = sampleDataSize / sizeof(i16);
		ExtendSampleBuffer(sampleBufferOut, numSamples);
		Assert(sampleBufferOut->samples != nullptr);
		Assert(sampleBufferOut->numSamplesAlloc >= numSamples);
		MyMemCopy(sampleBufferOut->samples, sampleData, sampleDataSize);
		sampleBufferOut->numSamples += numSamples;
	}
	
	//TODO: Put the audio samples somewhere where we can use them in the custom audio layer
	alGenBuffers(1, &soundOut->id);
	PrintOpenAlError("alGenBuffers");
	alBufferData(soundOut->id, soundOut->format, sampleData, sampleDataSize, soundOut->frequency);
	ALCenum error = alGetError();
	if (error != AL_NO_ERROR)
	{
		PrintLine_D("OpenAL Error after alBufferData: %s", GetOpenAlErrorStr(error));
		alDeleteBuffers(1, &soundOut->id);
		PrintOpenAlError("alDeleteBuffers");
		ClearPointer(soundOut);
		return false;
	}
	
	return true;
}

#endif
