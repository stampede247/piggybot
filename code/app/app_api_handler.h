/*
File:   app_api_handler.h
Author: Taylor Robbins
Date:   11\07\2020
*/

#ifndef _APP_API_HANDLER_H
#define _APP_API_HANDLER_H

struct ApiHandler_t
{
	bool created;
	
	bool makingRequest;
	
	bool loggedIn;
	char* accessToken;
	char* accessTokenType;
	u64 loginAttemptTime;
	u64 loginSuccessTime;
	
	bool haveViewCount;
	u32 currentViewCount;
	u64 streamsAttemptTime;
	u64 streamsSuccessTime;
};

#endif //  _APP_API_HANDLER_H
