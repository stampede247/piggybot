/*
File:   app.h
Author: Taylor Robbins
Date:   11\04\2017
*/

#ifndef _APP_H
#define _APP_H

struct AppData_t
{
	MemoryArena_t mainHeap;
	MemoryArena_t staticHeap;
	MemoryArena_t tempArena;
	MemoryArena_t platHeap;
	u32 appInitTempHighWaterMark;
	RenderContext_t renderContext;
	ColorPalette_t colorPalette;
	ResourceArrays_t resources;
	FrameBuffer_t mainFrameBuffer;
	FrameBuffer_t secondaryBuffer;
	SoundResourceList_t sndResourceList;
	
	//AppState Data sections
	SettingsMenuData_t settingsMenuData;
	VisualizerData_t visualizerData;
	DevMenuData_t devMenuData;
	GifferData_t gifferData;
	
	AppSettings_t settings;
	bool settingsChanged;
	
	//AppStates/AppMenus
	bool allowAppStateChanges;
	AppState_t calledAppState;
	AppMenu_t calledAppMenu;
	bool appStateChanged;
	AppStateChange_t appStateChange;
	bool appMenuChanged;
	AppMenuChange_t appMenuChange;
	bool appStateInitialized[AppState_NumStates+1];
	u32 numActiveStates;
	AppState_t activeStates[AppState_NumStates];
	bool appMenuInitialized[AppMenu_NumMenus+1];
	u32 numActiveMenus;
	AppMenu_t activeMenus[AppMenu_NumMenus];
	
	//Input
	bool scrollWheelXHandled;
	bool scrollWheelYHandled;
	bool buttonHandled[Buttons_NumButtons];
	bool gamepadHandled[MAX_NUM_GAMEPADS][Gamepad_NumButtons];
	bool buttonPressed[Buttons_NumButtons];
	bool gamepadPressed[MAX_NUM_GAMEPADS][Gamepad_NumButtons];
	u8 buttonPressModifiers[Buttons_NumButtons];
	u8 buttonOrder[Buttons_NumButtons];
	u8 gamepadOrder[MAX_NUM_GAMEPADS][Gamepad_NumButtons];
	u8 buttonPressQueueLength;
	Buttons_t buttonPressQueue[MAX_BUTTONS_QUEUED];
	u8 gamepadPressQueueLength[MAX_NUM_GAMEPADS];
	GamepadButtons_t gamepadPressQueue[MAX_NUM_GAMEPADS][MAX_BUTTONS_QUEUED];
	bool inputMethodChanged;
	InputMethod_t inputMethod;
	u32 inputMethodIndex;
	bool mouseOverOther;
	char* mouseOverName;
	char* mouseStartedOverName[3];
	
	//Audio
	SoundInstance_t soundInstances[MAX_SOUND_INSTANCES];
	BufferedSoundInstance_t bufferedSoundInstances[MAX_SOUND_INSTANCES];
	SoundTransition_t transitionType;
	u64 musicTransStart;
	u64 musicTransTime;
	SoundInstance_t* currentMusic;
	SoundInstance_t* lastMusic;
	DelayedMusicQueue_t delayedMusicQueue;
	
	//Transition Info
	TransInfoType_t transInfoType;
	void* transInfo;
	
	//Debug Overlay
	bool developerModeEnabled;
	bool showFramerateGraph;
	bool showMemoryUsage;
	bool showDebugOverlay;
	SampleBuffer_t audioSampleBuffer;
	r32 elapsedTimesGraphValues[FRAMERATE_GRAPH_WIDTH];
	r32 updateElapsedTimesGraphValues[FRAMERATE_GRAPH_WIDTH];
	r32 waitElapsedTimesGraphValues[FRAMERATE_GRAPH_WIDTH];
	bool showMouseOverName;
	DynArray_t trackedKeys;
	u32 keyTrackerIndex;
	bool showGpuStats;
	u64 expectedTexturesSize;
	u64 expectedFramebuffersSize;
	bool showLoadedResources;
	u64 baseAvailGpuMemory;
	bool showAudioOverlay;
	bool showConsoleArenaVisual;
	
	#if RECORD_RC_DEBUG_INFO
	bool showRcDebugInfo;
	u32 rcDebugInfoSize;
	RcDebugInfo_t* rcDebugInfo;
	u32 rcDebugInfoSizeLast;
	RcDebugInfo_t* rcDebugInfoLast;
	#endif
	
	//Debug Console
	Mutex_t dbgOutputMutex;
	DbgConsole_t dbgConsole;
	u32 numCrashDumps;
	char* lastDebugCommand;
	
	//Screenshot
	bool takeScreenshot;
	bool takeGameWindowScreenshot;
	r32 screenshotCountdown;
	GifStream_t gifStream;
	bool goToGiffer;
	
	r32 newNotificationAnim;
	DynArray_t notifications;
	
	RandomSeries_t appRandom;
	
	u32 crcTable[256];
	bool crcTableComputed = false;
	
	u32 nextTextboxId;
	
	ChatHandler_t chat;
	ApiHandler_t api;
};

#endif //  _APP_H
