/*
File:   app_font_flow.h
Author: Taylor Robbins
Date:   08\13\2020
*/

#ifndef _APP_FONT_FLOW_H
#define _APP_FONT_FLOW_H

// +==============================+
// |          FfrFlag_t           |
// +==============================+ //Ffr stands for Font Flow Render
enum
{
	FfrFlag_Emboss        = 0x00000001,
	FfrFlag_Underline     = 0x00000002,
	FfrFlag_Strikethrough = 0x00000004,
	
	FfrFlag_None = 0x00000000,
};

#define FlowBeforeRenderCallback_FUNCTION(functionName) void functionName(struct FontFlowState_t* flowState, struct FontFlowRenderState_t* renderState, char c, v2 position, const Font_t* font, r32 fontScale)
typedef FlowBeforeRenderCallback_FUNCTION(FlowBeforeRenderCallback_f);

struct FontFlowRenderState_t
{
	u32 flags;
	Color_t color;
	Color_t embossColor;
	r32 embossOffset;
	v2 underlineStartPos;
	v2 strikethroughStartPos;
};

// +==============================+
// |          FfsFlag_t           |
// +==============================+ //Ffs stands for Font Flow State
enum
{
	FfsFlag_Bold              = 0x00000001,
	FfsFlag_Italic            = 0x00000002,
	FfsFlag_StyleMarkers      = 0x00000004,
	FfsFlag_DontPreserveWords = 0x00000008,
	FfsFlag_PrintDebug        = 0x00000010,
	FfsFlag_MeasureWhitespace = 0x00000020,
	
	FfsFlag_None = 0x00000000,
};

#define FlowCharCallback_FUNCTION(functionName) rec functionName(FontFlowState_t* flowState, bool measureOnly, u32 charIndex, char c, v2 position, const Font_t* font, r32 fontScale, r32* advanceXOut, bool* wasUnprintable, bool* wasWhitespace)
typedef FlowCharCallback_FUNCTION(FlowCharCallback_f);

#define FlowBeforeCharCallback_FUNCTION(functionName) void functionName(FontFlowState_t* flowState, u32 charIndex, char c, v2 position)
typedef FlowBeforeCharCallback_FUNCTION(FlowBeforeCharCallback_f);
#define FlowAfterCharCallback_FUNCTION(functionName) void functionName(FontFlowState_t* flowState, u32 charIndex, char c, rec charRec, v2 afterPos)
typedef FlowAfterCharCallback_FUNCTION(FlowAfterCharCallback_f);

#define FlowIndexPosCallback_FUNCTION(functionName) void functionName(FontFlowState_t* flowState, u32 charIndex, v2 position)
typedef FlowIndexPosCallback_FUNCTION(FlowIndexPosCallback_f);

#define FlowBeforeLineCallback_FUNCTION(functionName) void functionName(FontFlowState_t* flowState, u32 lineIndex, u32 startIndex, u32 lineLength, rec lineBounds)
typedef FlowBeforeLineCallback_FUNCTION(FlowBeforeLineCallback_f);
#define FlowAfterLineCallback_FUNCTION(functionName) void functionName(FontFlowState_t* flowState, u32 lineIndex, u32 startIndex, u32 lineLength, rec lineBounds)
typedef FlowAfterLineCallback_FUNCTION(FlowAfterLineCallback_f);

struct FontFlowState_t
{
	const char* textPntr;
	u32 textLength;
	
	u32 flags;
	const Font_t* font;
	r32 fontScale;
	v2 startPos;
	r32 maxWidth;
	
	v2 offset;
	rec bounds;
	u32 textIndex;
	
	u32 lineIndex;
	u32 lineStartCharIndex;
	v2 lineStartPos;
	rec lineBounds;
	u32 lineCharIndex;
	
	FontFlowRenderState_t* renderState;
	
	FlowCharCallback_f* charCallback;
	
	void* userPntr;
	FlowIndexPosCallback_f* indexPosCallback;
	FlowBeforeCharCallback_f* beforeCharCallback;
	FlowAfterCharCallback_f* afterCharCallback;
	FlowBeforeLineCallback_f* beforeLineCallback;
	FlowAfterLineCallback_f* afterLineCallback;
};

#endif //  _APP_FONT_FLOW_H
