/*
File:   app_loading_functions.cpp
Author: Taylor Robbins
Date:   11\04\2017
Description: 
	** Includes a collection of functions that manage loading
	** various sorts of resources 
*/

Texture_t CreateTexture(const u8* bitmapData, i32 width, i32 height, bool pixelated = false, bool repeat = true)
{
	Texture_t result = {};
	
	result.width = width;
	result.height = height;
	
	// PrintLine_Dx(DbgFlag_Inverted, "Creating %dx%d Texture (%s)", width, height, FormattedSizeStr((u32)width*(u32)height*4));
	app->expectedTexturesSize += (u64)width*(u64)height*4;
	
	glGenTextures(1, &result.id);
	glBindTexture(GL_TEXTURE_2D, result.id);
	
	glTexImage2D(
		GL_TEXTURE_2D, 		//bound texture type
		0,					//image level
		GL_RGBA,			//internal format
		width,		        //image width
		height,		        //image height
		0,					//border
		GL_RGBA,			//format
		GL_UNSIGNED_BYTE,	//type
		bitmapData  		//data
	);
	
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, pixelated ? GL_NEAREST_MIPMAP_NEAREST : GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, pixelated ? GL_NEAREST : GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE);
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glGenerateMipmap(GL_TEXTURE_2D);
	
	result.isValid = true;
	return result;
}
Texture_t LoadTexture(const char* fileName, bool pixelated = false, bool repeat = true)
{
	Texture_t result = {};
	bool isValid = true;
	
	FileInfo_t textureFile = platform->ReadEntireFile(fileName);
	
	if (textureFile.content == nullptr)
	{
		PrintLine_E("Texture is missing: \"%s\"", fileName);
		textureFile = platform->ReadEntireFile(MISSING_TEXTURE_PATH);
		Assert(textureFile.content != nullptr);
		isValid = false;
	}
	
	i32 numChannels;
	i32 width, height;
	u8* imageData = stbi_load_from_memory(
		(u8*)textureFile.content, textureFile.size,
		&width, &height, &numChannels, 4);
	
	Assert(imageData != nullptr);
	Assert(width > 0 && height > 0);
	
	result = CreateTexture(imageData, width, height, pixelated, repeat);
	
	stbi_image_free(imageData);
	platform->FreeFileMemory(&textureFile);
	
	result.isValid = isValid;
	return result;
}
Texture_t LoadTextureFromData(const void* dataPntr, u32 dataSize, bool pixelated = false, bool repeat = true)
{
	Texture_t result = {};
	
	i32 numChannels;
	i32 width, height;
	u8* imageData = stbi_load_from_memory(
		(const u8*)dataPntr, dataSize,
		&width, &height, &numChannels, 4
	);
	if (imageData == nullptr) { return result; }
	
	Assert(imageData != nullptr);
	Assert(width > 0 && height > 0);
	
	result = CreateTexture(imageData, width, height, pixelated, repeat);
	
	stbi_image_free(imageData);
	
	result.isValid = true;
	return result;
}
bool LoadTextureInfo(const char* filePath, v2i* sizeOut)
{
	Texture_t result = {};
	bool isValid = true;
	
	FileInfo_t textureFile = platform->ReadEntireFile(filePath);
	if (textureFile.content == nullptr || textureFile.size == 0) { return false; }
	
	i32 numChannels;
	i32 width, height;
	i32 loadResult = stbi_info_from_memory(
		(u8*)textureFile.content, textureFile.size,
		&width, &height, &numChannels
	);
	platform->FreeFileMemory(&textureFile);
	
	if (loadResult == 1)
	{
		if (sizeOut != nullptr) { *sizeOut = NewVec2i(width, height); }
		return true;
	}
	else { return false; }
}
void DestroyTexture(Texture_t* texturePntr)
{
	Assert(texturePntr != nullptr);
	if (texturePntr->width <= 0 || texturePntr->height <= 0) { Assert(texturePntr->id == 0); return; }
	if (app->expectedTexturesSize >= (u64)texturePntr->width*(u64)texturePntr->height*4) { app->expectedTexturesSize -= (u64)texturePntr->width*(u64)texturePntr->height*4; }
	else { app->expectedTexturesSize = 0; }
	glDeleteTextures(1, &texturePntr->id);
	// PrintLine_Dx(DbgFlag_Inverted, "Destroyed %dx%d Texture (%s)", texturePntr->width, texturePntr->height, FormattedSizeStr((u32)texturePntr->width*(u32)texturePntr->height*4));
	ClearPointer(texturePntr);
}

void DestroyVertexBuffer(VertexBuffer_t* bufferPntr)
{
	Assert(bufferPntr != nullptr);
	glDeleteBuffers(1, &bufferPntr->id);
	ClearPointer(bufferPntr);
}
VertexBuffer_t CreateVertexBuffer(const Vertex_t* vertices, u32 numVertices)
{
	VertexBuffer_t result = {};
	result.isDynamic = false;
	result.numVertices = numVertices;
	result.isSheetVertices = false;
	
	glGenBuffers(1, &result.id);
	glBindBuffer(GL_ARRAY_BUFFER, result.id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex_t) * numVertices, vertices, GL_STATIC_DRAW);
	
	return result;
}
void DynamicVertexBufferExpand(VertexBuffer_t* vertBuffer, u32 numVertices, u32 chunkSize = DYNAMIC_VERT_BUFFER_CHUNK_SIZE, bool isSheetVertices = false)
{
	if (numVertices == 0) { return; }
	
	if (vertBuffer->numVertices == 0)
	{
		vertBuffer->isDynamic = true;
		vertBuffer->isSheetVertices = isSheetVertices;
		vertBuffer->numVertices = 0;
		glGenBuffers(1, &vertBuffer->id);
	}
	
	if (vertBuffer->numVertices < numVertices)
	{
		Assert(vertBuffer->isDynamic);
		Assert(vertBuffer->isSheetVertices == isSheetVertices);
		
		u32 newSize = vertBuffer->numVertices;
		while (newSize < numVertices) { newSize += chunkSize; }
		glBindBuffer(GL_ARRAY_BUFFER, vertBuffer->id);
		if (isSheetVertices) { glBufferData(GL_ARRAY_BUFFER, sizeof(SheetFrameVertex_t) * newSize, nullptr, GL_DYNAMIC_DRAW); } //uninitialized
		else { glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex_t) * newSize, nullptr, GL_DYNAMIC_DRAW); } //uninitialized
		vertBuffer->numVertices = newSize;
	}
}
void DynamicVertexBufferFill(VertexBuffer_t* vertBuffer, const Vertex_t* vertices, u32 numVertices)
{
	Assert(vertBuffer != nullptr);
	Assert(vertices != nullptr || numVertices == 0);
	Assert(numVertices <= vertBuffer->numVertices);
	Assert(vertBuffer->isDynamic);
	Assert(!vertBuffer->isSheetVertices);
	if (numVertices == 0) { return; }
	
	glBindBuffer(GL_ARRAY_BUFFER, vertBuffer->id);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex_t) * numVertices, vertices);
}
void DynamicVertexBufferFillSheet(VertexBuffer_t* vertBuffer, const SheetFrameVertex_t* vertices, u32 numVertices)
{
	Assert(vertBuffer != nullptr);
	Assert(vertices != nullptr || numVertices == 0);
	Assert(numVertices <= vertBuffer->numVertices);
	Assert(vertBuffer->isDynamic);
	Assert(vertBuffer->isSheetVertices);
	if (numVertices == 0) { return; }
	
	glBindBuffer(GL_ARRAY_BUFFER, vertBuffer->id);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(SheetFrameVertex_t) * numVertices, vertices);
}

u32 FindFragmentShaderSplit(const char* fileContents, u32 fileLength)
{
	Assert(fileContents != nullptr);
	if (fileLength == 0) { return 0; }
	u32 result = 0;
	
	//Find the titleStr
	const char* titleStr = "FRAGMENT_SHADER";
	u32 titleStrLength = MyStrLength32(titleStr);
	bool foundTitleStr = false;
	u32 titleStrIndex = 0;
	for (u32 cIndex = 0; cIndex+titleStrLength < fileLength; cIndex++)
	{
		if (MyStrCompare(&fileContents[cIndex], titleStr, titleStrLength) == 0)
		{
			foundTitleStr = true;
			titleStrIndex = cIndex;
			break;
		}
	}
	if (!foundTitleStr) { return 0; }
	
	//Go back to previous line break
	bool foundLineBreak = false;
	for (u32 cIndex = titleStrIndex; cIndex > 0; cIndex--)
	{
		if (fileContents[cIndex] == '\n' || fileContents[cIndex] == '\r')
		{
			titleStrIndex = cIndex+1;
			foundLineBreak = true;
			break;
		}
	}
	if (!foundLineBreak) { return 0; }
	
	return titleStrIndex;
}
Shader_t LoadShader(MemoryArena_t* memArena, const char* filePath)
{
	Assert(memArena != nullptr);
	Assert(filePath != nullptr);
	
	Shader_t result = {};
	result.isValid = true;
	result.usesSheetFrameVerts = false;
	result.allocArena = memArena;
	GLint compiled;
	int logLength;
	
	FileInfo_t shaderFile = platform->ReadEntireFile(filePath);
	if (shaderFile.content == nullptr)
	{
		PrintLine_W("Couldn't open shader file \"%s\"", filePath);
		return result;
	}
	
	const char* sheetFrameVertsIdentifier = "[SHEET_VERTS]";
	if (StrFindSubstringIgnoreCase((const char*)shaderFile.content, shaderFile.size, sheetFrameVertsIdentifier, MyStrLength32(sheetFrameVertsIdentifier))) { result.usesSheetFrameVerts = true; }
	
	u32 splitIndex = FindFragmentShaderSplit((const char*)shaderFile.content, shaderFile.size);
	if (splitIndex == 0 || splitIndex >= shaderFile.size)
	{
		PrintLine_W("Couldn't find the split between vertex and fragment shader in \"%s\"", filePath);
		return result;
	}
	
	TempPushMark();
	u32 vertShaderLength = splitIndex;
	char* vertShader = PushArray(TempArena, char, vertShaderLength+1);
	MyMemCopy(vertShader, (const char*)shaderFile.content, vertShaderLength);
	vertShader[vertShaderLength] = '\0';
	
	u32 fragShaderLength = shaderFile.size - splitIndex;
	char* fragShader = PushArray(TempArena, char, fragShaderLength+1);
	MyMemCopy(fragShader, (const char*)shaderFile.content + splitIndex, fragShaderLength);
	fragShader[fragShaderLength] = '\0';
	
	result.vertId = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(result.vertId, 1, (const GLchar* const*)&vertShader, NULL);
	glCompileShader(result.vertId);
	
	glGetShaderiv(result.vertId, GL_COMPILE_STATUS, &compiled);
	glGetShaderiv(result.vertId, GL_INFO_LOG_LENGTH, &logLength);
	if (!compiled) { PrintLine_E("%s: Vertex Shader Compilation Failed: %d byte log", GetFileNamePart(filePath), logLength); result.isValid = false; }
	if (logLength > 0)
	{
		result.vertLog = PushArray(memArena, char, logLength+1);
		Assert(result.vertLog != nullptr);
		glGetShaderInfoLog(result.vertId, logLength, NULL, result.vertLog);
		result.vertLog[logLength] = '\0';
		PrintLine_W("Vertex Error Log:\n%s\n", result.vertLog);
	}
	
	result.fragId = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(result.fragId, 1, (const GLchar* const*)&fragShader, NULL);
	glCompileShader(result.fragId);
	
	glGetShaderiv(result.fragId, GL_COMPILE_STATUS, &compiled);
	glGetShaderiv(result.fragId, GL_INFO_LOG_LENGTH, &logLength);
	if (!compiled) { PrintLine_E("%s: Fragment Shader Compilation Failed: %d byte log", GetFileNamePart(filePath), logLength); result.isValid = false; }
	if (logLength > 0)
	{
		result.fragLog = PushArray(memArena, char, logLength+1);
		Assert(result.fragLog != nullptr);
		glGetShaderInfoLog(result.fragId, logLength, NULL, result.fragLog);
		result.fragLog[logLength] = '\0';
		PrintLine_W("Fragment Error Log:\n%s\n", result.fragLog);
	}
	
	platform->FreeFileMemory(&shaderFile);
	TempPopMark();
	
	result.programId = glCreateProgram();
	glAttachShader(result.programId, result.fragId);
	glAttachShader(result.programId, result.vertId);
	glLinkProgram(result.programId);
	
	glGetProgramiv(result.programId, GL_LINK_STATUS, &compiled);
	glGetProgramiv(result.programId, GL_INFO_LOG_LENGTH, &logLength);
	if (!compiled) { PrintLine_E("%s: Shader Linking Failed: %d byte log", GetFileNamePart(filePath), logLength); result.isValid = false; }
	if (logLength > 0)
	{
		result.progLog = PushArray(memArena, char, logLength+1);
		Assert(result.progLog != nullptr);
		glGetShaderInfoLog(result.programId, logLength, NULL, result.progLog);
		result.progLog[logLength] = '\0';
		PrintLine_W("Linking Error Log:\n%s\n", result.progLog);
	}
	
	if (result.usesSheetFrameVerts)
	{
		result.locations.positionAttrib      = glGetAttribLocation(result.programId, "inPosition");
		result.locations.colorAttrib         = glGetAttribLocation(result.programId, "inColor");
		result.locations.texCoordAttrib      = glGetAttribLocation(result.programId, "inTexCoord");
		for (u32 rIndex = 0; rIndex < NUM_REPLACE_COLORS-4; rIndex++)
		{
			result.locations.replaceColorAttribs[rIndex] = glGetAttribLocation(result.programId, TempPrint("inReplaceColor%u", rIndex+1));
		}
	}
	else
	{
		result.locations.positionAttrib      = glGetAttribLocation(result.programId, "inPosition");
		result.locations.colorAttrib         = glGetAttribLocation(result.programId, "inColor");
		result.locations.texCoordAttrib      = glGetAttribLocation(result.programId, "inTexCoord");
	}
	
	result.locations.worldMatrix         = glGetUniformLocation(result.programId, "WorldMatrix");
	result.locations.viewMatrix          = glGetUniformLocation(result.programId, "ViewMatrix");
	result.locations.projectionMatrix    = glGetUniformLocation(result.programId, "ProjectionMatrix");
	
	result.locations.texture             = glGetUniformLocation(result.programId, "Texture");
	result.locations.textureSize         = glGetUniformLocation(result.programId, "TextureSize");
	result.locations.altTexture          = glGetUniformLocation(result.programId, "AltTexture");
	result.locations.altTextureSize      = glGetUniformLocation(result.programId, "AltTextureSize");
	result.locations.sourceRectangle     = glGetUniformLocation(result.programId, "SourceRectangle");
	result.locations.altSourceRectangle  = glGetUniformLocation(result.programId, "AltSourceRectangle");
	result.locations.maskRectangle       = glGetUniformLocation(result.programId, "MaskRectangle");
	result.locations.shiftVec            = glGetUniformLocation(result.programId, "ShiftVec");
	
	result.locations.primaryColor        = glGetUniformLocation(result.programId, "PrimaryColor");
	result.locations.secondaryColor      = glGetUniformLocation(result.programId, "SecondaryColor");
	result.locations.replaceColors       = glGetUniformLocation(result.programId, "ReplaceColors");
	
	result.locations.gradientEnabled     = glGetUniformLocation(result.programId, "GradientEnabled");
	result.locations.circleRadius        = glGetUniformLocation(result.programId, "CircleRadius");
	result.locations.circleInnerRadius   = glGetUniformLocation(result.programId, "CircleInnerRadius");
	result.locations.saturation          = glGetUniformLocation(result.programId, "Saturation");
	result.locations.brightness          = glGetUniformLocation(result.programId, "Brightness");
	result.locations.time                = glGetUniformLocation(result.programId, "Time");
	result.locations.value0              = glGetUniformLocation(result.programId, "Value0");
	result.locations.value1              = glGetUniformLocation(result.programId, "Value1");
	result.locations.value2              = glGetUniformLocation(result.programId, "Value2");
	result.locations.value3              = glGetUniformLocation(result.programId, "Value3");
	result.locations.value4              = glGetUniformLocation(result.programId, "Value4");
	result.locations.mousePos            = glGetUniformLocation(result.programId, "MousePos");
	result.locations.screenSize          = glGetUniformLocation(result.programId, "ScreenSize");
	
	glGenVertexArrays(1, &result.vertexArray);
	glBindVertexArray(result.vertexArray);
	if (result.usesSheetFrameVerts)
	{
		glEnableVertexAttribArray(result.locations.positionAttrib);
		glEnableVertexAttribArray(result.locations.colorAttrib);
		glEnableVertexAttribArray(result.locations.texCoordAttrib);
		for (u32 rIndex = 0; rIndex < NUM_REPLACE_COLORS-4; rIndex++)
		{
			glEnableVertexAttribArray(result.locations.replaceColorAttribs[rIndex]);
		}
	}
	else
	{
		glEnableVertexAttribArray(result.locations.positionAttrib);
		glEnableVertexAttribArray(result.locations.colorAttrib);
		glEnableVertexAttribArray(result.locations.texCoordAttrib);
	}
	
	return result;
}

void DestroyShader(Shader_t* shaderPntr)
{
	Assert(shaderPntr != nullptr);
	glDeleteVertexArrays(1, &shaderPntr->vertexArray);
	glDeleteProgram(shaderPntr->programId);
	glDeleteShader(shaderPntr->vertId);
	glDeleteShader(shaderPntr->fragId);
	if (shaderPntr->vertLog != nullptr)
	{
		Assert(shaderPntr->allocArena != nullptr);
		ArenaPop(shaderPntr->allocArena, shaderPntr->vertLog);
	}
	if (shaderPntr->fragLog != nullptr)
	{
		Assert(shaderPntr->allocArena != nullptr);
		ArenaPop(shaderPntr->allocArena, shaderPntr->fragLog);
	}
	if (shaderPntr->progLog != nullptr)
	{
		Assert(shaderPntr->allocArena != nullptr);
		ArenaPop(shaderPntr->allocArena, shaderPntr->progLog);
	}
	ClearPointer(shaderPntr);
}

void DestroySpriteSheet(SpriteSheet_t* sheetPntr)
{
	Assert(sheetPntr != nullptr);
	if (sheetPntr->frameFilledFlags != nullptr)
	{
		Assert(sheetPntr->allocArena != nullptr);
		ArenaPop(sheetPntr->allocArena, sheetPntr->frameFilledFlags);
	}
	DestroyTexture(&sheetPntr->texture);
	ClearPointer(sheetPntr);
}
void CreateSpriteSheet(MemoryArena_t* memArena, SpriteSheet_t* sheetOut, const void* textureData, v2i textureSize, u32 padding, v2i numFrames, bool pixelated)
{
	Assert(memArena != nullptr);
	Assert(sheetOut != nullptr);
	Assert(textureData != nullptr);
	Assert(textureSize.width > 0 && textureSize.height > 0);
	Assert(numFrames.x > 0 && numFrames.y > 0);
	
	v2i paddingVec = NewVec2i(padding);
	v2i frameSize = NewVec2i(textureSize.width / numFrames.x, textureSize.height / numFrames.y);
	v2i newFrameSize = frameSize + paddingVec*2;
	v2i newSize = Vec2iMultiply(newFrameSize, numFrames);
	
	sheetOut->allocArena = memArena;
	sheetOut->frameFilledFlags = PushArray(memArena, bool, (u32)(numFrames.x * numFrames.y));
	
	TempPushMark();
	u32* newData = PushArray(TempArena, u32, newSize.width*newSize.height);
	MyMemSet(newData, 0x00, sizeof(u32)*newSize.width*newSize.height);
	for (i32 frameY = 0; frameY < numFrames.y; frameY++)
	{
		for (i32 frameX = 0; frameX < numFrames.x; frameX++)
		{
			v2i frame = NewVec2i(frameX, frameY);
			v2i framePos = Vec2iMultiply(frameSize, frame);
			v2i newFramePos = Vec2iMultiply(newFrameSize, frame) + paddingVec;
			
			bool isFilled = false;
			for (i32 yPos = 0; yPos < frameSize.y; yPos++)
			{
				for (i32 xPos = 0; xPos < frameSize.x; xPos++)
				{
					v2i sourcePos = framePos + NewVec2i(xPos, yPos);
					const u32* source = &((const u32*)textureData)[(sourcePos.y*textureSize.width) + sourcePos.x];
					
					v2i newPos = newFramePos + NewVec2i(xPos, yPos);
					if (!isFilled && ((*source) & 0xFF000000) != 0)
					{
						isFilled = true;
						// PrintLine_D("pixel (%d, %d) 0x%08X fills frame (%d, %d)", sourcePos.x, sourcePos.y, *source, frameX, frameY);
					}
					newData[(newPos.y * newSize.width) + newPos.x] = *source;
					
					if (xPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(-1, 0);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (yPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(0, -1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == frameSize.width-1)
					{
						v2i adjPos = newPos + NewVec2i(1, 0);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (yPos == frameSize.height-1)
					{
						v2i adjPos = newPos + NewVec2i(0, 1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					
					if (xPos == 0 && yPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(-1, -1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == frameSize.width-1 && yPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(1, -1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == 0 && yPos == frameSize.height-1)
					{
						v2i adjPos = newPos + NewVec2i(-1, 1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == frameSize.width-1 && yPos == frameSize.height-1)
					{
						v2i adjPos = newPos + NewVec2i(1, 1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
				}
			}
			
			sheetOut->frameFilledFlags[frameY * numFrames.x + frameX] = isFilled;
		}
	}
	
	sheetOut->texture = CreateTexture((u8*)newData, newSize.width, newSize.height, pixelated, false);
	sheetOut->padding = paddingVec;
	sheetOut->frameSize = newFrameSize;
	sheetOut->innerFrameSize = newFrameSize - paddingVec*2;
	sheetOut->numFrames = numFrames;
	
	TempPopMark();
}
SpriteSheet_t LoadSpriteSheet(MemoryArena_t* memArena, const char* filePath, u32 padding, v2i numFrames, bool pixelated = true)
{
	Assert(filePath != nullptr);
	Assert(memArena != nullptr);
	Assert((numFrames.x >= 0 && numFrames.y >= 0) || numFrames == Vec2i_Zero);
	SpriteSheet_t result = {};
	
	FileInfo_t textureFile = platform->ReadEntireFile(filePath);
	if (textureFile.content == nullptr)
	{
		PrintLine_E("Failed to load spritesheet \"%s\"!", filePath);
		//TODO: Can we handle this better somehow? Maybe let the calling party know?
		return result;
	}
	
	i32 numChannels;
	v2i bitmapSize;
	u32* bitmapData = (u32*)stbi_load_from_memory(
		(u8*)textureFile.content, textureFile.size,
		&bitmapSize.width, &bitmapSize.height, &numChannels, 4);
	
	if (bitmapData == nullptr)
	{
		PrintLine_E("Failed to deserialize spritesheet \"%s\"", filePath);
		return result;
	}
	
	if (numFrames == Vec2i_Zero)
	{
		numFrames = NewVec2i(FloorR32i((r32)bitmapSize.width / (r32)bitmapSize.height), 1);
		if (numFrames.x == 0)
		{
			PrintLine_E("The sprite sheet image is taller than it is wide. This can't be an implicitely sized sprite sheet: \"%s\"", filePath);
			stbi_image_free((u8*)bitmapData);
			platform->FreeFileMemory(&textureFile);
			return result;
		}
	}
	
	CreateSpriteSheet(memArena, &result, bitmapData, bitmapSize, padding, numFrames, pixelated);
	
	stbi_image_free((u8*)bitmapData);
	platform->FreeFileMemory(&textureFile);
	
	return result;
}

bool BakeTtfFont(Font_t* font, void* fileContents, u32 fileSize, r32 fontSize, i32 bitmapWidth, i32 bitmapHeight, u8 firstCharacter, u8 numCharacters)
{
	Assert(font != nullptr);
	Assert(fileContents != nullptr);
	Assert(fileSize > 0);
	Assert(fontSize > 0);
	Assert(bitmapWidth > 0);
	Assert(bitmapHeight > 0);
	Assert(numCharacters > 0);
	Assert((u32)firstCharacter + (u32)numCharacters <= 255);
	Assert(font->charInfos != nullptr);
	
	TempPushMark();
	
	u8* grayscaleData = TempArray(u8, bitmapWidth * bitmapHeight);
	stbtt_bakedchar* charInfos = TempArray(stbtt_bakedchar, numCharacters);
	
	int bakeResult = stbtt_BakeFontBitmap((u8*)fileContents,
		0, fontSize,
		grayscaleData,
		bitmapWidth, bitmapHeight, 
		firstCharacter, numCharacters,
		charInfos
	);
	if (bakeResult <= 0)
	{
		PrintLine_E("Fit %d/%u characters in bake for font at %f size in %dx%d texture", -bakeResult, numCharacters, fontSize, bitmapWidth, bitmapHeight);
		TempPopMark();
		return false;
	}
	
	font->numChars = numCharacters;
	font->firstChar = firstCharacter;
	font->unknownCharIndex = 0x7F - font->firstChar;
	font->fontSize = fontSize;
	for (u8 cIndex = 0; cIndex < numCharacters; cIndex++)
	{
		font->charInfos[cIndex].position = NewVec2i(charInfos[cIndex].x0, charInfos[cIndex].y0);
		font->charInfos[cIndex].size = NewVec2i(charInfos[cIndex].x1 - charInfos[cIndex].x0, charInfos[cIndex].y1 - charInfos[cIndex].y0);
		font->charInfos[cIndex].offset = NewVec2(charInfos[cIndex].xoff, charInfos[cIndex].yoff);
		font->charInfos[cIndex].advanceX = charInfos[cIndex].xadvance;
	}
	
	u8* bitmapData = TempArray(u8, 4 * bitmapWidth * bitmapHeight);
	for (i32 y = 0; y < bitmapHeight; y++)
	{
		for (i32 x = 0; x < bitmapWidth; x++)
		{
			u8 grayscaleValue = grayscaleData[y*bitmapWidth + x];
			bitmapData[(y*bitmapWidth+x)*4 + 0] = 255; //R
			bitmapData[(y*bitmapWidth+x)*4 + 1] = 255; //G
			bitmapData[(y*bitmapWidth+x)*4 + 2] = 255; //B
			bitmapData[(y*bitmapWidth+x)*4 + 3] = grayscaleValue; 
		}
	}
	
	font->bitmap = CreateTexture(bitmapData, bitmapWidth, bitmapHeight, true, false);
	TempPopMark();
	
	if (!font->bitmap.isValid)
	{
		PrintLine_E("Failed to create %dx%d texture from baked font data", bitmapWidth, bitmapHeight);
		DestroyTexture(&font->bitmap);
		return false;
	}
	
	//Create information about character sizes
	{
		v2 maxSize = Vec2_Zero;
		v2 extendVertical = Vec2_Zero;
		for (u32 cIndex = 0; cIndex < font->numChars; cIndex++)
		{
			FontCharInfo_t* charInfo = &font->charInfos[cIndex];
			if (charInfo->height > maxSize.y) { maxSize.y = (r32)charInfo->height; }
			if (-charInfo->offset.y > extendVertical.x) { extendVertical.x = -charInfo->offset.y; }
			if (charInfo->offset.y + charInfo->height > extendVertical.y) { extendVertical.y = charInfo->offset.y + charInfo->height; }
			if (charInfo->advanceX > maxSize.x) { maxSize.x = charInfo->advanceX; }
		}
		font->maxCharWidth = maxSize.x;
		font->maxCharHeight = maxSize.y;
		font->maxExtendUp = extendVertical.x;
		font->maxExtendDown = extendVertical.y;
		font->lineHeight = font->maxExtendDown + font->maxExtendUp;
	}
	
	return true;
}
Font_t LoadFont(MemoryArena_t* memArena, const char* filePath, r32 fontSize, i32 bitmapWidth, i32 bitmapHeight, u8 firstCharacter, u8 numCharacters)
{
	Assert(memArena != nullptr);
	Font_t result = {};
	result.isValid = false;
	result.isBitmapFont = false;
	result.allocArena = memArena;
	
	FileInfo_t fontFile = platform->ReadEntireFile(filePath);
	if (fontFile.content == nullptr || fontFile.size == 0)
	{
		PrintLine_E("Failed to open font file at \"%s\"", filePath);
		return result;
	}
	PrintLine_D("Font file is %s", FormattedSizeStr(fontFile.size));
	
	result.charInfos = PushArray(memArena, FontCharInfo_t, numCharacters);
	Assert(result.charInfos != nullptr);
	MyMemSet(result.charInfos, 0x00, sizeof(FontCharInfo_t)*numCharacters);
	
	if (!BakeTtfFont(&result, fontFile.content, fontFile.size, fontSize, bitmapWidth, bitmapHeight, firstCharacter, numCharacters))
	{
		PrintLine_E("Failed to create bake for font at \"%s\" at size %f", filePath, fontSize);
		platform->FreeFileMemory(&fontFile);
		ArenaPop(memArena, result.charInfos);
		return result;
	}
	
	platform->FreeFileMemory(&fontFile);
	
	result.isValid = true;
	return result;
}
Font_t LoadPlatformFont(MemoryArena_t* memArena, const char* fontName, r32 fontSize, i32 bitmapWidth, i32 bitmapHeight, u8 firstCharacter, u8 numCharacters, bool bold, bool italic)
{
	Assert(memArena != nullptr);
	Font_t result = {};
	result.isValid = false;
	result.isBitmapFont = false;
	result.allocArena = memArena;
	
	PlatformFont_t fontFile = {};
	if (!platform->OpenPlatformFont(fontName, (i32)fontSize, bold, italic, &fontFile))
	{
		PrintLine_E("Failed to open platform font \"%s\" at size %f", fontName, fontSize);
		return result;
	}
	// PrintLine_D("Font file is %s", FormattedSizeStr(fontFile.dataSize));
	
	result.charInfos = PushArray(memArena, FontCharInfo_t, numCharacters);
	Assert(result.charInfos != nullptr);
	MyMemSet(result.charInfos, 0x00, sizeof(FontCharInfo_t)*numCharacters);
	
	if (!BakeTtfFont(&result, fontFile.dataPntr, fontFile.dataSize, fontSize, bitmapWidth, bitmapHeight, firstCharacter, numCharacters))
	{
		PrintLine_E("Failed to create bake for platform font \"%s\" at size %f", fontName, fontSize);
		platform->ClosePlatformFont(&fontFile);
		ArenaPop(memArena, result.charInfos);
		return result;
	}
	
	platform->ClosePlatformFont(&fontFile);
	
	result.isValid = true;
	return result;
}
void DestroyFont(Font_t* fontPntr)
{
	Assert(fontPntr != nullptr);
	DestroyTexture(&fontPntr->bitmap);
	if (fontPntr->charInfos != nullptr)
	{
		Assert(fontPntr->allocArena != nullptr);
		ArenaPop(fontPntr->allocArena, fontPntr->charInfos);
	}
	ClearPointer(fontPntr);
}

//TODO: Make this work with the custom audio stuff
#if AUDIO_ENABLED
void CreateSound(Sound_t* soundOut, const void* sampleData, u32 frequency, u32 sampleCount, u32 channelCount)
{
	Assert(soundOut != nullptr);
	Assert(sampleData != nullptr);
	Assert(frequency > 0);
	Assert(sampleCount > 0);
	Assert(channelCount == 1 || channelCount == 2);
	
	soundOut->frequency = (ALsizei)frequency;
	soundOut->format = (channelCount == 2) ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16;
	soundOut->channelCount = channelCount;
	soundOut->sampleCount = sampleCount;
	
	alGenBuffers(1, &soundOut->id);
	PrintOpenAlError("alGenBuffers");
	alBufferData(soundOut->id, soundOut->format, sampleData, sampleCount * channelCount * sizeof(i16), soundOut->frequency);
	ALCenum error = alGetError();
	if (error != AL_NO_ERROR)
	{
		PrintLine_D("OpenAL Error after alBufferData: %s", GetOpenAlErrorStr(error));
		soundOut->isValid = false;
		return;
	}
	
	soundOut->isValid = true;
}
Sound_t LoadSound(const char* fileName, SampleBuffer_t* sampleBufferOut = nullptr)
{
	Sound_t result = {};
	result.isValid = false;
	
	FileInfo_t sndFile = platform->ReadEntireFile(fileName);
	if (sndFile.content == nullptr)
	{
		// PrintLine_E("Sound file missing: \"%s\"", fileName);
		return result;
	}
	
	//TODO: Should we check the file extension rather than the contents?
	WAV_Header_t* wavHeader = (WAV_Header_t*)sndFile.content;
	if (wavHeader->RIFFID == WAV_ChunkID_RIFF &&
		wavHeader->WAVEID == WAV_ChunkID_WAVE)
	{
		bool parseSuccess = ParseWavFile(&sndFile, &result, sampleBufferOut);
		if (parseSuccess)
		{
			result.isValid = true;
		}
		else
		{
			PrintLine_E("Failed to parse WAV file \"%s\"", fileName);
		}
	}
	else //TODO: Should we check to see if it's a ogg file somehow before trying to parse it as ogg?
	{
		bool parseSuccess = ParseOggFile(&sndFile, &result, sampleBufferOut);
		if (parseSuccess)
		{
			result.isValid = true;
		}
		else
		{
			PrintLine_E("Failed to parse OGG file \"%s\"", fileName);
		}
	}
	
	platform->FreeFileMemory(&sndFile);
	return result;
}
bool LoadSoundInfo(const char* filePath, Sound_t* infoOut)
{
	Assert(filePath != nullptr);
	Assert(infoOut != nullptr);
	ClearPointer(infoOut);
	
	FileInfo_t sndFile = platform->ReadEntireFile(filePath);
	if (sndFile.content == nullptr || sndFile.size == 0) { return false; }
	
	bool result = false;
	WAV_Header_t* wavHeader = (WAV_Header_t*)sndFile.content;
	if (wavHeader->RIFFID == WAV_ChunkID_RIFF && wavHeader->WAVEID == WAV_ChunkID_WAVE) //TODO: Should we check the file extension rather than the contents?
	{
		//TODO: Implement this for wav files
		result = false;
	}
	else //TODO: Should we check to see if it's a ogg file somehow before trying to parse it as ogg?
	{
		bool parseSuccess = ParseOggFile(&sndFile, infoOut, nullptr, false, true);
		if (parseSuccess)
		{
			result = true;
		}
	}
	
	platform->FreeFileMemory(&sndFile);
	return result;
}
void DestroySound(Sound_t* soundPntr)
{
	Assert(soundPntr != nullptr);
	alDeleteBuffers(1, &soundPntr->id);
	PrintOpenAlError("alDeleteBuffers");
	ClearPointer(soundPntr);
}
#else
Sound_t LoadSound(const char* fileName, SampleBuffer_t* sampleBufferOut = nullptr)
{
	Sound_t result = {};
	return result;
}
void DestroySound(Sound_t* soundPntr)
{
	Assert(soundPntr != nullptr);
}
#endif

FrameBuffer_t CreateFrameBuffer(v2i size, bool includeTransparency = true, bool includeStencil = false)
{
	Assert(size.x > 0 && size.y > 0);
	FrameBuffer_t result = {};
	result.hasTransparency = includeTransparency;
	result.hasStencil = includeStencil;
	
	// PrintLine_Dx(DbgFlag_Inverted, "Created %dx%d Frame Buffer (%s)", size.width, size.height, FormattedSizeStr((u32)size.width*(u32)size.height*4));
	app->expectedFramebuffersSize += (u64)size.width*(u64)size.height*4;
	
	glGenFramebuffers(1, &result.id);
	glBindFramebuffer(GL_FRAMEBUFFER, result.id);
	
	result.texture.isValid = true;
	result.texture.size = size;
	glGenTextures(1, &result.texture.id);
	glBindTexture(GL_TEXTURE_2D, result.texture.id);
	if (includeTransparency)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	}
	else
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, size.x, size.y, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glGenRenderbuffers(1, &result.depthBuffId);
	glBindRenderbuffer(GL_RENDERBUFFER, result.depthBuffId);
	if (includeStencil)
	{
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, size.width, size.height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, result.depthBuffId);
	}
	else
	{
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, size.width, size.height);
	}
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, result.depthBuffId);
	
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, result.texture.id, 0);
	GLenum drawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, drawBuffers);
	
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		Print_E("Failed to create framebuffer. Status = ");
		switch (status)
		{
			case GL_FRAMEBUFFER_UNDEFINED:                     WriteLine_E("GL_FRAMEBUFFER_UNDEFINED"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:         WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:        WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:        WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER"); break;
			case GL_FRAMEBUFFER_UNSUPPORTED:                   WriteLine_E("GL_FRAMEBUFFER_UNSUPPORTED"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:        WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:      WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS"); break;
			default: PrintLine_E("%d", status);
		}
		Assert(false);
	}
	
	return result;
}
void DestroyFrameBuffer(FrameBuffer_t* frameBufferPntr)
{
	Assert(frameBufferPntr != nullptr);
	if (frameBufferPntr->texture.width <= 0 || frameBufferPntr->texture.height <= 0) { Assert(frameBufferPntr->texture.id == 0); }
	// PrintLine_Dx(DbgFlag_Inverted, "Destroyed %dx%d Frame Buffer (%s)", frameBufferPntr->texture.width, frameBufferPntr->texture.height, FormattedSizeStr((u32)frameBufferPntr->texture.width*(u32)frameBufferPntr->texture.height*4));
	if (app->expectedFramebuffersSize >= (u64)frameBufferPntr->texture.width*(u64)frameBufferPntr->texture.height*4) { app->expectedFramebuffersSize -= (u64)frameBufferPntr->texture.width*(u64)frameBufferPntr->texture.height*4; }
	else { app->expectedFramebuffersSize = 0; }
	glDeleteTextures(1, &frameBufferPntr->texture.id);
	glDeleteFramebuffers(1, &frameBufferPntr->id);
	glDeleteRenderbuffers(1, &frameBufferPntr->depthBuffId);
	ClearPointer(frameBufferPntr);
}

bool CreateDirectories(const char* directoryPath)
{
	TempPushMark();
	
	u32 strLength = MyStrLength32(directoryPath);
	char* tempString = PushArray(TempArena, char, strLength+1);
	MyMemCopy(tempString, directoryPath, strLength+1);
	
	for (u32 cIndex = 0; tempString[cIndex] != '\0'; cIndex++)
	{
		if (cIndex > 0 && (tempString[cIndex] == '\\' || tempString[cIndex] == '/'))
		{
			char oldChar = tempString[cIndex];
			tempString[cIndex] = '\0';
			if (!platform->DoesFolderExist(tempString))
			{
				if (platform->CreateFolder(tempString))
				{
					PrintLine_D("Creating directory \"%s\"", tempString);
				}
				else
				{
					NotifyPrint_E("Failed to create directory \"%s\"", tempString);
					TempPopMark();
					return false;
				}
			}
			else
			{
				// PrintLine_D("\"%s\" already exists", tempString);
			}
			tempString[cIndex] = oldChar;
		}
	}
	
	TempPopMark();
	return true;
}
