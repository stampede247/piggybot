/*
File:   app_new_textbox.h
Author: Taylor Robbins
Date:   08\12\2020
*/

#ifndef _APP_NEW_TEXTBOX_H
#define _APP_NEW_TEXTBOX_H

typedef enum
{
	TextboxType_Default = 0x00,
	TextboxType_Integer,
	TextboxType_UnsignedInteger,
	TextboxType_FloatingPoint,
	TextboxType_UnsignedFloatingPoint,
	TextboxType_Hex,
} TextboxType_t;

struct TextboxLine_t
{
	u32 startIndex;
	u32 length;
	v2 size;
	v2 offset;
	r32 advanceX;
};

struct NewTextbox_t
{
	MemoryArena_t* allocArena;
	
	u32 fontIndex;
	r32 fontScale;
	bool multiline;
	bool growingTextAllocation;
	v2 textPadding;
	u32 mouseHoverId;
	r32 scrollLag;
	
	TextboxType_t type;
	bool minValueEnabled;
	bool maxValueEnabled;
	union
	{
		struct { i32 minValueI32, maxValueI32; };
		struct { u32 minValueU32, maxValueU32; };
		struct { r32 minValueR32, maxValueR32; };
	};
	bool deselectOnEnter;
	bool validateTextEveryStroke;
	bool autoCorrectTextFormat;
	bool textIsInvalid;
	
	Color_t backColorInactive;
	Color_t textColorInactive;
	Color_t borderColorInactive;
	Color_t selectionColorInactive;
	
	Color_t backColorActive;
	Color_t textColorActive;
	Color_t borderColorActive;
	Color_t selectionColorActive;
	
	Color_t backColorInvalid;
	Color_t textColorInvalid;
	Color_t borderColorInvalid;
	Color_t selectionColorInvalid;
	
	u32 textLength;
	u32 allocLength;
	u32 allocChunkSize;
	char* text;
	
	u32 cursorStart;
	u32 cursorEnd;
	bool isSelected;
	bool clickIsDeselecting;
	v2 cursorStartPos;
	v2 cursorEndPos;
	bool clickIsSelectingText;
	bool justDoubleClicked;
	u64 lastSelectClickTime;
	
	i32 mouseHoverIndex;
	v2 mouseHoverCursorPos;
	
	bool scrollToCursor;
	bool selectionChanged;
	bool remeasureText;
	v2 textSize;
	v2 maxScroll;
	
	v2 scroll;
	v2 scrollGoto;
	
	rec drawRec;
	r32 maxWidth;
	v2 textPos;
};

#endif //  _APP_NEW_TEXTBOX_H
