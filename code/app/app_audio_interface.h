/*
File:   app_audio_interface.h
Author: Taylor Robbins
Date:   03\30\2020
*/

#ifndef _APP_AUDIO_INTERFACE_H
#define _APP_AUDIO_INTERFACE_H

typedef enum
{
	AlExtension_Alaw = 0,
	AlExtension_Bformat,
	AlExtension_Double,
	AlExtension_ExponentDistance,
	AlExtension_Float32,
	AlExtension_Ima4,
	AlExtension_LinearDistance,
	AlExtension_Mcformats,
	AlExtension_Mulaw,
	AlExtension_MulawBformat,
	AlExtension_MulawMcformats,
	AlExtension_Offset,
	AlExtension_SourceDistanceModel,
	AlExtension_Quadriphonic,
	AlExtension_BlockAlignment,
	AlExtension_BufferSamples,
	AlExtension_BufferSubData,
	AlExtension_DeferredUpdates,
	AlExtension_DirectChannels,
	AlExtension_LoopPoints,
	AlExtension_Msadpcm,
	AlExtension_SourceLatency,
	AlExtension_SourceLength,
	AlExtension_NumExtensions,
} AlExtension_t;

typedef enum
{
	AlcExtension_EnumerateAllExt = 0,
	AlcExtension_EnumerationExt,
	AlcExtension_Capture,
	AlcExtension_Dedicated,
	AlcExtension_Disconnect,
	AlcExtension_Efx,
	AlcExtension_ThreadLocalContext,
	AlcExtension_Hrtf,
	AlcExtension_Loopback,
	AlcExtension_PauseDevice,
	AlcExtension_DeviceClock,
	AlcExtension_NumExtensions,
} AlcExtension_t;

#endif //  _APP_AUDIO_INTERFACE_H
