/*
File:   app_defines.h
Author: Taylor Robbins
Date:   11\04\2017
*/

#ifndef _APP_DEFINES_H
#define _APP_DEFINES_H

#define DEBUG_FONT_NAME "Consolas"
#define GUI_SCALE 1.0f

#define GAME_WINDOW_TITLE         "Piggy Bot"
#define GAME_WINDOW_TITLE_ABBREV  "PIGBOT"

#define ALL_DEBUG_OUTPUT_ENABLED true
#define DEBUG_OUTPUT_ENABLED     (true && ALL_DEBUG_OUTPUT_ENABLED)
#define REGULAR_OUTPUT_ENABLED   (true && ALL_DEBUG_OUTPUT_ENABLED)
#define INFO_OUTPUT_ENABLED      (true && ALL_DEBUG_OUTPUT_ENABLED)
#define NOTIFY_OUTPUT_ENABLED    (true && ALL_DEBUG_OUTPUT_ENABLED)
#define OTHER_OUTPUT_ENABLED     (true && ALL_DEBUG_OUTPUT_ENABLED)
#define WARNING_OUTPUT_ENABLED   (true && ALL_DEBUG_OUTPUT_ENABLED)
#define ERROR_OUTPUT_ENABLED     (true && ALL_DEBUG_OUTPUT_ENABLED)
#define DISPLAY_PLATFORM_LAYER_OUTPUT (true && ALL_DEBUG_OUTPUT_ENABLED)

#define RECORD_RC_DEBUG_INFO         true
#define PLAY_MISSING_SOUNDS          false
#define PERFORMANCE_CHECKS           true
#define RESOURCE_AUTO_UNLOAD_TIME    60000 //60 seconds
#define TARGET_FRAMERATE             60 //frames/second
#define TARGET_FRAME_DURATION        (1000.0 / (TARGET_FRAMERATE)) //ms
#define EMOJI_CHAR_VALUE             '\x01'

#define RESOURCES_LOAD_PERCENT       0.55f
#define SOUNDS_LOAD_PERCENT          0.45f

#define DYNAMIC_VERT_BUFFER_CHUNK_SIZE 32 //vertices
#define MAX_BATCHING_FONTS             4
#define FONT_BATCH_CHUNK_SIZE          1024 //glyphs
#define FONT_BATCH_BASE_CHUNK_SIZE     2048 //glyphs
#define MAX_BATCHING_SHEETS            4
#define SHEET_BATCH_CHUNK_SIZE         1024 //glyphs
#define SHEET_BATCH_BASE_CHUNK_SIZE    2048 //glyphs

#define VisLightSeaGreen NewColor(0xFF1ABC9C)
#define VisLightGreen    NewColor(0xFF2ECC71)
#define VisLightBlue     NewColor(0xFF3498DB)
#define VisLightPurple   NewColor(0xFF9B59B6)
#define VisLightBlueGray NewColor(0xFF34495E)
#define VisSeaGreen      NewColor(0xFF16A085)
#define VisGreen         NewColor(0xFF27AE60)
#define VisBlue          NewColor(0xFF2980B9)
#define VisPurple        NewColor(0xFF8E44AD)
#define VisBlueGray      NewColor(0xFF2C3E50)
#define VisYellow        NewColor(0xFFF1C40F)
#define VisLightOrange   NewColor(0xFFE67E22)
#define VisRed           NewColor(0xFFE74C3C)
#define VisWhite         NewColor(0xFFECF0F1)
#define VisLightGray     NewColor(0xFF95A5A6)
#define VisDarkYellow    NewColor(0xFFF39C12)
#define VisOrange        NewColor(0xFFD35400)
#define VisDarkRed       NewColor(0xFFC0392B)
#define VisSilver        NewColor(0xFFBDC3C7)
#define VisGray          NewColor(0xFF7F8C8D)

#define MonokaiBack        NewColor(0xFF3B3A32)
#define MonokaiYellow      NewColor(0xFFE6DB74)
#define MonokaiLightYellow NewColor(0xFFFFE792)
#define MonokaiFadedYellow NewColor(0xFFFFEFB7)
#define MonokaiPurple      NewColor(0xFFAE81FF)
#define MonokaiLightPurple NewColor(0xFFE777FF)
#define MonokaiGreen       NewColor(0xFFA6E22E)
#define MonokaiDarkGreen   NewColor(0xFF829520)
#define MonokaiOrange      NewColor(0xFFFD971F)
#define MonokaiBrown       NewColor(0xFF9D550F)
#define MonokaiMagenta     NewColor(0xFFF92672)
#define MonokaiRed         NewColor(0xFFF83333)
#define MonokaiLightRed    NewColor(0xFFFF5959)
#define MonokaiBlue        NewColor(0xFF66D9EF)
#define MonokaiLightBlue   NewColor(0xFFA9FFFF)
#define MonokaiWhite       NewColor(0xFFF8F8F2)
#define MonokaiLightGray   NewColor(0xFFBBBBBB)
#define MonokaiGray1       NewColor(0xFFAFAFA2)
#define MonokaiGray2       NewColor(0xFF75715E)
#define MonokaiDarkGray    NewColor(0xFF212121)

#define NUM_REPLACE_COLORS 16
#define ReplacePink1       NewColor(0xFF710040)
#define ReplacePink2       NewColor(0xFFAA0060)
#define ReplacePink3       NewColor(0xFFBD006D)
#define ReplacePink4       NewColor(0xFFD4007B)
#define ReplacePink5       NewColor(0xFFEC008C)
#define ReplacePink6       NewColor(0xFFFF0897)
#define ReplacePink7       NewColor(0xFFFF24A3)
#define ReplacePink8       NewColor(0xFFED4E95)
#define ReplacePink9       NewColor(0xFFF06EAA)
#define ReplacePink10      NewColor(0xFFF088B8)
#define ReplacePink11      NewColor(0xFFF294BE)
#define ReplacePink12      NewColor(0xFFF6ACCD)
#define ReplacePink13      NewColor(0xFFF8B8CE)
#define ReplacePink14      NewColor(0xFFF9C5D0)
#define ReplacePink15      NewColor(0xFFFACED3)
#define ReplacePink16      NewColor(0xFFFCE1DE)

#define TAB_WIDTH                         4 //spaces
#define TRANSIENT_MAX_NUMBER_MARKS        32
#define TILE_SIZE                         16 //px
#define LARGER_TILE_SIZE                  22 //px
#define MASSIVE_TILE_SIZE                 64 //px
#define GROWING_HEAP_PAGE_SIZE            Megabytes(1)
#define GROWING_HEAP_MAX_NUM_PAGES        128
#define GAME_BUTTON_REPEAT_TIME           160 //ms
#define GAME_BUTTON_REPEAT_DELAY          400 //ms
#define UNDO_HOLD_REPEAT_TIME             100 //ms
#define UNDO_HOLD_REPEAT_DELAY            400 //ms
#define SAMPLE_BUFFER_ALLOC_SIZE          (1*1024*1024) //samples
#define OGG_READ_BUFFER_SIZE              (4*1024) //bytes
#define MAX_BUTTONS_QUEUED                16 //buttons

#define SCREENSHOT_MESSAGE_DISPLAY_TIME 3000 //ms
#define SCREENSHOT_MESSAGE_FADE_TIME    500 //ms
#define SCREENSHOT_COUNTDOWN_MAX_TIME   (10*1000) //ms

#define INITIAL_APP_STATE         AppState_SettingsMenu

//CSL stands for Debug Console
#define CSL_FIFO_SPACE_SIZE             Kilobytes(64)
#define CSL_LINE_BUILD_SIZE             Kilobytes(2)
#define CSL_PRINT_BUFFER_SIZE           Kilobytes(16)
#define CSL_OPEN_TIME                   100 //ms
#define CSL_CLOSE_TIME                  100 //ms
#define CSL_FONT                        (GetFont(debugFont))
#define CSL_FONT_SIZE                   1
#define CSL_GUTTER_NUM_FONT             (GetFont(debugFont))
#define CSL_GUTTER_NUM_FONT_SIZE        1
#define CSL_FILE_NAME_FONT              (GetFont(pixelFont))
#define CSL_FILE_NAME_FONT_SIZE         1
#define CSL_LINE_NUM_FONT               (GetFont(pixelFont))
#define CSL_LINE_NUM_FONT_SIZE          1
#define CSL_FUNC_NAME_FONT              (GetFont(pixelFont))
#define CSL_FUNC_NAME_FONT_SIZE         1
#define CSL_LINE_SPACING                1 //px
#define CSL_SCROLL_BAR_WIDTH            5 //px
#define CSL_MIN_SCROLLBAR_HEIGHT        20 //px
#define CSL_INPUT_BOX_MAX_CHARS         256 //chars
#define CSL_DEFAULT_LINE_WRAP           true
#define CSL_DEFAULT_SHOW_GUTTER_NUMS    true
#if DEVELOPER
#define CSL_DEFAULT_SHOW_FILE_NAMES     false
#define CSL_DEFAULT_SHOW_LINE_NUMBERS   true
#define CSL_DEFAULT_SHOW_FUNCTION_NAMES false
#else
#define CSL_DEFAULT_SHOW_FILE_NAMES     false
#define CSL_DEFAULT_SHOW_LINE_NUMBERS   false
#define CSL_DEFAULT_SHOW_FUNCTION_NAMES false
#endif
#define CSL_MAX_PREV_COMMADS_RECALL     16

//GFR stands for Giffer AppState
#define GFR_BUTTON_MARGIN               10 //px
#define GFR_BUTTON_PADDING              5 //px
#define GFR_BUTTON_ICON_SCALE           2
#define GFR_BUTTON_FONT_SCALE           3
#define GFR_TIMELINE_MARGIN_SIDES       50 //px
#define GFR_TIMELINE_SCROLL_SPEED       50 //multiplier
#define GFR_TIMELINE_SCROLL_LAG         8.0f //divisor
#define GFR_BRACKET_THICKNESS           2 //px
#define GFR_TIMELINE_FONT_SCALE         2

#define NEW_TB_DEF_ALLOC_CHUNK_SIZE            128 //chars
#define NEW_TB_DEF_SCROLL_LAG                  2.0f //divisor
#define NEW_TB_SCROLL_OVERSHOOT_AMOUNT         60 //px
#define NEW_TB_SCROLL_OVER_AMOUNT              100 //px
#define NEW_TB_DOUBLE_CLICK_TIME               400 //ms

#define MOVEMENT_STICK_DEADZONE 0.2f

#define MAX_SOUND_INSTANCES                  16
#define MAX_PRELOADED_SONGS                  4
#define DEF_MUSIC_TRANS_TIME                 2000 //ms
#define SND_EFFECT_COMBINE_TIME              33 //ms (2 frames)
#define FRAMERATE_GRAPH_WIDTH                120 //ticks
#define FRAMERATE_VERT_TICK_VAL              4 //ms
#define DEFAULT_NOTIFICATION_TIME            5000 //ms
#define MAX_NOTIFICATIONS_ON_SCREEN          8
#define MAX_NOTIFICATION_WIDTH               500 //px
#define NOTIFICATION_PADDING                 10 //px
#define NOTIFICATION_ANIM_TIME               200 //ms

#define GAME_BTN_ANALOG_DEADZONE   0.8f

#define FONTS_FLDR                 "Resources/Fonts/"
#define MUSIC_FLDR                 "Resources/Music/"
#define PALETTE_FLDR               "Resources/Palette/"
#define SHADERS_FLDR               "Resources/Shaders/"
#define SHEETS_FLDR                "Resources/Sheets/"
#define SOUNDS_FLDR                "Resources/Sounds/"
#define SPRITES_FLDR               "Resources/Sprites/"
#define TEXTURES_FLDR              "Resources/Textures/"
#define MISSING_TEXTURE_PATH       SPRITES_FLDR "missing.jpg"
#define SOUNDS_META_FILE_PATH      SOUNDS_FLDR "sounds_meta_info.txt"
#define MISSING_SOUND_PATH         SOUNDS_FLDR "missing.ogg"
#define MISSING_SOUND_PATH_LOOPING SOUNDS_FLDR "missing_loop.ogg"
#define PALETTE_FILE_PATH          PALETTE_FLDR "palette1.png"
#define SETTINGS_FILE_NAME         "settings.cfg"
#define GAME_NAME_FOR_FOLDERS      "PiggyBot"

#define TWITCH_IRC_URL                     "irc.twitch.tv"
#define TWITCH_WEB_SOCKET_URL              "irc-ws.twitch.tv"
#define BOT_USERNAME                       "piggybank_bot"
#define BOT_PASSWORD_FILE_PATH             "password.txt"
#define BOT_CHANNEL                        "piggybankstudios"
#define IRC_NORMAL_PORT                    6667
#define IRC_SECURE_PORT                    6697
#define HTTP_NORMAL_PORT                   80
#define HTTP_SECURE_PORT                   443
#define TWITCH_API_CLIENT_INFO_FILE_PATH   "client.txt"
#define TWITCH_API_LOGIN_URL               "id.twitch.tv/oauth2/token"
#define TWITCH_API_LOGIN_RETRY_WAIT_TIME   5000 //ms
#define TWITCH_API_STREAMS_URL             "api.twitch.tv/helix/streams" //?user_login=PiggybankStudios
#define TWITCH_API_STREAMS_RETRY_WAIT_TIME 30000 //ms (30 seconds)
#define TWITCH_API_STREAMS_PING_TIME       60000 //ms (1 minute)
#define NAMES_FILE_PATH                    "names.txt"

#define CHAR_DIAMOND          "\x03"
#define CHAR_CHECK            "\x04"
#define CHAR_ARROW_RIGHT      "\x10"
#define CHAR_ARROW_LEFT       "\x11"
#define CHAR_ARROW_UP         "\x1E"
#define CHAR_ARROW_DOWN       "\x1F"
#define CHAR_LINE_UP          "\x80"
#define CHAR_LINE_DOWN        "\x81"
#define CHAR_LINE_LOW         "\x82"
#define CHAR_LINE_HIGH        "\x83"
#define CHAR_LINE_UP_BROKEN   "\x84"
#define CHAR_LINE_DOWN_BROKEN "\x85"
#define CHAR_CW               "\x86"
#define CHAR_CCW              "\x87"
#define CHAR_UP               "\x88"
#define CHAR_DOWN             "\x89"
#define CHAR_HALF             "\x8A"
#define CHAR_FOURTH           "\x8B"

#endif //  _APP_DEFINES_H
