/*
File:   app_chat_handler.cpp
Author: Taylor Robbins
Date:   10\26\2020
Description: 
	** Holds a bunch of functions that handle sending various IRC commands through the
	** socket connection in a ChatConnection_t structure
*/

#include "app/app_chat_handler_log_macros.cpp"

void DestroyChatter(Chatter_t* chatter)
{
	NotNull(chatter);
	if (chatter->username != nullptr)
	{
		ArenaPop(mainHeap, chatter->username);
	}
	ClearPointer(chatter);
}

void DestroyChatHandler(ChatHandler_t* handler)
{
	NotNull(handler);
	if (handler->created)
	{
		platform->CloseIrcConnection(&handler->connection);
	}
	DestroyConsoleArena(&handler->log);
	for (u32 cIndex = 0; cIndex < handler->chatters.length; cIndex++)
	{
		Chatter_t* chatter = DynArrayGet(&handler->chatters, Chatter_t, cIndex);
		NotNull(chatter);
		DestroyChatter(chatter);
	}
	DestroyDynamicArray(&handler->chatters);
	for (u32 eIndex = 0; eIndex < handler->events.length; eIndex++)
	{
		ChatEvent_t* event = DynArrayGet(&handler->events, ChatEvent_t, eIndex);
		NotNull(event);
		if (event->message != nullptr)
		{
			ArenaPop(mainHeap, event->message);
		}
	}
	DestroyDynamicArray(&handler->events);
	if (handler->ourPrefix != nullptr) { ArenaPop(mainHeap, handler->ourPrefix); }
	ClearPointer(handler);
}

bool CreateChatHandler(ChatHandler_t* handler, bool useWebSocketProtocol, bool useSsl)
{
	NotNull(handler);
	ClearPointer(handler);
	
	bool connectionStarted = platform->OpenIrcConnection(
		useWebSocketProtocol ? TWITCH_WEB_SOCKET_URL : TWITCH_IRC_URL,
		useWebSocketProtocol ? (useSsl ? HTTP_SECURE_PORT : HTTP_NORMAL_PORT) : (useSsl ? IRC_SECURE_PORT : IRC_NORMAL_PORT),
		&handler->connection,
		mainHeap, Kilobytes(4) //TODO: What should the write fifo size be?
	);
	if (connectionStarted)
	{
		// void CreateDynamicArray(DynArray_t* array, MemoryArena_t* memArena, u32 itemSize, u32 allocChunkSize = 16, u32 initialSizeRequirement = 0)
		CreateDynamicArray(&handler->chatters, mainHeap, sizeof(Chatter_t));
		CreateDynamicArray(&handler->events, mainHeap, sizeof(ChatEvent_t));
		handler->nextChatterId = 1;
		handler->created = true;
		handler->didLogin = false;
		handler->gotJoinResponse = false;
		handler->sentCapRequests = false;
		InitializeConsoleArena(&handler->log, mainHeap, Kilobytes(16), Kilobytes(2), Kilobytes(2));
		return true;
	}
	else
	{
		WriteLine_E("Failed to start connection to chat");
		return false;
	}
}

//TODO: Automatically prefix the message depending on our login state
void SendChatCommand(ChatHandler_t* handler, const char* commandStr, const char* dataStr, u32 dataLength, bool prefixCommand = true)
{
	NotNull(handler);
	NotNull(commandStr);
	Assert(handler->created);
	Assert(handler->connection.isValid);
	
	if (handler->ourPrefix != nullptr && prefixCommand)
	{
		if (dataStr != nullptr && dataLength > 0) { ChatPrintLine_D(handler, "> :%s %s %.*s", handler->ourPrefix, commandStr, dataLength, dataStr); }
		else { ChatPrintLine_D(handler, "> :%s %s", handler->ourPrefix, commandStr); }
		FifoPush(&handler->connection.writeFifo, ':');
		FifoPushArray(&handler->connection.writeFifo, (const u8*)handler->ourPrefix, MyStrLength32(handler->ourPrefix));
		FifoPush(&handler->connection.writeFifo, ' ');
	}
	else
	{
		if (dataStr != nullptr && dataLength > 0) { ChatPrintLine_D(handler, "> %s %.*s", commandStr, dataLength, dataStr); }
		else { ChatPrintLine_D(handler, "> %s", commandStr); }
	}
	
	FifoPushArray(&handler->connection.writeFifo, (const u8*)commandStr, MyStrLength32(commandStr));
	if (dataStr != nullptr && dataLength > 0)
	{
		FifoPush(&handler->connection.writeFifo, ' ');
		FifoPushArray(&handler->connection.writeFifo, (const u8*)dataStr, dataLength);
	}
	FifoPush(&handler->connection.writeFifo, '\r');
	FifoPush(&handler->connection.writeFifo, '\n');
	// PrintLine_D("Sending command: %s %.*s", commandStr, dataLength, dataStr);
}
void SendChatCommandNt(ChatHandler_t* handler, const char* commandStr, const char* dataStr, bool prefixCommand = true)
{
	SendChatCommand(handler, commandStr, dataStr, (dataStr != nullptr) ? MyStrLength32(dataStr) : 0);
}

bool SplitIrcMessageIntoCommand(const char* messagePntr, u32 messageLength, IrcCommand_t* commandOut)
{
	NotNull(commandOut);
	Assert(messagePntr != nullptr || messageLength == 0);
	ClearPointer(commandOut);
	commandOut->messagePntr = messagePntr;
	commandOut->messageLength = messageLength;
	if (messageLength == 0) { return false; } //empty command is invalid
	
	TempPushMark();
	u32 numPieces = 0;
	StrSplitPiece_t* pieces = SplitString(TempArena, messagePntr, messageLength, " ", 1, &numPieces);
	Assert(pieces != nullptr || numPieces == 0);
	if (numPieces == 0) { TempPopMark(); return false; } //no pieces somehow even though there are characters in the string? Something went wrong somehow
	
	u32 firstArgIndex = 0;
	if (pieces[0].length > 1 && pieces[0].pntr[0] == ':')
	{
		if (numPieces < 2) { TempPopMark(); return false; } //first piece was prefix but there was no next piece for command
		commandOut->prefix.pntr = pieces[0].pntr+1;
		commandOut->prefix.length = pieces[0].length-1;
		commandOut->command = pieces[1];
		firstArgIndex = 2;
	}
	else
	{
		commandOut->command = pieces[0];
		firstArgIndex = 1;
	}
	
	commandOut->numArgs = 0;
	for (u32 pIndex = 0; pIndex < (numPieces - firstArgIndex); pIndex++)
	{
		if (commandOut->numArgs >= MAX_IRC_ARGUMENTS) { TempPopMark(); return false; } //Too many arguments in command
		StrSplitPiece_t* nextPiece = &pieces[firstArgIndex + pIndex];
		if (nextPiece->length > 0 && nextPiece->pntr[0] == ':')
		{
			Assert((nextPiece->pntr+1) <= (messagePntr + messageLength));
			commandOut->args[commandOut->numArgs].pntr = nextPiece->pntr+1;
			commandOut->args[commandOut->numArgs].length = (u32)((messagePntr + messageLength) - (nextPiece->pntr+1));
			commandOut->numArgs++;
			break;
		}
		else
		{
			commandOut->args[commandOut->numArgs].pntr = nextPiece->pntr;
			commandOut->args[commandOut->numArgs].length = nextPiece->length;
			commandOut->numArgs++;
		}
	}
	Assert(commandOut->numArgs <= MAX_IRC_ARGUMENTS);
	
	TempPopMark();
	return true;
}

void DeleteChatter(ChatHandler_t* handler, Chatter_t* chatter)
{
	NotNull(handler);
	NotNull(chatter);
	Assert(IsPntrInDynArray(&handler->chatters, chatter));
	u32 chatterIndex = GetDynArrayItemIndex(&handler->chatters, chatter);
	DestroyChatter(chatter);
	DynArrayRemove(&handler->chatters, chatterIndex);
}

Chatter_t* PushChatter(ChatHandler_t* handler, u8 flags, const char* username, Color_t displayColor)
{
	NotNull(handler);
	NotNull(username);
	Chatter_t* newChatter = DynArrayAdd(&handler->chatters, Chatter_t);
	NotNull(newChatter);
	ClearPointer(newChatter);
	newChatter->id = handler->nextChatterId;
	handler->nextChatterId++;
	newChatter->flags = flags;
	newChatter->displayColor = displayColor;
	newChatter->username = ArenaNtString(mainHeap, username);
	NotNull(newChatter->username);
	newChatter->joinTime = ProgramTime;
	newChatter->lastActivityTime = ProgramTime;
	return newChatter;
}
ChatEvent_t* PushChatEvent(ChatHandler_t* handler, ChatEventType_t type)
{
	NotNull(handler);
	ChatEvent_t* newEventPntr = nullptr;
	for (u32 eIndex = 0; eIndex < handler->events.length; eIndex++)
	{
		ChatEvent_t* event = DynArrayGet(&handler->events, ChatEvent_t, eIndex);
		NotNull(event);
		if (event->type == ChatEventType_None)
		{
			newEventPntr = event;
			break;
		}
	}
	if (newEventPntr == nullptr)
	{
		newEventPntr = DynArrayAdd(&handler->events, ChatEvent_t);
		NotNull(newEventPntr);
	}
	ClearPointer(newEventPntr);
	newEventPntr->type = type;
	newEventPntr->time = ProgramTime;
	return newEventPntr;
}

void ConsumeChatEvent(ChatHandler_t* handler, ChatEvent_t* event)
{
	NotNull(handler);
	NotNull(event);
	Assert(IsPntrInDynArray(&handler->events, event));
	if (event->message != nullptr)
	{
		ArenaPop(mainHeap, event->message);
	}
	ClearPointer(event);
}

Chatter_t* GetChatterById(ChatHandler_t* handler, u32 id)
{
	NotNull(handler);
	for (u32 cIndex = 0; cIndex < handler->chatters.length; cIndex++)
	{
		Chatter_t* chatter = DynArrayGet(&handler->chatters, Chatter_t, cIndex);
		NotNull(chatter);
		if (chatter->id == id) { return chatter; }
	}
	return nullptr;
}
Chatter_t* GetChatterByUsername(ChatHandler_t* handler, const char* username)
{
	NotNull(handler);
	NotNull(username);
	for (u32 cIndex = 0; cIndex < handler->chatters.length; cIndex++)
	{
		Chatter_t* chatter = DynArrayGet(&handler->chatters, Chatter_t, cIndex);
		NotNull(chatter);
		if (StrCompareIgnoreCaseNt(chatter->username, username))
		{
			return chatter;
		}
	}
	return nullptr;
}

void HandleChatMessage(ChatHandler_t* handler, const char* sender, const char* message)
{
	NotNull(handler);
	NotNull(sender);
	NotNull(message);
	
	Chatter_t* chatter = GetChatterByUsername(handler, sender);
	if (chatter == nullptr)
	{
		chatter = PushChatter(handler, 0x00, sender, PalRed);
		NotNull(chatter);
	}
	chatter->lastActivityTime = ProgramTime;
	
	ChatEvent_t* newEvent = PushChatEvent(handler, ChatEventType_Message);
	NotNull(newEvent);
	newEvent->message = ArenaNtString(mainHeap, message);
	NotNull(newEvent->message);
	newEvent->chatterId = chatter->id;
}

void UpdateChatHandler(ChatHandler_t* handler)
{
	NotNull(handler);
	
	if (handler->created)
	{
		if (!handler->connection.isValid) { DestroyChatHandler(handler); return; }
		if (handler->connection.state == IrcConnectionState_Error)
		{
			WriteLine_E("Socket error occurred. Closing chat connection");
			DestroyChatHandler(handler);
			return;
		}
		
		platform->UpdateIrcConnection(&handler->connection);
		
		if (handler->connection.state == IrcConnectionState_Connected)
		{
			// +==============================+
			// |        Perform Login         |
			// +==============================+
			if (!handler->didLogin)
			{
				FileInfo_t passwordFile = platform->ReadEntireFile(BOT_PASSWORD_FILE_PATH);
				Assert(passwordFile.content != nullptr);
				
				WriteLine_I("Sending login information...");
				ChatWriteLine_I(handler, "Sending login information...");
				
				SendChatCommand(handler, "PASS", (const char*)passwordFile.content, passwordFile.size);
				SendChatCommandNt(handler, "NICK", BOT_USERNAME);
				SendChatCommandNt(handler, "USER", BOT_USERNAME " " TWITCH_IRC_URL " bla :" BOT_USERNAME);
				
				SendChatCommandNt(handler, "JOIN", "#" BOT_CHANNEL);
				
				platform->FreeFileMemory(&passwordFile);
				handler->didLogin = true;
			}
			
			// +==============================+
			// |    Send CAP REQ Commands     |
			// +==============================+
			if (handler->didLogin && handler->gotJoinResponse && !handler->sentCapRequests)
			{
				WriteLine_I("Sending cap requests...");
				ChatWriteLine_I(handler, "Sending cap requests...");
				
				SendChatCommandNt(handler, "CAP REQ", "twitch.tv/membership");
				// SendChatCommandNt(handler, "CAP REQ", "twitch.tv/tags"); //Enabled the RAID messages?
				SendChatCommandNt(handler, "CAP REQ", "twitch.tv/commands");
				handler->sentCapRequests = true;
			}
			
			// +==============================+
			// |   Handle Command Responses   |
			// +==============================+
			char* newMessageStr = FifoPopLine(&handler->connection.readFifo, TempArena);
			while (newMessageStr != nullptr)
			{
				ChatPrintLine_D(handler, "< %s", newMessageStr);
				// PrintLine_D("< %s", newMessageStr);
				u32 newMessageLength = MyStrLength32(newMessageStr);
				if (newMessageStr[newMessageLength-1] == '\r') { newMessageLength--; newMessageStr[newMessageLength] = '\0'; }
				
				IrcCommand_t command = {};
				if (SplitIrcMessageIntoCommand(newMessageStr, newMessageLength, &command))
				{
					if (DoesSplitPieceEqualIgnoreCaseNt(&command.command, "001") || DoesSplitPieceEqualIgnoreCaseNt(&command.command, "002") ||
						DoesSplitPieceEqualIgnoreCaseNt(&command.command, "003") || DoesSplitPieceEqualIgnoreCaseNt(&command.command, "004") ||
						DoesSplitPieceEqualIgnoreCaseNt(&command.command, "375") || DoesSplitPieceEqualIgnoreCaseNt(&command.command, "372") ||
						DoesSplitPieceEqualIgnoreCaseNt(&command.command, "376"))
					{
						// if (command.numArgs >= 2)
						// {
						// 	PrintLine_D("Response %.*s: %.*s", command.command.length, command.command.pntr, command.args[1].length, command.args[1].pntr);
						// }
						// else { PrintLine_E("Expected at least two arguments in command %.*s. Got %u: %s", command.command.length, command.command.pntr, command.numArgs, newMessageStr); }
					}
					else if (DoesSplitPieceEqualIgnoreCaseNt(&command.command, "JOIN"))
					{
						if (command.prefix.pntr != nullptr && command.prefix.length > 0)
						{
							if (command.numArgs >= 1)
							{
								TrimSplitPieceWhitespace(&command.args[0]);
								if (DoesSplitPieceEqualIgnoreCaseNt(&command.args[0], "#" BOT_CHANNEL))
								{
									if (!handler->gotJoinResponse)
									{
										PrintLine_I("Got %sJOIN response", handler->gotJoinResponse ? "duplicate " : "");
										ChatPrintLine_I(handler, "Got %sJOIN response", handler->gotJoinResponse ? "duplicate " : "");
										if (handler->ourPrefix != nullptr) { ArenaPop(mainHeap, handler->ourPrefix); }
										handler->ourPrefix = ArenaString(mainHeap, command.prefix.pntr, command.prefix.length);
										NotNull(handler->ourPrefix);
										handler->gotJoinResponse = true;
									}
									else
									{
										PrintLine_I("Got JOIN message for %.*s", command.prefix.length, command.prefix.pntr);
									}
								}
								else
								{
									PrintLine_E("Got JOIN response for different channel than expected: \"%.*s\" expected \"%s\"", command.args[0].length, command.args[0].pntr, "#" BOT_CHANNEL);
									// Print_E("  [%u]{ ", command.args[0].length);
									// for (u32 cIndex = 0; cIndex < command.args[0].length; cIndex++)
									// {
									// 	Print_E("%02X ", command.args[0].pntr[cIndex]);
									// }
									// Write_E("}");
								}
							}
							else { PrintLine_E("Expected at least 1 argument in command JOIN. Got %u: %s", command.numArgs, newMessageStr); }
						}
						else { PrintLine_E("Expected prefix in JOIN response: %s", newMessageStr); }
					}
					else if (DoesSplitPieceEqualIgnoreCaseNt(&command.command, "353"))
					{
						WriteLine_D("Got response 353 (TODO: Handle this)");
						ChatWriteLine_D(handler, "Got response 353 (TODO: Handle this)");
					}
					else if (DoesSplitPieceEqualIgnoreCaseNt(&command.command, "366"))
					{
						WriteLine_D("Got response 366 (TODO: Handle this)");
						ChatWriteLine_D(handler, "Got response 366 (TODO: Handle this)");
					}
					else if (DoesSplitPieceEqualIgnoreCaseNt(&command.command, "PING"))
					{
						WriteLine_D("Responding to PING command");
						SendChatCommandNt(handler, "PONG", handler->ourPrefix, false);
					}
					else if (DoesSplitPieceEqualIgnoreCaseNt(&command.command, "PRIVMSG"))
					{
						if (command.prefix.pntr != nullptr && command.numArgs >= 1)
						{
							i32 firstPeriodIndex = FindChar(command.prefix.pntr, command.prefix.length, '.');
							if (firstPeriodIndex > 0)
							{
								char* wholeNameStr = ArenaString(TempArena, command.prefix.pntr, firstPeriodIndex);
								NotNull(wholeNameStr);
								u32 wholeNameLength = (u32)firstPeriodIndex;
								u32 numPieces = 0;
								u32 pieceStart = 0;
								StrSplitPiece_t pieces[3];
								for (u32 cIndex = 0; cIndex < wholeNameLength; cIndex++)
								{
									if (wholeNameStr[cIndex] == '!' || wholeNameStr[cIndex] == '@')
									{
										if (cIndex > pieceStart)
										{
											if (numPieces < ArrayCount(pieces))
											{
												pieces[numPieces].pntr = &wholeNameStr[pieceStart];
												pieces[numPieces].length = cIndex - pieceStart;
												wholeNameStr[cIndex] = '\0';
											}
											numPieces++;
										}
										else {} //empty piece, don't care
										pieceStart = cIndex+1;
									}
								}
								if (pieceStart < wholeNameLength)
								{
									if (numPieces < ArrayCount(pieces))
									{
										pieces[numPieces].pntr = &wholeNameStr[pieceStart];
										pieces[numPieces].length = wholeNameLength - pieceStart;
									}
									numPieces++;
								}
								if (numPieces > ArrayCount(pieces)) { NotifyPrint_E("Found %u pieces in chat PRIVMSG prefix: \"%.*s\"", numPieces, command.prefix.length, command.prefix.pntr); }
								
								if (numPieces > 0)
								{
									// PrintLine_D("Found %u pieces in prefix", numPieces);
									// for (u32 pIndex = 1; pIndex < numPieces; pIndex++)
									// {
									// 	PrintLine_D("  Piece[%u]: \"%s\"", pIndex, pieces[pIndex].pntr);
									// }
									bool namesAreEqual = true;
									for (u32 pIndex = 1; pIndex < numPieces; pIndex++)
									{
										if (!StrCompareIgnoreCaseNt(pieces[pIndex-1].pntr, pieces[pIndex].pntr))
										{
											namesAreEqual = false;
											break;
										}
									}
									if (!namesAreEqual)
									{
										NotifyPrint_E("PRIVMSG prefix name mismatch: \"%.*s\"", command.prefix.length, command.prefix.pntr);
									}
									
									const char* msgSender = pieces[0].pntr;
									const char* messageStr = command.args[command.numArgs-1].pntr;
									
									HandleChatMessage(handler, msgSender, messageStr);
								}
								else
								{
									NotifyPrint_E("Failed to parse prefix to find username of PRIVMSG: %s", newMessageStr);
								}
							}
							else
							{
								NotifyPrint_E("Found no period in prefix of PRIVMSG: %s", newMessageStr);
							}
						}
						else
						{
							NotifyPrint_E("No prefix or no arguments in PRIVMSG: %s", newMessageStr);
						}
					}
					else
					{
						if (command.prefix.pntr != nullptr)
						{
							PrintLine_W("Unknown IRC Command \"%.*s\" with %u arguments prefix \"%.*s\"", command.command.length, command.command.pntr, command.numArgs, command.prefix.length, command.prefix.pntr);
						}
						else
						{
							PrintLine_W("Unknown IRC Command \"%.*s\" with %u arguments unprefixed", command.command.length, command.command.pntr, command.numArgs);
						}
						for (u32 aIndex = 0; aIndex < command.numArgs; aIndex++)
						{
							StrSplitPiece_t* arg = &command.args[aIndex];
							PrintLine_W("  [%u]: %.*s", aIndex, arg->length, arg->pntr);
						}
					}
				}
				else
				{
					PrintLine_E("Failed to parse new message as IRC command:\n%s", newMessageStr);
				}
				newMessageStr = FifoPopLine(&handler->connection.readFifo, TempArena);
			}
		}
	}
}
