/*
File:   app_dynamic_bitmap.cpp
Author: Taylor Robbins
Date:   07\28\2020
Description: 
	** A Dyanmic Bitmap is like a selection that could be made by a magic wand tool in a image editor.
	** We have to save a 1D bitmap of which grid spaces are selected or not
*/

u32 _DynBitmapGetByteIndex(reci bounds, v2i pixelPos, u8* bitIndexOut = nullptr)
{
	u32 totalIndex = ((pixelPos.x - bounds.x) + ((pixelPos.y - bounds.y) * bounds.width));
	u32 byteIndex = (totalIndex / 8);
	if (bitIndexOut != nullptr) { *bitIndexOut = (u8)(totalIndex%8); }
	return byteIndex;
}

u32 DynBitmapGetBitmapSize(v2i boundsSize, bool roundUp = false)
{
	Assert(boundsSize.width >= 0 && boundsSize.height >= 0);
	u32 numTiles = (u32)boundsSize.width * (u32)boundsSize.height;
	if (numTiles == 0) { return 0; }
	u32 result = numTiles/8;
	if ((numTiles%8) != 0) { result++; }
	if (roundUp && (result%8) != 0) { result += (8 - (result % 8)); }
	return result;
}
void DynBitmapSetBit(DynBitmap_t* bitmap, v2i pixelPos, bool value)
{
	Assert(bitmap != nullptr);
	Assert(bitmap->bitmapData != nullptr);
	Assert(IsCoordInsideReci(bitmap->bounds, pixelPos));
	// u32 bitIndex = ((pixelPos.x - bitmap->x) + ((pixelPos.y - bitmap->y) * bitmap->width));
	// u32 byteIndex = (bitIndex / 8);
	// bitIndex = (bitIndex%8);
	u8 bitIndex = 0;
	u32 byteIndex = _DynBitmapGetByteIndex(bitmap->bounds, pixelPos, &bitIndex);
	Assert(byteIndex < bitmap->bitmapSize);
	if (IsFlagSet(bitmap->bitmapData[byteIndex], (1 << bitIndex)) != value)
	{
		FlagSetTo(bitmap->bitmapData[byteIndex], (1 << bitIndex), value);
		if (bitmap->debugOutput) { PrintLine_D("(%d,%d)%s [%u.%u]: %u -> %u", pixelPos.x, pixelPos.y, value ? "++" : "--", byteIndex, bitIndex, bitmap->area, bitmap->area + (value ? 1 : -1)); }
		if (value) { bitmap->area++; }
		else { Assert(bitmap->area > 0); bitmap->area--; }
	}
	
}
bool DynBitmapGetBit(DynBitmap_t* bitmap, v2i pixelPos)
{
	Assert(bitmap != nullptr);
	if (bitmap->bitmapData == nullptr) { return false; }
	if (!IsCoordInsideReci(bitmap->bounds, pixelPos)) { return false; }
	// u32 bitIndex = ((pixelPos.x - bitmap->x) + ((pixelPos.y - bitmap->y) * bitmap->width));
	// u32 byteIndex = (bitIndex / 8);
	// Assert(byteIndex < bitmap->bitmapSize);
	// bitIndex = (bitIndex%8);
	u8 bitIndex = 0;
	u32 byteIndex = _DynBitmapGetByteIndex(bitmap->bounds, pixelPos, &bitIndex);
	Assert(byteIndex < bitmap->bitmapSize);
	return IsFlagSet(bitmap->bitmapData[byteIndex], (1 << bitIndex));
}

void _DynBitmapShiftBitsDown(DynBitmap_t* bitmap, reci newBounds)
{
	reci oldBounds = bitmap->bounds;
	for (i32 yOffset = 0; yOffset < newBounds.height; yOffset++)
	{
		for (i32 xOffset = 0; xOffset < newBounds.width; xOffset++)
		{
			v2i tilePos = newBounds.topLeft + NewVec2i(xOffset, yOffset);
			if (IsCoordInsideReci(oldBounds, tilePos))
			{
				u8 oldBitIndex = 0;
				u32 oldByteIndex = _DynBitmapGetByteIndex(oldBounds, tilePos, &oldBitIndex);
				u8 newBitIndex = 0;
				u32 newByteIndex = _DynBitmapGetByteIndex(newBounds, tilePos, &newBitIndex);
				Assert(oldByteIndex > newByteIndex || (oldByteIndex == newByteIndex && oldBitIndex >= newBitIndex));
				bitmap->bounds = oldBounds;
				bool oldValue = DynBitmapGetBit(bitmap, tilePos);
				bitmap->bounds = newBounds;
				DynBitmapSetBit(bitmap, tilePos, oldValue);
			}
			else
			{
				bitmap->bounds = newBounds;
				DynBitmapSetBit(bitmap, tilePos, false);
			}
		}
	}
	bitmap->bounds = newBounds;
	if (bitmap->allocArena != nullptr) { Assert(ArenaValidate(bitmap->allocArena)); }
}
void _DynBitmapShiftBitsUp(DynBitmap_t* bitmap, reci newBounds)
{
	reci oldBounds = bitmap->bounds;
	for (i32 yOffset = newBounds.height-1; yOffset >= 0; yOffset--)
	{
		for (i32 xOffset = newBounds.width-1; xOffset >= 0; xOffset--)
		{
			v2i tilePos = newBounds.topLeft + NewVec2i(xOffset, yOffset);
			if (IsCoordInsideReci(oldBounds, tilePos))
			{
				u8 oldBitIndex = 0;
				u32 oldByteIndex = _DynBitmapGetByteIndex(oldBounds, tilePos, &oldBitIndex);
				u8 newBitIndex = 0;
				u32 newByteIndex = _DynBitmapGetByteIndex(newBounds, tilePos, &newBitIndex);
				Assert(oldByteIndex < newByteIndex || (oldByteIndex == newByteIndex && oldBitIndex <= newBitIndex));
				bitmap->bounds = oldBounds;
				bool oldValue = DynBitmapGetBit(bitmap, tilePos);
				bitmap->bounds = newBounds;
				DynBitmapSetBit(bitmap, tilePos, oldValue);
			}
			else
			{
				bitmap->bounds = newBounds;
				DynBitmapSetBit(bitmap, tilePos, false);
			}
		}
	}
	bitmap->bounds = newBounds;
	if (bitmap->allocArena != nullptr) { Assert(ArenaValidate(bitmap->allocArena)); }
}

void DynBitmapChangeBounds(DynBitmap_t* bitmap, reci newBounds)
{
	Assert(bitmap != nullptr);
	if (newBounds == bitmap->bounds) { return; }
	Assert(bitmap->allocArena != nullptr);
	u32 newBitmapSize = DynBitmapGetBitmapSize(newBounds.size);
	if (bitmap->debugOutput)
	{
		WriteLine_D("==========Changing Bounds===========");
		PrintLine_D("(%d, %d, %d, %d) -> (%d, %d, %d, %d)",
			bitmap->bounds.x, bitmap->bounds.y, bitmap->bounds.width, bitmap->bounds.height,
			newBounds.x, newBounds.y, newBounds.width, newBounds.height
		);
		Print_D("Area: %u Bitmap: %u {", bitmap->area, bitmap->bitmapSize);
		for (u32 bIndex = 0; bIndex < bitmap->bitmapSize; bIndex++)
		{
			Print_D(" %s%s%s%s%s%s%s%s",
				((bitmap->bitmapData[bIndex] & 0x01) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x02) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x04) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x08) ? "1" : "0"),
				((bitmap->bitmapData[bIndex] & 0x10) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x20) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x40) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x80) ? "1" : "0")
			);
		}
		WriteLine_D(" }");
	}
	bool reallocationNeeded = false;
	if (newBitmapSize == 0)
	{
		if (bitmap->debugOutput) { WriteLine_D("Bits are being left out to dry"); }
		bitmap->bounds = newBounds;
		if (bitmap->allocArena != nullptr) { Assert(ArenaValidate(bitmap->allocArena)); }
	}
	else if (newBitmapSize > bitmap->bitmapSize)
	{
		reallocationNeeded = true;
	}
	else
	{
		#if 0
		reallocationNeeded = true;
		#else
		Assert(bitmap->bitmapSize > 0);
		i32 changeWidth = newBounds.width - bitmap->width;
		i32 changeHeight = newBounds.height - bitmap->height;
		i32 changeX = newBounds.x - bitmap->x;
		i32 changeY = newBounds.y - bitmap->y;
		if ((changeWidth < 0 && changeX != 0) || (changeHeight < 0 && changeY != 0))
		{
			reallocationNeeded = true;
		}
		else
		{
			if (bitmap->debugOutput)
			{
				PrintLine_D("Bits are being shifted %s%ux %s%uy %s%uw %s%uh",
					(changeX >= 0) ? "+" : "-", (u32)AbsI32(changeX),
					(changeY >= 0) ? "+" : "-", (u32)AbsI32(changeY),
					(changeWidth >= 0) ? "+" : "-", (u32)AbsI32(changeWidth),
					(changeHeight >= 0) ? "+" : "-", (u32)AbsI32(changeHeight)
				);
			}
			reci currentBounds = bitmap->bounds;
			currentBounds.height += changeHeight;
			bitmap->bounds = currentBounds;
			
			if (changeWidth > 0)
			{
				currentBounds.width += changeWidth;
				_DynBitmapShiftBitsUp(bitmap, currentBounds);
				bitmap->bounds = currentBounds;
			}
			else if (changeWidth < 0)
			{
				Assert(changeX == 0);
				currentBounds.width += changeWidth;
				_DynBitmapShiftBitsDown(bitmap, currentBounds);
				bitmap->bounds = currentBounds;
			}
			
			if (changeX > 0)
			{
				Assert(changeWidth >= 0);
				currentBounds.x += changeX;
				_DynBitmapShiftBitsDown(bitmap, currentBounds);
				bitmap->bounds = currentBounds;
			}
			else if (changeX < 0)
			{
				Assert(changeWidth >= 0);
				currentBounds.x += changeX;
				_DynBitmapShiftBitsUp(bitmap, currentBounds);
				bitmap->bounds = currentBounds;
			}
			
			if (changeY > 0)
			{
				Assert(changeHeight >= 0);
				currentBounds.y += changeY;
				_DynBitmapShiftBitsDown(bitmap, currentBounds);
				bitmap->bounds = currentBounds;
			}
			else if (changeY < 0)
			{
				Assert(changeHeight >= 0);
				currentBounds.y += changeY;
				_DynBitmapShiftBitsUp(bitmap, currentBounds);
				bitmap->bounds = currentBounds;
			}
			
			Assert(bitmap->bounds == newBounds);
		}
		#endif
	}
	if (reallocationNeeded)
	{
		if (bitmap->debugOutput) { WriteLine_D("Bits are being reallocated"); }
		newBitmapSize = DynBitmapGetBitmapSize(newBounds.size, true);
		if (bitmap->bitmapData == nullptr)
		{
			Assert(bitmap->bitmapSize == 0);
			bitmap->bitmapSize = newBitmapSize;
			bitmap->bitmapData = PushArray(bitmap->allocArena, u8, bitmap->bitmapSize);
			MyMemSet(bitmap->bitmapData, 0x00, bitmap->bitmapSize);
			bitmap->bounds = newBounds;
			if (bitmap->allocArena != nullptr) { Assert(ArenaValidate(bitmap->allocArena)); }
		}
		else
		{
			Assert(bitmap->bitmapSize > 0);
			u8* newBitmapData = PushArray(bitmap->allocArena, u8, newBitmapSize);
			Assert(newBitmapData != nullptr);
			MyMemSet(newBitmapData, 0x00, newBitmapSize);
			u32 oldArea = bitmap->area;
			u8* oldBitmapData = bitmap->bitmapData;
			u32 oldBitmapSize = bitmap->bitmapSize;
			reci oldBounds = bitmap->bounds;
			for (i32 yOffset = 0; yOffset < newBounds.height; yOffset++)
			{
				for (i32 xOffset = 0; xOffset < newBounds.width; xOffset++)
				{
					v2i pixelPos = newBounds.topLeft + NewVec2i(xOffset, yOffset);
					bitmap->bitmapData = oldBitmapData;
					bitmap->bitmapSize = oldBitmapSize;
					bitmap->bounds = oldBounds;
					if (DynBitmapGetBit(bitmap, pixelPos))
					{
						bitmap->bitmapData = newBitmapData;
						bitmap->bitmapSize = newBitmapSize;
						bitmap->bounds = newBounds;
						DynBitmapSetBit(bitmap, pixelPos, true);
					}
				}
			}
			bitmap->bitmapData = newBitmapData;
			bitmap->bitmapSize = newBitmapSize;
			bitmap->bounds = newBounds;
			bitmap->area = oldArea;
			ArenaPop(bitmap->allocArena, oldBitmapData);
			if (bitmap->allocArena != nullptr) { Assert(ArenaValidate(bitmap->allocArena)); }
		}
	}
	if (bitmap->debugOutput)
	{
		Print_D("Area: %u Bitmap: %u {", bitmap->area, bitmap->bitmapSize);
		for (u32 bIndex = 0; bIndex < bitmap->bitmapSize; bIndex++)
		{
			Print_D(" %s%s%s%s%s%s%s%s",
				((bitmap->bitmapData[bIndex] & 0x01) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x02) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x04) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x08) ? "1" : "0"),
				((bitmap->bitmapData[bIndex] & 0x10) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x20) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x40) ? "1" : "0"), ((bitmap->bitmapData[bIndex] & 0x80) ? "1" : "0")
			);
		}
		WriteLine_D(" }");
		WriteLine_D("======= Done Changing Bounds ========");
	}
}

void DestroyDynBitmap(DynBitmap_t* bitmap)
{
	Assert(bitmap != nullptr);
	if (bitmap->bitmapData != nullptr)
	{
		Assert(bitmap->allocArena != nullptr);
		ArenaPop(bitmap->allocArena, bitmap->bitmapData);
	}
	ClearPointer(bitmap);
}

void CreateDynBitmap(DynBitmap_t* bitmap, MemoryArena_t* memArena, reci initialRec)
{
	Assert(bitmap != nullptr);
	Assert(memArena != nullptr);
	Assert(initialRec.width >= 0 && initialRec.height >= 0);
	
	ClearPointer(bitmap);
	bitmap->allocArena = memArena;
	bitmap->area = 0;
	bitmap->bounds = initialRec;
	
	if (bitmap->size != Vec2i_Zero)
	{
		Assert(bitmap->width > 0 && bitmap->height > 0);
		bitmap->bitmapSize = DynBitmapGetBitmapSize(bitmap->size, true);
		Assert(bitmap->bitmapSize > 0);
		bitmap->bitmapData = PushArray(bitmap->allocArena, u8, bitmap->bitmapSize);
		Assert(bitmap->bitmapData != nullptr);
		MyMemSet(bitmap->bitmapData, 0x00, bitmap->bitmapSize);
		for (i32 yOffset = 0; yOffset < initialRec.height; yOffset++)
		{
			for (i32 xOffset = 0; xOffset < initialRec.width; xOffset++)
			{
				v2i pixelPos = initialRec.topLeft + NewVec2i(xOffset, yOffset);
				DynBitmapSetBit(bitmap, pixelPos, true);
			}
		}
	}
}
void CreateDynBitmap(DynBitmap_t* bitmap, MemoryArena_t* memArena, const v2i* pixelPositions, u32 numPositions)
{
	Assert(bitmap != nullptr);
	Assert(memArena != nullptr);
	Assert(pixelPositions != nullptr || numPositions == 0);
	
	ClearPointer(bitmap);
	bitmap->allocArena = memArena;
	bitmap->area = 0;
	
	bitmap->bounds = Reci_Zero;
	for (u32 pIndex = 0; pIndex < numPositions; pIndex++)
	{
		v2i pixelPos = pixelPositions[pIndex];
		if (pIndex == 0)
		{
			bitmap->offset = pixelPos;
			bitmap->size = Vec2i_One;
		}
		else
		{
			if (pixelPos.x < bitmap->x) { bitmap->width += (bitmap->x - pixelPos.x); bitmap->x = pixelPos.x; }
			if (pixelPos.y < bitmap->y) { bitmap->width += (bitmap->y - pixelPos.y); bitmap->y = pixelPos.y; }
			if (bitmap->width <= pixelPos.x+1 - bitmap->x) { bitmap->width = (pixelPos.x+1 - bitmap->x); }
			if (bitmap->height <= pixelPos.y+1 - bitmap->y) { bitmap->height = (pixelPos.y+1 - bitmap->y); }
		}
	}
	
	if (bitmap->size != Vec2i_Zero)
	{
		Assert(bitmap->width > 0 && bitmap->height > 0);
		bitmap->bitmapSize = DynBitmapGetBitmapSize(bitmap->size, true);
		Assert(bitmap->bitmapSize > 0);
		bitmap->bitmapData = PushArray(bitmap->allocArena, u8, bitmap->bitmapSize);
		Assert(bitmap->bitmapData != nullptr);
		MyMemSet(bitmap->bitmapData, 0x00, bitmap->bitmapSize);
		for (u32 pIndex = 0; pIndex < numPositions; pIndex++)
		{
			v2i pixelPos = pixelPositions[pIndex];
			DynBitmapSetBit(bitmap, pixelPos, true);
		}
	}
	else { Assert(numPositions == 0); }
}
void InitDynBitmap(DynBitmap_t* bitmap, MemoryArena_t* memArena)
{
	Assert(bitmap != nullptr);
	Assert(memArena != nullptr);
	CreateDynBitmap(bitmap, memArena, Reci_Zero);
}

bool DynBitmapUpdateBounds(DynBitmap_t* bitmap)
{
	Assert(bitmap != nullptr);
	reci newBounds = Reci_Zero;
	for (i32 yOffset = 0; yOffset < bitmap->height; yOffset++)
	{
		for (i32 xOffset = 0; xOffset < bitmap->width; xOffset++)
		{
			v2i pixelPos = bitmap->offset + NewVec2i(xOffset, yOffset);
			if (DynBitmapGetBit(bitmap, pixelPos))
			{
				if (newBounds == Reci_Zero)
				{
					newBounds = NewReci(pixelPos, Vec2i_One);
				}
				else
				{
					if (pixelPos.x < newBounds.x) { newBounds.width += newBounds.x - pixelPos.x; newBounds.x = pixelPos.x; }
					if (pixelPos.y < newBounds.y) { newBounds.height += newBounds.y - pixelPos.y; newBounds.y = pixelPos.y; }
					if (newBounds.width < pixelPos.x+1 - newBounds.x) { newBounds.width = pixelPos.x+1 - newBounds.x; }
					if (newBounds.height < pixelPos.y+1 - newBounds.y) { newBounds.height = pixelPos.y+1 - newBounds.y; }
				}
			}
		}
	}
	if (bitmap->bounds != newBounds)
	{
		DynBitmapChangeBounds(bitmap, newBounds);
		return true;
	}
	else { return false; }
}
void DynBitmapClear(DynBitmap_t* bitmap, bool deallocate = false)
{
	Assert(bitmap != nullptr);
	if (deallocate)
	{
		if (bitmap->bitmapData != nullptr)
		{
			Assert(bitmap->allocArena != nullptr);
			ArenaPop(bitmap->allocArena, bitmap->bitmapData);
			bitmap->bitmapData = nullptr;
			bitmap->bitmapSize = 0;
		}
		else { Assert(bitmap->bitmapSize == 0); }
	}
	else
	{
		if (bitmap->bitmapData != nullptr)
		{
			MyMemSet(bitmap->bitmapData, 0x00, bitmap->bitmapSize);
		}
	}
	bitmap->bounds = Reci_Zero;
	bitmap->area = 0;
}

bool DynBitmapAddPosition(DynBitmap_t* bitmap, v2i pos)
{
	Assert(bitmap != nullptr);
	if (!IsCoordInsideReci(bitmap->bounds, pos))
	{
		reci newBounds = bitmap->bounds;
		if (newBounds.width <= 0 || newBounds.height <= 0)
		{
			newBounds = NewReci(pos, Vec2i_One);
		}
		else
		{
			if (pos.x < newBounds.x) { newBounds.width += (newBounds.x - pos.x); newBounds.x = pos.x; }
			if (pos.y < newBounds.y) { newBounds.height += (newBounds.y - pos.y); newBounds.y = pos.y; }
			if (newBounds.width < pos.x+1 - newBounds.x) { newBounds.width = (pos.x+1 - newBounds.x); }
			if (newBounds.height < pos.y+1 - newBounds.y) { newBounds.height = (pos.y+1 - newBounds.y); }
		}
		DynBitmapChangeBounds(bitmap, newBounds);
	}
	if (!DynBitmapGetBit(bitmap, pos)) { DynBitmapSetBit(bitmap, pos, true); return true; }
	else { return false; }
}
bool DynBitmapAddPositions(DynBitmap_t* bitmap, const v2i* positions, u32 numPositions)
{
	Assert(bitmap != nullptr);
	Assert(positions != nullptr || numPositions == 0);
	bool result = false;
	reci newBounds = bitmap->bounds;
	for (u32 pIndex = 0; pIndex < numPositions; pIndex++)
	{
		v2i pixelPos = positions[pIndex];
		if (!IsCoordInsideReci(bitmap->bounds, pixelPos))
		{
			if (newBounds.width <= 0 || newBounds.height <= 0)
			{
				newBounds = NewReci(pixelPos, Vec2i_One);
			}
			else
			{
				if (pixelPos.x < newBounds.x) { newBounds.width += (newBounds.x - pixelPos.x); newBounds.x = pixelPos.x; }
				if (pixelPos.y < newBounds.y) { newBounds.height += (newBounds.y - pixelPos.y); newBounds.y = pixelPos.y; }
				if (newBounds.width < pixelPos.x+1 - newBounds.x) { newBounds.width = (pixelPos.x+1 - newBounds.x); }
				if (newBounds.height < pixelPos.y+1 - newBounds.y) { newBounds.height = (pixelPos.y+1 - newBounds.y); }
			}
		}
		else
		{
			if (!DynBitmapGetBit(bitmap, pixelPos))
			{
				DynBitmapSetBit(bitmap, pixelPos, true);
				result = true;
			}
		}
	}
	if (newBounds != bitmap->bounds)
	{
		DynBitmapChangeBounds(bitmap, newBounds);
		for (u32 pIndex = 0; pIndex < numPositions; pIndex++)
		{
			v2i pixelPos = positions[pIndex];
			if (!DynBitmapGetBit(bitmap, pixelPos))
			{
				DynBitmapSetBit(bitmap, pixelPos, true);
				result = true;
			}
		}
	}
	
	return result;
}
bool DynBitmapAddReci(DynBitmap_t* bitmap, reci rectangle)
{
	Assert(bitmap != nullptr);
	reci newBounds = ReciBoth(bitmap->bounds, rectangle);
	if (newBounds != bitmap->bounds) { DynBitmapChangeBounds(bitmap, newBounds); }
	bool result = false;
	for (i32 yOffset = 0; yOffset < rectangle.height; yOffset++)
	{
		for (i32 xOffset = 0; xOffset < rectangle.width; xOffset++)
		{
			v2i pixelPos = rectangle.topLeft + NewVec2i(xOffset, yOffset);
			if (!DynBitmapGetBit(bitmap, pixelPos))
			{
				DynBitmapSetBit(bitmap, pixelPos, true);
				result = true;
			}
		}
	}
	return result;
}
bool DynBitmapAddArea(DynBitmap_t* bitmap, const DynBitmap_t* newArea)
{
	Assert(bitmap != nullptr);
	Assert(newArea != nullptr);
	bool result = false;
	for (i32 yOffset = 0; yOffset < newArea->height; yOffset++)
	{
		for (i32 xOffset = 0; xOffset < newArea->width; xOffset++)
		{
			v2i pixelPos = newArea->offset + NewVec2i(xOffset, yOffset);
			if (!DynBitmapGetBit(bitmap, pixelPos))
			{
				if (DynBitmapAddPosition(bitmap, pixelPos))
				{
					result = true;
				}
			}
		}
	}
	return result;
}

bool DynBitmapRemovePosition(DynBitmap_t* bitmap, v2i pos)
{
	Assert(bitmap != nullptr);
	if (DynBitmapGetBit(bitmap, pos))
	{
		DynBitmapSetBit(bitmap, pos, false);
		return true;
	}
	else { return false; }
}
bool DynBitmapRemovePositions(DynBitmap_t* bitmap, const v2i* positions, u32 numPositions)
{
	Assert(bitmap != nullptr);
	Assert(positions != nullptr || numPositions == 0);
	bool result = false;
	for (u32 pIndex = 0; pIndex < numPositions; pIndex++)
	{
		v2i pixelPos = positions[pIndex];
		if (DynBitmapGetBit(bitmap, pixelPos))
		{
			DynBitmapSetBit(bitmap, pixelPos, false);
			result = true;
		}
	}
	return result;
}
bool DynBitmapRemoveReci(DynBitmap_t* bitmap, reci rectangle)
{
	Assert(bitmap != nullptr);
	bool result = false;
	for (i32 yOffset = 0; yOffset < rectangle.height; yOffset++)
	{
		for (i32 xOffset = 0; xOffset < rectangle.width; xOffset++)
		{
			v2i pixelPos = rectangle.topLeft + NewVec2i(xOffset, yOffset);
			if (DynBitmapGetBit(bitmap, pixelPos))
			{
				DynBitmapSetBit(bitmap, pixelPos, false);
				result = true;
			}
		}
	}
	return result;
}
bool DynBitmapRemoveArea(DynBitmap_t* bitmap, const DynBitmap_t* targetArea)
{
	Assert(bitmap != nullptr);
	Assert(targetArea != nullptr);
	bool result = false;
	for (i32 yOffset = 0; yOffset < targetArea->height; yOffset++)
	{
		for (i32 xOffset = 0; xOffset < targetArea->width; xOffset++)
		{
			v2i pixelPos = targetArea->offset + NewVec2i(xOffset, yOffset);
			if (DynBitmapGetBit(bitmap, pixelPos))
			{
				if (DynBitmapRemovePosition(bitmap, pixelPos))
				{
					result = true;
				}
			}
		}
	}
	return result;
}

bool DynBitmapSetPosition(DynBitmap_t* bitmap, v2i pos, bool value)
{
	Assert(bitmap != nullptr);
	if (value) { return DynBitmapAddPosition(bitmap, pos); }
	else { return DynBitmapRemovePosition(bitmap, pos); }
}
bool DynBitmapSetPositions(DynBitmap_t* bitmap, const v2i* positions, u32 numPositions, bool value)
{
	Assert(bitmap != nullptr);
	if (value) { return DynBitmapAddPositions(bitmap, positions, numPositions); }
	else { return DynBitmapRemovePositions(bitmap, positions, numPositions); }
}
bool DynBitmapSetReci(DynBitmap_t* bitmap, reci rectangle, bool value)
{
	Assert(bitmap != nullptr);
	if (value) { return DynBitmapAddReci(bitmap, rectangle); }
	else { return DynBitmapRemoveReci(bitmap, rectangle); }
}
bool DynBitmapSetArea(DynBitmap_t* bitmap, const DynBitmap_t* targetArea, bool value)
{
	Assert(bitmap != nullptr);
	if (value) { return DynBitmapAddArea(bitmap, targetArea); }
	else { return DynBitmapRemoveArea(bitmap, targetArea); }
}
