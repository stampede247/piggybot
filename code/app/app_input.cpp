/*
File:   app_input.cpp
Author: Taylor Robbins
Date:   02\12\2019
Description: 
	** Holds helper functions that the game can use to decipher
	** keyboard, mouse, and controller input in various ways
*/

// +--------------------------------------------------------------+
// |                Pressed, Released, Down Macros                |
// +--------------------------------------------------------------+
#define ButtonPressedNoHandling(button)                      ((appInput->buttons[button].isDown && appInput->buttons[button].transCount > 0) || appInput->buttons[button].transCount >= 2)
#define ButtonReleasedNoHandling(button)                     ((!appInput->buttons[button].isDown && appInput->buttons[button].transCount > 0) || appInput->buttons[button].transCount >= 2)
#define ButtonDownNoHandling(button)                         (appInput->buttons[button].isDown)
#define ButtonRepeatedNoHandling(button)                     (appInput->buttons[button].repeatCount > 0 || ButtonPressedNoHandling(button))
#define ButtonRepeatedEveryNoHandling(button, period, delay) (_ButtonRepeatedEvery(button, (period), (delay)) || ButtonPressedNoHandling(button))
#define ButtonPressed(button)                                (app->buttonHandled[button] == false && ButtonPressedNoHandling(button))
#define ButtonReleased(button)                               (app->buttonHandled[button] == false && ButtonReleasedNoHandling(button))
#define ButtonDown(button)                                   (app->buttonHandled[button] == false && ButtonDownNoHandling(button))
#define ButtonRepeated(button)                               (app->buttonHandled[button] == false && ButtonRepeatedNoHandling(button))
#define ButtonRepeatedEvery(button, period, delay)           (app->buttonHandled[button] == false && ButtonRepeatedEveryNoHandling(button, (period), (delay)))

#define GamepadPressedNoHandling(padIndex, button)                      ((appInput->gamepads[padIndex].buttons[button].isDown && appInput->gamepads[padIndex].buttons[button].transCount > 0) || appInput->gamepads[padIndex].buttons[button].transCount >= 2)
#define GamepadRepeatedEveryNoHandling(padIndex, button, period, delay) (_GamepadRepeatedEvery(padIndex, button, (period), (delay)) || GamepadPressedNoHandling(padIndex, button))
#define GamepadReleasedNoHandling(padIndex, button)                     ((!appInput->gamepads[padIndex].buttons[button].isDown && appInput->gamepads[padIndex].buttons[button].transCount > 0) || appInput->gamepads[padIndex].buttons[button].transCount >= 2)
#define GamepadDownNoHandling(padIndex, button)                         (appInput->gamepads[padIndex].buttons[button].isDown)
#define GamepadPressed(padIndex, button)                                (app->gamepadHandled[padIndex][button] == false && GamepadPressedNoHandling(padIndex, button))
#define GamepadRepeatedEvery(padIndex, button, period)                  (app->gamepadHandled[padIndex][button] == false && GamepadRepeatedEveryNoHandling(padIndex, button, period))
#define GamepadReleased(padIndex, button)                               (app->gamepadHandled[padIndex][button] == false && GamepadReleasedNoHandling(padIndex, button))
#define GamepadDown(padIndex, button)                                   (app->gamepadHandled[padIndex][button] == false && GamepadDownNoHandling(padIndex, button))

#define ScrollWheelMovedNoHandling()  (appInput->scrollDelta.x != 0 || appInput->scrollDelta.y != 0)
#define ScrollWheelMovedXNoHandling() (appInput->scrollDelta.x != 0)
#define ScrollWheelMovedYNoHandling() (appInput->scrollDelta.y != 0)
#define ScrollWheelMoved()  ((ScrollWheelMovedXNoHandling() && !app->scrollWheelXHandled) || (ScrollWheelMovedYNoHandling() && !app->scrollWheelYHandled))
#define ScrollWheelMovedX() (ScrollWheelMovedXNoHandling() && !app->scrollWheelXHandled)
#define ScrollWheelMovedY() (ScrollWheelMovedYNoHandling() && !app->scrollWheelYHandled)

#if 0
void ButtonHandledCallback(Buttons_t button, const char* filePath, u32 lineNumber)
{
	if (button == Button_Escape)
	{
		PrintLine_D("Escape handled by \"%s\" line %u", filePath, lineNumber);
	}
}
void ScrollHandledCallback(const char* axis, const char* filePath, u32 lineNumber)
{
	PrintLine_D("Scroll %s handled by \"%s\" line %u", axis, filePath, lineNumber);
}
#else
#define ButtonHandledCallback(button, filePath, lineNumber) //nothing
#define ScrollHandledCallback(axis, filePath, lineNumber) //nothing
#endif

#define HandleButton(button)            do { app->buttonHandled[button] = true; ButtonHandledCallback((button), __FILE__, __LINE__); } while(0)
#define HandleScrollWheel()             do { app->scrollWheelXHandled = true; app->scrollWheelYHandled = true; ScrollHandledCallback("XY", __FILE__, __LINE__); } while(0)
#define HandleScrollWheelX()            do { app->scrollWheelXHandled = true; ScrollHandledCallback("X", __FILE__, __LINE__); } while(0)
#define HandleScrollWheelY()            do { app->scrollWheelYHandled = true; ScrollHandledCallback("Y", __FILE__, __LINE__); } while(0)
#define HandleGamepad(padIndex, button) do { app->gamepadHandled[padIndex][button] = true; } while(0)

#define IsMouseInside(rectangle) (IsInsideRec(rectangle, RenderMousePos) && appInput->mouseInsideWindow)
#define DidMouseStartInside(rectangle) (IsInsideRec(rectangle, RenderMouseStartPos))
#define ClickedOnRec(rectangle) (ButtonReleased(MouseButton_Left) && IsMouseInside(rectangle) && DidMouseStartInside(rectangle))

#define IsMouseOverOther() (app->mouseOverOther)
#define MouseOverMe(regionName) do                              \
{                                                               \
	if (app->mouseOverName != nullptr)                          \
	{                                                           \
		ArenaPop(mainHeap, app->mouseOverName);                 \
		app->mouseOverName = nullptr;                           \
	}                                                           \
	app->mouseOverName = ArenaNtString(mainHeap, (regionName)); \
	app->mouseOverOther = true;                                 \
} while(0)
#define MouseOverMePrint(formatStr, ...) do                              \
{                                                                        \
	if (app->mouseOverName != nullptr)                                   \
	{                                                                    \
		ArenaPop(mainHeap, app->mouseOverName);                          \
		app->mouseOverName = nullptr;                                    \
	}                                                                    \
	app->mouseOverName = ArenaPrint(mainHeap, formatStr, ##__VA_ARGS__); \
	app->mouseOverOther = true;                                          \
} while(0)
#define IsMouseOver(regionName) (app->mouseOverOther && app->mouseOverName != nullptr && StrCompareIgnoreCaseNt(app->mouseOverName, (regionName)))
#define IsMouseOverPrint(formatStr, ...) (app->mouseOverOther && app->mouseOverName != nullptr && StrCompareIgnoreCaseNt(app->mouseOverName, TempPrint((formatStr), ##__VA_ARGS__)))
#define IsMouseOverPartial(partialRegionName) (app->mouseOverOther && app->mouseOverName != nullptr && MyStrLength32(app->mouseOverName) >= MyStrLength32(partialRegionName) && StrCompareIgnoreCase(app->mouseOverName, (partialRegionName), MyStrLength32(partialRegionName)))
#define MouseMoved() (appInput->mouseMoved)
#define MouseStartedOver(mouseButton, regionName) (app->mouseStartedOverName[(mouseButton)] != nullptr && StrCompareIgnoreCaseNt(app->mouseStartedOverName[(mouseButton)], (regionName)))
#define MouseStartedOverPrint(mouseButton, formatStr, ...) (app->mouseStartedOverName[(mouseButton)] != nullptr && StrCompareIgnoreCaseNt(app->mouseStartedOverName[(mouseButton)], TempPrint((formatStr), ##__VA_ARGS__)))

bool _ButtonRepeatedEvery(Buttons_t button, u32 period, u32 delay)
{
	Assert(button < Buttons_NumButtons);
	Assert(appInput != nullptr);
	const ButtonState_t* btnState = &appInput->buttons[button];
	if (!btnState->isDown) { return false; }
	if (btnState->lastTransTime < (r32)ElapsedMs) { return false; }
	u32 currentTime = (u32)FloorR32i(btnState->lastTransTime);
	if (currentTime < delay) { currentTime = 0; }
	else { currentTime -= delay; }
	u32 previousTime = (u32)FloorR32i(btnState->lastTransTime - (r32)ElapsedMs);
	if (previousTime < delay) { previousTime = 0; }
	else { previousTime -= delay; }
	return ((previousTime / period) != (currentTime / period) || (delay > 0 && previousTime == 0 && currentTime > 0));
}
bool _GamepadRepeatedEvery(u8 padIndex, GamepadButtons_t button, u32 period, u32 delay)
{
	Assert(padIndex < MAX_NUM_GAMEPADS);
	Assert(button < Gamepad_NumButtons);
	Assert(appInput != nullptr);
	const ButtonState_t* btnState = &appInput->gamepads[padIndex].buttons[button];
	if (!btnState->isDown) { return false; }
	u32 currentTime = (u32)FloorR32i(btnState->lastTransTime);
	if (currentTime < delay) { currentTime = 0; }
	else { currentTime -= delay; }
	u32 previousTime = (u32)FloorR32i(btnState->lastTransTime - (r32)ElapsedMs);
	if (previousTime < delay) { previousTime = 0; }
	else { previousTime -= delay; }
	return ((previousTime / period) != (currentTime / period) || (delay > 0 && previousTime == 0 && currentTime > 0));
}

bool CaptureMouse(const char* regionName)
{
	if (IsMouseOverOther()) { return false; }
	else
	{
		if (app->mouseOverName != nullptr)
		{
			ArenaPop(mainHeap, app->mouseOverName);
			app->mouseOverName = nullptr;
		}
		app->mouseOverName = ArenaNtString(mainHeap, regionName);
		app->mouseOverOther = true;
		return true;
	}
}
bool CaptureMousePrint(const char* formatStr, ...)
{
	if (IsMouseOverOther()) { return false; }
	else
	{
		if (app->mouseOverName != nullptr)
		{
			ArenaPop(mainHeap, app->mouseOverName);
			app->mouseOverName = nullptr;
		}
		ArenaPrintVa(mainHeap, formattedStr, formattedLength, formatStr);
		app->mouseOverName = formattedStr;
		app->mouseOverOther = true;
		return true;
	}
}

bool HandleMouseClickedOn(Buttons_t button, const char* regionName)
{
	Assert(regionName != nullptr);
	Assert(button == MouseButton_Left || button == MouseButton_Right || button == MouseButton_Middle);
	if (ButtonPressed(button) && IsMouseOver(regionName))
	{
		HandleButton(button);
	}
	if (ButtonDown(button) && IsMouseOver(regionName))
	{
		HandleButton(button);
	}
	if (ButtonReleased(button) && IsMouseOver(regionName) && MouseStartedOver(button, regionName))
	{
		HandleButton(button);
		return true;
	}
	return false;
}
bool IsMouseClickingOn(Buttons_t button, const char* regionName)
{
	Assert(button == MouseButton_Left || button == MouseButton_Right || button == MouseButton_Middle);
	Assert(regionName != nullptr);
	if (ButtonDownNoHandling(button) && IsMouseOver(regionName) && MouseStartedOver(button, regionName)) { return true; }
	return false;
}

void AppInputUpdate()
{
	// +==============================+
	// | Clear Button Handled Arrays  |
	// +==============================+
	app->scrollWheelXHandled = false;
	app->scrollWheelYHandled = false;
	for (u32 bIndex = 0; bIndex < ArrayCount(app->buttonHandled); bIndex++)
	{
		app->buttonHandled[bIndex] = false;
	}
	for (u32 pIndex = 0; pIndex < ArrayCount(app->gamepadHandled); pIndex++)
	{
		for (u32 bIndex = 0; bIndex < ArrayCount(app->gamepadHandled[0]); bIndex++)
		{
			app->gamepadHandled[pIndex][bIndex] = false;
		}
	}
	
	// +==============================+
	// |     Accum Button Presses     |
	// +==============================+
	for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++)
	{
		if (ButtonPressedNoHandling(bIndex))
		{
			app->buttonPressed[bIndex] = true;
		}
	}
	for (u32 pIndex = 0; pIndex < MAX_NUM_GAMEPADS; pIndex++)
	{
		if (appInput->gamepads[pIndex].isConnected)
		{
			for (u32 bIndex = 0; bIndex < Gamepad_NumButtons; bIndex++)
			{
				if (GamepadPressedNoHandling(pIndex, bIndex))
				{
					app->gamepadPressed[pIndex][bIndex] = true;
				}
			}
		}
	}
	
	// +==============================+
	// |     Update Button Order      |
	// +==============================+
	for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++)
	{
		if (ButtonPressedNoHandling(bIndex))
		{
			app->buttonOrder[bIndex] = 255;
			//Decrement all other buttons so this button is most recent (highest number)
			for (u32 bIndex2 = 0; bIndex2 < Buttons_NumButtons; bIndex2++)
			{
				if (bIndex2 != bIndex && app->buttonOrder[bIndex2] > 0) { app->buttonOrder[bIndex2]--; }
			}
		}
	}
	for (u32 pIndex = 0; pIndex < MAX_NUM_GAMEPADS; pIndex++)
	{
		if (appInput->gamepads[pIndex].isConnected)
		{
			for (u32 bIndex = 0; bIndex < Gamepad_NumButtons; bIndex++)
			{
				if (GamepadPressedNoHandling(pIndex, bIndex))
				{
					app->gamepadOrder[pIndex][bIndex] = 255;
					//Decrement all other buttons so this button is most recent (highest number)
					for (u32 bIndex2 = 0; bIndex2 < Gamepad_NumButtons; bIndex2++)
					{
						if (bIndex2 != bIndex && app->gamepadOrder[pIndex][bIndex2] > 0) { app->gamepadOrder[pIndex][bIndex2]--; }
					}
				}
			}
		}
	}
	
	// +==============================+
	// |    Update Press Modifiers    |
	// +==============================+
	for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++)
	{
		if (ButtonPressedNoHandling((Buttons_t)bIndex))
		{
			app->buttonPressModifiers[bIndex] = 0x00;
			if (ButtonDownNoHandling(Button_Control)) { app->buttonPressModifiers[bIndex] |= Modifier_Ctrl; }
			if (ButtonDownNoHandling(Button_Alt)) { app->buttonPressModifiers[bIndex] |= Modifier_Alt; }
			if (ButtonDownNoHandling(Button_Shift)) { app->buttonPressModifiers[bIndex] |= Modifier_Shift; }
		}
	}
	
	// +==============================+
	// |     Update Input Method      |
	// +==============================+
	{
		InputMethod_t oldInputMethod = app->inputMethod;
		u32 oldInputMethodIndex = app->inputMethodIndex;
		for (u32 pIndex = 0; pIndex < MAX_NUM_GAMEPADS; pIndex++)
		{
			if (appInput->gamepads[pIndex].isConnected)
			{
				InputMethod_t thisControllerInputMethod = appInput->gamepads[pIndex].isPlaystation ? InputMethod_PsController : InputMethod_XbController;
				if (!appInput->gamepads[pIndex].wasConnected)
				{
					app->inputMethod = thisControllerInputMethod;
					app->inputMethodIndex = pIndex;
				}
				for (u32 bIndex = 0; bIndex < Gamepad_NumButtons; bIndex++)
				{
					if (appInput->gamepads[pIndex].buttons[bIndex].transCount > 0)
					{
						app->inputMethod = thisControllerInputMethod;
						app->inputMethodIndex = pIndex;
						break;
					}
				}
			}
		}
		for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++)
		{
			if (appInput->buttons[bIndex].transCount > 0)
			{
				app->inputMethod = InputMethod_KeyboardMouse;
				app->inputMethodIndex = 0;
			}
		}
		// if (appInput->mouseMoved) { app->inputMethod = InputMethod_KeyboardMouse; }
		if (appInput->scrollChanged)
		{
			app->inputMethod = InputMethod_KeyboardMouse;
		}
		
		if (app->inputMethod != oldInputMethod || app->inputMethodIndex != oldInputMethodIndex)
		{
			PrintLine_D("Input Method changed to %s[%u]", GetInputMethodStr(app->inputMethod), app->inputMethodIndex);
			app->inputMethodChanged = true;
		}
		else { app->inputMethodChanged = false; }
	}
	
	// +==============================+
	// |       Clear Mouse Over       |
	// +==============================+
	app->mouseOverOther = false;
	if (app->mouseOverName != nullptr)
	{
		ArenaPop(mainHeap, app->mouseOverName);
		app->mouseOverName = nullptr;
	}
	
	if (!appInput->mouseInsideWindow)
	{
		CaptureMouse("Outside");
	}
}

void AppInputUpdateAfter()
{
	for (u32 bIndex = 0; bIndex < ArrayCount(app->mouseStartedOverName); bIndex++)
	{
		Buttons_t button = (Buttons_t)bIndex;
		if (ButtonPressedNoHandling(button))
		{
			if (app->mouseStartedOverName[bIndex] != nullptr)
			{
				ArenaPop(mainHeap, app->mouseStartedOverName[bIndex]);
				app->mouseStartedOverName[bIndex] = nullptr;
			}
			if (app->mouseOverName != nullptr)
			{
				app->mouseStartedOverName[bIndex] = ArenaNtString(mainHeap, app->mouseOverName);
			}
		}
		else if (ButtonDownNoHandling(button))
		{
			if (app->mouseStartedOverName[bIndex] != nullptr)
			{
				if (app->mouseOverName == nullptr)
				{
					ArenaPop(mainHeap, app->mouseStartedOverName[bIndex]);
					app->mouseStartedOverName[bIndex] = nullptr;
				}
				else if (!StrCompareIgnoreCaseNt(app->mouseStartedOverName[bIndex], app->mouseOverName))
				{
					ArenaPop(mainHeap, app->mouseStartedOverName[bIndex]);
					app->mouseStartedOverName[bIndex] = nullptr;
				}
			}
		}
	}
}
