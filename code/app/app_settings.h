/*
File:   app_settings.h
Author: Taylor Robbins
Date:   06\17\2019
*/

#ifndef _APP_SETTINGS_H
#define _APP_SETTINGS_H

typedef enum
{
	SettingType_None = 0x00,
	SettingType_Boolean,
	SettingType_u8,
	SettingType_u32,
	SettingType_i32,
	SettingType_r32,
	SettingType_Version,
	SettingType_NumTypes,
} SettingType_t;

typedef enum
{
	Resolution_640x480 = 0x00,
	Resolution_720x480,
	Resolution_720x576,
	Resolution_800x600,
	Resolution_1024x768,
	Resolution_1152x864,
	Resolution_1176x664,
	Resolution_1280x720,
	Resolution_1280x768,
	Resolution_1280x800,
	Resolution_1280x960,
	Resolution_1280x1024,
	Resolution_1360x768,
	Resolution_1366x768,
	Resolution_1440x900,
	Resolution_1600x900,
	Resolution_1600x1024,
	Resolution_1600x1200,
	Resolution_1680x1050,
	Resolution_1768x992,
	Resolution_1920x1080,
	Resolution_1920x1200,
	Resolution_1920x1440,
	Resolution_2048x1536,
	Resolution_2560x1440,
	Resolution_2560x1600,
	Resolution_3840x2160,
	Resolution_NumOptions,
} ResolutionOption_t;

#define NUM_APP_SETTINGS 13

//NOTE: Any new settings also have to be added to the meta-data functions in app_settings.cpp (the ones declared below)
//NOTE: Make sure to update GetNumAppSettings() in app_settings.cpp
struct AppSettings_t
{
	r32 masterVolume;
	bool masterSoundEnabled;
	r32 musicVolume;
	bool musicEnabled;
	r32 soundEffectVolume;
	bool soundEffectsEnabled;
	
	ResolutionOption_t resolution;
	bool fullscreenEnabled;
	bool autoMinimize;
	
	bool developerConsoleEnabled;
	bool gifRecordHotkeyEnabled;
	r32 screenshotCountdownTime; //percent
	bool notificationPopupsEnabled;
};

v2i GetResolutionValue(ResolutionOption_t resolution)
{
	switch (resolution)
	{
		case Resolution_640x480:   return NewVec2i(640, 480);
		case Resolution_720x480:   return NewVec2i(720, 480);
		case Resolution_720x576:   return NewVec2i(720, 576);
		case Resolution_800x600:   return NewVec2i(800, 600);
		case Resolution_1024x768:  return NewVec2i(1024, 768);
		case Resolution_1152x864:  return NewVec2i(1152, 864);
		case Resolution_1176x664:  return NewVec2i(1176, 664);
		case Resolution_1280x720:  return NewVec2i(1280, 720);
		case Resolution_1280x768:  return NewVec2i(1280, 768);
		case Resolution_1280x800:  return NewVec2i(1280, 800);
		case Resolution_1280x960:  return NewVec2i(1280, 960);
		case Resolution_1280x1024: return NewVec2i(1280, 1024);
		case Resolution_1360x768:  return NewVec2i(1360, 768);
		case Resolution_1366x768:  return NewVec2i(1366, 768);
		case Resolution_1440x900:  return NewVec2i(1440, 900);
		case Resolution_1600x900:  return NewVec2i(1600, 900);
		case Resolution_1600x1024: return NewVec2i(1600, 1024);
		case Resolution_1600x1200: return NewVec2i(1600, 1200);
		case Resolution_1680x1050: return NewVec2i(1680, 1050);
		case Resolution_1768x992:  return NewVec2i(1768, 992);
		case Resolution_1920x1080: return NewVec2i(1920, 1080);
		case Resolution_1920x1200: return NewVec2i(1920, 1200);
		case Resolution_1920x1440: return NewVec2i(1920, 1440);
		case Resolution_2048x1536: return NewVec2i(2048, 1536);
		case Resolution_2560x1440: return NewVec2i(2560, 1440);
		case Resolution_2560x1600: return NewVec2i(2560, 1600);
		case Resolution_3840x2160: return NewVec2i(3840, 2160);
		default: return Vec2i_Zero;
	}
}

u32 GetNumAppSettings();
SettingType_t GetSettingInfo(AppSettings_t* settings, u32 settIndex, const char** nameOut, void** valueOut);
SettingType_t GetSettingInfo(const AppSettings_t* settings, u32 settIndex, const char** nameOut, const void** valueOut); //const variant
u32 GetSettingIndexByName(const char* settingName, u32 nameLength);
u32 GetSettingIndexByName(const char* settingName);
u32 GetSettingIndexByPntr(const AppSettings_t* settings, const void* pntr);
const char* GetSettingName(u32 settIndex);

bool GetSettingBool(AppSettings_t* settings, u32 settIndex);
u8 GetSettingU8(AppSettings_t* settings, u32 settIndex);
u32 GetSettingU32(AppSettings_t* settings, u32 settIndex);
i32 GetSettingI32(AppSettings_t* settings, u32 settIndex);
r32 GetSettingR32(AppSettings_t* settings, u32 settIndex);
Version_t GetSettingVersion(AppSettings_t* settings, u32 settIndex);

void SetSettingBool(AppSettings_t* settings, u32 settIndex, bool newValue);
void SetSettingU8(AppSettings_t* settings, u32 settIndex, u8 newValue);
void SetSettingU32(AppSettings_t* settings, u32 settIndex, u32 newValue);
void SetSettingI32(AppSettings_t* settings, u32 settIndex, i32 newValue);
void SetSettingR32(AppSettings_t* settings, u32 settIndex, r32 newValue);
void SetSettingVersion(AppSettings_t* settings, u32 settIndex, Version_t newValue);

bool SaveAppSettingsTo(const AppSettings_t* settings, const char* filePath); //in app_settings_serialization.cpp
bool SaveAppSettings(const AppSettings_t* settings); //in app_settings_serialization.cpp
bool LoadAppSettingsFrom(AppSettings_t* settings, const char* filePath); //in app_settings_serialization.cpp
bool LoadAppSettings(AppSettings_t* settings); //in app_settings_serialization.cpp

#endif //  _APP_SETTINGS_H
