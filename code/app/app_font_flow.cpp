/*
File:   app_font_flow.cpp
Author: Taylor Robbins
Date:   08\12\2020
Description: 
	** Handles laying out text characters in various ways and doing callbacks to
	** whoever is registered to receive the layout information.
*/

// +==============================+
// | DefaultFontFlowCharCallback  |
// +==============================+
// rec DefaultFontFlowCharCallback(FontFlowState_t* flowState, bool measureOnly, u32 charIndex, char c, v2 position, const Font_t* font, r32 fontScale, r32* advanceXOut, bool* wasUnprintable, bool* wasWhitespace)
FlowCharCallback_FUNCTION(DefaultFontFlowCharCallback)
{
	Assert(flowState != nullptr);
	Assert(font != nullptr);
	Assert(fontScale > 0.0f);
	if (c == '\t')
	{
		bool isUnknownChar = false;
		u32 fontCharIndex = GetFontCharIndex(font, ' ', &isUnknownChar);
		Assert(fontCharIndex < font->numChars);
		const FontCharInfo_t* charInfo = &font->charInfos[fontCharIndex];
		if (wasUnprintable != nullptr) { *wasUnprintable = false; }
		if (wasWhitespace != nullptr) { *wasWhitespace = true; }
		if (advanceXOut != nullptr) { *advanceXOut = charInfo->advanceX * fontScale * TAB_WIDTH; }
		return NewRec(position, Vec2_Zero);
	}
	else
	{
		bool isUnknownChar = false;
		u32 fontCharIndex = GetFontCharIndex(font, c, &isUnknownChar);
		Assert(fontCharIndex < font->numChars);
		const FontCharInfo_t* charInfo = &font->charInfos[fontCharIndex];
		rec drawRectangle = NewRec(
			position.x + (fontScale * charInfo->offset.x), 
			position.y + (fontScale * charInfo->offset.y), 
			(fontScale * charInfo->width), 
			(fontScale * charInfo->height)
		);
		if (!measureOnly && flowState->renderState != nullptr)
		{
			if (IsFlagSet(flowState->renderState->flags, FfrFlag_Emboss))
			{
				RcDrawCharacterEx(font, charInfo, fontCharIndex, position + NewVec2(0, flowState->renderState->embossOffset)*fontScale, flowState->renderState->embossColor, fontScale);
			}
			RcDrawCharacterEx(font, charInfo, fontCharIndex, position, flowState->renderState->color, fontScale);
		}
		
		if (wasUnprintable != nullptr) { *wasUnprintable = isUnknownChar; }
		if (wasWhitespace != nullptr) { *wasWhitespace = (charInfo->width <= 0 && charInfo->height <= 0); }
		if (advanceXOut != nullptr) { *advanceXOut = charInfo->advanceX * fontScale; }
		return drawRectangle;
	}
}

void PerformFontFlow(FontFlowState_t* flowState)
{
	Assert(flowState != nullptr);
	Assert(flowState->textPntr != nullptr || flowState->textLength == 0);
	Assert(flowState->font != nullptr);
	Assert(flowState->fontScale > 0.0f);
	Assert(flowState->maxWidth >= 0.0f);
	Assert(flowState->charCallback != nullptr);
	
	bool lineStarted = false;
	u32 nextLineLength = 0;
	u32 endOfLineSkipCount = 0;
	for (u32 cIndex = 0; cIndex <= flowState->textLength; )
	{
		//Search for the next line break or wrap point
		if (!lineStarted || cIndex - flowState->lineStartCharIndex >= nextLineLength)
		{
			if (lineStarted)
			{
				if (flowState->afterLineCallback != nullptr)
				{
					flowState->afterLineCallback(flowState, flowState->lineIndex, flowState->lineStartCharIndex, nextLineLength + endOfLineSkipCount, flowState->lineBounds);
				}
				if (IsFlagSet(flowState->flags, FfsFlag_PrintDebug)) { PrintLine_D("Finishing line %u", flowState->lineIndex); }
				for (u32 eIndex = 0; eIndex < endOfLineSkipCount; eIndex++)
				{
					if (flowState->indexPosCallback != nullptr) { flowState->indexPosCallback(flowState, cIndex, flowState->startPos + flowState->offset); }
					cIndex++;
				}
				if (cIndex >= flowState->textLength && endOfLineSkipCount == 0) { break; } //no new-line so last charIndex goes here, not on next line
				flowState->offset.x = 0;
				flowState->offset.y += flowState->font->lineHeight * flowState->fontScale;
				flowState->lineStartCharIndex = cIndex;
				flowState->lineIndex++;
				flowState->lineBounds = Rec_Zero;
				if (cIndex >= flowState->textLength) { break; } //new-line means last charIndex should happen after the line break
			}
			if (IsFlagSet(flowState->flags, FfsFlag_PrintDebug)) { PrintLine_D("Starting search for line %u at %u \'%c\'", flowState->lineIndex, cIndex, flowState->textPntr[cIndex]); }
			flowState->lineStartCharIndex = cIndex;
			nextLineLength = 0;
			v2 nextLineOffset = Vec2_Zero;
			rec nextLineBounds = NewRec(flowState->startPos.x + flowState->offset.x, flowState->startPos.y + flowState->offset.y - flowState->font->maxExtendUp * flowState->fontScale, 0, flowState->font->lineHeight * flowState->fontScale);
			bool foundWordBoundary = false;
			u32 lastWordBoundaryLength = 0;
			v2 lastWordBoundaryOffset = Vec2_Zero;
			rec lastWordBoundaryBounds = nextLineBounds;
			for (u32 cIndex2 = cIndex; cIndex2 <= flowState->textLength; cIndex2++)
			{
				bool atEndOfText = (cIndex2 >= flowState->textLength);
				char nextChar = !atEndOfText ? flowState->textPntr[cIndex2] : '\0';
				if (atEndOfText || nextChar == '\n' || nextChar == '\r')
				{
					nextLineLength = cIndex2 - cIndex;
					endOfLineSkipCount = 0;
					if (!atEndOfText)
					{
						endOfLineSkipCount = 1; //skip over \n or \r
						if (cIndex2+1 < flowState->textLength)
						{
							char nextNextChar = flowState->textPntr[cIndex2+1];
							if ((nextNextChar == '\n' || nextNextChar == '\r') && nextNextChar != nextChar)
							{
								endOfLineSkipCount++; //skip extra \n or \r to handle "\n\r" or "\r\n"
							}
						}
					}
					if (IsFlagSet(flowState->flags, FfsFlag_PrintDebug)) { PrintLine_D("Found %s at %u", atEndOfText ? "End-of-text" : "New-line", cIndex2); }
					break;
				}
				else
				{
					r32 charAdvanceX = 0;
					bool wasUnprintable = false;
					bool wasWhitespace = false;
					rec charRec = flowState->charCallback(flowState, true, cIndex2, nextChar, flowState->startPos + flowState->offset + nextLineOffset, flowState->font, flowState->fontScale, &charAdvanceX, &wasUnprintable, &wasWhitespace);
					
					if (cIndex2 > cIndex && flowState->maxWidth > 0 && (charRec.x + charRec.width - flowState->startPos.x > flowState->maxWidth || flowState->offset.x + charAdvanceX > flowState->maxWidth))
					{
						if (IsFlagSet(flowState->flags, FfsFlag_PrintDebug)) { PrintLine_D("Char %u \'%c\' is too big for maxWidth %f", cIndex2, nextChar, flowState->maxWidth); }
						endOfLineSkipCount = 0;
						if (IsFlagSet(flowState->flags, FfsFlag_DontPreserveWords) || !foundWordBoundary)
						{
							nextLineLength = cIndex2 - cIndex;
						}
						else
						{
							nextLineLength = lastWordBoundaryLength;
							nextLineBounds = lastWordBoundaryBounds;
							nextLineOffset = lastWordBoundaryOffset;
						}
						//TODO: Consume a single space that falls on the line break?
						break;
					}
					else
					{
						if (!IsCharClassAlphaNumeric(nextChar) && IsCharClassBeginningCharacter(nextChar) && cIndex2 > cIndex)
						{
							if (IsFlagSet(flowState->flags, FfsFlag_PrintDebug)) { PrintLine_D("Char %u \'%c\' is a beginning word boundary", cIndex2, nextChar); }
							lastWordBoundaryOffset = nextLineOffset;
							lastWordBoundaryBounds = nextLineBounds;
							lastWordBoundaryLength = cIndex2 - cIndex;
							foundWordBoundary = true;
						}
						if (IsFlagSet(flowState->flags, FfsFlag_MeasureWhitespace) && wasWhitespace)
						{
							charRec.topLeft = flowState->startPos + flowState->offset + nextLineOffset;
							charRec.width = charAdvanceX;
							charRec.height = 0;
						}
						nextLineBounds = RecBoth(nextLineBounds, charRec);
						nextLineOffset.x += charAdvanceX;
						if (!IsCharClassAlphaNumeric(nextChar) && !IsCharClassBeginningCharacter(nextChar))
						{
							if (IsFlagSet(flowState->flags, FfsFlag_PrintDebug)) { PrintLine_D("Char %u \'%c\' is an ending word boundary", cIndex2, nextChar); }
							lastWordBoundaryOffset = nextLineOffset;
							lastWordBoundaryBounds = nextLineBounds;
							lastWordBoundaryLength = cIndex2+1 - cIndex;
							foundWordBoundary = true;
						}
					}
				}
			}
			if (flowState->beforeLineCallback != nullptr)
			{
				flowState->beforeLineCallback(flowState, flowState->lineIndex, cIndex, nextLineLength + endOfLineSkipCount, nextLineBounds);
			}
			lineStarted = true;
		}
		
		//Put the next character on screen
		if (cIndex < flowState->textLength && cIndex - flowState->lineStartCharIndex < nextLineLength)
		{
			char nextChar = flowState->textPntr[cIndex];
			if (flowState->beforeCharCallback != nullptr)
			{
				flowState->beforeCharCallback(flowState, cIndex, nextChar, flowState->startPos + flowState->offset);
			}
			if (flowState->indexPosCallback != nullptr) { flowState->indexPosCallback(flowState, cIndex, flowState->startPos + flowState->offset); }
			
			r32 charAdvanceX = 0;
			bool wasUnprintable = false;
			bool wasWhitespace = false;
			rec charRec = flowState->charCallback(flowState, false, cIndex, nextChar, flowState->startPos + flowState->offset, flowState->font, flowState->fontScale, &charAdvanceX, &wasUnprintable, &wasWhitespace);
			if (IsFlagSet(flowState->flags, FfsFlag_MeasureWhitespace) && wasWhitespace)
			{
				charRec.topLeft = flowState->startPos + flowState->offset;
				charRec.width = charAdvanceX;
				charRec.height = 0;
			}
			
			if (flowState->bounds.size == Vec2_Zero) { flowState->bounds = charRec; }
			else { flowState->bounds = RecBoth(flowState->bounds, charRec); }
			if (flowState->lineBounds.size == Vec2_Zero) { flowState->lineBounds = charRec; }
			else { flowState->lineBounds = RecBoth(flowState->lineBounds, charRec); }
			flowState->offset.x += charAdvanceX;
			flowState->textIndex++;
			flowState->lineCharIndex++;
			
			if (flowState->afterCharCallback != nullptr)
			{
				flowState->afterCharCallback(flowState, cIndex, nextChar, charRec, flowState->startPos + flowState->offset);
			}
			if (flowState->indexPosCallback != nullptr) { flowState->indexPosCallback(flowState, cIndex+1, flowState->startPos + flowState->offset); }
			
			cIndex++;
		}
	}
	if (flowState->indexPosCallback != nullptr)
	{
		flowState->indexPosCallback(flowState, flowState->textLength, flowState->startPos + flowState->offset);
	}
}

v2 MeasureStringFlow(const Font_t* font, const char* textPntr, u32 textLength, r32 scale = 1.0f, r32 maxWidth = 0, u32 flags = 0x00000000)
{
	Assert(font != nullptr);
	FontFlowState_t flowState = {};
	flowState.flags = flags;
	flowState.textPntr = textPntr;
	flowState.textLength = textLength;
	flowState.font = font;
	flowState.fontScale = scale;
	flowState.maxWidth = maxWidth;
	flowState.charCallback = DefaultFontFlowCharCallback;
	PerformFontFlow(&flowState);
	return flowState.bounds.size;
}
v2 MeasureStringNtFlow(const Font_t* font, const char* nullTermString, r32 scale = 1.0f, r32 maxWidth = 0, u32 flags = 0x00000000)
{
	Assert(font != nullptr);
	Assert(nullTermString != nullptr);
	return MeasureStringFlow(font, nullTermString, MyStrLength32(nullTermString), scale, maxWidth, flags);
}

struct IndexPos_t
{
	v2 position;
	u32 index;
};

struct FindStringIndexInfo_t
{
	v2 relativePos;
	bool foundResult;
	v2 resultPos;
	u32 resultIndex;
	u32 resultLine;
	r32 resultLineCenterDist;
};

// +==================================+
// | FindStringIndexIndexPosCallback  |
// +==================================+
// void FlowIndexPosCallback_FUNCTION(FontFlowState_t* flowState, u32 charIndex, v2 position)
FlowIndexPosCallback_FUNCTION(FindStringIndexIndexPosCallback)
{
	Assert(flowState != nullptr);
	Assert(flowState->userPntr != nullptr);
	FindStringIndexInfo_t* info = (FindStringIndexInfo_t*)flowState->userPntr;
	r32 lineCenterY = position.y + ((-flowState->font->maxExtendUp + flowState->font->lineHeight/2) * flowState->fontScale);
	r32 lineCenterDist = AbsR32(info->relativePos.y - lineCenterY);
	if (!info->foundResult)
	{
		// if (ButtonPressedNoHandling(Button_4)) { PrintLine_D("Found %u as first index (line %f, mouse %f)", charIndex, lineCenterY, info->relativePos.y); }
		info->resultIndex = charIndex;
		info->resultLine = flowState->lineIndex;
		info->resultLineCenterDist = lineCenterDist;
		info->resultPos = position;
		info->foundResult = true;
	}
	else if (flowState->lineIndex != info->resultLine && lineCenterDist < info->resultLineCenterDist)
	{
		// if (ButtonPressedNoHandling(Button_4)) { PrintLine_D("Found %u instead of %u line %u instead of %u (line dist %f < %f) line %f mouse %f", charIndex, info->resultIndex, flowState->lineIndex, info->resultLine, lineCenterDist, info->resultLineCenterDist, lineCenterY, info->relativePos.y); }
		info->resultIndex = charIndex;
		info->resultLine = flowState->lineIndex;
		info->resultLineCenterDist = lineCenterDist;
		info->resultPos = position;
		info->foundResult = true;
	}
	else if (flowState->lineIndex == info->resultLine)
	{
		if (Vec2Length(info->relativePos - position) < Vec2Length(info->relativePos - info->resultPos))
		{
			// if (ButtonPressedNoHandling(Button_4)) { PrintLine_D("Found %u instead of %u (dist %f)", charIndex, info->resultIndex, Vec2Length(info->relativePos - position)); }
			info->resultIndex = charIndex;
			info->resultLine = flowState->lineIndex;
			info->resultLineCenterDist = lineCenterDist;
			info->resultPos = position;
			info->foundResult = true;
		}
	}
}

/*
// +====================================+
// | FindStringIndexBeforeCharCallback  |
// +====================================+
// void FindStringIndexBeforeCharCallback(FontFlowState_t* flowState, u32 charIndex, char c, v2 position)
FlowBeforeCharCallback_FUNCTION(FindStringIndexBeforeCharCallback)
{
	Assert(flowState != nullptr);
	Assert(flowState->userPntr != nullptr);
	FindStringIndexInfo_t* info = (FindStringIndexInfo_t*)flowState->userPntr;
	r32 lineCenterY = position.y + ((-flowState->font->maxExtendUp + flowState->font->lineHeight/2) * flowState->fontScale);
	r32 lineCenterDist = AbsR32(info->relativePos.y - lineCenterY);
	if (!info->foundResult)
	{
		if (ButtonPressedNoHandling(Button_4)) { PrintLine_D("Found %u as first index", charIndex); }
		info->resultIndex = charIndex;
		info->resultLine = flowState->lineIndex;
		info->resultLineCenterDist = lineCenterDist;
		info->resultPos = position;
		info->foundResult = true;
	}
	else if (flowState->lineIndex != info->resultLine && lineCenterDist < info->resultLineCenterDist)
	{
		if (ButtonPressedNoHandling(Button_4)) { PrintLine_D("Found %u instead of %u line %u instead of %u (dist %f)", charIndex, info->resultIndex, flowState->lineIndex, info->resultLine, Vec2Length(info->relativePos - position)); }
		info->resultIndex = charIndex;
		info->resultLine = flowState->lineIndex;
		info->resultLineCenterDist = lineCenterDist;
		info->resultPos = position;
		info->foundResult = true;
	}
	else if (flowState->lineIndex == info->resultLine)
	{
		if (Vec2Length(info->relativePos - position) < Vec2Length(info->relativePos - info->resultPos))
		{
			if (ButtonPressedNoHandling(Button_4)) { PrintLine_D("Found %u instead of %u (dist %f)", charIndex, info->resultIndex, Vec2Length(info->relativePos - position)); }
			info->resultIndex = charIndex;
			info->resultLine = flowState->lineIndex;
			info->resultLineCenterDist = lineCenterDist;
			info->resultPos = position;
			info->foundResult = true;
		}
	}
}

// +==================================+
// | FindStringIndexAfterCharCallback |
// +==================================+
// void FindStringIndexAfterCharCallback(FontFlowState_t* flowState, u32 charIndex, char c, rec charRec, v2 afterPos)
FlowAfterCharCallback_FUNCTION(FindStringIndexAfterCharCallback)
{
	Assert(flowState != nullptr);
	Assert(flowState->userPntr != nullptr);
	FindStringIndexInfo_t* info = (FindStringIndexInfo_t*)flowState->userPntr;
	r32 lineCenterY = afterPos.y + ((-flowState->font->maxExtendUp + flowState->font->lineHeight/2) * flowState->fontScale);
	r32 lineCenterDist = AbsR32(info->relativePos.y - lineCenterY);
	if (!info->foundResult)
	{
		if (ButtonPressedNoHandling(Button_4)) { PrintLine_D("Found %u as first index", charIndex+1); }
		info->resultIndex = charIndex+1;
		info->resultLine = flowState->lineIndex;
		info->resultLineCenterDist = lineCenterDist;
		info->resultPos = afterPos;
		info->foundResult = true;
	}
	else if (flowState->lineIndex != info->resultLine && lineCenterDist < info->resultLineCenterDist)
	{
		if (ButtonPressedNoHandling(Button_4)) { PrintLine_D("Found %u instead of %u line %u instead of %u (dist %f)", charIndex+1, info->resultIndex, flowState->lineIndex, info->resultLine, Vec2Length(info->relativePos - afterPos)); }
		info->resultIndex = charIndex+1;
		info->resultLine = flowState->lineIndex;
		info->resultLineCenterDist = lineCenterDist;
		info->resultPos = afterPos;
		info->foundResult = true;
	}
	else if (flowState->lineIndex == info->resultLine)
	{
		if (Vec2Length(info->relativePos - afterPos) < Vec2Length(info->relativePos - info->resultPos))
		{
			if (ButtonPressedNoHandling(Button_4)) { PrintLine_D("Found %u instead of %u (dist %f)", charIndex+1, info->resultIndex, Vec2Length(info->relativePos - afterPos)); }
			info->resultIndex = charIndex+1;
			info->resultLine = flowState->lineIndex;
			info->resultLineCenterDist = lineCenterDist;
			info->resultPos = afterPos;
			info->foundResult = true;
		}
	}
}
*/

bool FindStringIndexForLocationFlow(FindStringIndexInfo_t* infoOut, const Font_t* font, const char* textPntr, u32 textLength, v2 relativePos, r32 scale = 1.0f, r32 maxWidth = 0, u32 flags = 0x00000000)
{
	Assert(infoOut != nullptr);
	Assert(font != nullptr);
	Assert(textPntr != nullptr || textLength == 0);
	Assert(scale > 0);
	
	ClearPointer(infoOut);
	infoOut->relativePos = relativePos;
	infoOut->foundResult = false;
	
	FontFlowState_t flowState = {};
	flowState.flags = flags;
	flowState.textPntr = textPntr;
	flowState.textLength = textLength;
	flowState.font = font;
	flowState.fontScale = scale;
	flowState.maxWidth = maxWidth;
	flowState.userPntr = infoOut;
	flowState.charCallback = DefaultFontFlowCharCallback;
	// flowState.beforeCharCallback = FindStringIndexBeforeCharCallback;
	// flowState.afterCharCallback = FindStringIndexAfterCharCallback;
	flowState.indexPosCallback = FindStringIndexIndexPosCallback;
	
	PerformFontFlow(&flowState);
	
	return infoOut->foundResult;
}
