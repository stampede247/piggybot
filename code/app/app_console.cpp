/*
File:   app_console.cpp
Author: Taylor Robbins
Date:   03\22\2019
Description: 
	** Holds functions that handle capturing debug output and
	** rendering it in the console window overlay
*/

void DbgConsoleInit(DbgConsole_t* console, void* fifoSpace, u32 fifoSize, void* lineBuildSpace, u32 lineBuildSize, void* printBufferSpace, u32 printBufferSize)
{
	Assert(console != nullptr);
	Assert(fifoSpace != nullptr);
	Assert(fifoSize > CSL_INPUT_BOX_MAX_CHARS+1 + sizeof(ConsoleLine_t));
	
	ClearPointer(console);
	console->isOpen = false;
	console->openLarge = false;
	console->openAnimProgress = 0.0f;
	
	InitializeConsoleArenaExternAlloc(&console->arena, fifoSpace, fifoSize);
	console->arena.lineBuildSpace = (char*)lineBuildSpace;
	console->arena.lineBuildSize = lineBuildSize;
	console->arena.printBuffer = (char*)printBufferSpace;
	console->arena.printBufferSize = printBufferSize;
	
	console->lineWrap          = CSL_DEFAULT_LINE_WRAP;
	console->showGutterNumbers = CSL_DEFAULT_SHOW_GUTTER_NUMS;
	console->showFileNames     = CSL_DEFAULT_SHOW_FILE_NAMES;
	console->showLineNumbers   = CSL_DEFAULT_SHOW_LINE_NUMBERS;
	console->showFuncNames     = CSL_DEFAULT_SHOW_FUNCTION_NAMES;
	console->scrollToEnd = true;
	console->scroll = Vec2_Zero;
	console->scrollGoto = console->scroll;
	console->contentSize = Vec2_Zero;
	console->remeasureContent = false;
	
	console->inputBoxSelected = false;
	console->fullyOpaque = false;
	
	CreateDynamicArray(&console->prevCommands, mainHeap, sizeof(char*));
	console->recallIndex = 0;
	console->topRecallCommand = nullptr;
}
void DbgConsoleInitAfterResources(DbgConsole_t* console)
{
	Assert(console != nullptr);
	console->inputBox = NewTextBox(console->inputBoxRec, "", 0, CSL_INPUT_BOX_MAX_CHARS+1, &app->platHeap, CSL_FONT);
}

v2 MeasureDbgConsoleLine(DbgConsole_t* console, ConsoleLine_t* line, r32 maxWidth)
{
	Assert(line != nullptr);
	
	v2 result = Vec2_Zero;
	RcBindFont(CSL_FONT, CSL_FONT_SIZE);
	result.height = RcLineHeight();
	
	bool addPadding = false;
	
	TempPushMark();
	line->gutterNumWidth = 0;
	if (console->showGutterNumbers)
	{
		char* gutterNumStr = TempPrint("%u", line->gutterNumber);
		Assert_(gutterNumStr != nullptr);
		v2 textSize = MeasureString(CSL_GUTTER_NUM_FONT, gutterNumStr) * CSL_GUTTER_NUM_FONT_SIZE;
		if (textSize.width < console->maxGutterNumWidth) { textSize.width = console->maxGutterNumWidth; }
		line->gutterNumWidth = textSize.width+5;
		result.width += line->gutterNumWidth;
		maxWidth -= line->gutterNumWidth;
	}
	
	const char* filePath = GetConsoleLineFilePath(line);
	line->fileNameWidth = 0;
	if (console->showFileNames && filePath != nullptr)
	{
		const char* fileName = GetFileNamePart(filePath);
		v2 textSize = MeasureString(CSL_FILE_NAME_FONT, fileName) * CSL_FILE_NAME_FONT_SIZE;
		if (textSize.width < console->maxFileNameWidth) { textSize.width = console->maxFileNameWidth; }
		line->fileNameWidth = textSize.width + (console->showLineNumbers ? 0 : 1);
		result.width += line->fileNameWidth;
		maxWidth -= line->fileNameWidth;
		addPadding = true;
	}
	
	line->lineNumWidth = 0;
	if (console->showLineNumbers)
	{
		char* lineNumStr = TempPrint(":%u", line->fileLineNumber);
		Assert_(lineNumStr != nullptr);
		v2 textSize = MeasureString(CSL_LINE_NUM_FONT, lineNumStr) * CSL_LINE_NUM_FONT_SIZE;
		if (textSize.width < console->maxLineNumWidth) { textSize.width = console->maxLineNumWidth; }
		line->lineNumWidth = textSize.width+1;
		result.width += line->lineNumWidth;
		maxWidth -= line->lineNumWidth;
		addPadding = true;
	}
	
	const char* funcName = GetConsoleLineFuncName(line);
	line->funcNameWidth = 0;
	if (console->showFuncNames && funcName != nullptr)
	{
		v2 textSize = MeasureString(CSL_FUNC_NAME_FONT, funcName) * CSL_FUNC_NAME_FONT_SIZE;
		if (textSize.width < console->maxFuncNameWidth) { textSize.width = console->maxFuncNameWidth; }
		line->funcNameWidth = textSize.width+1;
		result.width += line->funcNameWidth;
		maxWidth -= line->funcNameWidth;
		addPadding = true;
	}
	
	line->taskNumWidth = 0;
	if (console->showTaskNumbers && line->taskId != 0)
	{
		char* taskNumStr = TempPrint("[%u]", line->taskId);
		Assert_(taskNumStr != nullptr);
		v2 textSize = MeasureString(CSL_LINE_NUM_FONT, taskNumStr) * CSL_LINE_NUM_FONT_SIZE;
		line->taskNumWidth = textSize.width+1;
		result.width += line->taskNumWidth;
		maxWidth -= line->taskNumWidth;
		addPadding = true;
	}
	
	TempPopMark();
	
	if (addPadding) { result.width += 4; maxWidth -= 4; }
	line->messageOffsetX = result.width;
	
	const char* message = GetConsoleLineMessage(line);
	if (message != nullptr)
	{
		if (console->lineWrap)
		{
			v2 textSize = RcMeasureFormattedStringNt(message, maxWidth);
			result.width += textSize.width;
			if (result.height < textSize.height) { result.height = textSize.height; }
		}
		else
		{
			v2 textSize = RcMeasureStringNt(message);
			result.width += textSize.width;
			if (result.height < textSize.height) { result.height = textSize.height; }
		}
	}
	
	line->drawRec.size = result;
	return result;
}

void DbgConsoleLineRemovedCallback(const struct ConsoleArena_t* arena, const ConsoleLine_t* removedLine, void* userPntr)
{
	Assert_(arena != nullptr);
	Assert_(removedLine != nullptr);
	Assert_(userPntr != nullptr);
	DbgConsole_t* console = (DbgConsole_t*)userPntr;
	r32 removedLineHeight = removedLine->drawRec.height + CSL_LINE_SPACING;
	console->scroll.y      -= removedLineHeight;
	console->scrollGoto.y  -= removedLineHeight;
	console->contentSize.y -= removedLineHeight;
}

// +--------------------------------------------------------------+
// |                     Update Debug Console                     |
// +--------------------------------------------------------------+
void DbgConsoleRemeasureContent(DbgConsole_t* console)
{
	Assert(console != nullptr);
	
	console->contentSize = Vec2_Zero;
	console->maxGutterNumWidth = 0.0f;
	console->maxFileNameWidth = 0.0f;
	console->maxLineNumWidth = 0.0f;
	console->maxFuncNameWidth = 0.0f;
	
	ConsoleLine_t* line = console->arena.firstLine;
	while (line != nullptr)
	{
		TempPushMark();
		char* gutterNumStr = TempPrint("%u", line->gutterNumber);
		const char* filePath = GetConsoleLineFilePath(line);
		char* lineNumStr = TempPrint(":%u", line->fileLineNumber);
		const char* funcName = GetConsoleLineFuncName(line);
		if (gutterNumStr != nullptr)
		{
			v2 gutterNumSize = MeasureString(CSL_GUTTER_NUM_FONT, gutterNumStr) * CSL_GUTTER_NUM_FONT_SIZE;
			if (console->maxGutterNumWidth < gutterNumSize.width) { console->maxGutterNumWidth = gutterNumSize.width; }
		}
		if (filePath != nullptr)
		{
			const char* fileName = GetFileNamePart(filePath);
			v2 fileNameSize = MeasureString(CSL_FILE_NAME_FONT, fileName) * CSL_FILE_NAME_FONT_SIZE;
			if (console->maxFileNameWidth < fileNameSize.width) { console->maxFileNameWidth = fileNameSize.width; }
		}
		if (lineNumStr != nullptr)
		{
			v2 lineNumSize = MeasureString(CSL_LINE_NUM_FONT, lineNumStr) * CSL_LINE_NUM_FONT_SIZE;
			if (console->maxLineNumWidth < lineNumSize.width) { console->maxLineNumWidth = lineNumSize.width; }
		}
		if (funcName != nullptr)
		{
			v2 funcNameSize = MeasureString(CSL_FUNC_NAME_FONT, funcName) * CSL_FUNC_NAME_FONT_SIZE;
			if (console->maxFuncNameWidth < funcNameSize.width) { console->maxFuncNameWidth = funcNameSize.width; }
		}
		line = line->next;
		TempPopMark();
	}
	
	line = console->arena.firstLine;
	while (line != nullptr)
	{
		v2 newLineSize = MeasureDbgConsoleLine(console, line, console->viewRec.width - 2*2);
		if (console->contentSize.width < newLineSize.width) { console->contentSize.width = newLineSize.width; }
		console->contentSize.height += newLineSize.height + CSL_LINE_SPACING;
		line = line->next;
	}
}

void DbgConsoleClampScrollValues(DbgConsole_t* console)
{
	Assert(console != nullptr);
	r32 viewableWidth = console->viewRec.width - 2*2;
	r32 viewableHeight = console->viewRec.height - 2*2;
	if (console->scroll.x > console->contentSize.width - viewableWidth) { console->scroll.x = console->contentSize.width - viewableWidth; }
	if (console->scroll.x < 0) { console->scroll.x = 0; }
	if (console->scroll.y > console->contentSize.height - viewableHeight) { console->scroll.y = console->contentSize.height - viewableHeight; }
	if (console->scroll.y < 0) { console->scroll.y = 0; }
	if (console->scrollGoto.x > console->contentSize.width - viewableWidth) { console->scrollGoto.x = console->contentSize.width - viewableWidth; }
	if (console->scrollGoto.x < 0) { console->scrollGoto.x = 0; }
	if (console->scrollGoto.y > console->contentSize.height - viewableHeight) { console->scrollGoto.y = console->contentSize.height - viewableHeight; }
	if (console->scrollGoto.y < 0) { console->scrollGoto.y = 0; }
	if (console->lineWrap) { console->scroll.x = 0; console->scrollGoto.x = 0; }
}

void DbgConsoleUpdateRecs(DbgConsole_t* console)
{
	Assert(console != nullptr);
	
	console->drawRec = NewRec(0, 0, RenderScreenSize.width, RenderScreenSize.height/4);
	if (console->openLarge) { console->drawRec.height *= 3; }
	console->drawRec.y -= console->drawRec.height * (1.0f - console->openAnimProgress);
	
	RcBindFont(CSL_FONT, CSL_FONT_SIZE);
	r32 textHeight = RcLineHeight();
	console->inputBoxRec = NewRec(console->drawRec.x + 5, console->drawRec.y + console->drawRec.height - 5, console->drawRec.width - 5*2, textHeight + 5*2);
	console->inputBoxRec.y -= console->inputBoxRec.height;
	
	r32 oldViewableWidth = console->viewRec.width - 2*2;
	console->viewRec = NewRec(console->drawRec.x + 5, console->drawRec.y + 5, console->drawRec.width - 5*2 - CSL_SCROLL_BAR_WIDTH, 0);
	console->viewRec.height = console->inputBoxRec.y-5 - console->viewRec.y;
	
	console->scrollGutterRec = NewRec(console->viewRec.x + console->viewRec.width, console->viewRec.y, CSL_SCROLL_BAR_WIDTH, console->viewRec.height);
	
	DbgConsoleClampScrollValues(console);
	
	r32 viewableWidth = console->viewRec.width - 2*2;
	r32 viewableHeight = console->viewRec.height - 2*2;
	
	if (oldViewableWidth != viewableWidth) { console->remeasureContent = true; }
	if (console->remeasureContent)
	{
		DbgConsoleRemeasureContent(console);
		console->remeasureContent = false;
	}
	
	r32 viewablePercent = viewableHeight / console->contentSize.height;
	r32 scrollPercent = console->scroll.y / (console->contentSize.height - viewableHeight);
	
	if (viewablePercent < 1.0f)
	{
		console->scrollBarRec = NewRec(console->scrollGutterRec.x, console->scrollGutterRec.y, console->scrollGutterRec.width, console->scrollGutterRec.height * viewablePercent);
		if (console->scrollBarRec.height < CSL_MIN_SCROLLBAR_HEIGHT) { console->scrollBarRec.height = CSL_MIN_SCROLLBAR_HEIGHT; }
		r32 scrollableHeight = console->scrollGutterRec.height - console->scrollBarRec.height;
		console->scrollBarRec.y += scrollableHeight * scrollPercent;
	}
	else
	{
		console->viewRec.width += console->scrollGutterRec.width;
		console->scrollGutterRec.x += console->scrollGutterRec.width;
		console->scrollGutterRec.width = 0;
		console->scrollBarRec = console->scrollGutterRec;
	}
	
	if (console->inputBox.drawRec.x != console->inputBoxRec.x || console->inputBox.drawRec.y != console->inputBoxRec.y ||
		console->inputBox.drawRec.width != console->inputBoxRec.width || console->inputBox.drawRec.height != console->inputBoxRec.height)
	{
		TextBoxRellocate(&console->inputBox, console->inputBoxRec);
	}
}

void DbgConsolePushRecallCommand(DbgConsole_t* console, const char* newCommandStr, u32 newCommandLength)
{
	Assert(console != nullptr);
	Assert(newCommandStr != nullptr || newCommandLength == 0);
	
	if (console->prevCommands.length > 0)
	{
		char* prevCommand = *DynArrayGet(&console->prevCommands, char*, 0);
		if (prevCommand != nullptr && MyStrLength32(prevCommand) == newCommandLength && MyStrCompare(prevCommand, newCommandStr, newCommandLength) == 0)//it's a repeated command
		{
			if (console->topRecallCommand != nullptr)
			{
				ArenaPop(mainHeap, console->topRecallCommand);
				console->topRecallCommand = nullptr;
			}
			console->recallIndex = 0;
			return;
		}
	}
	
	//make space for a new command
	while (console->prevCommands.length >= CSL_MAX_PREV_COMMADS_RECALL)
	{
		char* lastCommandStr = *DynArrayGet(&console->prevCommands, char*, console->prevCommands.length-1);
		if (lastCommandStr != nullptr) { ArenaPop(mainHeap, lastCommandStr); }
		DynArrayRemove(&console->prevCommands, console->prevCommands.length-1);
	}
	
	char** newItemPntr = DynArrayInsert(&console->prevCommands, char*, 0);
	char* newStr = PushArray(mainHeap, char, newCommandLength+1);
	if (newCommandLength > 0) { MyMemCopy(newStr, newCommandStr, newCommandLength); }
	newStr[newCommandLength] = '\0';
	*newItemPntr = newStr;
	
	if (console->topRecallCommand != nullptr)
	{
		ArenaPop(mainHeap, console->topRecallCommand);
		console->topRecallCommand = nullptr;
	}
	console->recallIndex = 0;
}

void DbgConsoleCaptureMouse(DbgConsole_t* console)
{
	Assert(console != nullptr);
	DbgConsoleUpdateRecs(console);
	if (IsMouseInside(console->scrollBarRec))    { CaptureMouse("DebugConsoleScrollBar");    }
	if (IsMouseInside(console->scrollGutterRec)) { CaptureMouse("DebugConsoleScrollGutter"); }
	if (IsMouseInside(console->inputBoxRec))     { CaptureMouse("DebugConsoleInputBox");     }
	if (IsMouseInside(console->viewRec))         { CaptureMouse("DebugConsoleView");         }
	if (IsMouseInside(console->drawRec))         { CaptureMouse("DebugConsole");             }
}

void DbgConsoleUpdateFunctionPointers(DbgConsole_t* console)
{
	Assert(console != nullptr);
	console->arena.removeLineCallback = DbgConsoleLineRemovedCallback;
	console->arena.removeLineCallbackUserPntr = (void*)console;
}

void DbgConsoleUpdate(DbgConsole_t* console)
{
	Assert(console != nullptr);
	
	bool consumeInputBox = false;
	
	// +======================================+
	// | Process Platform Layer Debug Output  |
	// +======================================+
	#if DISPLAY_PLATFORM_LAYER_OUTPUT
	for (u32 lIndex = 0; lIndex < appInput->numDebugLines; lIndex++)
	{
		Assert(appInput->debugLines != nullptr);
		const PlatDebugLine_t* newLine = &appInput->debugLines[lIndex];
		AppDebugOutput(DbgFlag_PlatformLayer, newLine->fileNamePntr, newLine->lineNumber, newLine->functionNamePntr, newLine->level, true, newLine->linePntr);
	}
	#endif
	
	// +==============================+
	// |   Update Console Open Anim   |
	// +==============================+
	if (console->isOpen && console->openAnimProgress < 1.0f)
	{
		console->openAnimProgress += (ElapsedMs / CSL_OPEN_TIME);
		if (console->openAnimProgress >= 1.0f) { console->openAnimProgress = 1.0f; }
	}
	if (!console->isOpen && console->openAnimProgress > 0.0f)
	{
		console->openAnimProgress -= (ElapsedMs / CSL_CLOSE_TIME);
		if (console->openAnimProgress <= 0.0f) { console->openAnimProgress = 0.0f; }
	}
	
	DbgConsoleUpdateRecs(console);
	r32 viewableWidth = console->viewRec.width - 2*2;
	r32 viewableHeight = console->viewRec.height - 2*2;
	r32 maxScrollY = console->contentSize.height - viewableHeight;
	
	if (console->scrollGoto.y >= maxScrollY)
	{
		console->scrollToEnd = true;
	}
	if (console->scrollToEnd)
	{
		console->scrollGoto.y = maxScrollY;
	}
	
	// +==============================+
	// |     Handle Escape Button     |
	// +==============================+
	if (ButtonPressed(Button_Escape) && console->isOpen && console->fullyOpaque)
	{
		HandleButton(Button_Escape);
		if (console->inputBoxSelected)
		{
			console->inputBoxSelected = false;
		}
		else
		{
			console->isOpen = false;
		}
	}
	
	// +==============================+
	// | Handle Command Repeat Hotkey |
	// +==============================+
	if (ButtonPressed(Button_F6) && ButtonDownNoHandling(Button_Control))
	{
		HandleButton(Button_F6);
		if (console->prevCommands.length > 0)
		{
			char** prevCommandPntr = DynArrayGet(&console->prevCommands, char*, 0);
			Assert(prevCommandPntr != nullptr);
			char* prevCommandStr = *prevCommandPntr;
			if (prevCommandStr != nullptr) { TextBoxSet(&console->inputBox, prevCommandStr, MyStrLength32(prevCommandStr)); }
			else { TextBoxClear(&console->inputBox); }
			consumeInputBox = true;
		}
	}
	
	// +===============================+
	// | Handle Command Recall Up/Down |
	// +===============================+
	if (ButtonRepeated(Button_Up) && console->isOpen && console->inputBoxSelected)
	{
		HandleButton(Button_Up);
		if (console->recallIndex < console->prevCommands.length)
		{
			if (console->recallIndex == 0)
			{
				Assert(console->topRecallCommand == nullptr);
				console->topRecallCommand = ArenaString(mainHeap, console->inputBox.chars, console->inputBox.numChars);
			}
			
			char* prevCommand = *DynArrayGet(&console->prevCommands, char*, console->recallIndex);
			if (prevCommand != nullptr) { TextBoxSet(&console->inputBox, prevCommand, MyStrLength32(prevCommand)); }
			else { TextBoxClear(&console->inputBox); }
			
			// PrintLine_D("Recalled %u", console->recallIndex);
			console->recallIndex++;
		}
	}
	if (ButtonRepeated(Button_Down) && console->isOpen && console->inputBoxSelected)
	{
		HandleButton(Button_Down);
		if (console->recallIndex > 0)
		{
			char* nextCommand = nullptr;
			console->recallIndex--;
			if (console->recallIndex == 0)
			{
				Assert(console->topRecallCommand != nullptr);
				nextCommand = console->topRecallCommand;
			}
			else
			{
				nextCommand = *DynArrayGet(&console->prevCommands, char*, console->recallIndex-1);
			}
			
			if (nextCommand != nullptr) { TextBoxSet(&console->inputBox, nextCommand, MyStrLength32(nextCommand)); }
			else { TextBoxClear(&console->inputBox); }
			
			if (console->recallIndex == 0)
			{
				ArenaPop(mainHeap, console->topRecallCommand);
				console->topRecallCommand = nullptr;
			}
			
			// PrintLine_D("Recalled %u", console->recallIndex);
		}
	}
	
	// +==============================+
	// |     Handle Enter Button      |
	// +==============================+
	if (ButtonPressed(Button_Enter) && console->isOpen && console->inputBoxSelected)
	{
		HandleButton(Button_Enter);
		consumeInputBox = true;
	}
	
	// +==============================+
	// |       Update Input Box       |
	// +==============================+
	if (console->isOpen)
	{
		TextBoxUpdate(&console->inputBox, console->inputBoxSelected, false);
	}
	
	// +==============================+
	// | Determine Mouse Hover Index  |
	// +==============================+
	console->mouseHoverLineIndex = -1;
	console->mouseHoverTextOffset = 0;
	bool mouseStartedInsideHover = false;
	rec viewableRec = RecDeflate(console->viewRec, 2);
	if (appInput->mouseInsideWindow && console->isOpen && console->fullyOpaque && IsInsideRec(viewableRec, RenderMousePos))
	{
		i32 lineIndex = 0;
		ConsoleLine_t* line = console->arena.firstLine;
		while (line != nullptr)
		{
			rec lineRec = line->drawRec;
			lineRec.width = viewableRec.x + viewableRec.width - lineRec.x;
			if (RecIntersects(lineRec, viewableRec))
			{
				rec backgroundRec = line->drawRec;
				backgroundRec.x += line->gutterNumWidth + line->fileNameWidth + line->lineNumWidth + line->funcNameWidth;
				backgroundRec.width = viewableRec.x + viewableRec.width - backgroundRec.x;
				
				rec gutterNumRec = lineRec;
				gutterNumRec.width = line->gutterNumWidth;
				rec fileNameRec = lineRec;
				fileNameRec.x += line->gutterNumWidth;
				fileNameRec.width = line->fileNameWidth;
				rec lineNumRec = lineRec;
				lineNumRec.x += line->gutterNumWidth;
				lineNumRec.x += line->fileNameWidth;
				lineNumRec.width = line->lineNumWidth;
				rec funcNameRec = lineRec;
				funcNameRec.x += line->gutterNumWidth;
				funcNameRec.x += line->fileNameWidth;
				funcNameRec.x += line->lineNumWidth;
				funcNameRec.width = line->funcNameWidth;
				
				if (IsInsideRec(lineRec, RenderMousePos))
				{
					console->mouseHoverLineIndex = lineIndex;
					console->mouseHoverTextOffset = 0;
					if (console->showGutterNumbers && IsInsideRec(gutterNumRec, RenderMousePos))
					{
						console->mouseHoverTextOffset = -4;
						// appOutput->cursorType = Cursor_Pointer;
						mouseStartedInsideHover = IsInsideRec(gutterNumRec, RenderMouseStartPos);
					}
					else if (console->showFileNames && IsInsideRec(fileNameRec, RenderMousePos))
					{
						console->mouseHoverTextOffset = -3;
						// appOutput->cursorType = Cursor_Pointer;
						mouseStartedInsideHover = IsInsideRec(fileNameRec, RenderMouseStartPos);
					}
					else if (console->showLineNumbers && IsInsideRec(lineNumRec, RenderMousePos))
					{
						console->mouseHoverTextOffset = -2;
						appOutput->cursorType = Cursor_Pointer;
						mouseStartedInsideHover = IsInsideRec(lineNumRec, RenderMouseStartPos);
					}
					else if (console->showFuncNames && IsInsideRec(funcNameRec, RenderMousePos))
					{
						console->mouseHoverTextOffset = -1;
						// appOutput->cursorType = Cursor_Pointer;
						mouseStartedInsideHover = IsInsideRec(funcNameRec, RenderMouseStartPos);
					}
					else
					{
						appOutput->cursorType = Cursor_Text;
						mouseStartedInsideHover = IsInsideRec(lineRec, RenderMouseStartPos);
						//TODO: Determine the text offset using font format helper functions
					}
					break;
				}
				
			}
			line = line->next;
			lineIndex++;
		}
	}
	
	ConsoleLine_t* hoverLine = (console->mouseHoverLineIndex >= 0) ? GetConsoleLine(&console->arena, (u32)console->mouseHoverLineIndex) : nullptr;
	
	// +==============================+
	// |       Show Source File       |
	// +==============================+
	if (ButtonReleased(MouseButton_Left) && hoverLine != nullptr && console->mouseHoverTextOffset == -2 && mouseStartedInsideHover)
	{
		HandleButton(MouseButton_Left);
		const char* filePath = GetConsoleLineFilePath(hoverLine);
		u32 fileLineNumber = hoverLine->fileLineNumber;
		
		if (platform->ShowSourceFile(filePath, fileLineNumber))
		{
			// PrintLine_I("Showed %s:%u in Sublime Text", filePath, fileLineNumber);
		}
		else
		{
			NotifyPrint_W("Couldn't show %s:%u in Sublime Text", filePath, fileLineNumber);
		}
	}
	
	// +==============================+
	// |      Mouse Interactions      |
	// +==============================+
	if (/*!mouseOverOther &&*/ IsInsideRec(console->drawRec, RenderMousePos) && appInput->mouseInsideWindow && console->isOpen)
	{
		// mouseOverOther = true; //TODO: Make this work with the rest of the application
		
		// +==============================+
		// |     Scroll Wheel Scroll      |
		// +==============================+
		if (console->fullyOpaque && ScrollWheelMovedY() && IsInsideRec(console->viewRec, RenderMousePos))
		{
			HandleScrollWheelY();
			console->scrollGoto.y += -appInput->scrollDelta.y * 16*4; //TODO: Make this a define
			if (console->scrollGoto.y < maxScrollY)
			{
				console->scrollToEnd = false;
			}
		}
		
		
		// +==============================+
		// |      inputBox Selection      |
		// +==============================+
		if (!console->inputBoxSelected && IsInsideRec(console->inputBoxRec, RenderMouseStartPos))
		{
			if (ButtonPressed(MouseButton_Left))
			{
				HandleButton(MouseButton_Left);
			}
			if (ButtonReleased(MouseButton_Left) && IsInsideRec(console->inputBoxRec, RenderMousePos))
			{
				HandleButton(MouseButton_Left);
				console->inputBox.cursorBegin = 0;
				console->inputBox.cursorEnd = console->inputBox.numChars;
				console->inputBoxSelected = true;
				console->fullyOpaque = true;
			}
		}
	}
	
	// +====================================+
	// | Handle Click to Deselect inputBox  |
	// +====================================+
	if (/*!mouseOverOther &&*/ console->inputBoxSelected && appInput->mouseInsideWindow && console->isOpen && !IsInsideRec(console->inputBoxRec, RenderMousePos))
	{
		// mouseOverOther = true; //TODO: Make this work with the rest of the application
		
		if (ButtonPressed(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
		}
		if (ButtonDown(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
		}
		if (ButtonReleased(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			console->inputBoxSelected = false;
			if (!IsInsideRec(console->drawRec, RenderMousePos))
			{
				console->fullyOpaque = false;
			}
		}
	}
	
	// +==============================+
	// |    Handle Console Clicks     |
	// +==============================+
	if (/*!mouseOverOther &&*/ console->fullyOpaque && appInput->mouseInsideWindow && console->isOpen)
	{
		if (ButtonPressed(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
		}
		if (ButtonDown(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
		}
		if (ButtonReleased(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			if (!IsInsideRec(console->drawRec, RenderMousePos))
			{
				console->fullyOpaque = false;
			}
		}
	}
	
	
	// +==============================+
	// |      Consume Input Box       |
	// +==============================+
	if (consumeInputBox)
	{
		TempPushMark();
		DbgConsolePushRecallCommand(console, console->inputBox.chars, console->inputBox.numChars);
		char* commandStr = ArenaString(TempArena, console->inputBox.chars, console->inputBox.numChars);
		
		PrintLine_Rx(DbgFlag_UserInput, ">> %s", commandStr);
		HandleConsoleCommand(commandStr);
		if (app->lastDebugCommand != nullptr) { ArenaPop(mainHeap, app->lastDebugCommand); }
		app->lastDebugCommand = ArenaNtString(mainHeap, commandStr);
		TempPopMark();
		TextBoxSet(&console->inputBox, "", 0);
		
		#if 0
		ConsoleLine_t* line = console->lastLine;
		if (line != nullptr)
		{
			TempPushMark();
			const char* filePathPntr = GetConsoleLineFilePath(line);
			char* filePath = ArenaString(TempArena, filePathPntr, line->filePathLength);
			u32 filePathLength = line->filePathLength;
			const char* funcNamePntr = GetConsoleLineFuncName(line);
			char* funcName = ArenaString(TempArena, funcNamePntr, line->funcNameLength);
			u32 funcNameLength = line->funcNameLength;
			const char* messagePntr = GetConsoleLineMessage(line);
			char* message = ArenaString(TempArena, messagePntr, line->messageLength);
			u32 messageLength = line->messageLength;
			u64 time = line->time;
			DbgLevel_t dbgLevel = line->dbgLevel;
			u32 fileLineNumber = line->fileLineNumber;
			
			PrintLine_R("%s %lu [%u][%u][%u]", GetDbgLevelStr(dbgLevel), time, filePathLength, funcNameLength, messageLength);
			PrintLine_R(" File: \"%s\"", filePath);
			PrintLine_R(" Function: \"%s\" line %u", funcName, fileLineNumber);
			PrintLine_R(" Message: \"%s\"", message);
			
			TempPopMark();
		}
		#endif
	}
	
	// +==============================+
	// |    Update Display Scroll     |
	// +==============================+
	{
		DbgConsoleClampScrollValues(console);
		
		if (AbsR32(console->scrollGoto.x - console->scroll.x) > 1.0f)
		{
			console->scroll.x += (console->scrollGoto.x - console->scroll.x) / 3; //TODO: Make this a define
		}
		else { console->scroll.x = console->scrollGoto.x; }
		
		if (AbsR32(console->scrollGoto.y - console->scroll.y) > 1.0f)
		{
			console->scroll.y += (console->scrollGoto.y - console->scroll.y) / 3; //TODO: Make this a define
		}
		else { console->scroll.y = console->scrollGoto.y; }
	}
	
}

Color_t GetConsoleLineColor(const ConsoleLine_t* line)
{
	Color_t result = MonokaiWhite;
	if (line->dbgLevel == DbgLevel_Debug)   { result = MonokaiGray1;     }
	if (line->dbgLevel == DbgLevel_Regular) { result = MonokaiWhite;     }
	if (line->dbgLevel == DbgLevel_Info)    { result = MonokaiGreen;     }
	if (line->dbgLevel == DbgLevel_Notify)  { result = MonokaiPurple;    }
	if (line->dbgLevel == DbgLevel_Other)   { result = MonokaiLightBlue; }
	if (line->dbgLevel == DbgLevel_Warning) { result = MonokaiOrange;    }
	if (line->dbgLevel == DbgLevel_Error)   { result = MonokaiMagenta;   }
	return result;
}

Color_t GetConsoleLineFunctionColor(const ConsoleLine_t* line)
{
	Color_t result = MonokaiWhite;
	const char* filePath = GetConsoleLineFilePath(line);
	if (filePath != nullptr)
	{
		const char* fileName = GetFileNamePart(filePath);
		u32 fileNameLength = MyStrLength32(fileName);
		if (fileNameLength >= 3  && MyStrCompare(fileName, "app", 3)            == 0) { result = MonokaiBlue;      }
		if (fileNameLength >= 4  && MyStrCompare(fileName, "game", 4)           == 0) { result = MonokaiGreen;     }
		if (fileNameLength >= 6  && MyStrCompare(fileName, "editor", 6)         == 0) { result = MonokaiPurple;    }
		if (fileNameLength >= 5  && MyStrCompare(fileName, "multi", 5)          == 0) { result = MonokaiMagenta;   }
		if (fileNameLength >= 9  && MyStrCompare(fileName, "main_menu", 9)      == 0) { result = MonokaiOrange;    }
		if (fileNameLength >= 8  && MyStrCompare(fileName, "progress", 8)       == 0) { result = MonokaiYellow;    }
		if (fileNameLength >= 13 && MyStrCompare(fileName, "settings_menu", 13) == 0) { result = MonokaiLightBlue; }
		if (fileNameLength >= 12 && MyStrCompare(fileName, "dungeon_menu", 12)  == 0) { result = MonokaiDarkGreen; }
		if (fileNameLength >= 5  && MyStrCompare(fileName, "win32", 5)          == 0) { result = MonokaiBrown;     }
		if (fileNameLength >= 2  && MyStrCompare(fileName, "my", 2)             == 0) { result = MonokaiGray1;     }
	}
	return result;
}

void DbgConsoleRender(DbgConsole_t* console)
{
	Assert(console != nullptr);
	
	if (console->openAnimProgress > 0.0f)
	{
		r32 consoleAlpha = 0.25f;
		// if (IsInsideRec(console->drawRec, RenderMousePos) && appInput->mouseInsideWindow)
		if (console->fullyOpaque)
		{
			consoleAlpha = 1.0f;
		}
		
		RcDrawButton(console->drawRec, ColorTransparent(MonokaiGray2, consoleAlpha), MonokaiWhite, 1);
		
		// RcDrawRectangle(console->inputBoxRec, ColorTransparent(MonokaiWhite, consoleAlpha));
		TextBoxRender(&console->inputBox, console->inputBoxSelected);
		
		RcDrawRectangle(console->viewRec, ColorTransparent(MonokaiDarkGray, consoleAlpha));
		
		if (console->showGutterNumbers)
		{
			Color_t gutterColor = ColorTransparent(MonokaiGray1, consoleAlpha);
			rec gutterRec = NewRec(console->viewRec.x, console->viewRec.y, 2+console->maxGutterNumWidth, console->viewRec.height);
			RcDrawRectangle(gutterRec, gutterColor);
		}
		
		// +==============================+
		// |          Draw Lines          |
		// +==============================+
		RcBindFont(CSL_FONT, CSL_FONT_SIZE);
		RcStartFontBatch(&CSL_FONT->bitmap, mainHeap);
		RcStartFontBatch(&CSL_LINE_NUM_FONT->bitmap, mainHeap);
		rec oldViewport = rc->viewport;
		RcSetViewport(RecDeflate(console->viewRec, 2));
		r32 maxWidth = console->viewRec.width - 2*2;
		v2 textPos = console->viewRec.topLeft + NewVec2(2, 2 + RcMaxExtendUp()) - console->scroll;
		textPos = Vec2Round(textPos);
		// r32 endDrawY = console->viewRec.y + console->viewRec.height + RcMaxExtendUp();
		r32 sameFileNameBegin = textPos.y - RcMaxExtendUp();
		r32 sameLineNumBegin = sameFileNameBegin;
		r32 sameFuncNameBegin = sameFileNameBegin;
		r32 sameTaskNumBegin = sameFileNameBegin;
		
		ConsoleLine_t* line = console->arena.firstLine;
		u32 lIndex = 0;
		while (line != nullptr)
		{
			TempPushMark();
			
			line->drawRec.topLeft = textPos + NewVec2(0, -RcMaxExtendUp());
			
			const char* messageStr = GetConsoleLineMessage(line);
			
			char* gutterNumStr = TempPrint("%u", line->gutterNumber);
			
			const char* filePath = GetConsoleLineFilePath(line);
			const char* nextFilePath = nullptr;
			if (line->next != nullptr) { nextFilePath = GetConsoleLineFilePath(line->next); }
			bool nextFileIsSame = (filePath != nullptr && nextFilePath != nullptr && MyStrCompareNt(filePath, nextFilePath) == 0);
			
			char* lineNumStr = TempPrint(":%u", line->fileLineNumber);
			bool nextLineNumIsSame = (line->next != nullptr && line->fileLineNumber == line->next->fileLineNumber);
			
			char* taskNumStr = TempPrint("[%u]", line->taskId);
			bool nextTaskNumIsSame = (line->next != nullptr && line->taskId == line->next->taskId);
			
			const char* funcName = GetConsoleLineFuncName(line);
			const char* nextFuncName = nullptr;
			if (line->next != nullptr) { nextFuncName = GetConsoleLineFuncName(line->next); }
			bool nextFuncIsSame = (funcName != nullptr && nextFuncName != nullptr && MyStrCompareNt(funcName, nextFuncName) == 0);
			
			Color_t fileNameBackColor = ColorTransparent(GetConsoleLineFunctionColor(line), consoleAlpha);
			Color_t fileNameColor = MonokaiDarkGray;
			Color_t textColor = GetConsoleLineColor(line);
			v2 messagePos = textPos;
			r32 messageMaxWidth = maxWidth;
			bool addPadding = false;
			bool drawBackground = false;
			Color_t backgroundColor = MonokaiDarkGray;
			rec backgroundRec = line->drawRec;
			backgroundRec.x += line->gutterNumWidth + line->fileNameWidth + line->lineNumWidth + line->funcNameWidth;
			backgroundRec.width = rc->viewport.x + rc->viewport.width - backgroundRec.x;
			bool isHoverLine = (console->mouseHoverLineIndex >= 0 && lIndex == (u32)console->mouseHoverLineIndex);
			if (IsFlagSet(line->flags, DbgFlag_Inverted))
			{
				drawBackground = true;
				backgroundColor = textColor;
				textColor = MonokaiDarkGray;
				if (isHoverLine && console->mouseHoverTextOffset >= 0) { backgroundColor = ColorDarken(backgroundColor, 16); }
			}
			else if (isHoverLine && console->mouseHoverTextOffset >= 0)
			{
				drawBackground = true;
				backgroundColor = Black;
			}
			
			// RcDrawRectangle(line->drawRec, (lIndex%2) ? VisRed : VisPurple);
			// +==============================+
			// |     Draw Line Background     |
			// +==============================+
			if (drawBackground)
			{
				RcDrawRectangle(backgroundRec, ColorTransparent(backgroundColor, consoleAlpha));
			}
			
			// +==============================+
			// |     Draw UserInput Back      |
			// +==============================+
			if (IsFlagSet(line->flags, DbgFlag_UserInput))
			{
				rec feedRec = backgroundRec;
				// feedRec.x = line->drawRec.x + line->messageOffsetX;
				feedRec.width = line->drawRec.x + line->messageOffsetX + RcMeasureStringNt(">>").width + 2 - backgroundRec.x;
				RcDrawRectangle(feedRec, ColorTransparent(ColorComplimentary(textColor), consoleAlpha));
				// RcDrawRectangle(NewRec(backgroundRec.x, backgroundRec.y, backgroundRec.width, 1), ColorTransparent(textColor, consoleAlpha));
				// RcDrawRectangle(NewRec(backgroundRec.x, backgroundRec.y + backgroundRec.height-1, backgroundRec.width, 1), ColorTransparent(textColor, consoleAlpha));
			}
			
			// +==============================+
			// |     Draw Gutter Numbers      |
			// +==============================+
			RcBindFont(CSL_GUTTER_NUM_FONT, CSL_GUTTER_NUM_FONT_SIZE);
			if (console->showGutterNumbers)
			{
				v2 gutterNumStrSize = RcMeasureStringNt(gutterNumStr);
				rec gutterNumRec = NewRec(messagePos.x, line->drawRec.y, console->maxGutterNumWidth, line->drawRec.height);
				if (RecIntersects(gutterNumRec, console->viewRec))
				{
					rec viewport = rc->viewport;
					rec usableRec = RecOverlap(gutterNumRec, viewport);
					
					v2 gutterNumStrPos = NewVec2(gutterNumRec.x + gutterNumRec.width/2 - gutterNumStrSize.width/2, gutterNumRec.y + gutterNumRec.height/2 + RcCenterOffset());
					gutterNumStrPos = Vec2Round(gutterNumStrPos);
					
					Color_t numColor = MonokaiDarkGray;
					RcSetViewport(usableRec);
					RcDrawStringNt(gutterNumStr, gutterNumStrPos, numColor);
					RcSetViewport(viewport);
				}
				messagePos.x += console->maxGutterNumWidth+5;
				messageMaxWidth -= console->maxGutterNumWidth+5;
			}
			
			// +==============================+
			// |       Draw File Names        |
			// +==============================+
			RcBindFont(CSL_FILE_NAME_FONT, CSL_FILE_NAME_FONT_SIZE);
			if (filePath != nullptr && console->showFileNames)
			{
				const char* fileName = GetFileNamePart(filePath);
				v2 fileNameSize = RcMeasureStringNt(fileName);
				if (!nextFileIsSame)
				{
					rec fileNameRec = NewRec(messagePos.x, sameFileNameBegin, console->maxFileNameWidth, line->drawRec.y + line->drawRec.height - sameFileNameBegin);
					if (RecIntersects(fileNameRec, console->viewRec))
					{
						rec viewport = rc->viewport;
						rec usableRec = RecOverlap(fileNameRec, viewport);
						
						v2 functionNamePos = NewVec2(fileNameRec.x + fileNameRec.width/2 - fileNameSize.width/2, fileNameRec.y + fileNameRec.height/2 + RcCenterOffset());
						if (functionNamePos.y > usableRec.y + usableRec.height - 2 - RcMaxExtendDown()) { functionNamePos.y = usableRec.y + usableRec.height - 2 - RcMaxExtendUp(); }
						if (functionNamePos.y < usableRec.y + 2 + RcMaxExtendUp()) { functionNamePos.y = usableRec.y + 2 + RcMaxExtendUp(); }
						if (functionNamePos.y < fileNameRec.y + RcMaxExtendUp()) { functionNamePos.y = fileNameRec.y + RcMaxExtendUp(); }
						if (functionNamePos.y > fileNameRec.y + fileNameRec.height - RcMaxExtendDown()) { functionNamePos.y = fileNameRec.y + fileNameRec.height - RcMaxExtendUp(); }
						functionNamePos = Vec2Round(functionNamePos);
						
						Color_t backColor = fileNameBackColor;
						if (console->fullyOpaque && console->showLineNumbers)
						{
							rec lineNumsRec = usableRec;
							lineNumsRec.x = messagePos.x + line->fileNameWidth;
							lineNumsRec.width = line->lineNumWidth;
							if (IsInsideRec(lineNumsRec, RenderMousePos))
							{
								backColor = ColorDarken(backColor, 16);
							}
						}
						RcDrawRectangle(fileNameRec, backColor);
						RcSetViewport(usableRec);
						RcDrawStringNt(fileName, functionNamePos, fileNameColor);
						RcSetViewport(viewport);
					}
					sameFileNameBegin = line->drawRec.y + line->drawRec.height + CSL_LINE_SPACING;
				}
				messagePos.x += console->maxFileNameWidth + (console->showLineNumbers ? 0 : 1);
				messageMaxWidth -= console->maxFileNameWidth + (console->showLineNumbers ? 0 : 1);
				addPadding = true;
			}
			else
			{
				sameFileNameBegin = line->drawRec.y + line->drawRec.height + CSL_LINE_SPACING;
			}
			
			// +==============================+
			// |    Draw File Line Numbers    |
			// +==============================+
			RcBindFont(CSL_LINE_NUM_FONT, CSL_LINE_NUM_FONT_SIZE);
			if (console->showLineNumbers)
			{
				v2 lineNumStrSize = RcMeasureStringNt(lineNumStr);
				if (!nextLineNumIsSame || !nextFileIsSame)
				{
					rec lineNumRec = NewRec(messagePos.x, sameLineNumBegin, console->maxLineNumWidth, line->drawRec.y + line->drawRec.height - sameLineNumBegin);
					if (RecIntersects(lineNumRec, console->viewRec))
					{
						rec viewport = rc->viewport;
						rec usableRec = RecOverlap(lineNumRec, viewport);
						
						v2 lineNumStrPos = NewVec2(lineNumRec.x + lineNumRec.width/2 - lineNumStrSize.width/2, lineNumRec.y + lineNumRec.height/2 + RcCenterOffset());
						if (lineNumStrPos.y > usableRec.y + usableRec.height - 2 - RcMaxExtendDown()) { lineNumStrPos.y = usableRec.y + usableRec.height - 2 - RcMaxExtendDown(); }
						if (lineNumStrPos.y < usableRec.y + 2 + RcMaxExtendUp()) { lineNumStrPos.y = usableRec.y + 2 + RcMaxExtendUp(); }
						if (lineNumStrPos.y < lineNumRec.y + RcMaxExtendUp()) { lineNumStrPos.y = lineNumRec.y + RcMaxExtendUp(); }
						if (lineNumStrPos.y > lineNumRec.y + lineNumRec.height - RcMaxExtendDown()) { lineNumStrPos.y = lineNumRec.y + lineNumRec.height - RcMaxExtendDown(); }
						lineNumStrPos = Vec2Round(lineNumStrPos);
						
						Color_t backColor = fileNameBackColor;
						if (console->fullyOpaque && IsInsideRec(usableRec, RenderMousePos)) { backColor = ColorDarken(backColor, 16); }
						RcDrawRectangle(lineNumRec, backColor);
						RcSetViewport(usableRec);
						RcDrawStringNt(lineNumStr, lineNumStrPos, fileNameColor);
						RcSetViewport(viewport);
					}
					sameLineNumBegin = line->drawRec.y + line->drawRec.height + CSL_LINE_SPACING;
				}
				messagePos.x += console->maxLineNumWidth+1;
				messageMaxWidth -= console->maxLineNumWidth+1;
				addPadding = true;
			}
			else
			{
				sameLineNumBegin = line->drawRec.y + line->drawRec.height + CSL_LINE_SPACING;
			}
			
			// +==============================+
			// |     Draw Function Names      |
			// +==============================+
			RcBindFont(CSL_FUNC_NAME_FONT, CSL_FUNC_NAME_FONT_SIZE);
			if (funcName != nullptr && console->showFuncNames)
			{
				v2 funcNameSize = RcMeasureStringNt(funcName);
				if (!nextFuncIsSame || !nextFileIsSame)
				{
					rec funcNameRec = NewRec(messagePos.x, sameFuncNameBegin, console->maxFuncNameWidth, line->drawRec.y + line->drawRec.height - sameFuncNameBegin);
					if (RecIntersects(funcNameRec, console->viewRec))
					{
						rec viewport = rc->viewport;
						rec usableRec = RecOverlap(funcNameRec, viewport);
						
						v2 functionNamePos = NewVec2(funcNameRec.x + funcNameRec.width/2 - funcNameSize.width/2, funcNameRec.y + funcNameRec.height/2 + RcCenterOffset());
						if (functionNamePos.y > usableRec.y + usableRec.height - 2 - RcMaxExtendDown()) { functionNamePos.y = usableRec.y + usableRec.height - 2 - RcMaxExtendDown(); }
						if (functionNamePos.y < usableRec.y + 2 + RcMaxExtendUp()) { functionNamePos.y = usableRec.y + 2 + RcMaxExtendUp(); }
						if (functionNamePos.y < funcNameRec.y + RcMaxExtendUp()) { functionNamePos.y = funcNameRec.y + RcMaxExtendUp(); }
						if (functionNamePos.y > funcNameRec.y + funcNameRec.height - RcMaxExtendDown()) { functionNamePos.y = funcNameRec.y + funcNameRec.height - RcMaxExtendDown(); }
						functionNamePos = Vec2Round(functionNamePos);
						
						RcDrawRectangle(funcNameRec, fileNameBackColor);
						RcSetViewport(usableRec);
						RcDrawStringNt(funcName, functionNamePos, fileNameColor);
						RcSetViewport(viewport);
					}
					sameFuncNameBegin = line->drawRec.y + line->drawRec.height + CSL_LINE_SPACING;
				}
				messagePos.x += console->maxFuncNameWidth+1;
				messageMaxWidth -= console->maxFuncNameWidth+1;
				addPadding = true;
			}
			else
			{
				sameFuncNameBegin = line->drawRec.y + line->drawRec.height + CSL_LINE_SPACING;
			}
			
			// +==============================+
			// |      Draw Task Numbers       |
			// +==============================+
			RcBindFont(CSL_LINE_NUM_FONT, CSL_LINE_NUM_FONT_SIZE);
			if (console->showTaskNumbers && line->taskId != 0)
			{
				v2 taskNumStrSize = RcMeasureStringNt(taskNumStr);
				if (!nextTaskNumIsSame)
				{
					rec taskNumRec = NewRec(messagePos.x, sameTaskNumBegin, taskNumStrSize.width, line->drawRec.y + line->drawRec.height - sameTaskNumBegin);
					if (RecIntersects(taskNumRec, console->viewRec))
					{
						rec viewport = rc->viewport;
						rec usableRec = RecOverlap(taskNumRec, viewport);
						
						v2 taskNumStrPos = NewVec2(taskNumRec.x + taskNumRec.width/2 - taskNumStrSize.width/2, taskNumRec.y + taskNumRec.height/2 + RcCenterOffset());
						if (taskNumStrPos.y > usableRec.y + usableRec.height - 2 - RcMaxExtendDown()) { taskNumStrPos.y = usableRec.y + usableRec.height - 2 - RcMaxExtendDown(); }
						if (taskNumStrPos.y < usableRec.y + 2 + RcMaxExtendUp()) { taskNumStrPos.y = usableRec.y + 2 + RcMaxExtendUp(); }
						if (taskNumStrPos.y < taskNumRec.y + RcMaxExtendUp()) { taskNumStrPos.y = taskNumRec.y + RcMaxExtendUp(); }
						if (taskNumStrPos.y > taskNumRec.y + taskNumRec.height - RcMaxExtendDown()) { taskNumStrPos.y = taskNumRec.y + taskNumRec.height - RcMaxExtendDown(); }
						taskNumStrPos = Vec2Round(taskNumStrPos);
						
						Color_t backColor = fileNameBackColor;
						// if (console->fullyOpaque && IsInsideRec(usableRec, RenderMousePos)) { backColor = ColorDarken(backColor, 16); }
						RcDrawRectangle(taskNumRec, backColor);
						RcSetViewport(usableRec);
						RcDrawStringNt(taskNumStr, taskNumStrPos, fileNameColor);
						RcSetViewport(viewport);
					}
					sameTaskNumBegin = line->drawRec.y + line->drawRec.height + CSL_LINE_SPACING;
				}
				messagePos.x += taskNumStrSize.width+1;
				messageMaxWidth -= taskNumStrSize.width+1;
				addPadding = true;
			}
			else
			{
				sameTaskNumBegin = line->drawRec.y + line->drawRec.height + CSL_LINE_SPACING;
			}
			
			if (addPadding) { messagePos.x += 4; messageMaxWidth -= 4; }
			
			// +==============================+
			// |      Draw Line Message       |
			// +==============================+
			if (messageStr != nullptr && RecIntersects(line->drawRec, console->viewRec))
			{
				Assert(messageStr[line->messageLength] == '\0');
				messagePos = Vec2Round(messagePos);
				RcBindFont(CSL_FONT, CSL_FONT_SIZE);
				if (console->lineWrap)
				{
					RcDrawFormattedString(messageStr, messagePos, messageMaxWidth, textColor);
				}
				else
				{
					RcDrawStringNt(messageStr, messagePos, textColor);
				}
			}
			
			textPos.y += line->drawRec.height + CSL_LINE_SPACING;
			line = line->next;
			lIndex++;
			TempPopMark();
		}
		RcFinishFontBatches(GetShader(fontShader));
		RcSetViewport(oldViewport);
		
		RcDrawRectangle(console->scrollGutterRec, ColorTransparent(MonokaiDarkGray, consoleAlpha));
		RcDrawRectangle(console->scrollBarRec, MonokaiWhite);
	}
}

char* DbgConsoleCrashDump(DbgConsole_t* console, MemoryArena_t* memArena, u32* resultSizeOut, const char* crashFunction, const char* crashFilename, int crashLineNumber, const char* crashExpressionStr, const RealTime_t* realTime, u64 programTime)
{
	char* result = nullptr;
	u32 resultSize = 0;
	if (resultSizeOut != nullptr) { *resultSizeOut = 0; }
	
	bool isDebugOutputCorrupted = false;
	if (console == nullptr || app == nullptr || console->arena.fifoSpace == nullptr || console->arena.firstLine == nullptr) { isDebugOutputCorrupted = true; }
	else
	{
		ConsoleLine_t* line = console->arena.firstLine;
		while (line != nullptr)
		{
			if ((u8*)line < console->arena.fifoSpace) { isDebugOutputCorrupted = true; break; }
			if ((u8*)line + sizeof(ConsoleLine_t) > console->arena.fifoSpace + console->arena.fifoSize) { isDebugOutputCorrupted = true; break; }
			u32 totalSize = sizeof(ConsoleLine_t) + line->filePathLength+1 + line->funcNameLength+1 + line->messageLength+1;
			if ((u8*)line + totalSize > console->arena.fifoSpace + console->arena.fifoSize) { isDebugOutputCorrupted = true; break; }
			line = line->next;
		}
	}
	
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 numChars = 0;
		
		TwoPassPrint(&numChars, result, resultSize, "+==============================+\n");
		TwoPassPrint(&numChars, result, resultSize, "| App %s Dump v%u.%02u(%5u)  |\n", (crashFilename != nullptr) ? "Crash" : "Debug", APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD);
		TwoPassPrint(&numChars, result, resultSize, "+==============================+\n");
		TwoPassPrint(&numChars, result, resultSize, "Time: %s %s %s %u at %u:%02u:%02u%s\n", GetDayOfWeekStr((DayOfWeek_t)realTime->dayOfWeek), GetMonthStr((Month_t)realTime->month), GetDayOfMonthString(realTime->day), realTime->year, Convert24HourTo12Hour(realTime->hour), realTime->minute, realTime->second, IsPostMeridian(realTime->hour) ? "pm" : "am");
		if (crashFilename != nullptr)
		{
			TwoPassPrint(&numChars, result, resultSize, "Failed Assertion in %s:%u function %s: (%s) is false\n\n", crashFilename, crashLineNumber, crashFunction, crashExpressionStr);
		}
		else
		{
			TwoPassPrint(&numChars, result, resultSize, "\n");
		}
		
		if (!isDebugOutputCorrupted)
		{
			ConsoleLine_t* line = console->arena.firstLine;
			while (line != nullptr)
			{
				const char* message = GetConsoleLineMessage(line);
				const char* filePath = GetConsoleLineFilePath(line);
				const char* funcName = GetConsoleLineFuncName(line);
				u64 programStartTimestamp = realTime->timestamp;
				if (programStartTimestamp > (programTime/1000)) { programStartTimestamp -= (programTime/1000); }
				else { programStartTimestamp = 0; }
				u64 lineTimestamp = programStartTimestamp + (line->time/1000);
				u64 milliseconds = (line->time%1000);
				if (milliseconds >= (programTime%1000)) { milliseconds -= (programTime%1000); }
				else { milliseconds += 1000 - (programTime%1000); }
				milliseconds = (milliseconds % 1000);
				RealTime_t lineRealTime = {}; GetRealTime(lineTimestamp, true, &lineRealTime); //TODO: This calculation is totally wrong, time is in ms and relative to program time
				TwoPassPrint(&numChars, result, resultSize, "%s %s %s %u at %2u:%02u:%02u.%03u%s ", GetDayOfWeekStr((DayOfWeek_t)lineRealTime.dayOfWeek), GetMonthStr((Month_t)lineRealTime.month), GetDayOfMonthString(lineRealTime.day), lineRealTime.year, Convert24HourTo12Hour(lineRealTime.hour), lineRealTime.minute, lineRealTime.second, (u16)milliseconds, IsPostMeridian(lineRealTime.hour) ? "pm" : "am");
				TwoPassPrint(&numChars, result, resultSize, "%3u %.*s:%u %.*s [%s] - ", line->gutterNumber, line->filePathLength, filePath, line->fileLineNumber, line->funcNameLength, funcName, GetDbgLevelStr(line->dbgLevel));
				TwoPassPrint(&numChars, result, resultSize, "%.*s\n", line->messageLength, message);
				line = line->next;
			}
		}
		else
		{
			TwoPassPrint(&numChars, result, resultSize, "[Debug Output is Corrupted]\n");
		}
		
		if (pass == 0)
		{
			if (numChars == 0) { return nullptr; }
			resultSize = numChars;
			result = PushArray(memArena, char, numChars+1);
			if (result == nullptr) { return nullptr; }
		}
		else
		{
			if (numChars != resultSize) { ArenaPop(memArena, result); return nullptr; }
			result[resultSize] = '\0';
		}
	}
	if (resultSizeOut != nullptr) { *resultSizeOut = resultSize; }
	return result;
}

void DumpConsoleArenaLines(const ConsoleArena_t* arena)
{
	Assert(arena != nullptr);
	ConsoleLine_t* line = arena->firstLine;
	while (line != nullptr)
	{
		AppDebugOutput(line->flags, GetConsoleLineFilePath(line), line->fileLineNumber, GetConsoleLineFuncName(line), line->dbgLevel, true, GetConsoleLineMessage(line));
		line = line->next;
	}
}
