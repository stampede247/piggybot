/*
File:   app_tasks.cpp
Author: Taylor Robbins
Date:   01\26\2020
Description: 
	** Holds functions that help us keep track of and carry out various tasks that get run using the ThreadedTask system in the platform layer
*/

void CarryOutAppTask(ThreadedTask_t* task, const TaskThreadInfo_t* threadInfo)
{
	Assert(task != nullptr);
	// Assert(threadInfo != nullptr); //TODO: Uncomment me when you are done debugging running tasks on the main thread
	
	// PrintLine_I("Application is performing task %u on thread %u", task->id, threadInfo->index);
	
	AppTaskType_t taskType = (AppTaskType_t)task->taskType;
	switch (taskType)
	{
		// +==============================+
		// |     AppTaskType_SaveFile     |
		// +==============================+
		case AppTaskType_SaveFile:
		{
			if (task->taskPntr1 == nullptr) { PrintLine_E("No filePath passed in SaveFile AppTask %u", task->id); break; }
			if (task->taskPntr2 == nullptr) { PrintLine_E("No contents passed in SaveFile AppTask %u", task->id); break; }
			const char* filePath = (const char*)task->taskPntr1;
			u32 contentsSize = task->taskSize2;
			const void* contentsPntr = (const void*)task->taskPntr2;
			
			if (platform->WriteEntireFile(filePath, contentsPntr, contentsSize))
			{
				PrintLine_I("Wrote \"%s\" successfully", filePath);
				task->resultCode = 0x01;
			}
			else
			{
				PrintLine_E("Failed to write \"%s\"", filePath);
				task->resultCode = 0x00;
			}
		} break;
		
		// +==============================+
		// |     AppTaskType_LoadFile     |
		// +==============================+
		case AppTaskType_LoadFile:
		{
			if (task->taskPntr1 == nullptr) { PrintLine_E("No filePath passed in LoadFile AppTask %u", task->id); break; }
			const char* filePath = (const char*)task->taskPntr1;
			
			FileInfo_t fileInfo = platform->ReadEntireFile(filePath);
			task->resultPntr1 = fileInfo.content;
			task->resultSize1 = fileInfo.size;
		} break;
		
		// +==============================+
		// |     AppTaskType_LoadSong     |
		// +==============================+
		case AppTaskType_LoadSong:
		{
			task->resultCode = 0x01; //Generic failure
			if (task->taskPntr1 == nullptr)
			{
				PrintLine_E("No filePath passed in LoadSong AppTask %u", task->id);
				task->resultCode = 0x02;
				break;
			}
			const char* filePath = (const char*)task->taskPntr1;
			Assert(task->taskPntr2 != nullptr && task->taskSize2 == sizeof(Sound_t));
			if (task->taskPntr2 == nullptr || task->taskSize2 != sizeof(Sound_t))
			{
				PrintLine_E("No allocatedSound passed in LoadSong AppTask %u", task->id);
				task->resultCode = 0x02;
				break;
			}
			Sound_t* allocatedSound = (Sound_t*)task->taskPntr2;
			
			FileInfo_t sndFile = platform->ReadEntireFile(filePath);
			if (sndFile.content == nullptr || sndFile.size == 0)
			{
				PrintLine_E("Couldn't open file \"%s\" to load as song", filePath);
				task->resultCode = 0x03;
				break;
			}
			
			SampleBuffer_t sampleBuffer = {};
			bool parseSuccess = ParseOggFile(&sndFile, allocatedSound, &sampleBuffer);
			if (!parseSuccess || sampleBuffer.samples == nullptr || sampleBuffer.numSamples == 0)
			{
				PrintLine_E("Failed to parse OGG file \"%s\"", filePath);
				task->resultCode = 0x04;
				platform->FreeFileMemory(&sndFile);
				break;
			}
			platform->FreeFileMemory(&sndFile);
			
			task->resultSize1 = sampleBuffer.numSamples;
			task->resultPntr1 = sampleBuffer.samples;
			task->resultCode = 0x00; //success
		} break;
		
		// +==============================+
		// |   AppTaskType_LoadTexture    |
		// +==============================+
		case AppTaskType_LoadTexture:
		{
			if (task->taskPntr1 == nullptr) { PrintLine_E("No filePath passed in LoadTexture AppTask %u", task->id); break; }
			const char* filePath = (const char*)task->taskPntr1;
			
			FileInfo_t textureFile = platform->ReadEntireFile(filePath);
			if (textureFile.content == nullptr || textureFile.size == 0)
			{
				PrintLine_E("Couldn't open texture at \"%s\"", filePath);
				task->resultCode = 0x01;
				break;
			}
			
			i32 numChannels;
			i32 width, height;
			u8* pixelData = stbi_load_from_memory(
				(u8*)textureFile.content, textureFile.size,
				&width, &height, &numChannels, 4
			);
			platform->FreeFileMemory(&textureFile);
			
			if (pixelData == nullptr)
			{
				PrintLine_E("Couldn't parse texture at \"%s\"", filePath);
				task->resultCode = 0x02;
				break;
			}
			Assert(width > 0 && height > 0);
			
			task->resultPntr1 = pixelData;
			task->resultSize1 = (u32)width;
			task->resultSize2 = (u32)height;
			task->resultCode = 0x00;
		} break;
		
		// +==============================+
		// |     Unknown AppTaskType      |
		// +==============================+
		default:
		{
			PrintLine_E("Unhandled AppTaskType: 0x%02X", task->taskType);
		} break;
	}
	
	// PrintLine_I("Application finished task %u on thread %u", task->id, threadInfo->index);
}

void CleanUpAfterTaskFinished(ThreadedTask_t* task)
{
	AppTaskType_t taskType = (AppTaskType_t)task->taskType;
	switch (taskType)
	{
		case AppTaskType_SaveFile:
		{
			if (task->taskPntr1 != nullptr) { ArenaPop(mainHeap, task->taskPntr1); }
			if (task->taskPntr2 != nullptr) { ArenaPop(&app->platHeap, task->taskPntr2); }
		} break;
		case AppTaskType_LoadFile:
		{
			FileInfo_t pretendFileInfo = {};
			pretendFileInfo.content = task->resultPntr1;
			pretendFileInfo.size = task->resultSize1;
			if (task->callback != nullptr)
			{
				LoadFileCallback_f* callbackFunc = (LoadFileCallback_f*)task->callback;
				callbackFunc((const char*)task->taskPntr1, &pretendFileInfo);
			}
			if (task->taskPntr1 != nullptr) { ArenaPop(mainHeap, task->taskPntr1); }
			if (task->resultPntr1 != nullptr) { platform->FreeFileMemory(&pretendFileInfo); }
		} break;
		case AppTaskType_LoadSong:
		{
			if (task->resultCode != 0x00) { PrintLine_E("LoadSong task failed with result code 0x%02X", task->resultCode); }
			Assert(task->taskPntr2 != nullptr && task->taskSize2 == sizeof(Sound_t));
			Sound_t* allocatedSound = (Sound_t*)task->taskPntr2;
			SampleBuffer_t pretendSampleBuffer = {};
			pretendSampleBuffer.samples = (i16*)task->resultPntr1;
			pretendSampleBuffer.numSamples = task->resultSize1;
			pretendSampleBuffer.numSamplesAlloc = task->resultSize1;
			if (task->callback != nullptr)
			{
				LoadSongCallback_f* callbackFunc = (LoadSongCallback_f*)task->callback;
				callbackFunc((const char*)task->taskPntr1, (task->resultCode == 0x00), &pretendSampleBuffer, allocatedSound);
			}
			if (task->taskPntr1 != nullptr) { ArenaPop(mainHeap, task->taskPntr1); }
			if (task->taskPntr2 != nullptr) { ArenaPop(mainHeap, task->taskPntr2); }
			if (pretendSampleBuffer.samples != nullptr) { DestroySampleBuffer(&pretendSampleBuffer); }
		} break;
		case AppTaskType_LoadTexture:
		{
			if (task->callback != nullptr)
			{
				LoadTextureCallback_f* callbackFunc = (LoadTextureCallback_f*)task->callback;
				callbackFunc((const char*)task->taskPntr1, (task->resultCode == 0x00), task->resultSize1, task->resultSize2, (const u8*)task->resultPntr1, task->taskNumber);
			}
			if (task->taskPntr1 != nullptr) { ArenaPop(mainHeap, task->taskPntr1); }
			if (task->resultPntr1 != nullptr) { stbi_image_free(task->resultPntr1); }
		} break;
	}
}

bool QueueFileSave(const char* filePath, const void* contentsPntr, u32 contentsSize)
{
	Assert(IsThisMainThread());
	Assert(filePath != nullptr);
	ThreadedTask_t newTask = {};
	newTask.taskType = AppTaskType_SaveFile;
	newTask.taskPntr1 = ArenaNtString(mainHeap, filePath);
	PrintLine_D("Queuing up write of %s file", FormattedSizeStr(contentsSize));
	if (contentsSize > 0)
	{
		newTask.taskPntr2 = ArenaPush(&app->platHeap, contentsSize);
		Assert(newTask.taskPntr2 != nullptr);
		MyMemCopy(newTask.taskPntr2, contentsPntr, contentsSize);
		newTask.taskSize2 = contentsSize;
	}
	u32 newTaskId = platform->QueueThreadedTask(&newTask);
	if (newTaskId != 0) { return true; }
	else { return false; }
}

bool QueueSongLoad(const char* filePath, LoadSongCallback_f* callback) //pre-declared in app_func_defs.cpp
{
	Assert(IsThisMainThread());
	Assert(filePath != nullptr);
	ThreadedTask_t newTask = {};
	newTask.taskType = AppTaskType_LoadSong;
	newTask.taskPntr1 = ArenaNtString(mainHeap, filePath);
	Assert(newTask.taskPntr1 != nullptr);
	newTask.taskSize2 = sizeof(Sound_t);
	newTask.taskPntr2 = PushStruct(mainHeap, Sound_t);
	Assert(newTask.taskPntr2 != nullptr);
	MyMemSet(newTask.taskPntr2, 0x00, sizeof(Sound_t));
	newTask.callback = (void*)callback;
	u32 newTaskId = platform->QueueThreadedTask(&newTask);
	if (newTaskId != 0) { return true; }
	else { return false; }
}

bool QueueTextureLoad(const char* filePath, u32 taskNumber, LoadTextureCallback_f* callback) //pre-declared in app_func_defs.cpp
{
	Assert(IsThisMainThread());
	Assert(filePath != nullptr);
	ThreadedTask_t newTask = {};
	newTask.taskType = AppTaskType_LoadTexture;
	newTask.taskNumber = taskNumber;
	newTask.taskPntr1 = ArenaNtString(mainHeap, filePath);
	Assert(newTask.taskPntr1 != nullptr);
	newTask.callback = (void*)callback;
	u32 newTaskId = platform->QueueThreadedTask(&newTask);
	if (newTaskId != 0) { return true; }
	else { return false; }
}
