/*
File:   app_json.cpp
Author: Taylor Robbins
Date:   11\07\2020
Description: 
	** Holds functions that help us parse JSON data and search through it in various ways.
	** This file makes use of the JSMN library to do the majority of the parsing 
*/

struct JsonData_t
{
	MemoryArena_t* allocArena;
	
	const char* strPntr;
	u32 strLength;
	
	bool parseSuccess;
	i32 errorCode;
	u32 numTokens;
	jsmntok_t* tokens;
};

void DestroyJsonData(JsonData_t* jsonData)
{
	NotNull(jsonData);
	if (jsonData->tokens != nullptr)
	{
		NotNull(jsonData->allocArena);
		ArenaPop(jsonData->allocArena, jsonData->tokens);
	}
	ClearPointer(jsonData);
}

bool ParseJsonString(MemoryArena_t* memArena, const char* strPntr, u32 strLength, JsonData_t* jsonOut)
{
	NotNull(memArena);
	NotNull(jsonOut);
	Assert(strPntr != nullptr || strLength == 0);
	ClearPointer(jsonOut);
	jsonOut->strPntr = strPntr;
	jsonOut->strLength = strLength;
	jsonOut->allocArena = memArena;
	if (strLength == 0) { jsonOut->errorCode = 0; return false; }
	
	jsmn_parser parser;
	jsmn_init(&parser);
	int parseResult = jsmn_parse(&parser, strPntr, strLength, nullptr, 0);
	if (parseResult <= 0)
	{
		jsonOut->errorCode = parseResult;
		return false;
	}
	
	jsonOut->numTokens = (u32)parseResult;
	jsonOut->tokens = PushArray(memArena, jsmntok_t, jsonOut->numTokens);
	NotNull(jsonOut->tokens);
	MyMemSet(jsonOut->tokens, 0x00, sizeof(jsmntok_t) * jsonOut->numTokens);
	
	jsmn_init(&parser);
	parseResult = jsmn_parse(&parser, strPntr, strLength, jsonOut->tokens, jsonOut->numTokens);
	Assert(parseResult > 0 && (u32)parseResult == jsonOut->numTokens);
	
	jsonOut->parseSuccess = true;
	return true;
}

i32 JsonGetTokenIndex(const JsonData_t* json, const jsmntok_t* token)
{
	NotNull(json);
	NotNull(token);
	Assert(token >= json->tokens && token < (json->tokens + json->numTokens));
	return (i32)(token - json->tokens);
}

StrSplitPiece_t JsonGetTokenStr(JsonData_t* json, const jsmntok_t* token)
{
	NotNull(json);
	NotNull(token);
	NotNull(json->tokens);
	NotNull(json->strPntr);
	Assert(token->start <= token->end);
	Assert(token->start >= 0 && (u32)token->start < json->strLength);
	Assert(token->end >= 0 && (u32)token->end < json->strLength);
	StrSplitPiece_t result;
	result.pntr = &json->strPntr[token->start];
	result.length = (u32)token->end - (u32)token->start;
	return result;
}
StrSplitPiece_t JsonGetTokenStr(const JsonData_t* json, const jsmntok_t* token) //const version
{
	return JsonGetTokenStr((JsonData_t*)json, token);
}
bool JsonDoesStrTokenEqualIgnoreCase(const JsonData_t* json, const jsmntok_t* token, const char* strPntr, u32 strLength)
{
	NotNull(json);
	NotNull(token);
	Assert(strPntr != nullptr || strLength == 0);
	if (token->type != JSMN_STRING) { return false; }
	Assert(token->start <= token->end);
	if ((u32)(token->end - token->start) != strLength) { return false; }
	if (strLength == 0) { return false; } //looking for empty and found empty
	StrSplitPiece_t tokenValue = JsonGetTokenStr(json, token);
	return DoesSplitPieceEqualIgnoreCase(&tokenValue, strPntr, strLength);
	
}
bool JsonDoesStrTokenEqualIgnoreCaseNt(const JsonData_t* json, const jsmntok_t* token, const char* nullTermString)
{
	u32 strLength = (nullTermString != nullptr) ? MyStrLength32(nullTermString) : 0;
	return JsonDoesStrTokenEqualIgnoreCase(json, token, nullTermString, strLength);
}

jsmntok_t* JsonFindChildByName(JsonData_t* json, const char* childName, const jsmntok_t* parent = nullptr, u32 skip = 0)
{
	Assert(json->tokens != nullptr || json->numTokens == 0);
	if (json->numTokens == 0) { return nullptr; }
	NotNull(json);
	NotNull(childName);
	if (parent == nullptr) { parent = &json->tokens[0]; }
	i32 parentIndex = JsonGetTokenIndex(json, parent);
	u32 childIndex = 0;
	u32 foundIndex = 0;
	for (u32 tIndex = 0; tIndex < json->numTokens; tIndex++)
	{
		jsmntok_t* token = &json->tokens[tIndex];
		if (token->parent == parentIndex)
		{
			if (JsonDoesStrTokenEqualIgnoreCaseNt(json, token, childName))
			{
				if (foundIndex >= skip) { return token; }
				foundIndex++;
			}
			childIndex++;
		}
	}
	return nullptr;
}

jsmntok_t* JsonFindChildValueByName(JsonData_t* json, const char* childName, const jsmntok_t* parent = nullptr, u32 skip = 0)
{
	jsmntok_t* keyToken = JsonFindChildByName(json, childName, parent, skip);
	if (keyToken == nullptr) { return nullptr; }
	i32 keyTokenIndex = JsonGetTokenIndex(json, keyToken);
	for (u32 tIndex = 0; tIndex < json->numTokens; tIndex++)
	{
		jsmntok_t* token = &json->tokens[tIndex];
		if (token->parent == keyTokenIndex) { return token; }
	}
	return nullptr;
}
