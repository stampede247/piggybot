/*
File:   app_func_defs.h
Author: Taylor Robbins
Date:   11\05\2017
*/

#ifndef _APP_FUNC_DEFS_H
#define _APP_FUNC_DEFS_H

//app_helpers.cpp
bool IsThisMainThread();
MemoryArena_t* GetThreadTempArena();

//app_sound.cpp
SoundInstance_t* GetSndInstanceBySrc(const Sound_t* srcPntr, u32 index = 0);

//app_tasks.cpp
bool QueueSongLoad(const char* filePath, LoadSongCallback_f* callback);
bool QueueTextureLoad(const char* filePath, u32 taskNumber, LoadTextureCallback_f* callback);

//app_render_functions.cpp
void RcDrawRectangle(rec rectangle, Color_t color);
void RcDrawCharacterEx(const Font_t* font, const FontCharInfo_t* charInfo, u32 charIndex, v2 bottomLeft, Color_t color, r32 scale = 1.0f);

//res_bitmap_font_loading.cpp
bool LoadBitmapFont(MemoryArena_t* memArena, const char* filePath, const char* metaFilePath, Font_t* fontOut);

//app_console_commands.cpp
void HandleConsoleCommand(const char* command);

//app_notifications.cpp
AppNotification_t* PushNotification(const char* filePath, u32 lineNumber, const char* funcName, const char* message, Color_t textColor, r32 duration = DEFAULT_NOTIFICATION_TIME);
AppNotification_t* PushNotificationPrint(const char* filePath, u32 lineNumber, const char* funcName, Color_t textColor, r32 duration, const char* formatStr, ...);
#define Notify_D(message) PushNotification(__FILE__, __LINE__, __func__, message, MonokaiGray1)
#define Notify_R(message) PushNotification(__FILE__, __LINE__, __func__, message, MonokaiWhite)
#define Notify_I(message) PushNotification(__FILE__, __LINE__, __func__, message, MonokaiGreen)
#define Notify_N(message) PushNotification(__FILE__, __LINE__, __func__, message, MonokaiPurple)
#define Notify_O(message) PushNotification(__FILE__, __LINE__, __func__, message, MonokaiLightBlue)
#define Notify_E(message) PushNotification(__FILE__, __LINE__, __func__, message, MonokaiMagenta)
#define Notify_W(message) PushNotification(__FILE__, __LINE__, __func__, message, MonokaiOrange)

#define NotifyPrint_D(formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiGray1,     DEFAULT_NOTIFICATION_TIME, (formatStr), __VA_ARGS__)
#define NotifyPrint_R(formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiWhite,     DEFAULT_NOTIFICATION_TIME, (formatStr), __VA_ARGS__)
#define NotifyPrint_I(formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiGreen,     DEFAULT_NOTIFICATION_TIME, (formatStr), __VA_ARGS__)
#define NotifyPrint_N(formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiPurple,    DEFAULT_NOTIFICATION_TIME, (formatStr), __VA_ARGS__)
#define NotifyPrint_O(formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiLightBlue, DEFAULT_NOTIFICATION_TIME, (formatStr), __VA_ARGS__)
#define NotifyPrint_E(formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiOrange,    DEFAULT_NOTIFICATION_TIME, (formatStr), __VA_ARGS__)
#define NotifyPrint_W(formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiMagenta,   DEFAULT_NOTIFICATION_TIME, (formatStr), __VA_ARGS__)

#define NotifyTimed_D(message, duration) PushNotification(__FILE__, __LINE__, __func__, (message), MonokaiGray1,     (duration))
#define NotifyTimed_R(message, duration) PushNotification(__FILE__, __LINE__, __func__, (message), MonokaiWhite,     (duration))
#define NotifyTimed_I(message, duration) PushNotification(__FILE__, __LINE__, __func__, (message), MonokaiGreen,     (duration))
#define NotifyTimed_N(message, duration) PushNotification(__FILE__, __LINE__, __func__, (message), MonokaiPurple,    (duration))
#define NotifyTimed_O(message, duration) PushNotification(__FILE__, __LINE__, __func__, (message), MonokaiLightBlue, (duration))
#define NotifyTimed_E(message, duration) PushNotification(__FILE__, __LINE__, __func__, (message), MonokaiOrange,    (duration))
#define NotifyTimed_W(message, duration) PushNotification(__FILE__, __LINE__, __func__, (message), MonokaiMagenta,   (duration))

#define NotifyPrintTimed_D(duration, formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiGray1,     (duration), (formatStr), __VA_ARGS__)
#define NotifyPrintTimed_R(duration, formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiWhite,     (duration), (formatStr), __VA_ARGS__)
#define NotifyPrintTimed_I(duration, formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiGreen,     (duration), (formatStr), __VA_ARGS__)
#define NotifyPrintTimed_N(duration, formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiPurple,    (duration), (formatStr), __VA_ARGS__)
#define NotifyPrintTimed_O(duration, formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiLightBlue, (duration), (formatStr), __VA_ARGS__)
#define NotifyPrintTimed_E(duration, formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiOrange,    (duration), (formatStr), __VA_ARGS__)
#define NotifyPrintTimed_W(duration, formatStr, ...) PushNotificationPrint(__FILE__, __LINE__, __func__, MonokaiMagenta,   (duration), (formatStr), __VA_ARGS__)


#endif //  _APP_FUNC_DEFS_H
