/*
File:   app_render_context.h
Author: Taylor Robbins
Date:   11\04\2017
*/

#ifndef _APP_RENDER_CONTEXT_H
#define _APP_RENDER_CONTEXT_H

struct RcDebugInfo_t
{
	u32 numShaderBinds;
	u32 numSheetFrameDraws;
	u32 numSheetFramesBatched;
	u32 numTextureBinds;
	u32 numDrawCalls;
	u32 numBatchedGlyphs;
	u32 numDrawnGlyphs;
};

struct RenderContext_t
{
	VertexBuffer_t squareBuffer;
	VertexBuffer_t equilTriangleBuffer;
	Texture_t dotTexture;
	Texture_t gradientTexture;
	
	const Shader_t* boundShader;
	
	u32 numBatchingFonts;
	const Texture_t* batchingFontBakes[MAX_BATCHING_FONTS];
	DynArray_t batchingFontVerts[MAX_BATCHING_FONTS];
	VertexBuffer_t batchFontVertBuffers[MAX_BATCHING_FONTS];
	
	u32 numBatchingSheets;
	const SpriteSheet_t* batchingSheets[MAX_BATCHING_SHEETS];
	DynArray_t batchingSheetVerts[MAX_BATCHING_SHEETS];
	VertexBuffer_t batchSheetVertBuffers[MAX_BATCHING_SHEETS];
	
	v2 lastTextSize;
	v2 lastTextStartPos;
	v2 lastTextEndPos;
	u32 lastTextLength;
	u32 lastTextNumLines;
	FontFlowRenderState_t flowRenderState;
	FontFlowState_t flowState;
	
	mat4 worldMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	rec viewport;
	
	const FrameBuffer_t* boundFrameBuffer;
	const VertexBuffer_t* boundBuffer;
	const Texture_t* boundTexture;
	const Texture_t* boundAltTexture;
	const Font_t* boundFont;
	r32 fontScale;
	r32 strPosSnapEnabled;
	
	Color_t primaryColor;
	Color_t secondaryColor;
	v4 replaceColors[NUM_REPLACE_COLORS];
	
	rec  sourceRectangle;
	rec  altSourceRectangle;
	rec  maskRectangle;
	v2   shiftVec;
	bool gradientEnabled;
	r32  circleRadius;
	r32  circleInnerRadius;
	r32  saturation;
	r32  brightness;
	r32  time;
	r32  depth;
	r32  value0;
	r32  value1;
	r32  value2;
	r32  value3;
	r32  value4;
	v2   mousePos;
	v2   screenSize;
};

#endif //  _APP_RENDER_CONTEXT_H
