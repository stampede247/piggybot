/*
File:   app_tasks.h
Author: Taylor Robbins
Date:   01\26\2020
*/

#ifndef _APP_TASKS_H
#define _APP_TASKS_H

#define LoadFileCallback_DEFINITION(functionName) void functionName(const char* filePath, const FileInfo_t* fileInfo)
typedef LoadFileCallback_DEFINITION(LoadFileCallback_f);

#define LoadSongCallback_DEFINITION(functionName) void functionName(const char* filePath, bool success, const SampleBuffer_t* sampleBuffer, const Sound_t* soundInfo)
typedef LoadSongCallback_DEFINITION(LoadSongCallback_f);

#define LoadTextureCallback_DEFINITION(functionName) void functionName(const char* filePath, bool success, u32 width, u32 height, const u8* pixelData, u32 taskNumber)
typedef LoadTextureCallback_DEFINITION(LoadTextureCallback_f);

typedef enum
{
	AppTaskType_None = 0x00,
	AppTaskType_SaveFile,
	AppTaskType_LoadFile,
	AppTaskType_LoadSong,
	AppTaskType_LoadTexture,
	AppTaskType_NumTypes,
} AppTaskType_t;

const char* GetAppTaskTypeStr(AppTaskType_t taskType)
{
	switch (taskType)
	{
		case AppTaskType_None:            return "None";
		case AppTaskType_SaveFile:        return "SaveFile";
		case AppTaskType_LoadFile:        return "LoadFile";
		case AppTaskType_LoadSong:        return "LoadSong";
		case AppTaskType_LoadTexture:     return "LoadTexture";
		default: return "Unknown";
	}
}

#endif //  _APP_TASKS_H
