/*
File:   app_performance.cpp
Author: Taylor Robbins
Date:   03\26\2020
Description: 
	** Holds a bunch of functions and macros that help us track performance of the game in various ways
*/

void PrintPerformanceTimes(DbgLevel_t dbgLevel, const PerfTime_t* times, const char** names, u32 numTimes)
{
	Assert(times != nullptr);
	Assert(names != nullptr);
	if (numTimes >= 2)
	{
		const PerfTime_t* totalStartTime = &times[0];
		const PerfTime_t* totalEndTime = &times[numTimes-1];
		r64 totalTime = platform->GetPerfTimeDiff(totalStartTime, totalEndTime);
		PrintLineAt(dbgLevel, "Total Time: %s", FormattedMillisecondsR64(totalTime));
		for (u32 tIndex = 1; tIndex < numTimes; tIndex++)
		{
			const PerfTime_t* startTime = &times[tIndex-1];
			const PerfTime_t* endTime = &times[tIndex];
			r64 timeValue = platform->GetPerfTimeDiff(startTime, endTime);
			const char* name = names[tIndex];
			if (name == nullptr) { name = "Untitled"; }
			PrintLineAt(dbgLevel, "%s: %s %.0lf%%", name, FormattedMillisecondsR64(timeValue), (timeValue / totalTime) * 100.0);
		}
	}
}
