/*
File:   app_ogg.cpp
Author: Taylor Robbins
Date:   03\19\2019
Description: 
	** Holds the functions that work with libogg to parse audio files and feed them to OpenAL 
*/

#if AUDIO_ENABLED

#include "ogg/ogg.h" //comes from libogg include folder
#include "vorbis/codec.h" //comes from libvorbis include folder

//NOTE: If sampleBufferOut is given then the audio samples will not be deleted off the mainHeap, but instead the pointer for them will be placed into sampleBufferOut
bool ParseOggFile(FileInfo_t* file, Sound_t* soundOut, SampleBuffer_t* sampleBufferOut = nullptr, bool passToOpenAl = true, bool loadInfoOnly = false)
{
	Assert(file != nullptr && file->content != nullptr);
	Assert(soundOut != nullptr);
	const u8* filePntr = (const u8*)file->content;
	u32 fileIndex = 0;
	u32 fileSize = file->size;
	if (fileSize < 1024) { PrintLine_W("File is too small to be OGG audio: %u bytes", fileSize); return false; }
	if (loadInfoOnly) { Assert(!passToOpenAl && sampleBufferOut == nullptr); }
	
	ogg_sync_state   syncState;   //oy
	ogg_stream_state streamState; //os
	ogg_page         oggPage;     //og
	ogg_packet       oggPacket;   //op
	vorbis_info      vorbInfo;    //vi
	vorbis_comment   vorbComment; //vc
	vorbis_dsp_state vorbState;   //vd
	vorbis_block     vorbBlock;   //vb
	
	i32 syncResult = ogg_sync_init(&syncState); //initialize the syncState
	Assert(syncResult == 0);
	char* syncBuffer = ogg_sync_buffer(&syncState, OGG_READ_BUFFER_SIZE); //tell the syncState to allocate a buffer
	Assert(syncBuffer != nullptr);
	
	// +==============================+
	// |     Read the first page      |
	// +==============================+
	{
		u32 numBytesToRead = MinU32(fileSize - fileIndex, OGG_READ_BUFFER_SIZE);
		MyMemCopy(syncBuffer, filePntr + fileIndex, numBytesToRead); //read some data into the buffer from our file to kickstart our decode process
		fileIndex += numBytesToRead;
		ogg_sync_wrote(&syncState, numBytesToRead); //tell the syncState we read some bytes into the buffer
		
		i32 pageoutResult = ogg_sync_pageout(&syncState, &oggPage);
		if (pageoutResult != 1) //if there isn't a page of data after our first read then it's probably not an OGG file
		{
			PrintLine_E("Couldn't find first page of OGG file in %u bytes: %d", numBytesToRead, pageoutResult);
			ogg_sync_clear(&syncState);
			return false;
		}
	}
	
	// +==============================+
	// |    Initialize the Stream     |
	// +==============================+
	{
		i32 streamInitResult = ogg_stream_init(&streamState, ogg_page_serialno(&oggPage)); //initialize the streamState using the serial no. from the first page
		if (streamInitResult != 0)
		{
			PrintLine_E("Failed the initialize OGG Stream: %d", streamInitResult);
			ogg_stream_clear(&streamState);
			ogg_sync_clear(&syncState);
			return false;
		}
	}
	
	// +===============================+
	// | Check that the data is Vorbis |
	// +===============================+
	{
		vorbis_info_init(&vorbInfo);
		vorbis_comment_init(&vorbComment);
		
		i32 pageinResult = ogg_stream_pagein(&streamState, &oggPage);
		if (pageinResult < 0)
		{
			PrintLine_E("Failed to read first page of OGG bitstream data: %d", pageinResult);
			ogg_stream_clear(&streamState);
			vorbis_comment_clear(&vorbComment);
			vorbis_info_clear(&vorbInfo);
			ogg_sync_clear(&syncState);
			return false;
		}
		
		i32 packetoutResult = ogg_stream_packetout(&streamState, &oggPacket);
		if (packetoutResult != 1)
		{
			PrintLine_E("Error reading initial header packet: %d", packetoutResult);
			ogg_stream_clear(&streamState);
			vorbis_comment_clear(&vorbComment);
			vorbis_info_clear(&vorbInfo);
			ogg_sync_clear(&syncState);
			return false;
		}
		
		i32 headerinResult = vorbis_synthesis_headerin(&vorbInfo, &vorbComment, &oggPacket);
		if (headerinResult < 0)
		{
			PrintLine_E("The OGG file does not contain Vorbis audio data: %d", headerinResult);
			ogg_stream_clear(&streamState);
			vorbis_comment_clear(&vorbComment);
			vorbis_info_clear(&vorbInfo);
			ogg_sync_clear(&syncState);
			return false;
		}
	}
	
	// +==================================+
	// | Read the first 2 Vorbis Headers  |
	// +==================================+
	{
		u8 numPacketsRead = 0;
		while (numPacketsRead < 2)
		{
			//Read up to 2 pages/packets
			while (numPacketsRead < 2)
			{
				i32 pageoutResult = ogg_sync_pageout(&syncState, &oggPage); //get the page from the syncState
				if (pageoutResult == 0) { break; } //need more data, so keep reading more data in
				else if (pageoutResult < 0)
				{
					PrintLine_E("pageout failed in vorbis header[%u]: %d", numPacketsRead, pageoutResult);
					ogg_stream_clear(&streamState);
					vorbis_comment_clear(&vorbComment);
					vorbis_info_clear(&vorbInfo);
					ogg_sync_clear(&syncState);
					return false;
				}
				else //valid page found
				{
					ogg_stream_pagein(&streamState, &oggPage); //submit the page to the streamState
					//can ignore errors, since they'll show up in packetout
					
					//read up to 2 packets
					while (numPacketsRead < 2)
					{
						i32 packetoutResult = ogg_stream_packetout(&streamState, &oggPacket);
						if (packetoutResult == 0) { break; } //no packet ready, need more pages
						else if (packetoutResult < 0)
						{
							PrintLine_E("Found corrupt Vorbis header[%u]: %d", numPacketsRead, packetoutResult);
							ogg_stream_clear(&streamState);
							vorbis_comment_clear(&vorbComment);
							vorbis_info_clear(&vorbInfo);
							ogg_sync_clear(&syncState);
							return false;
						}
						else //valid packet found
						{
							i32 headerinResult = vorbis_synthesis_headerin(&vorbInfo, &vorbComment, &oggPacket);
							if (headerinResult < 0)
							{
								PrintLine_E("Found corrupt Vorbis Header[%u]: %d", numPacketsRead, headerinResult);
								ogg_stream_clear(&streamState);
								vorbis_comment_clear(&vorbComment);
								vorbis_info_clear(&vorbInfo);
								ogg_sync_clear(&syncState);
								return false;
							}
							
							numPacketsRead++;
						}
					}
				}
			}
			
			if (numPacketsRead < 2)
			{
				if (fileIndex < fileSize)
				{
					syncBuffer = ogg_sync_buffer(&syncState, OGG_READ_BUFFER_SIZE);
					u32 numBytesToRead = MinU32(fileSize - fileIndex, OGG_READ_BUFFER_SIZE);
					MyMemCopy(syncBuffer, filePntr + fileIndex, numBytesToRead); //read some data into the buffer from our file
					fileIndex += numBytesToRead;
					ogg_sync_wrote(&syncState, numBytesToRead); //tell the syncState we read some bytes into the buffer
				}
				else
				{
					PrintLine_E("Reached end of file before finding first two Vorbis headers: %u bytes, %u packets", fileSize, numPacketsRead);
					ogg_stream_clear(&streamState);
					vorbis_comment_clear(&vorbComment);
					vorbis_info_clear(&vorbInfo);
					ogg_sync_clear(&syncState);
					return false;
				}
			}
		}
		
		// WriteLine_D("Found Vorbis Info and Comments:");
		// PrintLine_D("version:        %d", vorbInfo.version);
		// PrintLine_D("channels:       %d", vorbInfo.channels); //these are the two we care about
		// PrintLine_D("rate:           %ld", vorbInfo.rate); //these are the two we care about
		// PrintLine_D("bitrate_upper:  %ld", vorbInfo.bitrate_upper);
		// PrintLine_D("bitrate_nominal:%ld", vorbInfo.bitrate_nominal);
		// PrintLine_D("bitrate_lower:  %ld", vorbInfo.bitrate_lower);
		// PrintLine_D("bitrate_window: %ld", vorbInfo.bitrate_window);
		// PrintLine_D("codec_setup:    %p", vorbInfo.codec_setup);
		// PrintLine_D("vendor:         %s", vorbComment.vendor);
		// PrintLine_D("%d Comments:", vorbComment.comments);
		// for (i32 cIndex = 0; cIndex < vorbComment.comments; cIndex++)
		// {
		// 	PrintLine_D("  \"%s\"", vorbComment.user_comments[cIndex]);
		// }
	}
	
	//check the vorbInfo to make sure it's a format we support
	if (vorbInfo.channels != 1 && vorbInfo.channels != 2)
	{
		PrintLine_E("We don't support %d channel audio", vorbInfo.channels);
		ogg_stream_clear(&streamState);
		vorbis_comment_clear(&vorbComment);
		vorbis_info_clear(&vorbInfo);
		ogg_sync_clear(&syncState);
		return false;
	}
	u8 numChannels = (u8)vorbInfo.channels;
	soundOut->channelCount = numChannels;
	Assert(soundOut->channelCount != 0);
	soundOut->format = (numChannels == 2) ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16;
	soundOut->frequency = (ALsizei)vorbInfo.rate;
	// PrintLine_I("File is %u channel %dbps", soundOut->channelCount, soundOut->frequency);
	
	// +==============================+
	// |   Initialize the vorbState   |
	// +==============================+
	{
		i32 vorbInitResult = vorbis_synthesis_init(&vorbState, &vorbInfo);
		if (vorbInitResult != 0)
		{
			PrintLine_E("Corrupt header during playback initialization: %d", vorbInitResult);
			ogg_stream_clear(&streamState);
			vorbis_comment_clear(&vorbComment);
			vorbis_info_clear(&vorbInfo);
			ogg_sync_clear(&syncState);
			return false;
		}
		
		vorbis_block_init(&vorbState, &vorbBlock); //always succeeds
	}
	
	// +==============================+
	// |     Collect the Samples      |
	// +==============================+
	SampleBuffer_t sampleBuffer = {};
	u32 totalNumSamples = 0;
	{
		bool endOfStream = false;
		while (!endOfStream)
		{
			//Read as many pages/packets as possible
			while (!endOfStream)
			{
				i32 pageoutResult = ogg_sync_pageout(&syncState, &oggPage);
				if (pageoutResult == 0) { break; } //need more data for a valid page
				else if (pageoutResult < 0)
				{
					WriteLine_W("WARNING: Corrupt or missing data in ogg bitstream!");
					continue;
				}
				ogg_stream_pagein(&streamState, &oggPage);
				// WriteLine_D("Found an oggPage");
				
				//Read as many packets as possible
				while (true)
				{
					i32 packetoutResult = ogg_stream_packetout(&streamState, &oggPacket);
					if (packetoutResult == 0) { break; } //need more pages for a valid packet
					else if (packetoutResult < 0)
					{
						//no reason to complain, already compained above
						continue;
					}
					
					// WriteLine_D("Found an oggPacket");
					if (vorbis_synthesis(&vorbBlock, &oggPacket) == 0)
					{
						vorbis_synthesis_blockin(&vorbState, &vorbBlock);
					}
					
					//NOTE: pcmData[0] = left, pcmData[1] = right (if the audio is stereo, otherwise there's only one channel)
					r32** pcmData = nullptr;
					i32 numSamples = 0;
					numSamples = vorbis_synthesis_pcmout(&vorbState, &pcmData);
					while (numSamples > 0)
					{
						bool audioClipped = false;
						int numSamplesRead = numSamples; //TODO: Can we always read all the samples at one time?
						
						// PrintLine_D("Found %d samples", numSamplesRead);
						
						u32 totalNewSamples = (u32)(numSamples * numChannels);
						if (loadInfoOnly)
						{
							totalNumSamples += totalNewSamples;
						}
						else
						{
							ExtendSampleBuffer(&sampleBuffer, sampleBuffer.numSamples + totalNewSamples);
							Assert(sampleBuffer.samples != nullptr);
							Assert(sampleBuffer.numSamplesAlloc >= sampleBuffer.numSamples + totalNewSamples);
							
							for (u8 chIndex = 0; chIndex < numChannels; chIndex++) //loop over channels
							{
								r32* channelPntr = pcmData[chIndex];
								for (u32 sIndex = 0; sIndex < (u32)numSamples; sIndex++) //loop for samples for a single channel
								{
									r32 pcmSampleValue = channelPntr[sIndex];
									i32 sampleValue = FloorR32i(pcmSampleValue * 32767.f + 0.5f);
									if (sampleValue >  32767) { sampleValue =  32767; audioClipped = true; }
									if (sampleValue < -32768) { sampleValue = -32768; audioClipped = true; }
									
									// r32 time = (r32)(sampleBuffer.numSamples/numChannels + sIndex) / (r32)soundOut->frequency;
									// if (chIndex == 0 && time >= 0.49f && time < 0.5f)
									// {
									// 	PrintLine_D("[%u]: %d %f", sampleBuffer.numSamples + sIndex*numChannels + chIndex, sampleValue, pcmSampleValue);
									// }
									
									i16* samplePntr = &sampleBuffer.samples[sampleBuffer.numSamples + sIndex*numChannels + chIndex];
									Assert(samplePntr >= sampleBuffer.samples);
									Assert(samplePntr < sampleBuffer.samples + sampleBuffer.numSamplesAlloc);
									*samplePntr = (i16)(sampleValue);
								}
							}
							sampleBuffer.numSamples += totalNewSamples;
						}
						
						// if (audioClipped) { WriteLine_W("WARNING: Audio samples clipped in audio file"); } //TODO: Re-enable me when we don't have any clipped audio files!
						vorbis_synthesis_read(&vorbState, numSamplesRead); //tell vorbis that we read those samples
						numSamples = vorbis_synthesis_pcmout(&vorbState, &pcmData); //grab more samples
					}
				}
				if (ogg_page_eos(&oggPage))
				{
					endOfStream = true;
				}
			}
			
			//read more data
			if (!endOfStream)
			{
				if (fileIndex < fileSize)
				{
					syncBuffer = ogg_sync_buffer(&syncState, OGG_READ_BUFFER_SIZE);
					u32 numBytesToRead = MinU32(fileSize - fileIndex, OGG_READ_BUFFER_SIZE);
					// PrintLine_D("Reading %u more bytes from file", numBytesToRead);
					MyMemCopy(syncBuffer, filePntr + fileIndex, numBytesToRead); //read some data into the buffer from our file
					fileIndex += numBytesToRead;
					ogg_sync_wrote(&syncState, numBytesToRead); //tell the syncState we read some bytes into the buffer
				}
				else
				{
					endOfStream = true;
				}
			}
		}
	}
	
	// PrintLine_I("TotalNumSamples: %u (%u channels, %u per channel) %u alloc", sampleBuffer.numSamples, numChannels, sampleBuffer.numSamples / numChannels, sampleBuffer.numSamplesAlloc);
	
	if (loadInfoOnly)
	{
		DestroySampleBuffer(&sampleBuffer);
		vorbis_block_clear(&vorbBlock);
		vorbis_dsp_clear(&vorbState);
		ogg_stream_clear(&streamState);
		vorbis_comment_clear(&vorbComment);
		vorbis_info_clear(&vorbInfo); //this one is order dependant I guess?
		ogg_sync_clear(&syncState);
		soundOut->sampleCount = totalNumSamples;
		return true;
	}
	
	//TODO: Make this put the audio samples somewhere where the custom audio layer can use it
	// +==============================+
	// |    Pass Samples to OpenAL    |
	// +==============================+
	if (passToOpenAl)
	{
		alGenBuffers(1, &soundOut->id);
		PrintOpenAlError("alGenBuffers");
		alBufferData(soundOut->id, soundOut->format, sampleBuffer.samples, sampleBuffer.numSamples * sizeof(i16), soundOut->frequency);
		soundOut->sampleCount = sampleBuffer.numSamples;
		ALCenum error = alGetError();
		if (error != AL_NO_ERROR)
		{
			PrintLine_D("OpenAL Error after alBufferData: %s", GetOpenAlErrorStr(error));
			alDeleteBuffers(1, &soundOut->id);
			DestroySampleBuffer(&sampleBuffer);
			vorbis_block_clear(&vorbBlock);
			vorbis_dsp_clear(&vorbState);
			ogg_stream_clear(&streamState);
			vorbis_comment_clear(&vorbComment);
			vorbis_info_clear(&vorbInfo); //this one is order dependant I guess?
			ogg_sync_clear(&syncState);
			return false;
		}
	}
	
	if (sampleBufferOut != nullptr) { MyMemCopy(sampleBufferOut, &sampleBuffer, sizeof(SampleBuffer_t)); }
	else { DestroySampleBuffer(&sampleBuffer); }
	vorbis_block_clear(&vorbBlock);
	vorbis_dsp_clear(&vorbState);
	ogg_stream_clear(&streamState);
	vorbis_comment_clear(&vorbComment);
	vorbis_info_clear(&vorbInfo); //this one is order dependant I guess?
	ogg_sync_clear(&syncState);
	return true;
}
#else
bool ParseOggFile(FileInfo_t* file, Sound_t* soundOut, SampleBuffer_t* sampleBufferOut = nullptr, bool passToOpenAl = true)
{
	return true;
}
#endif //AUDIO_ENABLED

