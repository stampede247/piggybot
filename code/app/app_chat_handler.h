/*
File:   app_chat_handler.h
Author: Taylor Robbins
Date:   10\26\2020
*/

#ifndef _APP_CHAT_HANDLER_H
#define _APP_CHAT_HANDLER_H

typedef enum
{
	ChatterFlag_Follower   = 0x01,
	ChatterFlag_Subscriber = 0x02,
	ChatterFlag_Regular    = 0x04,
	ChatterFlag_Moderator  = 0x08,
} ChatterFlags_t;

struct Chatter_t
{
	u32 id;
	u8 flags;
	char* username;
	Color_t displayColor;
	u64 joinTime;
	u64 lastActivityTime;
};

typedef enum
{
	ChatEventType_None = 0,
	ChatEventType_Message,
	ChatEventType_Follow,
	ChatEventType_Subscription,
	ChatEventType_RewardRedeemed,
	ChatEventType_NumTypes,
} ChatEventType_t;

typedef enum
{
	ChatEventFlag_Handled = 0x01,
} ChatEventFlag_t;

struct ChatEvent_t
{
	u8 flags;
	ChatEventType_t type;
	u64 time;
	
	char* message;
	u32 chatterId;
};

struct ChatHandler_t
{
	bool created;
	IrcConnection_t connection;
	bool didLogin;
	ConsoleArena_t log;
	bool gotJoinResponse;
	char* ourPrefix;
	bool sentCapRequests;
	
	u32 nextChatterId;
	DynArray_t chatters;
	DynArray_t events;
};

#define MAX_IRC_ARGUMENTS 15

struct IrcCommand_t
{
	const char* messagePntr;
	u32 messageLength;
	
	StrSplitPiece_t prefix;
	StrSplitPiece_t command;
	u32 numArgs;
	StrSplitPiece_t args[MAX_IRC_ARGUMENTS];
};

const char* GetChatEventTypeStr(ChatEventType_t type)
{
	switch (type)
	{
		case ChatEventType_None:           return "None";
		case ChatEventType_Message:        return "Message";
		case ChatEventType_Follow:         return "Follow";
		case ChatEventType_Subscription:   return "Subscription";
		case ChatEventType_RewardRedeemed: return "RewardRedeemed";
		default: return "Unknown";
	}
}

#endif //  _APP_CHAT_HANDLER_H
