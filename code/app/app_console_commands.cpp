/*
File:   app_console_commands.cpp
Author: Taylor Robbins
Date:   03\22\2019
Description: 
	** Holds the HandleConsoleCommand function
*/

const char* consoleCommands[] =
{
	// +==============================+
	// |       General Commands       |
	// +==============================+
	"help {page}",               "Displays a list of available commands. A page number can be passed optionally",
	"test",                      "Used for testing various things",
	"memory",                    "Displays some information about memory being used by the game",
	"memory_meta",               "Displays some meta information about the memory arenas",
	"states",                    "Displays information about the currently running/initialized AppStates",
	"levels",                    "Prints out a list of all the levels that are currently indexed",
	"resources",                 "Prints out a list of all the resources loaded by the game",
	"settings",                  "Prints out a list of the application settings and their current values",
	"dev_menu",                  "Jumps to the DevMenu AppState",
	"monitor [index]",           "Displays some information about a specific monitor",
	"email",                     "Tests opening an email window to PrincessCastleQuest@gmail.com",
	"http",                      "Tests making an http request using WinHttp",
	"mouse_over",                "Toggles a display for the name of what the mouse is hovering over",
	"font [size] [name]",        "Tests opening a platform font file with the specified name and size",
	"notify [message]",          "Push a test notification message",
	"threads",                   "Displays some information about the currently running threads",
	"assert",                    "Runs Assert(false)",
	"debug",                     "Toggles the debug overlay",
	"dump",                      "Dumps the contents of the debug output to a file and opens a file browser showing that file",
	#if AUDIO_ENABLED
	"audio_debug",               "Prints out some information about the audio to try and help debug problems",
	"play_sound {soundName}",    "Plays the sound specified or a default sound if none is specified",
	#endif
	#if DEVELOPER
	"dev_mode",                  "Enables developer mode",
	#endif
	"track_key [button]",        "Adds a keyboard button to the list of tracks buttons that are displayed by the key tracker display",
	"clear_tracker",             "Removes all of the the tracked buttons from the key tracker",
	"gpu_debug",                 "Enables/Disables a visual display of the graphics card stats like memory usage",
	"texture_sizes",             "Prints out the list of resource textures and sprite sheets and their corresponding GPU memory size",
	"show_resources",            "Enables/Disabled the display of all of the loaded resources",
	#if DEBUG
	"break",                     "Triggers a debug break-point",
	#endif
	"reload [resource_name]",    "Reloads a specific resource by name",
	"args",                      "Prints out the program arguments that were passed to the application at startup",
	"console_arena",             "Toggles a little visual display of the memory usage for the debug console text lines",
	"connect",                   "Starts the chat connection process",
	"disconnect",                "Closes the current chat connection forcibly (no QUIT message)",
	"quit",                      "Sends the quit message to the server through the connected IRC socket",
	"send [raw_irc_msg]",        "Sends the raw_irc_msg across the currently open irc connection",
	"send_irc [irc_command] {args}", "Sends the specified command with the arguments with the prefix automatically attached",
	"api_connect",               "Starts the Twitch API connection process",
	"api_disconnect",            "Stops the Twitch API connection process",
	
	// +==============================+
	// |    Serialization Commands    |
	// +==============================+
	"reload_settings",        "Reloads the settings from the disk",
	"save_settings",          "Saves the current settings to the disk",
	"export_sound [path]",    "Loads a sound byte from a specified file and exports it as raw audio samples formatted for use as a static array of i16 values in C",
	"folder [purpose]",       "Looks up a special folder by some purpose. The current options are \"Saves\" \"Settings\" \"Screenshots\" and \"Custom Levels\"",
	"appdata",                "Opens the AppData folder",
	
	// +==============================+
	// |        Other Commands        |
	// +==============================+
	"show_gutter",   "Enables the gutter line numbers in the debug console",
	"hide_gutter",   "Disables the gutter line numbers in the debug console",
	"toggle_gutter", "Toggles the gutter line numbers in the debug console",
	"show_files",    "Enables the file names display in the debug console",
	"hide_files",    "Disables the file names display in the debug console",
	"toggle_files",  "Toggles the file names display in the debug console",
	"show_lines",    "Enables the file lines numbers in the debug console",
	"hide_lines",    "Disables the file lines numbers in the debug console",
	"toggle_lines",  "Toggles the file lines numbers in the debug console",
	"show_funcs",    "Enables the function names display in the debug console",
	"hide_funcs",    "Disables the function names display in the debug console",
	"toggle_funcs",  "Toggles the function names display in the debug console",
};

void TestHttpRequestCallback(bool success, const HttpRequest_t* request, const char* responseData, u32 responseLength)
{
	PrintLineAt(success ? DbgLevel_Info : DbgLevel_Error, "Test HTTP Request %s", success ? "Succeeded" : "Failed");
	if (success)
	{
		PrintLine_I("Got %u bytes of response data:", responseLength);
		WriteLine_D(responseData);
	}
}

//NOTE: Also defined in app_func_defs.h
void HandleConsoleCommand(const char* command)
{
	u32 commandLength = MyStrLength32(command);
	u32 numPieces = CountStringPartsWithQuotes(command, commandLength, " ", 1);
	i32 firstSpaceIndex = FindChar(command, commandLength, ' ');
	const char* baseCmd = (char*)command;
	const char* argsStr = (char*)(command + commandLength);
	if (firstSpaceIndex >= 0) { baseCmd = ArenaString(TempArena, command, (u32)firstSpaceIndex); argsStr = &command[firstSpaceIndex+1]; }
	// PrintLine_I("Command: \"%s\" Args: \"%s\"", baseCmd, argsStr);
	
	// +--------------------------------------------------------------+
	// |                       General Commands                       |
	// +--------------------------------------------------------------+
	// +==============================+
	// |             help             |
	// +==============================+
	HandleFirstCommand("help")
	{
		StartCmdArgs(1);
		DefCmdArgU32("index", false); argDef->allowParseErrors = true;
		ParseCmdArgs(argsStr, args, numArgs);
		
		if (WasCmdArgValid(&args, "index"))
		{
			u32 commandIndex = GetCmdArgU32(&args, "index");
			if (commandIndex*2 >= ArrayCount(consoleCommands)) { PrintLine_E("Invalid command index. There are only %u commands", ArrayCount(consoleCommands)/2); return; }
			PrintLine_I("%s - %s", consoleCommands[commandIndex*2 + 0], consoleCommands[commandIndex*2 + 1]);
		}
		else if (WasCmdArgGiven(&args, "index"))
		{
			const StrSplitPiece_t* commandName = GetCmdArgRaw(&args, "index");
			Assert(commandName != nullptr);
			bool foundCommand = false;
			for (u32 cIndex = 0; cIndex < ArrayCount(consoleCommands); cIndex += 2)
			{
				if (StrCompareIgnoreCase(consoleCommands[cIndex], commandName->pntr, commandName->length))
				{
					PrintLine_I("%s - %s", consoleCommands[cIndex], consoleCommands[cIndex + 1]);
					foundCommand = true;
					break;
				}
			}
			if (!foundCommand) { PrintLine_E("Unknown command \"%.*s\"", commandName->length, commandName->pntr); }
		}
		else
		{
			PrintLine_N("%u Commands:", ArrayCount(consoleCommands)/2);
			for (u32 cIndex = 0; cIndex < ArrayCount(consoleCommands); cIndex += 2)
			{
				PrintLine_I("[%d] %s", cIndex/2, consoleCommands[cIndex]);
			}
		}
	}
	// +==============================+
	// |             test             |
	// +==============================+
	HandleCommand("test")
	{
		// PrintLine_I("Nothing to test right now");
		
		// if (platform->OpenIrcConnection(TWITCH_IRC_URL, IRC_NORMAL_PORT, &app->ircConnection, mainHeap, Kilobytes(4)))
		// {
		// 	PrintLine_I("Connecting to %s:%u", TWITCH_IRC_URL, IRC_NORMAL_PORT);
		// }
		// else
		// {
		// 	WriteLine_E("Failed to start IRC connection");
		// }
		
		PrintLine_D("OurPrefix: %s", app->chat.ourPrefix);
	}
	// +==============================+
	// |            memory            |
	// +==============================+
	HandleCommand("memory")
	{
		PrintLine_I("Static Heap: %s / %s (%%%.2f filled)", FormattedSizeStr(staticHeap->used), FormattedSizeStr(staticHeap->size), ((r32)staticHeap->used / (r32)staticHeap->size) * 100.0f);
		PrintLine_I("Main Heap: %s / %s (%%%.2f filled)", FormattedSizeStr(mainHeap->used), FormattedSizeStr(mainHeap->size), ((r32)mainHeap->used / (r32)mainHeap->size) * 100.0f);
		PrintLine_I("TempArena: %s / %s (%%%.2f filled) %u marks", FormattedSizeStr(TempArena->used), FormattedSizeStr(TempArena->size), ((r32)TempArena->used / (r32)TempArena->size) * 100.0f, ArenaNumMarks(TempArena));
		u32 fifoSize = app->dbgConsole.arena.fifoSize;
		u32 fifoUsage = app->dbgConsole.arena.fifoUsage;
		r32 fifoUsagePercentage = ((r32)fifoUsage / (r32)fifoSize) * 100.0f;
		u32 numLines = app->dbgConsole.arena.numLines;
		PrintLine_I("Console Fifo: %s / %s (%%%.2f filled) %u lines", FormattedSizeStr(fifoUsage), FormattedSizeStr(fifoSize), fifoUsagePercentage, numLines);
		WriteLine_I("");
		PrintLine_I("AppData_t          %s", FormattedSizeStr(sizeof(AppData_t)));
		PrintLine_I("SettingsMenuData_t %s", FormattedSizeStr(sizeof(SettingsMenuData_t)));
		PrintLine_I("VisualizerData_t   %s", FormattedSizeStr(sizeof(VisualizerData_t)));
		PrintLine_I("DevMenuData_t      %s", FormattedSizeStr(sizeof(DevMenuData_t)));
		PrintLine_I("GifferData_t       %s", FormattedSizeStr(sizeof(GifferData_t)));
	}
	// +==============================+
	// |         memory_meta          |
	// +==============================+
	HandleCommand("memory_meta")
	{
		#if MEMORY_ARENA_META_INFO_ENABLED
		
		StartCmdArgs(2);
		DefCmdArgU32("start_index", false);
		DefCmdArgU32("num_to_display", false);
		ParseCmdArgs(argsStr, args, numArgs);
		
		u32 totalMetaSize = 0;
		u32 numMetaEntries = 0;
		ArenaAllocMetaInfo_t* currentInfo = mainHeap->firstMetaInfo;
		while (currentInfo != nullptr)
		{
			totalMetaSize += currentInfo->allocSize;
			numMetaEntries++;
			currentInfo = currentInfo->next;
		}
		
		PrintLine_I("Found meta info for %s / %s", FormattedSizeStr(totalMetaSize), FormattedSizeStr(mainHeap->used));
		PrintLine_I("%u Meta Entries:", numMetaEntries);
		
		u32 startIndex = TryGetCmdArgU32(args, "start_index", 0);
		u32 numToDisplay = TryGetCmdArgU32(args, "num_to_display", 50);
		
		u32 mIndex = 0;
		currentInfo = mainHeap->firstMetaInfo;
		while (currentInfo != nullptr)
		{
			if (mIndex >= startIndex)
			{
				char* fileName = (char*)(currentInfo+1);
				char* funcName = fileName + (currentInfo->fileNameLength+1);
				PrintLine_I("  [%u] %u Bytes (%p): %s:%u %s", mIndex, currentInfo->allocSize, currentInfo->allocPntr, fileName, currentInfo->lineNumber, funcName);
			}
			mIndex++;
			if (mIndex >= startIndex + numToDisplay) { break; }
			currentInfo = currentInfo->next;
		}
		
		#else
		WriteLine_E("Arena meta info not enabled");
		#endif
	}
	// +==============================+
	// |            states            |
	// +==============================+
	HandleCommand("states")
	{
		PrintLine_R("%u Active AppStates:", app->numActiveStates);
		for (u32 sIndex = 0; sIndex < app->numActiveStates; sIndex++)
		{
			AppState_t appState = app->activeStates[sIndex];
			const char* appStateName = GetAppStateName(appState);
			PrintLine_I("  [%u]: %s", sIndex, appStateName);
		}
		WriteLine_R("Initialized AppStates:");
		for (u32 sIndex = 0; sIndex < AppState_NumStates; sIndex++)
		{
			AppState_t appState = (AppState_t)(sIndex+1);
			const char* appStateName = GetAppStateName(appState);
			bool isInitialized = app->appStateInitialized[appState];
			if (isInitialized) { PrintLine_I("  %s: Initialized", appStateName); }
			else { PrintLine_W("  %s: Deinitialized", appStateName); }
		}
		
		PrintLine_R("%u Active AppMenus:", app->numActiveMenus);
		for (u32 sIndex = 0; sIndex < app->numActiveMenus; sIndex++)
		{
			AppMenu_t appMenu = app->activeMenus[sIndex];
			const char* appMenuName = GetAppMenuName(appMenu);
			PrintLine_I("  [%u]: %s", sIndex, appMenuName);
		}
		WriteLine_R("Initialized AppMenus:");
		for (u32 sIndex = 0; sIndex < AppMenu_NumMenus; sIndex++)
		{
			AppMenu_t appMenu = (AppMenu_t)(sIndex+1);
			const char* appMenuName = GetAppMenuName(appMenu);
			bool isInitialized = app->appMenuInitialized[appMenu];
			if (isInitialized) { PrintLine_I("  %s: Initialized", appMenuName); }
			else { PrintLine_W("  %s: Deinitialized", appMenuName); }
		}
	}
	// +==============================+
	// |          resources           |
	// +==============================+
	HandleCommand("resources")
	{
		ResourceType_t lastResourceType = ResourceType_NumTypes;
		for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
		{
			Resource_t* resource = GetResourceByResourceIndex(rIndex);
			Assert(resource != nullptr);
			if (resource->type != lastResourceType)
			{
				PrintLine_R("%u %s Resources:", GetNumResourcesOfType(resource->type), GetResourceTypeStr(resource->type));
				lastResourceType = resource->type;
			}
			if (IsResourceLoaded(resource))
			{
				PrintLine_I("  %s[%u]: %s \"%s\"", GetResourceTypeStr(resource->type), resource->subIndex, resource->readableName, (resource->filePath != nullptr) ? resource->filePath : "");
			}
			else
			{
				PrintLine_W("  %s[%u]: %s", GetResourceTypeStr(resource->type), resource->subIndex, resource->readableName);
			}
		}
	}
	// +==============================+
	// |           settings           |
	// +==============================+
	HandleCommand("settings")
	{
		Assert(sett != nullptr);
		
		u32 numSettings = GetNumAppSettings();
		PrintLine_N("%u Settings:", numSettings);
		
		for (u32 sIndex = 0; sIndex < numSettings; sIndex++)
		{
			const char* settingName = nullptr;
			void* settingValuePntr = nullptr;
			SettingType_t settingType = GetSettingInfo(sett, sIndex, &settingName, &settingValuePntr);
			Assert(settingType != SettingType_None);
			Assert(settingName != nullptr);
			Assert(settingValuePntr != nullptr);
			if (settingType == SettingType_Boolean) { bool boolValue = *((bool*)settingValuePntr); PrintLine_N("  [%3u] bool %s = %s", sIndex, settingName, boolValue ? "True" : "False"); }
			else if (settingType == SettingType_u8)  { u8  u8Value  = *((u8*)settingValuePntr);  PrintLine_N("  [%3u] u8   %s = %u",  sIndex, settingName, u8Value);  }
			else if (settingType == SettingType_u32) { u32 u32Value = *((u32*)settingValuePntr); PrintLine_N("  [%3u] u32  %s = %u",  sIndex, settingName, u32Value); }
			else if (settingType == SettingType_i32) { i32 i32Value = *((i32*)settingValuePntr); PrintLine_N("  [%3u] i32  %s = %d",  sIndex, settingName, i32Value); }
			else if (settingType == SettingType_r32) { r32 r32Value = *((r32*)settingValuePntr); PrintLine_N("  [%3u] r32  %s = %f",  sIndex, settingName, r32Value); }
			else
			{
				PrintLine_E("  [%3u] Unknown Type %s", sIndex, settingName);
			}
		}
	}
	// +==============================+
	// |           dev_menu           |
	// +==============================+
	HandleCommand("dev_menu")
	{
		if (IsAppStateActive(AppState_DevMenu)) { WriteLine_E("DevMenu is already active!"); return; }
		
		WriteLine_I("Jumping to the DevMenu AppState");
		Assert(app->allowAppStateChanges == false);
		app->allowAppStateChanges = true;
		AppStateChange_t change = {};
		change.type = AppStateChangeType_Push;
		change.newAppState = AppState_DevMenu;
		change.stayInitialized = false;
		change.forceResetNewState = false;
		app->transInfoType = TransInfoType_Nothing;
		PushAppState_(&change);
		app->allowAppStateChanges = false;
	}
	// +==============================+
	// |       monitor [index]        |
	// +==============================+
	HandleCommand("monitor")
	{
		StartCmdArgs(1);
		DefCmdArgU32Max("index", true, platform->numMonitors-1);
		ParseCmdArgs(argsStr, args, numArgs);
		
		u32 monitorIndex = GetCmdArgU32(&args, "index");
		const MonitorInfo_t* monitorInfo = &platform->monitors[monitorIndex];
		PrintLine_I("Monitor[%u]: \"%s\" Offset (%d, %d) Original (%d, %d) Scale (%.1f, %.1f) Work Area (%d, %d, %d, %d) %u modes", monitorIndex,
			monitorInfo->name, monitorInfo->offset.x, monitorInfo->offset.y,
			monitorInfo->originalOffset.x, monitorInfo->originalOffset.y,
			monitorInfo->contentScale.x, monitorInfo->contentScale.y,
			monitorInfo->workArea.x, monitorInfo->workArea.y, monitorInfo->workArea.width, monitorInfo->workArea.height,
			monitorInfo->numModes
		);
		
		for (u32 dIndex = 0; dIndex < monitorInfo->numModes; dIndex++)
		{
			const MonitorDisplayMode_t* displayMode = &monitorInfo->modes[dIndex];
			PrintLine_I("  Mode[%u]: %dx%d %ubit %.1ffps%s%s", dIndex,
				displayMode->resolution.x, displayMode->resolution.y,
				displayMode->bitDepth, displayMode->refreshRate,
				(dIndex == monitorInfo->currentMode) ? " (Current)" : "",
				(dIndex == monitorInfo->originalMode) ? " (Original)" : ""
			);
		}
	}
	// +==============================+
	// |            email             |
	// +==============================+
	HandleCommand("email")
	{
		bool success = platform->OpenEmailPrompt("PrincessCastleQuest@gmail.com", "Feedback", "This is a test!");
		if (success) { WriteLine_I("Opened an email prompt successfully!"); }
		else { WriteLine_E("Failed to open the email prompt"); }
	}
	// +==============================+
	// |             http             |
	// +==============================+
	HandleCommand("http")
	{
		if (platform->HttpRequestInProgress()) { WriteLine_E("Another request is currently in progress"); return; }
		
		HttpRequest_t request = {};
		request.url = "test.princesscastlequest.com/api/get_all_levels";
		request.requestType = HttpRequestType_Get;
		request.numPostFields = 0;
		request.postKeys[0] = "version"; request.postValues[0] = TempPrint("%u.%u(%u)", APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD);
		request.callback = TestHttpRequestCallback;
		
		if (platform->HttpRequestStart(&request))
		{
			PrintLine_D("Performing test HTTP request to %s...", request.url);
		}
		else
		{
			WriteLine_E("Failed to start HTTP request");
		}
	}
	// +==============================+
	// |          mouse_over          |
	// +==============================+
	HandleCommand("mouse_over")
	{
		app->showMouseOverName = !app->showMouseOverName;
		PrintLine_I("%s Mouse Hover Name", app->showMouseOverName ? "Showing" : "Hiding");
	}
	// +==============================+
	// |      font [size] [name]      |
	// +==============================+
	HandleCommand("font")
	{
		StartCmdArgs(2);
		DefCmdArgI32("size", true);
		DefCmdArgString("name", true);
		ParseCmdArgs(argsStr, args, numArgs);
		
		i32 fontSize = GetCmdArgI32(&args, "size");
		const StrSplitPiece_t* fontNamePiece = GetCmdArgString(&args, "name");
		char* fontName = ArenaNtString(TempArena, fontNamePiece->pntr);
		Assert(fontName != nullptr);
		
		PlatformFont_t platFont = {};
		if (platform->OpenPlatformFont(fontName, fontSize, false, false, &platFont))
		{
			PrintLine_I("Opened font \"%s\" at size %d", fontName, fontSize);
			PrintLine_I("Font file is %s", FormattedSizeStr(platFont.dataSize));
			for (u32 bIndex = 0; bIndex < platFont.dataSize && bIndex < 32; bIndex++)
			{
				if ((bIndex%16) == 0) { if (bIndex > 0) { WriteLine_I(""); } }
				else { Write_I(" "); }
				Print_I("0x%02X", platFont.dataPntr[bIndex]);
			}
			WriteLine_I("");
			platform->WriteEntireFile("output.ttf", platFont.dataPntr, platFont.dataSize);
			platform->ClosePlatformFont(&platFont);
		}
		else
		{
			PrintLine_E("Failed to open font \"%s\" at size %d", fontName, fontSize);
		}
	}
	// +==============================+
	// |       notify [message]       |
	// +==============================+
	HandleCommand("notify")
	{
		StartCmdArgs(1);
		DefCmdArgString("message", true);
		ParseCmdArgs(argsStr, args, numArgs);
		const StrSplitPiece_t* message = GetCmdArgString(&args, "message");
		Notify_I(message->pntr);
	}
	// +==============================+
	// |           threads            |
	// +==============================+
	HandleCommand("threads")
	{
		PrintLine_I("MainThreadId: %u (Current ThreadId: %u)", MainThreadId, platform->GetThisThreadId());
		PrintLine_I("numAvailableTaskThreads: %u", appInput->numAvailableTaskThreads);
		PrintLine_I("numActiveThreadedTasks: %u", appInput->numActiveThreadedTasks);
		PrintLine_I("numQueuedThreadedTasks: %u", appInput->numQueuedThreadedTasks);
		PrintLine_I("numTotalThreadedTasks: %u", appInput->numTotalThreadedTasks);
		
	}
	// +==============================+
	// |            assert            |
	// +==============================+
	HandleCommand("assert")
	{
		Assert(false);
	}
	// +==============================+
	// |           assert2            |
	// +==============================+
	HandleCommand("assert2")
	{
		Assert(false);
		Assert(false && false);
	}
	// +==============================+
	// |            debug             |
	// +==============================+
	HandleCommand("debug")
	{
		app->showDebugOverlay = !app->showDebugOverlay;
		PrintLine_I("%s Debug Overlay", app->showDebugOverlay ? "Showing" : "Hiding");
	}
	// +==============================+
	// |             dump             |
	// +==============================+
	HandleCommand("dump")
	{
		u32 crashDumpSize = 0;
		char* crashDump = nullptr;
		crashDump = DbgConsoleCrashDump(&app->dbgConsole, mainHeap, &crashDumpSize,
			nullptr, nullptr, 0, nullptr,
			&SystemTime, ProgramTime
		);
		
		if (crashDump != nullptr)
		{
			char* crashFilename = TempPrint("dump_%u-%u-%u_%u-%u-%u.txt", LocalTime.year, LocalTime.month+1, LocalTime.day+1, LocalTime.hour, LocalTime.minute, LocalTime.second);
			Assert(crashFilename != nullptr);
			char* crashFilePath = platform->CompleteRelativePath(TempArena, crashFilename, MyStrLength32(crashFilename));
			Assert(crashFilePath != nullptr);
			if (platform->WriteEntireFile(crashFilePath, crashDump, crashDumpSize))
			{
				PrintLine_I("Saved debug dump to \"%s\"", crashFilePath);
				bool showSuccess = platform->ShowFile(crashFilePath);
				if (!showSuccess) { WriteLine_E("Failed open file window showing the dump file location"); }
			}
			ArenaPop(mainHeap, crashDump);
		}
		else
		{
			WriteLine_E("Couldn't generate debug dump");
		}
	}
	#if AUDIO_ENABLED
	// +==============================+
	// |         audio_debug          |
	// +==============================+
	HandleCommand("audio_debug")
	{
		PrintLineAt(sett->masterSoundEnabled  ? DbgLevel_Info : DbgLevel_Warning, "Master Sound %s: %.0f%%",  sett->masterSoundEnabled  ? "Enabled" : "Disabled", sett->masterVolume      * 100);
		PrintLineAt(sett->musicEnabled        ? DbgLevel_Info : DbgLevel_Warning, "Music %s: %.0f%%",         sett->musicEnabled        ? "Enabled" : "Disabled", sett->musicVolume       * 100);
		PrintLineAt(sett->soundEffectsEnabled ? DbgLevel_Info : DbgLevel_Warning, "Sound Effects %s: %.0f%%", sett->soundEffectsEnabled ? "Enabled" : "Disabled", sett->soundEffectVolume * 100);
		
		ALboolean enumeration = alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT");
		PrintOpenAlError("alcIsExtensionPresent");
		if (enumeration != AL_FALSE)
		{
			const ALCchar* devices = alcGetString(NULL, ALC_DEVICE_SPECIFIER);
			PrintOpenAlError("alcGetString");
			const ALCchar *device = devices, *next = devices + 1;
			size_t len = 0;
			u32 dIndex = 0;
			PrintLine_N("OpenAL Devices:");
			while (device && *device != '\0' && next && *next != '\0')
			{
				PrintLine_I("  [%u]: %s", dIndex, device);
				len = MyStrLength(device);
				device += (len + 1);
				next += (len + 2);
				dIndex++;
			}
		}
		
		//TODO: Re-enable this printout when the sound instances are working with the custom audio layer
		u32 numSndInstances = 0;
		for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
		{
			if (app->soundInstances[sIndex].playing) { numSndInstances++; }
		}
		PrintLineAt((numSndInstances < MAX_SOUND_INSTANCES) ? DbgLevel_Notify : DbgLevel_Error, "%u/%u Sound Instances Active", numSndInstances, MAX_SOUND_INSTANCES);
		for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
		{
			SoundInstance_t* instance = &app->soundInstances[sIndex];
			if (instance->playing)
			{
				const char* sndName = nullptr;
				for (u32 rIndex = 0; rIndex < ArrayCount(soundsList->sounds); rIndex++)
				{
					SoundResource_t* sndResource = &soundsList->sounds[rIndex];
					if (instance->soundId == sndResource->sound.id)
					{
						sndName = sndResource->readableName;
						break;
					}
				}
				for (u32 mIndex = 0; mIndex < NUM_RESOURCE_MUSICS; mIndex++)
				{
					Resource_t* musicResource = GetResourceByType(ResourceType_Music, mIndex);
					Assert(musicResource != nullptr);
					if (IsResourceLoaded(musicResource) && instance->soundId == musicResource->music.id)
					{
						Assert(musicResource->filePath != nullptr);
						sndName = GetFileNamePart(musicResource->filePath);
						break;
					}
				}
				if (sndName != nullptr)
				{
					PrintLine_I("  [%u]: %s %s %.1f%%", sIndex, sndName, FormattedMilliseconds(TimeSince(instance->startTime)), instance->distVolume * 100);
				}
				else
				{
					PrintLine_W("  [%u]: Unknown SoundId %u", sIndex, instance->soundId);
				}
			}
		}
		
		WriteLine_N("Supported OpenAL Extensions:");
		Write_I("{");
		for (u32 eIndex = 0; eIndex < AlExtension_NumExtensions; eIndex++)
		{
			ALboolean supported = alIsExtensionPresent((const ALchar*)GetAlExtensionName((AlExtension_t)eIndex, true));
			if (supported) { Print_I(" %s", GetAlExtensionName((AlExtension_t)eIndex, false)); }
		}
		WriteLine_I(" }");
		Write_I("{");
		for (u32 eIndex = 0; eIndex < AlcExtension_NumExtensions; eIndex++)
		{
			ALCboolean supported = alcIsExtensionPresent(platform->alDevice, (const ALCchar*)GetAlcExtensionName((AlcExtension_t)eIndex, true));
			if (supported) { Print_I(" %s", GetAlcExtensionName((AlcExtension_t)eIndex, false)); }
		}
		WriteLine_I(" }");
		
		if (soundsList != nullptr && soundsList->filled)
		{
			u32 numLoadedSounds = 0;
			for (u32 sIndex = 0; sIndex < NUM_SOUND_EFFECT_RESOURCES; sIndex++)
			{
				SoundResource_t* sndResource = &soundsList->sounds[sIndex];
				if (sndResource->sound.isValid) { numLoadedSounds++; }
			}
			PrintLineAt((numLoadedSounds > 0) ? DbgLevel_Notify : DbgLevel_Error, "Sounds Loaded: %u/%u", numLoadedSounds, NUM_SOUND_EFFECT_RESOURCES);
			
			SoundResource_t* testSound = &soundsList->ledOnSound;
			if (testSound->sound.isValid)
			{
				r64 soundLengthMs = (1.0 / (r64)testSound->sound.frequency) * (r64)testSound->sound.sampleCount * 1000.0;
				PrintLine_I("%s: \"%s\" Valid (%s)", testSound->internalName, testSound->filePath, FormattedMillisecondsR64(soundLengthMs));
			}
			else
			{
				PrintLine_W("%s: \"%s\" Invalid ", testSound->internalName, testSound->filePath);
			}
		}
		else { WriteLine_E("soundsList hasn't been initialized"); }
	}
	// +==============================+
	// |          play_sound          |
	// +==============================+
	HandleCommand("play_sound")
	{
		StartCmdArgs(1);
		DefCmdArgString("sound_name", false);
		ParseCmdArgs(argsStr, args, numArgs);
		
		SoundResource_t* sndResource = &soundsList->ledOnSound;
		if (WasCmdArgValid(&args, "sound_name"))
		{
			const StrSplitPiece_t* specifiedSndStr = GetCmdArgString(&args, "sound_name");
			bool foundSound = false;
			for (u32 sIndex = 0; sIndex < NUM_SOUND_EFFECT_RESOURCES; sIndex++)
			{
				SoundResource_t* newResource = &soundsList->sounds[sIndex];
				if (StrCompareIgnoreCaseNt(specifiedSndStr->pntr, newResource->internalName) ||
					StrCompareIgnoreCaseNt(specifiedSndStr->pntr, newResource->readableName) ||
					StrCompareIgnoreCaseNt(specifiedSndStr->pntr, newResource->filePath) ||
					StrCompareIgnoreCaseNt(specifiedSndStr->pntr, GetFileNamePart(newResource->filePath)))
				{
					sndResource = newResource;
					foundSound = true;
					break;
				}
			}
			if (!foundSound)
			{
				PrintLine_E("Unknown sound specifier: \"%s\"", specifiedSndStr->pntr);
				return;
			}
		}
		
		if (sndResource->sound.isValid)
		{
			r64 soundLengthMs = (1.0 / (r64)sndResource->sound.frequency) * (r64)sndResource->sound.sampleCount * 1000.0;
			//TODO: Re-enable me when this works with the custom audio layer
			SoundInstance_t* instance = StartSndInstance(&sndResource->sound, false, 1.0f, 1.0f);
			if (instance != nullptr)
			{
				PrintLine_I("Playing %s: \"%s\" is Valid (%s)", sndResource->internalName, sndResource->filePath, FormattedMillisecondsR64(soundLengthMs));
			}
			else
			{
				WriteLine_E("Couldn't play the sound effect because the instance buffer was full");
			}
		}
		else
		{
			PrintLine_W("%s: \"%s\" is Invalid ", sndResource->internalName, sndResource->filePath);
		}
	}
	#else //!AUDIO_ENABLED
	HandleCommand("audio_debug")
	{
		WriteLine_E("Audio is disabled by the build options");
	}
	HandleCommand("play_sound")
	{
		WriteLine_E("Audio is disabled by the build options");
	}
	#endif //AUDIO_ENABLED
	// +==============================+
	// |           dev_mode           |
	// +==============================+
	HandleCommand("dev_mode")
	{
		app->developerModeEnabled = !app->developerModeEnabled;
		PrintLine_I("Developer Mode %s", app->developerModeEnabled ? "Enabled" : "Disabled");
	}
	// +==============================+
	// |      track_key [button]      |
	// +==============================+
	HandleCommand("track_key")
	{
		StartCmdArgs(1);
		DefCmdArgString("key", true);
		ParseCmdArgs(argsStr, args, numArgs);
		const StrSplitPiece_t* keyStr = GetCmdArgString(&args, "key");
		
		Buttons_t button = Buttons_NumButtons;
		for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++)
		{
			Buttons_t nextButton = (Buttons_t)bIndex;
			const char* nextButtonName = GetButtonName(nextButton);
			Assert(nextButtonName != nullptr);
			u32 nextButtonNameLength = MyStrLength32(nextButtonName);
			if (keyStr->length == nextButtonNameLength && StrCompareIgnoreCase(keyStr->pntr, nextButtonName, nextButtonNameLength))
			{
				button = nextButton;
				break;
			}
		}
		if (button >= Buttons_NumButtons) { PrintLine_E("Unknown button \"%.*s\"", keyStr->length, keyStr->pntr); return; }
		
		KeyTrackerButton_t* newKey = DynArrayAdd(&app->trackedKeys, KeyTrackerButton_t);
		Assert(newKey != nullptr);
		ClearPointer(newKey);
		newKey->button = button;
		
		PrintLine_I("Added Button \"%s\" to the list of tracked keys", GetButtonName(button));
	}
	// +==============================+
	// |        clear_tracker         |
	// +==============================+
	HandleCommand("clear_tracker")
	{
		PrintLine_I("Removed %u key%s from the key tracker", app->trackedKeys.length, (app->trackedKeys.length == 1) ? "" : "s");
		DynArrayClear(&app->trackedKeys);
	}
	// +==============================+
	// |          gpu_debug           |
	// +==============================+
	HandleCommand("gpu_debug")
	{
		if (!app->showGpuStats)
		{
			app->showGpuStats = true;
			WriteLine_I("Enabled GPU Stats Display");
			PrintLine_D("GLFW Version: %d.%d(%d)", platform->glfwVersion.major, platform->glfwVersion.minor, platform->glfwVersion.revision);
			PrintLine_D("OpenGL Version: %u.%u", platform->glfwVersion.major, platform->glfwVersion.minor);
		}
		else
		{
			app->showGpuStats = false;
			WriteLine_W("Enabled GPU Stats Display");
		}
	}
	// +==============================+
	// |        texture_sizes         |
	// +==============================+
	HandleCommand("texture_sizes")
	{
		PrintLine_N("%u Textures:", NUM_RESOURCE_TEXTURES);
		u64 totalTexturesSize = 0;
		for (u32 tIndex = 0; tIndex < NUM_RESOURCE_TEXTURES; tIndex++)
		{
			Resource_t* textureResource = GetResourceByType(ResourceType_Texture, tIndex);
			Assert(textureResource != nullptr);
			if (IsResourceLoaded(textureResource))
			{
				u64 textureSize = (u64)textureResource->texture.width*(u64)textureResource->texture.height*4;
				PrintLine_I("  [%u] %s: %dx%d (%s)", tIndex, GetFileNamePart(GetTextureFilePath(tIndex)), textureResource->texture.width, textureResource->texture.height, FormattedSizeStr(textureSize));
				totalTexturesSize += textureSize;
			}
			else
			{
				PrintLine_W("  [%u] %s: Not-loaded", tIndex, GetFileNamePart(GetTextureFilePath(tIndex)));
			}
		}
		PrintLine_O("Textures: %s", FormattedSizeStr(totalTexturesSize));
		PrintLine_N("%u Sprite Sheets:", NUM_RESOURCE_SHEETS);
		u64 totalSpriteSheetsSize = 0;
		for (u32 sIndex = 0; sIndex < NUM_RESOURCE_SHEETS; sIndex++)
		{
			Resource_t* sheetResource = GetResourceByType(ResourceType_SpriteSheet, sIndex);
			Assert(sheetResource != nullptr);
			if (IsResourceLoaded(sheetResource))
			{
				u64 textureSize = (u64)sheetResource->sheet.texture.width*(u64)sheetResource->sheet.texture.height*4;
				PrintLine_I("  [%u] %s: %dx%d (%s)", sIndex, GetFileNamePart(GetSpriteSheetFilePath(sIndex)), sheetResource->sheet.texture.width, sheetResource->sheet.texture.height, FormattedSizeStr(textureSize));
				totalSpriteSheetsSize += textureSize;
			}
			else
			{
				PrintLine_W("  [%u] %s: Not-loaded", sIndex, GetFileNamePart(GetSpriteSheetFilePath(sIndex)));
			}
		}
		PrintLine_O("Sprite Sheets: %s", FormattedSizeStr(totalSpriteSheetsSize));
		PrintLine_Ox(DbgFlag_Inverted, "Total: %s", FormattedSizeStr(totalTexturesSize + totalSpriteSheetsSize));
	}
	// +==============================+
	// |        show_resources        |
	// +==============================+
	HandleCommand("show_resources")
	{
		app->showLoadedResources = !app->showLoadedResources;
		PrintLine_I("%s Resources List", app->showLoadedResources ? "Showing" : "Hiding");
	}
	#if DEBUG
	// +==============================+
	// |            break             |
	// +==============================+
	HandleCommand("break")
	{
		MyDebugBreak();
	}
	#endif
	// +==============================+
	// |    reload [resource_name]    |
	// +==============================+
	HandleCommand("reload")
	{
		StartCmdArgs(1);
		DefCmdArgString("resource_name", true);
		ParseCmdArgs(argsStr, args, numArgs);
		const StrSplitPiece_t* searchStr = GetCmdArgString(&args, "resource_name");
		
		Resource_t* resourcePntr = nullptr;
		for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
		{
			Resource_t* resource = GetResourceByResourceIndex(rIndex);
			Assert(resource != nullptr);
			Assert(resource->readableName != nullptr);
			u32 readableNameLength = MyStrLength32(resource->readableName);
			if (searchStr->length <= readableNameLength && StrCompareIgnoreCase(searchStr->pntr, resource->readableName, searchStr->length))
			{
				resourcePntr = resource;
				break;
			}
		}
		if (resourcePntr == nullptr) { PrintLine_W("Couldn't find resource \"%s\"", searchStr->pntr); return; }
		
		LoadResource(resourcePntr);
		resourcePntr->wasPreloaded = true;
		PrintLine_I("Loaded %s[%u]: %s \"%s\"", GetResourceTypeStr(resourcePntr->type), resourcePntr->subIndex, resourcePntr->readableName, resourcePntr->filePath);
	}
	// +==============================+
	// |             args             |
	// +==============================+
	HandleCommand("args")
	{
		PrintLine_N("%u Arguments Passed:", platform->numProgramArguments);
		for (u32 aIndex = 0; aIndex < platform->numProgramArguments; aIndex++)
		{
			PrintLine_I("  [%u]: \"%s\"", aIndex, platform->programArguments[aIndex]);
		}
	}
	// +==============================+
	// |        console_arena         |
	// +==============================+
	HandleCommand("console_arena")
	{
		app->showConsoleArenaVisual = !app->showConsoleArenaVisual;
		PrintLine_I("%s Console Arena Visualization", app->showConsoleArenaVisual ? "Enabled" : "Disabled");
	}
	// +==============================+
	// |           connect            |
	// +==============================+
	HandleCommand("connect")
	{
		if (app->chat.created) { WriteLine_E("Connection already started"); return; }
		if (CreateChatHandler(&app->chat, false, false))
		{
			WriteLine_I("Chat Handler Connecting...");
		}
		else
		{
			WriteLine_E("Chat Handler failed to start connection!");
		}
	}
	// +==============================+
	// |          disconnect          |
	// +==============================+
	HandleCommand("disconnect")
	{
		if (!app->chat.created) { WriteLine_E("Connection already stopped"); return; }
		WriteLine_W("Forcibly closing connection...");
		DestroyChatHandler(&app->chat);
		WriteLine_W("Connection Closed");
	}
	// +==============================+
	// |             quit             |
	// +==============================+
	HandleCommand("quit")
	{
		if (!app->chat.created) { WriteLine_E("Connection is not open"); return; }
		if (app->chat.connection.state != IrcConnectionState_Connected) { WriteLine_E("The connection has not yet been established"); return; }
		if (!app->chat.didLogin) { WriteLine_E("We have not yet logged in"); return; }
		WriteLine_W("Sending QUIT command...");
		SendChatCommandNt(&app->chat, "QUIT", nullptr);
	}
	// +==============================+
	// |      send [raw_irc_msg]      |
	// +==============================+
	HandleCommand("send")
	{
		StartCmdArgs(1);
		DefCmdArgString("raw_irc_msg", true);
		ParseCmdArgs(argsStr, args, numArgs);
		const StrSplitPiece_t* messageStr = GetCmdArgString(&args, "raw_irc_msg");
		if (!app->chat.created) { WriteLine_E("IRC Connection Not Opened"); return; }
		if (app->chat.connection.state != IrcConnectionState_Connected) { WriteLine_E("IRC Connection Not Connected"); return; }
		if (!FifoPushArray(&app->chat.connection.writeFifo, (const u8*)messageStr->pntr, MyStrLength32(messageStr->pntr)))
		{
			WriteLine_W("Couldn't fit the entire message into the write fifo");
		}
		FifoPush(&app->chat.connection.writeFifo, '\r');
		FifoPush(&app->chat.connection.writeFifo, '\n');
	}
	// +===============================+
	// | send_irc [irc_command] {args} |
	// +===============================+
	HandleCommand("send_irc")
	{
		StartCmdArgs(2);
		DefCmdArgString("irc_command", true);
		DefCmdArgString("args", false);
		ParseCmdArgs(argsStr, args, numArgs);
		const StrSplitPiece_t* commandStr = GetCmdArgString(&args, "irc_command");
		const StrSplitPiece_t* argumentsStr = TryGetCmdArgString(&args, "args", nullptr);
		if (!app->chat.created) { WriteLine_E("IRC Connection Not Opened"); return; }
		if (app->chat.connection.state != IrcConnectionState_Connected) { WriteLine_E("IRC Connection Not Connected"); return; }
		char* commandStrNt = ArenaString(TempArena, commandStr->pntr, commandStr->length);
		if (argumentsStr != nullptr)
		{
			SendChatCommand(&app->chat, commandStrNt, argumentsStr->pntr, MyStrLength32(argumentsStr->pntr));
		}
		else
		{
			SendChatCommand(&app->chat, commandStrNt, nullptr, 0);
		}
	}
	// +==============================+
	// |      msg [chat_message]      |
	// +==============================+
	HandleCommand("msg")
	{
		StartCmdArgs(1);
		DefCmdArgString("chat_message", true);
		ParseCmdArgs(argsStr, args, numArgs);
		const StrSplitPiece_t* messageStr = GetCmdArgString(&args, "chat_message");
		if (!app->chat.created) { WriteLine_E("IRC Connection Not Opened"); return; }
		if (app->chat.connection.state != IrcConnectionState_Connected) { WriteLine_E("IRC Connection Not Connected"); return; }
		
		char* arguments = TempPrint("#%s :%s", BOT_CHANNEL, messageStr->pntr);
		NotNull(arguments);
		SendChatCommandNt(&app->chat, "PRIVMSG", arguments);
	}
	// +==============================+
	// |         api_connect          |
	// +==============================+
	HandleCommand("api_connect")
	{
		if (app->api.created) { WriteLine_E("API Connection already started"); return; }
		WriteLine_I("API Handler Connecting...");
		CreateApiHandler(&app->api);
	}
	// +==============================+
	// |        api_disconnect        |
	// +==============================+
	HandleCommand("api_disconnect")
	{
		if (!app->api.created) { WriteLine_E("API Connection already stopped"); return; }
		DestroyApiHandler(&app->api);
		WriteLine_I("API Handler Disconnected");
	}
	
	// +--------------------------------------------------------------+
	// |                    Serialization Commands                    |
	// +--------------------------------------------------------------+
	// +==============================+
	// |       reload_settings        |
	// +==============================+
	HandleCommand("reload_settings")
	{
		WriteLine_I("Loading settings...");
		if (LoadAppSettings(&app->settings))
		{
			WriteLine_I("Loaded successfully!");
			app->settingsChanged = false;
		}
		else
		{
			WriteLine_E("Failed to load!");
		}
	}
	// +==============================+
	// |        save_settings         |
	// +==============================+
	HandleCommand("save_settings")
	{
		WriteLine_I("Saving settings...");
		if (SaveAppSettings(&app->settings))
		{
			WriteLine_I("Saved successfully!");
			app->settingsChanged = false;
		}
		else
		{
			WriteLine_E("Failed to save!");
		}
	}
	// +==============================+
	// |     export_sound [path]      |
	// +==============================+
	HandleCommand("export_sound")
	{
		StartCmdArgs(1);
		DefCmdArgString("path", true);
		ParseCmdArgs(argsStr, args, numArgs);
		const StrSplitPiece_t* pathStr = GetCmdArgString(&args, "path");
		
		SampleBuffer_t sampleBuffer = {};
		Sound_t sound = LoadSound(pathStr->pntr, &sampleBuffer);
		
		if (sound.isValid)
		{
			PrintLine_I("Loaded successfully. %u samples", sampleBuffer.numSamples);
			
			char* result = nullptr;
			u32 resultSize = 0;
			for (u8 pass = 0; pass < 2; pass++)
			{
				u32 offset = 0;
				
				SrlPrint(result, resultSize, &offset, "#define TEST_SAMPLE_COUNT %u\nconst i16 testSamples[TEST_SAMPLE_COUNT] = {\n\t", sampleBuffer.numSamples);
				for (u32 sIndex = 0; sIndex < sampleBuffer.numSamples; sIndex++)
				{
					i16 sample = sampleBuffer.samples[sIndex];
					if ((sIndex%32) == 0 && sIndex > 0) { SrlPrint(result, resultSize, &offset, "\n\t"); }
					SrlPrint(result, resultSize, &offset, " %5d%s", sample, (sIndex+1 < sampleBuffer.numSamples) ? "," : "");
				}
				SrlPrint(result, resultSize, &offset, "\n};");
				
				if (pass == 0)
				{
					Assert(result == nullptr);
					resultSize = offset + 1; //add 1 byte to compensate for vsnprintf wierdness in SrlPrint
					Assert(resultSize > 0);
					result = PushArray(&app->platHeap, char, resultSize);
				}
				else
				{
					Assert(offset+1 == resultSize);
					result[offset] = '\n';
					if (platform->WriteEntireFile("output.txt", result, resultSize))
					{
						PrintLine_I("Serialized %u samples successfully to %s file \"output.txt\"", sampleBuffer.numSamples, FormattedSizeStr(resultSize));
					}
					else
					{
						PrintLine_E("Failed to write %u samples as %s file \"output.txt\"", sampleBuffer.numSamples, FormattedSizeStr(resultSize));
					}
				}
			}
			
			if (result != nullptr)
			{
				ArenaPop(&app->platHeap, result);
			}
		}
		else { PrintLine_E("Couldn't load \"%s\"", pathStr->pntr); }
		
		// DestroySampleBuffer(&sampleBuffer);
		DestroySampleBuffer(&app->audioSampleBuffer);
		MyMemCopy(&app->audioSampleBuffer, &sampleBuffer, sizeof(SampleBuffer_t));
		DestroySound(&sound);
	}
	// +==============================+
	// |       folder [purpose]       |
	// +==============================+
	HandleCommand("folder")
	{
		StartCmdArgs(1);
		DefCmdArgString("purpose", true);
		ParseCmdArgs(argsStr, args, numArgs);
		const StrSplitPiece_t* purposeStr = GetCmdArgString(&args, "purpose");
		
		SpecialFolder_t specialFolder = SpecialFolder_NumFolders;
		for (u32 fIndex = 0; fIndex < SpecialFolder_NumFolders; fIndex++)
		{
			const char* folderName = GetSpecialFolderStr((SpecialFolder_t)fIndex);
			if (StrCompareIgnoreCaseNt(folderName, purposeStr->pntr))
			{
				specialFolder = (SpecialFolder_t)fIndex;
				break;
			}
		}
		if (specialFolder >= SpecialFolder_NumFolders) { PrintLine_E("Unknown folder type \"%s\"", purposeStr->pntr); return; }
		
		TempPushMark();
		char* result = platform->GetSpecialFolderPath(TempArena, specialFolder, GAME_NAME_FOR_FOLDERS);
		if (result != nullptr)
		{
			PrintLine_I("%s Path: \"%s\"", GetSpecialFolderStr(specialFolder), result);
		}
		else
		{
			PrintLine_E("Couldn't obtian the path for %s Folder", GetSpecialFolderStr(specialFolder));
		}
		TempPopMark();
	}
	// +==============================+
	// |           appdata            |
	// +==============================+
	HandleCommand("appdata")
	{
		char* appDataFolder = platform->GetSpecialFolderPath(TempArena, SpecialFolder_Settings, GAME_NAME_FOR_FOLDERS);
		if (appDataFolder == nullptr) { WriteLine_E("Failed to get AppData folder location"); return; }
		if (!platform->DoesFolderExist(appDataFolder)) { PrintLine_E("The AppData folder for PCQ doesn't exist at \"%s\"", appDataFolder); return; }
		char* settingsFilePath = TempPrint("%s%s", appDataFolder, SETTINGS_FILE_NAME);
		
		if (platform->ShowFile(settingsFilePath))
		{
			PrintLine_I("Showing \"%s\"", appDataFolder);
		}
		else { PrintLine_E("Failed to show \"%s\"", appDataFolder); }
	}
	
	// +--------------------------------------------------------------+
	// |                        Other Commands                        |
	// +--------------------------------------------------------------+
	// +==========================================+
	// | show_gutter, hide_gutter, toggle_gutter  |
	// +==========================================+
	else if (MyStrCompareNt(baseCmd, "show_gutter") == 0 || MyStrCompareNt(baseCmd, "hide_gutter") == 0 || MyStrCompareNt(baseCmd, "toggle_gutter") == 0)
	{
		bool show = app->dbgConsole.showGutterNumbers;
		if (MyStrCompareNt(baseCmd, "show_gutter") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_gutter") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_gutter") == 0) { show = !show; }
		if (show != app->dbgConsole.showGutterNumbers)
		{
			app->dbgConsole.showGutterNumbers = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console Gutter Numbers", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	// +======================================+
	// | show_files, hide_files, toggle_files |
	// +======================================+
	else if (MyStrCompareNt(baseCmd, "show_files") == 0 || MyStrCompareNt(baseCmd, "hide_files") == 0 || MyStrCompareNt(baseCmd, "toggle_files") == 0)
	{
		bool show = app->dbgConsole.showFileNames;
		if (MyStrCompareNt(baseCmd, "show_files") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_files") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_files") == 0) { show = !show; }
		if (show != app->dbgConsole.showFileNames)
		{
			app->dbgConsole.showFileNames = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console File Names", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	// +======================================+
	// | show_lines, hide_lines, toggle_lines |
	// +======================================+
	else if (MyStrCompareNt(baseCmd, "show_lines") == 0 || MyStrCompareNt(baseCmd, "hide_lines") == 0 || MyStrCompareNt(baseCmd, "toggle_lines") == 0)
	{
		bool show = app->dbgConsole.showLineNumbers;
		if (MyStrCompareNt(baseCmd, "show_lines") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_lines") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_lines") == 0) { show = !show; }
		if (show != app->dbgConsole.showLineNumbers)
		{
			app->dbgConsole.showLineNumbers = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console Line Numbers", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	// +======================================+
	// | show_funcs, hide_funcs, toggle_funcs |
	// +======================================+
	else if (MyStrCompareNt(baseCmd, "show_funcs") == 0 || MyStrCompareNt(baseCmd, "hide_funcs") == 0 || MyStrCompareNt(baseCmd, "toggle_funcs") == 0)
	{
		bool show = app->dbgConsole.showFuncNames;
		if (MyStrCompareNt(baseCmd, "show_funcs") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_funcs") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_funcs") == 0) { show = !show; }
		if (show != app->dbgConsole.showFuncNames)
		{
			app->dbgConsole.showFuncNames = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console Function Names", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	// +======================================+
	// | show_tasks, hide_tasks, toggle_tasks |
	// +======================================+
	else if (MyStrCompareNt(baseCmd, "show_tasks") == 0 || MyStrCompareNt(baseCmd, "hide_tasks") == 0 || MyStrCompareNt(baseCmd, "toggle_tasks") == 0)
	{
		bool show = app->dbgConsole.showTaskNumbers;
		if (MyStrCompareNt(baseCmd, "show_tasks") == 0) { show = true; }
		if (MyStrCompareNt(baseCmd, "hide_tasks") == 0) { show = false; }
		if (MyStrCompareNt(baseCmd, "toggle_tasks") == 0) { show = !show; }
		if (show != app->dbgConsole.showTaskNumbers)
		{
			app->dbgConsole.showTaskNumbers = show;
			app->dbgConsole.remeasureContent = true;
			PrintLine_I("%s Console Task Numbers", show ? "Showing" : "Hiding");
		}
		else
		{
			PrintLine_W("Already %s", show ? "Shown" : "Hidden");
		}
	}
	
	// +--------------------------------------------------------------+
	// |                       Unknown Commands                       |
	// +--------------------------------------------------------------+
	else
	{
		PrintLine_W("Unknown command");
	}
}
