/*
File:   app_version.h
Author: Taylor Robbins
Date:   11\04\2017
*/

#ifndef _APP_VERSION_H
#define _APP_VERSION_H

#define APP_VERSION_MAJOR    0
#define APP_VERSION_MINOR    1
//NOTE: Auto-incremented by a python script before each build
#define APP_VERSION_BUILD    870

#endif //  _APP_VERSION_H
