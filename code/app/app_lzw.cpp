/*
File:   app_lzw.cpp
Author: Taylor Robbins
Date:   09\27\2019
Description: 
	** Holds our encoder used for the GIF image data streams which is based off the LZW compression algorithm
*/

//This is a local helper function that just makes adding a byte to a DynArray_t a little nicer looking
void _LzwAddBufferU8(DynArray_t* bufferOut, u8 byteValue)
{
	Assert(bufferOut != nullptr);
	u8* newSpace = DynArrayAdd(bufferOut, u8);
	Assert(newSpace != nullptr);
	*newSpace = byteValue;
}

void _LzwWriteCode(DynArray_t* bufferOut, u8* byteBufferPntr, u8* bitIndexPntr, u32 code, u8 codeLength)
{
	Assert(bufferOut != nullptr);
	u32 maskPos = 0;
	// PrintLine_D("Writing code 0b%s", BinaryString(code, codeLength));
	u8 byteBuffer = *byteBufferPntr;
	u8 bitIndex = *bitIndexPntr;
	Assert(bitIndex < 8);
	while (maskPos < codeLength)
	{
		if (IsFlagSet(code, (1 << maskPos))) { byteBuffer |= (1 << bitIndex); }
		maskPos++;
		bitIndex++;
		if (bitIndex >= 8)
		{
			// PrintLine_D("Writing byte 0x%02X, %u left", byteBuffer, codeLength - maskPos);
			_LzwAddBufferU8(bufferOut, byteBuffer);
			byteBuffer = 0x00;
			bitIndex = 0;
		}
	}
	*byteBufferPntr = byteBuffer;
	*bitIndexPntr = bitIndex;
}

void LzwEncodeBuffer(DynArray_t* bufferOut, MemoryArena_t* memArena, u8 pixelWidth, const u8* dataPntr, u32 dataLength)
{
	Assert(bufferOut != nullptr);
	Assert(bufferOut->items != nullptr);
	Assert(bufferOut->itemSize == 1);
	Assert(memArena != nullptr);
	Assert(dataPntr != nullptr || dataLength == 0);
	Assert(pixelWidth >= 2 && pixelWidth <= 8);
	if (dataLength == 0) { return; }
	
	u8 byteBuffer = 0x00;
	u8 bitIndex = 0;
	
	u8 codeLength = pixelWidth+1;
	u32 numInitialColors = (u32)(1 << pixelWidth);
	
	_LzwWriteCode(bufferOut, &byteBuffer, &bitIndex, (1 << pixelWidth), codeLength); //clear code
	
	//NOTE: These bytes show up in the tutorial I followed by not in an test gif made by
	//      by GIMP. Seems like the gif file format doesn't put these bytes in
	// _LzwAddBufferU8(bufferOut, 0x1f);
	// _LzwAddBufferU8(bufferOut, 0x1f); //LZW magic numbers
	// _LzwAddBufferU8(bufferOut, 0x9d);
	// _LzwAddBufferU8(bufferOut, 0x90); //bit 7 must be set for compatibility
	
	//Fill the dictionary with initial values
	u32 dictLength = 0;
	u32** dictionary = PushArray(memArena, u32*, (u32)(1 << codeLength));
	Assert(dictionary != nullptr);
	MyMemSet(dictionary, 0x00, sizeof(u32*) * (u32)(1 << codeLength));
	for (dictLength = 0; dictLength < numInitialColors; dictLength++)
	{
		u32* lengthPntr = (u32*)ArenaPush(memArena, sizeof(u32)+1);
		Assert(lengthPntr != nullptr);
		*lengthPntr = 1;
		u8* entryPntr = (u8*)(lengthPntr + 1);
		*entryPntr = (u8)dictLength;
		dictionary[dictLength] = lengthPntr;
	}
	dictLength++; //skip over clear code
	dictLength++; //skip over end of info code
	
	u32 bIndex = 0;
	const u8* bytePntr = dataPntr;
	while (bIndex < dataLength)
	{
		bool foundMatch = false;
		for (i32 dIndex = dictLength-1; dIndex >= 0; dIndex--)
		{
			if (dictionary[dIndex] != nullptr)
			{
				u32 dictEntryLength = *dictionary[dIndex];
				u8* dictEntryPntr = (u8*)(dictionary[dIndex] + 1);
				if (dictEntryLength <= dataLength - bIndex && MyMemCompare(dictEntryPntr, bytePntr, dictEntryLength) == 0)
				{
					//write the code to the file
					u32 code = (u32)dIndex;
					// PrintLine_I("Matched %u bytes to code 0b%s", dictEntryLength, BinaryString(code, codeLength));
					_LzwWriteCode(bufferOut, &byteBuffer, &bitIndex, code, codeLength);
					bytePntr += dictEntryLength;
					bIndex += dictEntryLength;
					
					if (bIndex < dataLength)
					{
						//Make the dictionary bigger if needed
						bool cleared = false;
						if (dictLength >= (u32)(1 << codeLength))
						{
							if (codeLength >= MAX_GIF_LZW_CODE_WIDTH)
							{
								_LzwWriteCode(bufferOut, &byteBuffer, &bitIndex, (1 << pixelWidth), codeLength); //clear code
								codeLength = pixelWidth+1;
								
								//create the initial dictionary again
								for (u32 eIndex = 0; eIndex < dictLength; eIndex++) { if (dictionary[eIndex] != nullptr) { ArenaPop(memArena, dictionary[eIndex]); } }
								ArenaPop(memArena, dictionary);
								dictLength = 0;
								dictionary = PushArray(memArena, u32*, (u32)(1 << codeLength));
								Assert(dictionary != nullptr);
								MyMemSet(dictionary, 0x00, sizeof(u32*) * (u32)(1 << codeLength));
								for (dictLength = 0; dictLength < numInitialColors; dictLength++)
								{
									u32* lengthPntr = (u32*)ArenaPush(memArena, sizeof(u32)+1);
									Assert(lengthPntr != nullptr);
									*lengthPntr = 1;
									u8* entryPntr = (u8*)(lengthPntr + 1);
									*entryPntr = (u8)dictLength;
									dictionary[dictLength] = lengthPntr;
								}
								dictLength++; //skip over clear code
								dictLength++; //skip over end of info code
								
								cleared = true;
							}
							else
							{
								codeLength++;
								u32 newDictLength = (u32)(1 << codeLength);
								u32** newDictionary = PushArray(memArena, u32*, newDictLength);
								Assert(newDictionary != nullptr);
								MyMemSet(newDictionary, 0x00, sizeof(u32*) * newDictLength);
								MyMemCopy(newDictionary, dictionary, sizeof(u32*) * dictLength);
								// for (u32 eIndex = 0; eIndex < dictLength; eIndex++) { if (dictionary[eIndex] != nullptr) { ArenaPop(memArena, dictionary[eIndex]); } }
								ArenaPop(memArena, dictionary);
								dictionary = newDictionary;
							}
						}
						
						if (!cleared)
						{
							//Add a new entry to the dictionary
							u32* lengthPntr = (u32*)ArenaPush(memArena, sizeof(u32) + dictEntryLength+1);
							Assert(lengthPntr != nullptr);
							*lengthPntr = dictEntryLength+1;
							u8* entryPntr = (u8*)(lengthPntr + 1);
							MyMemCopy(entryPntr, dictEntryPntr, dictEntryLength);
							entryPntr[dictEntryLength] = *bytePntr; //add the next character to the new dictionary entry
							dictionary[dictLength] = lengthPntr;
							dictLength++;
						}
					}
					
					foundMatch = true;
					break;
				}
			}
		}
		
		//NOTE: We should always match something in the dictionary since we pre-filled
		//      it with all possible character values at the beginning
		Assert(foundMatch);
	}
	
	_LzwWriteCode(bufferOut, &byteBuffer, &bitIndex, (1 << pixelWidth) + 1, codeLength); //end of info code
	if (bitIndex > 0) { _LzwAddBufferU8(bufferOut, byteBuffer); } //flush any leftover bits into a final byte
	
	//Deallocate dictionary
	for (u32 dIndex = 0; dIndex < dictLength; dIndex++) { if (dictionary[dIndex] != nullptr) { ArenaPop(memArena, dictionary[dIndex]); } }
	ArenaPop(memArena, dictionary);
}
