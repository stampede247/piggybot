/*
File:   app_sprite.cpp
Author: Taylor Robbins
Date:   06\11\2020
Description: 
	** Holds functions that help you create and handle Sprite_t and SpriteEx_t structures
*/

inline Sprite_t NewSpriteAll_(const SpriteSheet_t* sheetPntr, v2i frame, Color_t drawColor, Dir2_t rotation)
{
	Sprite_t result;
	result.sheet = sheetPntr;
	result.frame = frame;
	result.drawColor = drawColor;
	result.rotation = rotation;
	return result;
}
inline SpriteEx_t NewSpriteExAll_(const SpriteSheet_t* sheetPntr, v2i frame, const PalColor_t* palColor, Color_t drawColor, Dir2_t rotation, v2i maskFrame)
{
	SpriteEx_t result;
	result.sheet = sheetPntr;
	result.frame = frame;
	result.drawColor = drawColor;
	result.rotation = rotation;
	result.flippedX = false;
	result.flippedY = false;
	if (palColor != nullptr) { memcpy(&result.palColor, palColor, sizeof(PalColor_t)); }
	else { result.palColor = plt->target; }
	result.maskFrame = maskFrame;
	return result;
}

#define NewSpriteAll(sheetName, frameX, frameY, drawColor, rotation) NewSpriteAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), (drawColor), (rotation))
#define NewSpriteColored(sheetName, frameX, frameY, drawColor)       NewSpriteAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), (drawColor), Dir2_Down)
#define NewSpriteRot(sheetName, frameX, frameY, rotation)            NewSpriteAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), White,       (rotation))
#define NewSprite(sheetName, frameX, frameY)                         NewSpriteAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), White,       Dir2_Down)

#define NewSpriteExAll(sheetName, frameX, frameY, colorName, drawColor, rotation, maskFrame) NewSpriteExAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), &plt->colorName, (drawColor), (rotation), (maskFrame))
#define NewSpriteExMask(sheetName, frameX, frameY, colorName, maskFrame)                     NewSpriteExAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), &plt->colorName, White,       Dir2_Down,  (maskFrame))
#define NewSpriteExRot(sheetName, frameX, frameY, colorName, rotation)                       NewSpriteExAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), &plt->colorName, White,       (rotation), Vec2i_Zero)
#define NewSpriteEx(sheetName, frameX, frameY, colorName)                                    NewSpriteExAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), &plt->colorName, White,       Dir2_Down,  Vec2i_Zero)

#define DeclSpriteAll(varName, sheetName, frameX, frameY, drawColor, rotation) Sprite_t varName = NewSpriteAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), (drawColor), (rotation))
#define DeclSpriteColored(varName, sheetName, frameX, frameY, drawColor)       Sprite_t varName = NewSpriteAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), (drawColor), Dir2_Down)
#define DeclSpriteRot(varName, sheetName, frameX, frameY, rotation)            Sprite_t varName = NewSpriteAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), White,       (rotation))
#define DeclSprite(varName, sheetName, frameX, frameY)                         Sprite_t varName = NewSpriteAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), White,       Dir2_Down)

#define DeclSpriteExAll(varName, sheetName, frameX, frameY, colorName, drawColor, rotation, maskFrame) SpriteEx_t varName = NewSpriteExAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), &plt->colorName, (drawColor), (rotation), (maskFrame))
#define DeclSpriteExMask(varName, sheetName, frameX, frameY, colorName, maskFrame)                     SpriteEx_t varName = NewSpriteExAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), &plt->colorName, White,       Dir2_Down,  (maskFrame))
#define DeclSpriteExRot(varName, sheetName, frameX, frameY, colorName, rotation)                       SpriteEx_t varName = NewSpriteExAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), &plt->colorName, White,       (rotation), Vec2i_Zero)
#define DeclSpriteEx(varName, sheetName, frameX, frameY, colorName)                                    SpriteEx_t varName = NewSpriteExAll_(GetSheet(sheetName), NewVec2i(frameX, frameY), &plt->colorName, White,       Dir2_Down,  Vec2i_Zero)
