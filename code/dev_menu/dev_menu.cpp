/*
File:   dev_menu.cpp
Author: Taylor Robbins
Date:   11\17\2017
Description: 
	** Holds all the code for the Developer Menu
*/

// +--------------------------------------------------------------+
// |                     Initialize Dev Menu                      |
// +--------------------------------------------------------------+
void InitializeDevMenu()
{
	Assert(devMenu != nullptr);
	
	
	
	devMenu->initialized = true;
}

// +--------------------------------------------------------------+
// |                        Start Dev Menu                        |
// +--------------------------------------------------------------+
bool StartDevMenu(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	// SoftQueueMusicChange(GetMusic(menuMusic), SoundTransition_FadeIn);
	
	
	return true;
}

// +--------------------------------------------------------------+
// |                    Deinitialize Dev Menu                     |
// +--------------------------------------------------------------+
void DeinitializeDevMenu()
{
	Assert(devMenu != nullptr);
	Assert(devMenu->initialized == true);
	
	
	
	devMenu->initialized = false;
	ClearPointer(devMenu);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesDevMenuCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                       Update Dev Menu                        |
// +--------------------------------------------------------------+
void UpdateDevMenu(AppMenu_t appMenuAbove)
{
	Assert(devMenu != nullptr);
	Assert(devMenu->initialized);
	
	if (appMenuAbove == AppMenu_None)
	{
		// +==============================+
		// |        Tilde Shortcut        |
		// +==============================+
		if (ButtonPressedNoHandling(Button_Tilde) || ButtonPressed(Button_Escape))
		{
			HandleButton(Button_Tilde);
			HandleButton(Button_Escape);
			WriteLine_D("Exiting Dev Menu");
			PopAppState();
		}
	}
}

// +--------------------------------------------------------------+
// |                       Render Dev Menu                        |
// +--------------------------------------------------------------+
void RenderDevMenu(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	// +==============================+
	// |         Render Setup         |
	// +==============================+
	RcBegin(GetShader(tileShader), NewRec(Vec2_Zero, RenderScreenSize), GetFont(pixelFont), renderBuffer, true);
	RcClear(PureBlue, 1.0f);
	
	
}
