/*
File:   giffer.cpp
Author: Taylor Robbins
Date:   09\30\2019
Description: 
	** Giffer is an AppState that allows you to look at, modify, and then export sections of a recorded GifStream_t
*/

#include "giffer/giffer_functions.cpp"

// +--------------------------------------------------------------+
// |                          Initialize                          |
// +--------------------------------------------------------------+
void InitializeGiffer()
{
	Assert(giffer != nullptr);
	
	//TODO: Anything we need to allocate?
	
	giffer->initialized = true;
}

// +--------------------------------------------------------------+
// |                            Start                             |
// +--------------------------------------------------------------+
bool StartGiffer(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	SoftQueueMusicChange(nullptr);
	
	if (app->gifStream.isActive)
	{
		if (app->gifStream.numFrames == 0)
		{
			FinishGifStream(&app->gifStream, false);
			Notify_E("The GIF stream has no frames recorded! Can't enter Giffer AppState");
			return false;
		}
		MyMemCopy(&giffer->stream, &app->gifStream, sizeof(GifStream_t));
		ClearStruct(app->gifStream);
		giffer->frameStart = 0;
		giffer->frameEnd = giffer->stream.numFrames-1;
		giffer->currentFrame = giffer->frameStart;
		giffer->frameChanged = true;
		giffer->isPlaying = true;
		giffer->frameTime = 0;
		giffer->exportGif = false;
		giffer->timelineZoom = 1.0f;
		while (giffer->stream.numFrames * (giffer->timelineZoom+1) < (RenderScreenSize.width*3/4)) { giffer->timelineZoom += 1; }
		giffer->timelineScroll = 0;
		giffer->timelineScrollGoto = giffer->timelineScroll;
		return true;
	}
	else if (!giffer->stream.isActive)
	{
		Notify_E("The GIF stream was not started. Can't enter Giffer AppState");
		return false;
	}
	else
	{
		WriteLine_W("Resuming GIF stream editing");
		return true;
	}
}

// +--------------------------------------------------------------+
// |                         Deinitialize                         |
// +--------------------------------------------------------------+
void DeinitializeGiffer()
{
	Assert(giffer != nullptr);
	Assert(giffer->initialized == true);
	
	FinishGifStream(&giffer->stream, false);
	DestroyTexture(&giffer->viewTexture);
	
	giffer->initialized = false;
	ClearPointer(giffer);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesGifferCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                        Update Giffer                         |
// +--------------------------------------------------------------+
void UpdateGiffer(AppMenu_t appMenuAbove)
{
	// +==============================+
	// |     Handle Escape Button     |
	// +==============================+
	if (ButtonPressed(Button_Escape))
	{
		HandleButton(Button_Escape);
		FinishGifStream(&giffer->stream, false);
		PopAppState();
		return;
	}
	// +==============================+
	// |      Handle Gif Export       |
	// +==============================+
	if (giffer->exportGif)
	{
		PrintLine_I("Kicking off gif export...");
		GifStreamCut(&giffer->stream, giffer->frameStart, giffer->frameEnd+1);
		FinishGifStream(&giffer->stream, true);
		PopAppState();
		return;
	}
	
	bool mouseOverUi = false;
	
	GifferUpdateRecs();
	
	// +==============================+
	// |    Handle Discard Button     |
	// +==============================+
	if (!mouseOverUi && appInput->mouseInsideWindow && IsInsideRec(giffer->discardBtnRec, RenderMousePos))
	{
		mouseOverUi = true;
		appOutput->cursorType = Cursor_Pointer;
		if (ButtonReleased(MouseButton_Left) && IsInsideRec(giffer->discardBtnRec, RenderMouseStartPos))
		{
			HandleButton(MouseButton_Left);
			FinishGifStream(&giffer->stream, false);
			PopAppState();
			return;
		}
	}
	
	// +==============================+
	// |     Handle Export Button     |
	// +==============================+
	if (!mouseOverUi && appInput->mouseInsideWindow && IsInsideRec(giffer->exportBtnRec, RenderMousePos))
	{
		mouseOverUi = true;
		appOutput->cursorType = Cursor_Pointer;
		if (ButtonReleased(MouseButton_Left) && IsInsideRec(giffer->exportBtnRec, RenderMouseStartPos))
		{
			HandleButton(MouseButton_Left);
			giffer->exportGif = true;
		}
	}
	
	// +==============================+
	// |   Handle Beginning Button    |
	// +==============================+
	if (!mouseOverUi && appInput->mouseInsideWindow && IsInsideRec(giffer->begBtnRec, RenderMousePos))
	{
		mouseOverUi = true;
		appOutput->cursorType = Cursor_Pointer;
		if (ButtonReleased(MouseButton_Left) && IsInsideRec(giffer->begBtnRec, RenderMouseStartPos))
		{
			HandleButton(MouseButton_Left);
			if (giffer->currentFrame != giffer->frameStart)
			{
				giffer->currentFrame = giffer->frameStart;
				giffer->frameChanged = true;
			}
		}
	}
	
	// +==============================+
	// |      Handle End Button       |
	// +==============================+
	if (!mouseOverUi && appInput->mouseInsideWindow && IsInsideRec(giffer->endBtnRec, RenderMousePos))
	{
		mouseOverUi = true;
		appOutput->cursorType = Cursor_Pointer;
		if (ButtonReleased(MouseButton_Left) && IsInsideRec(giffer->endBtnRec, RenderMouseStartPos))
		{
			HandleButton(MouseButton_Left);
			if (giffer->currentFrame != giffer->frameEnd)
			{
				giffer->currentFrame = giffer->frameEnd;
				giffer->frameChanged = true;
			}
		}
	}
	
	// +==============================+
	// |    Refresh View Shortcut     |
	// +==============================+
	if (ButtonPressed(Button_V))
	{
		HandleButton(Button_V);
		WriteLine_I("Updating the view texture");
		giffer->frameChanged = true;
	}
	
	// +==============================+
	// |        Export Hotkey         |
	// +==============================+
	if (ButtonPressed(Button_Enter))
	{
		HandleButton(Button_Enter);
		giffer->exportGif = true;
	}
	
	// +==============================+
	// |         Home Hotkey          |
	// +==============================+
	if (ButtonPressed(Button_Home))
	{
		HandleButton(Button_Home);
		if (giffer->currentFrame != giffer->frameStart)
		{
			giffer->currentFrame = giffer->frameStart;
			giffer->frameChanged = true;
		}
	}
	// +==============================+
	// |          End Hotkey          |
	// +==============================+
	if (ButtonPressed(Button_End))
	{
		HandleButton(Button_End);
		if (giffer->currentFrame != giffer->frameEnd)
		{
			giffer->currentFrame = giffer->frameEnd;
			giffer->frameChanged = true;
		}
	}
	
	// +==============================+
	// |       Move Left/Right        |
	// +==============================+
	if (ButtonPressed(Button_Right))
	{
		HandleButton(Button_Right);
		if (giffer->currentFrame+1 < giffer->stream.numFrames)
		{
			giffer->currentFrame++;
			giffer->frameChanged = true;
		}
	}
	if (ButtonPressed(Button_Left))
	{
		HandleButton(Button_Left);
		if (giffer->currentFrame > 0)
		{
			giffer->currentFrame--;
			giffer->frameChanged = true;
		}
	}
	
	// +==============================+
	// |      Mouse Move Cursor       |
	// +==============================+
	if (!mouseOverUi && appInput->mouseInsideWindow && (IsInsideRec(giffer->timelineRec, RenderMousePos) || IsInsideRec(giffer->tickGutterRec, RenderMousePos)))
	{
		mouseOverUi = true;
		appOutput->cursorType = Cursor_Pointer;
		if (ButtonDown(MouseButton_Left) && (IsInsideRec(giffer->timelineRec, RenderMouseStartPos) || IsInsideRec(giffer->tickGutterRec, RenderMouseStartPos)))
		{
			HandleButton(MouseButton_Left);
			v2 timelinePos = GifferScreenToTimeline(RenderMousePos);
			i32 mouseFrame = RoundR32i(timelinePos.x);
			if (mouseFrame < 0) { mouseFrame = 0; }
			if (mouseFrame > (i32)giffer->stream.numFrames-1) { mouseFrame = (i32)giffer->stream.numFrames-1; }
			if (mouseFrame != (i32)giffer->currentFrame)
			{
				giffer->currentFrame = (u32)mouseFrame;
				giffer->frameChanged = true;
			}
		}
	}
	
	// +==============================+
	// |     Handle Scroll Wheel      |
	// +==============================+
	if (appInput->mouseInsideWindow && (IsInsideRec(giffer->timelineRec, RenderMousePos) || IsInsideRec(giffer->tickGutterRec, RenderMousePos)) && ScrollWheelMovedY())
	{
		HandleScrollWheelY();
		if (ButtonDownNoHandling(Button_Control))
		{
			if (appInput->scrollDelta.y > 0) { giffer->timelineZoom += 1; }
			else if (giffer->timelineZoom > 1) { giffer->timelineZoom -= 1; }
		}
		else
		{
			giffer->timelineScrollGoto -= appInput->scrollDelta.y * GFR_TIMELINE_SCROLL_SPEED / giffer->timelineZoom;
		}
	}
	
	GifferUpdateRecs();
	
	// +==============================+
	// |    Update Timeline Scroll    |
	// +==============================+
	r32 scrollDelta = giffer->timelineScrollGoto - giffer->timelineScroll;
	if (AbsR32(scrollDelta) >= 1.0f/giffer->timelineZoom)
	{
		giffer->timelineScroll += scrollDelta / GFR_TIMELINE_SCROLL_LAG;
	}
	else
	{
		giffer->timelineScroll = giffer->timelineScrollGoto;
	}
	
	// +==============================+
	// |    Set Start Time Hotkey     |
	// +==============================+
	if (ButtonPressed(Button_OpenBracket))
	{
		HandleButton(Button_OpenBracket);
		giffer->frameStart = giffer->currentFrame;
		if (giffer->frameEnd < giffer->frameStart)
		{
			giffer->frameEnd = giffer->frameStart;
		}
	}
	// +==============================+
	// |     Set End Time Hotkey      |
	// +==============================+
	if (ButtonPressed(Button_CloseBracket))
	{
		HandleButton(Button_CloseBracket);
		giffer->frameEnd = giffer->currentFrame;
		if (giffer->frameStart > giffer->frameEnd)
		{
			giffer->frameStart = giffer->frameEnd;
		}
	}
	
	// +==============================+
	// |     Update View Texture      |
	// +==============================+
	if (giffer->frameChanged)
	{
		GifferUpdateViewTexture();
		giffer->frameChanged = false;
	}
}

// +--------------------------------------------------------------+
// |                        Render Giffer                         |
// +--------------------------------------------------------------+
void RenderGiffer(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	// +==============================+
	// |         Render Setup         |
	// +==============================+
	RcBegin(GetShader(tileShader), NewRec(Vec2_Zero, RenderScreenSize), GetFont(pixelFont), renderBuffer);
	RcClear(Grey11, 1.0f);
	if (!giffer->stream.isActive) { return; }
	
	GifferUpdateRecs();
	
	// RcDrawRectangle(NewRec(0, 0, 100, 100), PureRed);
	
	// +==============================+
	// |       Render Viewport        |
	// +==============================+
	{
		RcBindTexture(GetTexture(alphaTexture));
		RcDrawTexturedRec(giffer->viewRec, White, NewRec(0, 0, giffer->viewRec.width/4, giffer->viewRec.height/4));
		if (giffer->viewTexture.isValid)
		{
			RcBindTexture(&giffer->viewTexture);
			RcDrawTexturedRec(giffer->viewRec, White);
		}
		RcDrawPixelatedButton(RecInflate(giffer->viewRec, 4), NoColor, White, 4);
	}
	
	// +==============================+
	// |      Render Changed Rec      |
	// +==============================+
	if (app->showDebugOverlay)
	{
		GifFrame_t* currentFrame = GetGifStreamFrame(&giffer->stream, giffer->currentFrame);
		Assert(currentFrame != nullptr);
		rec changedRec = NewRec(currentFrame->changedRec) * giffer->viewScale;
		changedRec.topLeft += giffer->viewRec.topLeft;
		
		RcDrawButton(changedRec + NewVec2(0, 1), NoColor, Black, 1);
		RcDrawButton(changedRec, ColorTransparent(VisRed, 0.2f), VisRed, 1);
		
		RcBindFont(GetFont(pixelFont), 2.0f);
		v2 textPos = changedRec.topLeft + NewVec2(0, -(4 + RcMaxExtendDown()));
		RcPrintStringEmbossed(textPos, VisRed, Black, 1.0f, 1, "(%d, %d, %d, %d)",
			currentFrame->changedRec.x, currentFrame->changedRec.y, currentFrame->changedRec.width, currentFrame->changedRec.height
		);
	}
	
	// +==============================+
	// |       Render Timeline        |
	// +==============================+
	{
		RcDrawRectangle(giffer->timelineRec, VisBlueGray);
		RcSetViewport(giffer->timelineRec);
		GifFrame_t* frame = giffer->stream.firstFrame;
		for (u32 fIndex = 0; fIndex < giffer->stream.numFrames; fIndex++)
		{
			Assert(frame != nullptr);
			v2 framePos = GifferTimelineToScreen(NewVec2((r32)fIndex, 0));
			rec frameRec = NewRec(framePos.x, framePos.y, giffer->timelineZoom, giffer->timelineRec.height);
			rec frameStartRec = frameRec;
			frameStartRec.width = 1;
			if (frame->changedRec.width > 0 && frame->changedRec.height > 0)
			{
				r32 percentChanged = (r32)(frame->changedRec.width * frame->changedRec.height) / (r32)(giffer->stream.width * giffer->stream.height);
				frameRec.width *= percentChanged;
				Color_t backColor = VisLightBlueGray;
				if (percentChanged >= 1.0f) { backColor = VisBlue; }
				RcDrawRectangle(frameRec, backColor);
				RcDrawRectangle(frameStartRec, VisLightBlue);
			}
			frame = frame->next;
		}
		RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
	}
	
	// +==============================+
	// |      Render Tick Gutter      |
	// +==============================+
	{
		RcDrawRectangle(giffer->tickGutterRec, VisWhite);
		
		RcSetViewport(giffer->tickGutterRec);
		for (u32 tIndex = 0; tIndex < giffer->stream.numFrames; tIndex++)
		{
			v2 tickPos = GifferTimelineToScreen(NewVec2((r32)tIndex, 0));
			rec tickRec = NewRec(tickPos.x, giffer->tickGutterRec.y + giffer->tickGutterRec.height/2, 1, giffer->tickGutterRec.height/2);
			
			RcDrawRectangle(tickRec, VisBlueGray);
			
			if (tIndex == giffer->currentFrame)
			{
				rec cursorRec = NewRec(tickPos.x, giffer->timelineRec.y, 1, giffer->timelineRec.height);
				RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
				RcDrawRectangle(cursorRec, VisRed);
				RcSetViewport(giffer->tickGutterRec);
			}
		}
		RcSetViewport(NewRec(Vec2_Zero, RenderScreenSize));
	}
	
	// +==============================+
	// |   Render Selected Brackets   |
	// +==============================+
	{
		v2 leftBracketPos = GifferTimelineToScreen(NewVec2((r32)giffer->frameStart, 0));
		rec leftBracketRec = NewRec(leftBracketPos.x - GFR_BRACKET_THICKNESS, giffer->tickGutterRec.y, GFR_BRACKET_THICKNESS, giffer->tickGutterRec.height + giffer->timelineRec.height);
		rec leftBracketTopRec = leftBracketRec;
		leftBracketTopRec.height = GFR_BRACKET_THICKNESS;
		leftBracketTopRec.x += leftBracketRec.width;
		leftBracketTopRec.width = 4;
		rec leftBracketBottomRec = leftBracketTopRec;
		leftBracketBottomRec.y = leftBracketRec.y + leftBracketRec.height - leftBracketBottomRec.height;
		RcDrawRectangle(leftBracketRec, VisLightOrange);
		RcDrawRectangle(leftBracketTopRec, VisLightOrange);
		RcDrawRectangle(leftBracketBottomRec, VisLightOrange);
		
		v2 rightBracketPos = GifferTimelineToScreen(NewVec2((r32)giffer->frameEnd + 1, 0));
		rec rightBracketRec = NewRec(rightBracketPos.x, giffer->tickGutterRec.y, GFR_BRACKET_THICKNESS, giffer->tickGutterRec.height + giffer->timelineRec.height);
		rec rightBracketTopRec = rightBracketRec;
		rightBracketTopRec.height = GFR_BRACKET_THICKNESS;
		rightBracketTopRec.width = 4;
		rightBracketTopRec.x -= rightBracketTopRec.width;
		rec rightBracketBottomRec = rightBracketTopRec;
		rightBracketBottomRec.y = rightBracketRec.y + rightBracketRec.height - rightBracketBottomRec.height;
		RcDrawRectangle(rightBracketRec, VisLightOrange);
		RcDrawRectangle(rightBracketTopRec, VisLightOrange);
		RcDrawRectangle(rightBracketBottomRec, VisLightOrange);
		
		RcBindFont(GetFont(pixelFont), GFR_TIMELINE_FONT_SCALE);
		u32 numFrames = giffer->frameEnd+1 - giffer->frameStart;
		char* textStr = TempPrint("%u frame%s", numFrames, (numFrames == 1) ? "" : "s");
		v2 textSize = RcMeasureStringNt(textStr);
		v2 textPos = NewVec2((rightBracketRec.x + leftBracketRec.x) / 2 - textSize.width/2, giffer->tickGutterRec.y - 2*GFR_TIMELINE_FONT_SCALE - RcMaxExtendDown());
		textPos = Vec2Round(textPos);
		RcDrawStringEmbossedNt(textStr, textPos, VisYellow, VisOrange);
	}
	
	// +==============================+
	// |    Render Discard Button     |
	// +==============================+
	{
		bool isHovering = (appInput->mouseInsideWindow && IsInsideRec(giffer->discardBtnRec, RenderMousePos));
		bool isPressing = (isHovering && ButtonDownNoHandling(MouseButton_Left) && IsInsideRec(giffer->discardBtnRec, RenderMouseStartPos));
		if (ButtonDownNoHandling(Button_Escape)) { isPressing = true; }
		rec iconRec = NewRecCentered(giffer->discardBtnRec.topLeft + giffer->discardBtnRec.size/2, GetSheetFrameSizef(cutsceneIcons) * GFR_BUTTON_ICON_SCALE);
		iconRec.topLeft = Vec2Round(iconRec.topLeft);
		
		Color_t backColor = VisLightOrange;
		Color_t iconColor = VisWhite;
		if (isPressing)
		{
			backColor = VisYellow;
			iconColor = VisRed;
		}
		else if (isHovering)
		{
			backColor = VisOrange;
		}
		
		RcDrawRectangle(giffer->discardBtnRec, backColor);
		RcDrawSheetFrame(GetSheet(cutsceneIcons), NewVec2i(21, 1), iconRec, iconColor);
		RcDrawPixelatedButton(RecInflate(giffer->discardBtnRec, GFR_BUTTON_ICON_SCALE), NoColor, iconColor, GFR_BUTTON_ICON_SCALE);
	}
	
	// +==============================+
	// |     Render Export Button     |
	// +==============================+
	{
		RcBindFont(GetFont(pixelFont), GFR_BUTTON_FONT_SCALE);
		bool isHovering = (appInput->mouseInsideWindow && IsInsideRec(giffer->exportBtnRec, RenderMousePos));
		bool isPressing = (isHovering && ButtonDownNoHandling(MouseButton_Left) && IsInsideRec(giffer->exportBtnRec, RenderMouseStartPos));
		if (ButtonDownNoHandling(Button_Enter)) { isPressing = true; }
		const char* exportStr = "Export";
		v2 exportStrSize = RcMeasureStringNt(exportStr);
		v2 textPos = NewVec2(
			giffer->exportBtnRec.x + giffer->exportBtnRec.width/2 - exportStrSize.width/2,
			giffer->exportBtnRec.y + giffer->exportBtnRec.height/2 - exportStrSize.height/2 + RcMaxExtendUp()
		);
		textPos = Vec2Round(textPos);
		
		Color_t backColor = VisLightOrange;
		Color_t textColor = VisWhite;
		if (isPressing)
		{
			backColor = VisYellow;
			textColor = VisRed;
		}
		else if (isHovering)
		{
			backColor = VisOrange;
		}
		
		RcDrawRectangle(giffer->exportBtnRec, backColor);
		RcDrawStringNt(exportStr, textPos, textColor);
		RcDrawPixelatedButton(RecInflate(giffer->exportBtnRec, GFR_BUTTON_ICON_SCALE), NoColor, textColor, GFR_BUTTON_ICON_SCALE);
	}
	
	// +==============================+
	// |   Render Beginning Button    |
	// +==============================+
	{
		bool isHovering = (appInput->mouseInsideWindow && IsInsideRec(giffer->begBtnRec, RenderMousePos));
		bool isPressing = (isHovering && ButtonDownNoHandling(MouseButton_Left) && IsInsideRec(giffer->begBtnRec, RenderMouseStartPos));
		if (ButtonDownNoHandling(Button_Home)) { isPressing = true; }
		rec iconRec = NewRecCentered(giffer->begBtnRec.topLeft + giffer->begBtnRec.size/2, GetSheetFrameSizef(cutsceneIcons) * GFR_BUTTON_ICON_SCALE);
		iconRec.topLeft = Vec2Round(iconRec.topLeft);
		
		Color_t backColor = VisLightOrange;
		Color_t iconColor = VisWhite;
		if (isPressing)
		{
			backColor = VisYellow;
			iconColor = VisRed;
		}
		else if (isHovering)
		{
			backColor = VisOrange;
		}
		
		RcDrawRectangle(giffer->begBtnRec, backColor);
		RcSetReplaceColorLight(iconColor);
		RcSetReplaceColorDark(ColorDarken(iconColor, 40));
		RcDrawSheetFrame(GetSheet(cutsceneIcons), NewVec2i(26, 1), iconRec, White);
		RcDrawPixelatedButton(RecInflate(giffer->begBtnRec, GFR_BUTTON_ICON_SCALE), NoColor, iconColor, GFR_BUTTON_ICON_SCALE);
	}
	
	// +==============================+
	// |      Render End Button       |
	// +==============================+
	{
		bool isHovering = (appInput->mouseInsideWindow && IsInsideRec(giffer->endBtnRec, RenderMousePos));
		bool isPressing = (isHovering && ButtonDownNoHandling(MouseButton_Left) && IsInsideRec(giffer->endBtnRec, RenderMouseStartPos));
		if (ButtonDownNoHandling(Button_End)) { isPressing = true; }
		rec iconRec = NewRecCentered(giffer->endBtnRec.topLeft + giffer->endBtnRec.size/2, GetSheetFrameSizef(cutsceneIcons) * GFR_BUTTON_ICON_SCALE);
		iconRec.topLeft = Vec2Round(iconRec.topLeft);
		
		Color_t backColor = VisLightOrange;
		Color_t iconColor = VisWhite;
		if (isPressing)
		{
			backColor = VisYellow;
			iconColor = VisRed;
		}
		else if (isHovering)
		{
			backColor = VisOrange;
		}
		
		RcDrawRectangle(giffer->endBtnRec, backColor);
		RcSetReplaceColorLight(iconColor);
		RcSetReplaceColorDark(ColorDarken(iconColor, 40));
		RcDrawSheetFrame(GetSheet(cutsceneIcons), NewVec2i(25, 1), iconRec, White);
		RcDrawPixelatedButton(RecInflate(giffer->endBtnRec, GFR_BUTTON_ICON_SCALE), NoColor, iconColor, GFR_BUTTON_ICON_SCALE);
	}
}
