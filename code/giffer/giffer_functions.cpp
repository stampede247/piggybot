/*
File:   giffer_functions.cpp
Author: Taylor Robbins
Date:   09\30\2019
Description: 
	** Holds some functions that the Giffer AppState uses to do it's job
*/

void GifferUpdateRecs()
{
	giffer->timelineRec.size = NewVec2(RenderScreenSize.width - GFR_TIMELINE_MARGIN_SIDES*2, 40);
	giffer->timelineRec.topLeft = NewVec2(GFR_TIMELINE_MARGIN_SIDES, RenderScreenSize.height - giffer->timelineRec.height);
	
	giffer->tickGutterRec = giffer->timelineRec;
	giffer->tickGutterRec.height = 15;
	giffer->tickGutterRec.y -= giffer->tickGutterRec.height;
	
	v2 availableSpace = NewVec2(RenderScreenSize.width, giffer->tickGutterRec.y);
	giffer->viewScale = 1.0f;
	if (giffer->stream.width * giffer->viewScale + 4 > availableSpace.width || giffer->stream.height * giffer->viewScale + 4 > availableSpace.height)
	{
		while (giffer->viewScale > 0.1f && (giffer->stream.width * giffer->viewScale + 4 > availableSpace.width || giffer->stream.height * giffer->viewScale + 4 > availableSpace.height)) { giffer->viewScale -= 0.1f; }
	}
	else
	{
		while (giffer->stream.width * (giffer->viewScale + 1.0f) + 4 < availableSpace.width && giffer->stream.height * (giffer->viewScale + 1.0f) + 4 < availableSpace.height) { giffer->viewScale += 1.0f; }
	}
	
	
	giffer->viewRec.size = NewVec2(giffer->stream.size) * giffer->viewScale;
	giffer->viewRec.topLeft = Vec2Round(availableSpace/2 - giffer->viewRec.size/2);
	
	giffer->endBtnRec.size = GetSheetFrameSizef(cutsceneIcons) * GFR_BUTTON_ICON_SCALE + NewVec2(GFR_BUTTON_PADDING*2);
	giffer->endBtnRec.topLeft = NewVec2(RenderScreenSize.width - GFR_BUTTON_MARGIN - giffer->endBtnRec.width, GFR_BUTTON_MARGIN);
	giffer->begBtnRec = giffer->endBtnRec;
	giffer->begBtnRec.x -= GFR_BUTTON_MARGIN + giffer->begBtnRec.width;
	
	const char* exportStr = "Export";
	v2 exportStrSize = MeasureString(GetFont(pixelFont), exportStr) * GFR_BUTTON_FONT_SCALE;
	giffer->exportBtnRec.size = exportStrSize + NewVec2(GFR_BUTTON_PADDING*2);
	giffer->exportBtnRec.topLeft = NewVec2(giffer->endBtnRec.x + giffer->endBtnRec.width - giffer->exportBtnRec.width, giffer->endBtnRec.y + giffer->endBtnRec.height + GFR_BUTTON_MARGIN);
	// giffer->exportBtnRec.topLeft = NewVec2(RenderScreenSize.width - giffer->exportBtnRec.width, giffer->exportBtnRec.y + giffer->exportBtnRec.height + GFR_BUTTON_MARGIN);
	
	giffer->discardBtnRec.size = GetSheetFrameSizef(cutsceneIcons) * GFR_BUTTON_ICON_SCALE + NewVec2(GFR_BUTTON_PADDING*2);
	giffer->discardBtnRec.topLeft = NewVec2(GFR_BUTTON_MARGIN, GFR_BUTTON_MARGIN);
	
	giffer->timelineMaxScroll = giffer->stream.numFrames - (giffer->timelineRec.width / giffer->timelineZoom);
	if (giffer->timelineMaxScroll < 0)
	{
		giffer->timelineMaxScroll = 0;
		giffer->timelineScroll = (r32)giffer->stream.numFrames/2 - (giffer->timelineRec.width/2 / giffer->timelineZoom);
		giffer->timelineScrollGoto = giffer->timelineScroll;
	}
	else
	{
		if (giffer->timelineScroll < 0) { giffer->timelineScroll = 0; }
		if (giffer->timelineScroll > giffer->timelineMaxScroll) { giffer->timelineScroll = giffer->timelineMaxScroll; }
		if (giffer->timelineScrollGoto < 0) { giffer->timelineScrollGoto = 0; }
		if (giffer->timelineScrollGoto > giffer->timelineMaxScroll) { giffer->timelineScrollGoto = giffer->timelineMaxScroll; }
	}
}

void GifferUpdateViewTexture()
{
	DestroyTexture(&giffer->viewTexture);
	
	TempPushMark();
	u32 pixelDataSize = 0;
	u8* pixelData = GetGifStreamFramePixelData(TempArena, &giffer->stream, giffer->currentFrame, true, &pixelDataSize);
	Assert(pixelData != nullptr);
	giffer->viewTexture = CreateTexture(pixelData, giffer->stream.width, giffer->stream.height, true, false);
	giffer->viewTexture.isValid = true;
	TempPopMark();
}

v2 GifferScreenToTimeline(v2 screenPos)
{
	v2 result = screenPos - giffer->timelineRec.topLeft;
	result.x /= giffer->timelineZoom;
	result.x += giffer->timelineScroll;
	return result;
}
v2 GifferTimelineToScreen(v2 timelinePos)
{
	v2 result = timelinePos;
	result.x -= giffer->timelineScroll;
	result.x *= giffer->timelineZoom;
	result += giffer->timelineRec.topLeft;
	return result;
}
