/*
File:   res_deser_context.h
Author: Taylor Robbins
Date:   07\06\2020
*/

#ifndef _RES_DESER_CONTEXT_H
#define _RES_DESER_CONTEXT_H

typedef enum
{
	DeserResult_None = 0x00,
	DeserResult_Unrecognized,
	DeserResult_Failed,
	DeserResult_Partial,
	DeserResult_Warnings,
	DeserResult_Info,
	DeserResult_Success,
	DeserResult_NumResultTypes,
} DeserResult_t;

const char* GetDeserResultStr(DeserResult_t result)
{
	switch (result)
	{
		case DeserResult_None:         return "None";
		case DeserResult_Unrecognized: return "Unrecognized";
		case DeserResult_Failed:       return "Failed";
		case DeserResult_Partial:      return "Partial";
		case DeserResult_Warnings:     return "Warnings";
		case DeserResult_Info:         return "Info";
		case DeserResult_Success:      return "Success";
		default: return "Unknown";
	}
}
bool IsDeserResultOkay(DeserResult_t result)
{
	if (result == DeserResult_Partial) { return true; }
	if (result == DeserResult_Warnings) { return true; }
	if (result == DeserResult_Info) { return true; }
	if (result == DeserResult_Success) { return true; }
	return false;
}

typedef enum
{
	DeserErrorCode_None = 0x00,
	DeserErrorCode_Generic,
	DeserErrorCode_HeaderInvalid,
	DeserErrorCode_HeaderIncomplete,
	DeserErrorCode_FutureVersion,
	DeserErrorCode_ExpectedMoreBytes,
	DeserErrorCode_InvalidSize,
	DeserErrorCode_NoLevelInFile,
	DeserErrorCode_NestedDeserProblem,
	DeserErrorCode_NumCodes,
} DeserErrorCode_t;

const char* GetDeserErrorCodeStr(DeserErrorCode_t errorCode)
{
	switch (errorCode)
	{
		case DeserErrorCode_None:               return "None";
		case DeserErrorCode_Generic:            return "Generic";
		case DeserErrorCode_HeaderIncomplete:   return "Header Incomplete";
		case DeserErrorCode_FutureVersion:      return "Future Version";
		case DeserErrorCode_ExpectedMoreBytes:  return "Expected More Bytes";
		case DeserErrorCode_InvalidSize:        return "Invalid Size";
		case DeserErrorCode_NoLevelInFile:      return "No Level In File";
		case DeserErrorCode_NestedDeserProblem: return "Nexted Deser Problem";
		default: return "Unknown";
	}
}

struct DeserContext_t
{
	MemoryArena_t* allocArena;
	char* filePath;
	char* processName;
	bool isBatchDeserProcess;
	ConsoleArena_t output;
	
	const void* dataPntr;
	u32 dataSize;
	u32 byteIndex;
	
	//NOTE: implies that memory was allocated for the Level_t, or SaveGame_t, or whatever we are deserializing.
	//      Used to clean allocations in the event of a failed deserialization half-way through the process
	bool itemCreated;
	
	u8 formatVersionMajor;
	u8 formatVersionMinor;
	bool isOldVersion;
	DeserResult_t result;
	DeserErrorCode_t errorCode;
};

#endif //  _RES_DESER_CONTEXT_H
