/*
File:   res_palette.h
Author: Taylor Robbins
Date:   10\25\2018
*/

#ifndef _RES_PALETTE_H
#define _RES_PALETTE_H

#define SUPER_PACKED_COLOR_CUSTOM_BIT 0x80000000

#define PAL_DARKEST_INDEX     0
#define PAL_DARK4_INDEX       1
#define PAL_DARK3_INDEX       2
#define PAL_DARK2_INDEX       3
#define PAL_DARK_INDEX        4
#define PAL_PRIMARY4_INDEX    5
#define PAL_PRIMARY3_INDEX    6
#define PAL_PRIMARY2_INDEX    7
#define PAL_PRIMARY_INDEX     8
#define PAL_SECONDARY_INDEX   9
#define PAL_SECONDARY2_INDEX  10
#define PAL_LIGHT_INDEX       11
#define PAL_LIGHT2_INDEX      12
#define PAL_LIGHT3_INDEX      13
#define PAL_LIGHT4_INDEX      14
#define PAL_LIGHT5_INDEX      15

#include "res_palette_list.h" //include for the first time to have it define NUM_PALETTE_COLORS

union PalColor_t
{
	Color_t colors[NUM_REPLACE_COLORS];
	struct
	{
		Color_t darkest;
		Color_t dark4;
		Color_t dark3;
		Color_t dark2;
		Color_t dark; //target3, c3
		Color_t primary4;
		Color_t primary3;
		Color_t primary2;
		Color_t primary; //target1, c0
		Color_t secondary; //target4, c1
		Color_t secondary2;
		Color_t light; //target2, c2
		Color_t light2;
		Color_t light3;
		Color_t light4;
		Color_t light5; //TODO: Rename this lightest
	};
};

union PaletteColorList_t
{
	PalColor_t colors[NUM_PALETTE_COLORS];
	struct
	{
		#define PALETTE_ENTRY(varName, readableName) PalColor_t varName
		#include "res_palette_list.h"
	};
	struct
	{
		PalColor_t dummy1;
		PalColor_t variants[5];
		PalColor_t teams[4];
	};
};

struct ColorPalette_t
{
	u32 numColors;
	PaletteColorList_t* colors;
};

const char* GetPaletteColorStr(u32 colorIndex)
{
	u32 cIndex = 0;
	#define PALETTE_ENTRY(varName, readableName) if (colorIndex == cIndex) { return readableName; } cIndex++
	#include "res_palette_list.h"
	return "Unknown";
}

const char* GetPaletteSubIndexStr(u8 subIndex)
{
	switch (subIndex)
	{
		case 0: return "Darkest";
		case 1: return "Dark4";
		case 2: return "Dark3";
		case 3: return "Dark2";
		case 4: return "Dark";
		case 5: return "Primary4";
		case 6: return "Primary3";
		case 7: return "Primary2";
		case 8: return "Primary";
		case 9: return "Secondary";
		case 10: return "Secondary2";
		case 11: return "Light";
		case 12: return "Light2";
		case 13: return "Light3";
		case 14: return "Light4";
		case 15: return "Light5";
		default: return "Unknown";
	}
}
const char* GetPaletteSubIndexStrShort(u8 subIndex)
{
	switch (subIndex)
	{
		case 0: return "Dst";
		case 1: return "Dk4";
		case 2: return "Dk3";
		case 3: return "Dk2";
		case 4: return "Dark";
		case 5: return "Pm4";
		case 6: return "Pm3";
		case 7: return "Pm2";
		case 8: return "Prmry";
		case 9: return "Scndry";
		case 10: return "Sc2";
		case 11: return "Light";
		case 12: return "Lt2";
		case 13: return "Lt3";
		case 14: return "Lt4";
		case 15: return "Lt5";
		default: return "Unk";
	}
}

void SetAllPalColorsTo(PalColor_t* palColorOut, Color_t color)
{
	Assert(palColorOut != nullptr);
	for (u32 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++) { palColorOut->colors[cIndex] = color; }
}
void LerpAllPalColorsTo(PalColor_t* palColorOut, Color_t lerpColor, r32 amount)
{
	Assert(palColorOut != nullptr);
	for (u32 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
	{
		palColorOut->colors[cIndex] = ColorLerp(palColorOut->colors[cIndex], lerpColor, amount);
	}
}
PalColor_t LerpPalColors(PalColor_t color1, PalColor_t color2, r32 amount)
{
	PalColor_t result = {};
	for (u32 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
	{
		result.colors[cIndex] = ColorLerp(color1.colors[cIndex], color2.colors[cIndex], amount);
	}
	return result;
}

//positive amount values shifts the lighter colors down making things brighter. Negative makes things darker
void PalColorShift(PalColor_t* palColor, i32 amount)
{
	Assert(palColor != nullptr);
	if (amount > 0)
	{
		for (u32 cIndex = 0; cIndex < NUM_REPLACE_COLORS; cIndex++)
		{
			palColor->colors[cIndex] = palColor->colors[ClampI32((i32)cIndex + amount, 0, NUM_REPLACE_COLORS-1)];
		}
	}
	else if (amount < 0)
	{
		for (u32 cIndex = NUM_REPLACE_COLORS; cIndex > 0; cIndex--)
		{
			palColor->colors[cIndex-1] = palColor->colors[ClampI32((i32)cIndex-1 + amount, 0, NUM_REPLACE_COLORS-1)];
		}
	}
}
//TODO: This function doesn't work very well for negative amount values
void PalColorShiftSectionLooped(PalColor_t* palColor, u8 sectionStart, u8 sectionLength, i32 amount)
{
	Assert(palColor != nullptr);
	Assert(sectionStart < NUM_REPLACE_COLORS);
	Assert(sectionStart+sectionLength <= NUM_REPLACE_COLORS);
	if (sectionLength == 0) { return; }
	if (amount == 0) { return; }
	PalColor_t temp = *palColor;
	for (u8 cIndex = 0; cIndex < sectionLength; cIndex++)
	{
		u8 srcIndex = (u8)(((i32)cIndex + amount) % sectionLength);
		palColor->colors[sectionStart + cIndex] = temp.colors[sectionStart + srcIndex];
	}
}

#endif //  _RES_PALETTE_H
