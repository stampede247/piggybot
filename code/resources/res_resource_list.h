/*
File:   res_resource_list.h
Author: Taylor Robbins
Date:   05\04\2020
Description:
	** This is a special file that can be included multiple times to obtain different effects. Before the file is included you must define
	** RESOURCE_TEXTURE, RESOURCE_SHEET, RESOURCE_FONT_TTF, RESOURCE_FONT_BITMAP, RESOURCE_SHADER, and or RESOURCE_MUSIC in order for
	** this file to have any real effect (besides defining NUM_RESOURCE_... defines)
*/

// +--------------------------------------------------------------+
// |                        Sprite Sheets                         |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_SHEETS
#define NUM_RESOURCE_SHEETS 8
#endif
#ifndef RESOURCE_SHEET
#define RESOURCE_SHEET(varName, readableName, filePath, sheetSize, appStatesBits) //nothing
#endif

RESOURCE_SHEET(entitiesSheet,    "Entities Sheet",     SHEETS_FLDR "_entities.png",      NewVec2i(32, 32),  AppState_None);
RESOURCE_SHEET(cutsceneIcons,    "Cutscene Icons",     SHEETS_FLDR "cutscene_icons.png", NewVec2i(32,  3),  AppState_None);
RESOURCE_SHEET(personSheet,      "Person Sheet",       SHEETS_FLDR "_person.png",        NewVec2i(32, 32),  AppStateBit_Visualizer);
RESOURCE_SHEET(partsSheet,       "Parts Sheet",        SHEETS_FLDR "_parts.png",         NewVec2i(32, 32),  AppStateBit_Visualizer);
RESOURCE_SHEET(messageBackSheet, "Message Back Sheet", SHEETS_FLDR "message_back.png",   NewVec2i(3, 3),    AppStateBit_Visualizer);
RESOURCE_SHEET(emojiSheet,       "Emoji Sheet",        FONTS_FLDR  "emoji.png",          NewVec2i(32, 32),  AppStateBit_Visualizer);
RESOURCE_SHEET(tractorBeamSheet, "Tractor Beam Sheet", SHEETS_FLDR "tractor_beam.png",   NewVec2i(16,  3),  AppStateBit_Visualizer);
RESOURCE_SHEET(stretchAnimSheet, "Stretch Anim Sheet", SHEETS_FLDR "stretch_anim.png",   NewVec2i(2,  1),   AppStateBit_Visualizer);

#undef RESOURCE_SHEET

// +--------------------------------------------------------------+
// |                           Textures                           |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_TEXTURES
#define NUM_RESOURCE_TEXTURES 6
#endif
#ifndef RESOURCE_TEXTURE
#define RESOURCE_TEXTURE(varName, readableName, filePath, pixelated, looping, appStatesBits) //nothing
#endif

RESOURCE_TEXTURE(logoSprite,       "Logo Sprite",       SPRITES_FLDR  "logo.png",              true,  false, AppState_None);
RESOURCE_TEXTURE(selectionTexture, "Selection Texture", TEXTURES_FLDR "selection.png",         true,  true,  AppState_None);
RESOURCE_TEXTURE(alphaTexture,     "Alpha Texture",     TEXTURES_FLDR "alpha.png",             true,  true,  AppState_None);
RESOURCE_TEXTURE(backgroundBorder, "Background Border", SPRITES_FLDR  "background_border.png", true,  false, AppStateBit_Visualizer);
RESOURCE_TEXTURE(backgroundBack,   "Background Back",   SPRITES_FLDR  "background_back.png",   true,  false, AppStateBit_Visualizer);
RESOURCE_TEXTURE(humanSpaceship,   "Human Spaceship",   SPRITES_FLDR  "human_spaceship.png",   true,  false, AppStateBit_Visualizer);

#undef RESOURCE_TEXTURE

// +--------------------------------------------------------------+
// |                            Musics                            |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_MUSICS
#define NUM_RESOURCE_MUSICS 1
#endif
#ifndef RESOURCE_MUSIC
#define RESOURCE_MUSIC(varName, readableName, filePath) //nothing
#endif

RESOURCE_MUSIC(testSong, "Test Song", MUSIC_FLDR "_test.ogg");

#undef RESOURCE_MUSIC

// +--------------------------------------------------------------+
// |                            Fonts                             |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_FONTS
#define NUM_RESOURCE_FONTS 7
#endif
#ifndef RESOURCE_FONT_TTF
#define RESOURCE_FONT_TTF(varName, readableName, fontName, fontSize, isBold, isItalic, firstChar, numChars, bitmapSize) //nothing
#endif
#ifndef RESOURCE_FONT_BITMAP
#define RESOURCE_FONT_BITMAP(varName, readableName, filePath, charSize, charUnderhang, firstChar, numChars) //nothing
#endif

RESOURCE_FONT_TTF(debugFont,    "Debug Font",     DEBUG_FONT_NAME, 18.0f, true, false, ' ', 96, 256);
RESOURCE_FONT_TTF(smallFont,    "Small Font",     DEBUG_FONT_NAME, 14.0f, true, false, ' ', 96, 256);
RESOURCE_FONT_TTF(bigDebugFont, "Big Debug Font", DEBUG_FONT_NAME, 32.0f, true, false, ' ', 96, 256);

RESOURCE_FONT_BITMAP(pixelFont,          "Pixel Font",            FONTS_FLDR "pixelFont.png",            NewVec2i( 6,  8), 0, ' ', 96);
RESOURCE_FONT_BITMAP(bigPixelFont,       "Big Pixel Font",        FONTS_FLDR "pixelFontLarge.png",       NewVec2i(12, 16), 2, ' ', 96);
RESOURCE_FONT_BITMAP(pixelFontOutline,   "Pixel Font Outline",    FONTS_FLDR "pixelFontOutline.png",     NewVec2i( 8, 10), 0, ' ', 96);
RESOURCE_FONT_BITMAP(titleFont,          "Title Font",            FONTS_FLDR "titleFont.png",            NewVec2i(20, 30), 0, ' ', 96);

// RESOURCE_FONT_TTF(georgiaRegular,  "Georgia Regular",  "Georgia",  36.0f, true, false, ' ', 96, 256);
// RESOURCE_FONT_TTF(georgiaLarge,    "Georgia Large",    "Georgia",  56.0f, true, false, ' ', 96, 512);
// RESOURCE_FONT_TTF(consolasRegular, "Consolas Regular", "Consolas", 32.0f, true, false, ' ', 96, 512);
// RESOURCE_FONT_TTF(consolasLarge,   "Consolas Large",   "Consolas", 46.0f, true, false, ' ', 96, 512);

#undef RESOURCE_FONT_TTF
#undef RESOURCE_FONT_BITMAP

// +--------------------------------------------------------------+
// |                           Shaders                            |
// +--------------------------------------------------------------+
#ifndef NUM_RESOURCE_SHADERS
#define NUM_RESOURCE_SHADERS 7
#endif
#ifndef RESOURCE_SHADER
#define RESOURCE_SHADER(varName, readableName, filePath) //nothing
#endif

RESOURCE_SHADER(mainShader,         "Main Shader",          SHADERS_FLDR "main.glsl");
RESOURCE_SHADER(tileShader,         "Tile Shader",          SHADERS_FLDR "tile.glsl");
RESOURCE_SHADER(fontShader,         "Font Shader",          SHADERS_FLDR "font.glsl");
RESOURCE_SHADER(mainPremulShader,   "Main Premul Shader",   SHADERS_FLDR "main_premul_alpha.glsl");
RESOURCE_SHADER(tilePremulShader,   "Tile Premul Shader",   SHADERS_FLDR "tile_premul_alpha.glsl");
RESOURCE_SHADER(octogramShader,     "Octogram Shader",      SHADERS_FLDR "octogram.glsl");
RESOURCE_SHADER(cineShader,         "Cine Shader",          SHADERS_FLDR "cine.glsl");

// RESOURCE_SHADER(presentShader1,     "Present Shader 1",     SHADERS_FLDR "test.glsl");
// RESOURCE_SHADER(presentShader2,     "Present Shader 2",     SHADERS_FLDR "test-old.glsl");
// RESOURCE_SHADER(presentShader3,     "Present Shader 3",     SHADERS_FLDR "test-lighting.glsl");
// RESOURCE_SHADER(presentShader4,     "Present Shader 4",     SHADERS_FLDR "hue_slider.glsl");

#undef RESOURCE_SHADER

#ifndef NUM_RESOURCES
#define NUM_RESOURCES (NUM_RESOURCE_SHEETS + NUM_RESOURCE_TEXTURES + NUM_RESOURCE_MUSICS + NUM_RESOURCE_FONTS + NUM_RESOURCE_SHADERS)
#endif
