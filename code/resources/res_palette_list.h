/*
File:   res_palette_list.h
Author: Taylor Robbins
Date:   06\25\2020
Description:
	** This is a special file that can be included multiple times to obtain different effects. Before the file is included you must define
	** PALETTE_ENTRY in order for this file to have any real effect (besides defining NUM_PALETTE_COLORS when you include it for the first time)
*/


#ifndef NUM_PALETTE_COLORS
#define NUM_PALETTE_COLORS 28
#endif

#ifndef PALETTE_ENTRY
#define PALETTE_ENTRY(varName, readableName) //nothing
#endif

PALETTE_ENTRY(target,          "Target");          //0
PALETTE_ENTRY(variant1,        "Variant1");        //1
PALETTE_ENTRY(variant2,        "Variant2");        //2
PALETTE_ENTRY(variant3,        "Variant3");        //3
PALETTE_ENTRY(variant4,        "Variant4");        //4
PALETTE_ENTRY(variant5,        "Variant5");        //5
PALETTE_ENTRY(team1,           "Team1");           //6
PALETTE_ENTRY(team2,           "Team2");           //7
PALETTE_ENTRY(team3,           "Team3");           //8
PALETTE_ENTRY(team4,           "Team4");           //9
PALETTE_ENTRY(alienNormal,     "Alien Normal");    //10
PALETTE_ENTRY(alienGreen,      "Alien Green");     //11
PALETTE_ENTRY(alienBlue,       "Alien Blue");      //12
PALETTE_ENTRY(alienYellow,     "Alien Yellow");    //13
PALETTE_ENTRY(alienOrange,     "Alien Orange");    //14
PALETTE_ENTRY(alienRed,        "Alien Red");       //15
PALETTE_ENTRY(alienWhite,      "Alien White");     //16
PALETTE_ENTRY(alienCyan,       "Alien Cyan");      //17
PALETTE_ENTRY(alienPink,       "Alien Pink");      //18
PALETTE_ENTRY(alienBrown,      "Alien Brown");     //19
PALETTE_ENTRY(alienLime,       "Alien Lime");      //20
PALETTE_ENTRY(alienPurple,     "Alien Purple");    //21
PALETTE_ENTRY(alienSky,        "Alien Sky");       //22
PALETTE_ENTRY(alienInvisible,  "Alien Invisible"); //23
PALETTE_ENTRY(alienGrey,       "Alien Grey");      //24
PALETTE_ENTRY(alienBlack,      "Alien Black");     //25
PALETTE_ENTRY(alienSlate,      "Alien Slate");     //26
PALETTE_ENTRY(alienRainbow,    "Alien Rainbow");   //27

#undef PALETTE_ENTRY
