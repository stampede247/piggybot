/*
File:   res_serialization_helpers.cpp
Author: Taylor Robbins
Date:   11\01\2018
Description: 
	** Holds functions that help us write the serialization functions more readably
*/

#define BIN_CONTENT_WRITE(contentPntr, index, maxIndex, type, value) do \
{                                                                       \
	if ((contentPntr) != nullptr)                                       \
	{                                                                   \
		Assert((index) + sizeof(type) <= (maxIndex));                   \
		*((type*)((contentPntr) + (index))) = (value);                  \
	}                                                                   \
	(index) += sizeof(type);                                            \
} while(0)
#define BIN_CONTENT_WRITE_BYTES(contentPntr, index, maxIndex, numBytes, valuePntr) do \
{                                                                                     \
	if ((contentPntr) != nullptr)                                                     \
	{                                                                                 \
		Assert((index) + numBytes <= (maxIndex));                                     \
		MyMemCopy((contentPntr) + (index), (valuePntr), numBytes);                    \
	}                                                                                 \
	(index) += (numBytes);                                                            \
} while(0)
#define BIN_CONTENT_WRITE_STRUCT(contentPntr, index, maxIndex, type, structVarName) \
	type* structVarName = (type*)((contentPntr) + (index));                         \
	Assert((contentPntr) == nullptr || (index) + sizeof(type) <= (maxIndex));       \
	(index) += sizeof(type);                                                        \
	if ((contentPntr) != nullptr)


void SrlPrint(char* levelData, u32 levelDataLength, u32* offsetPntr, const char* formatStr, ...)
{
	va_list args;
	va_start(args, formatStr);
	u32 length = (u32)vsnprintf(nullptr, 0, formatStr, args);//Measure first
	va_end(args);
	
	if (levelData == nullptr)
	{
		*offsetPntr += length;
		return;
	}
	else
	{
		Assert(*offsetPntr + length+1 <= levelDataLength);
		char* outputPntr = &levelData[*offsetPntr];
		*offsetPntr += length;
		
		va_start(args, formatStr);
		vsnprintf(outputPntr, length+1, formatStr, args); //Real printf
		va_end(args);
	}
}
