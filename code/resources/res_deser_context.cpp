/*
File:   res_deser_context.cpp
Author: Taylor Robbins
Date:   07\06\2020
Description: 
	** Holds some functions that help us create and handle deserialization contexts in various ways
	** The Deserialization context is a way to bundle information about why or where we are deserializing things from
	** and also a way for to provide the deserialization code a place to put debug output about what
	** happened during the deserialization process so that if something goes wrong we can save the debug
	** output and present it to the user somehow later
*/

void DestroyDeserContext(DeserContext_t* context)
{
	Assert(context != nullptr);
	if (context->filePath != nullptr)
	{
		Assert(context->allocArena != nullptr);
		ArenaPop(context->allocArena, context->filePath);
	}
	if (context->processName != nullptr)
	{
		Assert(context->allocArena != nullptr);
		ArenaPop(context->allocArena, context->processName);
	}
	DestroyConsoleArena(&context->output);
	ClearPointer(context);
}
void InitializeDeserContext(DeserContext_t* context, MemoryArena_t* memArena, const char* filePath, const char* processName, u32 outputTextArenaSize, u32 outputLineBuildSize, MemoryArena_t* printArena)
{
	Assert(context != nullptr);
	Assert(memArena != nullptr);
	ClearPointer(context);
	context->allocArena = memArena;
	if (filePath != nullptr)
	{
		context->filePath = ArenaNtString(memArena, filePath);
		Assert(context->filePath != nullptr);
	}
	if (processName != nullptr)
	{
		context->processName = ArenaNtString(memArena, processName);
		Assert(context->processName != nullptr);
	}
	if (outputTextArenaSize > 0)
	{
		InitializeConsoleArena(&context->output, memArena, outputTextArenaSize, outputLineBuildSize, 0);
		context->output.printArena = printArena;
		context->output.saveFilePaths = false;
		context->output.saveFunctionNames = false;
	}
	else { Assert(outputLineBuildSize == 0 && printArena == nullptr); }
}

void CreateLevelDeserContext(DeserContext_t* context, const char* filePath, const char* processName, bool isBatchProcess)
{
	Assert(context != nullptr);
	InitializeDeserContext(context, mainHeap, filePath, processName, Kilobytes(4), Kilobytes(1), TempArena);
	context->output.saveFilePaths = false;
	context->output.saveFunctionNames = false;
}
void SetDeserContextFilePath(DeserContext_t* context, const char* filePath)
{
	Assert(context != nullptr);
	Assert(filePath != nullptr);
	Assert(context->allocArena != nullptr);
	if (context->filePath != nullptr)
	{
		ArenaPop(context->allocArena, context->filePath);
	}
	context->filePath = ArenaNtString(context->allocArena, filePath);
	Assert(context->filePath != nullptr);
}
void SetDeserContextProcessName(DeserContext_t* context, const char* processName)
{
	Assert(context != nullptr);
	Assert(processName != nullptr);
	Assert(context->allocArena != nullptr);
	if (context->processName != nullptr)
	{
		ArenaPop(context->allocArena, context->processName);
	}
	context->processName = ArenaNtString(context->allocArena, processName);
	Assert(context->processName != nullptr);
}

void HandleLevelDeserContext(DeserContext_t* context, bool recycle = false)
{
	Assert(context != nullptr);
	if (context->result != DeserResult_Success)
	{
		if (context->isBatchDeserProcess)
		{
			//We don't want to immediately let the user know about the problems.
			//TODO: We should file away the output log somewhere where the user can find it
		}
		else
		{
			//The user is waiting on this deserialization process that failed
			//TODO: We should let the user know immediately what happened to the best of our ability
		}
	}
	if (recycle)
	{
		ConsoleArenaClearLines(&context->output);
		context->dataPntr = nullptr;
		context->dataSize = 0;
		context->byteIndex = 0;
		context->itemCreated = false;
		context->formatVersionMajor = 0;
		context->formatVersionMinor = 0;
		context->isOldVersion = false;
		context->result = DeserResult_None;
		context->errorCode = DeserErrorCode_None;
	}
	else
	{
		DestroyDeserContext(context);
	}
}

u32 DeserNumBytesLeft(DeserContext_t* context)
{
	Assert(context != nullptr);
	return context->dataSize - context->byteIndex;
}
const u8* DeserConsumeBytes(DeserContext_t* context, u32 numBytes)
{
	Assert(context != nullptr);
	if (context->byteIndex + numBytes <= context->dataSize)
	{
		const u8* result = ((const u8*)context->dataPntr) + context->byteIndex;
		context->byteIndex += numBytes;
		return result;
	}
	else { return nullptr; }
}
#define DeserConsumeType(context, typeName) (const typeName*)DeserConsumeBytes((context), sizeof(typeName))
const u8* DeserCurrentDataPntr(DeserContext_t* context)
{
	Assert(context != nullptr);
	return DeserConsumeBytes(context, 0);
}

#define DeserUnrecognizedFile(context, error, formatStr, ...) do         \
{                                                                        \
	CslArenaPrintLine_E(&(context)->output, (formatStr), ##__VA_ARGS__); \
	(context)->errorCode = (error);                                      \
	return DeserResult_Unrecognized;                                     \
} while(0)
#define DeserFatalError(context, error, formatStr, ...) do               \
{                                                                        \
	CslArenaPrintLine_E(&(context)->output, (formatStr), ##__VA_ARGS__); \
	(context)->errorCode = (error);                                      \
	return DeserResult_Failed;                                           \
} while(0)
#define DeserPartialEnd(context, formatStr, ...) do                      \
{                                                                        \
	CslArenaPrintLine_E(&(context)->output, (formatStr), ##__VA_ARGS__); \
	return DeserResult_Partial;                                          \
} while(0)
#define DeserWarning(context, hasWarnings, formatStr, ...) do            \
{                                                                        \
	CslArenaPrintLine_W(&(context)->output, (formatStr), ##__VA_ARGS__); \
	hasWarnings = true;                                                  \
} while(0)
#define DeserInfo(context, hasImportantInfo, formatStr, ...) do          \
{                                                                        \
	CslArenaPrintLine_W(&(context)->output, (formatStr), ##__VA_ARGS__); \
	hasImportantInfo = true;                                             \
} while(0)

bool DeserVerifyHeader(DeserContext_t* context, u32 validHeaderLength, const char* validHeaderStr, bool consumeBytes)
{
	Assert(context != nullptr);
	Assert(validHeaderStr != nullptr);
	Assert(validHeaderLength > 0);
	const u8* headerPntr = nullptr;
	if (consumeBytes)
	{
		headerPntr = DeserConsumeBytes(context, validHeaderLength);
		if (headerPntr == nullptr)
		{
			CslArenaPrintLine_E(&context->output, "Expected %u bytes for header. Only %u bytes left", validHeaderLength, DeserNumBytesLeft(context));
			return false;
		}
	}
	else
	{
		if (DeserNumBytesLeft(context) < validHeaderLength)
		{
			CslArenaPrintLine_E(&context->output, "Expected %u bytes for header. Only %u bytes left", validHeaderLength, DeserNumBytesLeft(context));
			return false;
		}
		headerPntr = DeserConsumeBytes(context, 0);
		Assert(headerPntr != nullptr);
	}
	if (MyMemCompare(headerPntr, validHeaderStr, validHeaderLength) != 0)
	{
		CslArenaPrint_E(&context->output, "Invalid %u byte header: \"", validHeaderLength);
		for (u32 cIndex = 0; cIndex < validHeaderLength; cIndex++)
		{
			char nextChar = (char)headerPntr[cIndex];
			if (IsCharClassPrintable(nextChar)) { CslArenaPrint_E(&context->output, "%c", nextChar); }
			else { CslArenaPrint_E(&context->output, "[%02X]", (u8)nextChar); }
		}
		CslArenaWrite_E(&context->output, "\" expected \"");
		for (u32 cIndex = 0; cIndex < validHeaderLength; cIndex++)
		{
			char nextChar = validHeaderStr[cIndex];
			if (IsCharClassPrintable(nextChar)) { CslArenaPrint_E(&context->output, "%c", nextChar); }
			else { CslArenaPrint_E(&context->output, "[%02X]", (u8)nextChar); }
		}
		CslArenaWriteLine_E(&context->output, "\"");
		return false;
	}
	return true;
}

bool DeserConsumeVarStruct_(DeserContext_t* context, u32 totalStructSize, u32 sizeVarOffset, u32 sizeVarSize, u32 requiredNumBytes, void* structPntr)
{
	Assert(context != nullptr);
	Assert(structPntr != nullptr);
	Assert(sizeVarSize == sizeof(u32));
	u32 numBytesNeededForSizeVar = sizeVarOffset + sizeVarSize;
	Assert(numBytesNeededForSizeVar <= requiredNumBytes);
	const u8* firstBytes = DeserConsumeBytes(context, numBytesNeededForSizeVar);
	if (firstBytes == nullptr)
	{
		CslArenaPrintLine_E(&context->output, "Needed %u bytes to read next struct. Only %u bytes left", numBytesNeededForSizeVar, DeserNumBytesLeft(context));
		return false;
	}
	u32 declaredSize = *((const u32*)(firstBytes + sizeVarOffset));
	if (declaredSize < requiredNumBytes)
	{
		CslArenaPrintLine_E(&context->output, "Structure is too small. Declared %u bytes but %u required", declaredSize, requiredNumBytes);
		return false;
	}
	MyMemCopy(structPntr, firstBytes, numBytesNeededForSizeVar);
	u32 numBytesLeft = declaredSize - numBytesNeededForSizeVar;
	if (numBytesLeft > 0)
	{
		const u8* finalBytes = DeserConsumeBytes(context, numBytesLeft);
		if (finalBytes == nullptr)
		{
			CslArenaPrintLine_E(&context->output, "Structure declared %u bytes but only %u bytes left", declaredSize, numBytesNeededForSizeVar + DeserNumBytesLeft(context));
			return false;
		}
		MyMemCopy(((u8*)structPntr) + numBytesNeededForSizeVar, finalBytes, numBytesLeft);
	}
	return true;
}
#define DeserConsumeVarStruct(context, structType, sizeVarName, lastRequiredVarName, structPntr) DeserConsumeVarStruct_((context), sizeof(structType), STRUCT_VAR_OFFSET(structType, sizeVarName), STRUCT_VAR_SIZE(structType, sizeVarName), STRUCT_VAR_END_OFFSET(structType, lastRequiredVarName), (void*)(structPntr))

bool DeserIsOptionalVarMissing_(u32 declaredStructSize, u32 varEndOffset)
{
	if (declaredStructSize >= varEndOffset) { return true; }
	else { return false; }
}
#define DeserIsOptionalVarMissing(declaredStructSize, structType, varName) DeserIsOptionalVarMissing_((declaredStructSize), STRUCT_VAR_END_OFFSET(structType, varName))

