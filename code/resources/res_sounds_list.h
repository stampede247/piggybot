/*
File:   res_sounds_list.h
Author: Taylor Robbins
Date:   11\21\2019
Description:
	** This is a special file that can be included multiple times to obatin different effects. Before the file is included you must define
	** SOUND_EFFECT_ENTRY(internalName, looping, readableName, fileName)
*/

#ifndef SOUND_EFFECT_ENTRY
#error res_sounds_list.h was included without SOUND_EFFECT_ENTRY being defined
#endif

#ifndef NUM_SOUND_EFFECT_RESOURCES
#define NUM_SOUND_EFFECT_RESOURCES 26
#endif

SOUND_EFFECT_ENTRY(ledOnSound,                false, "Led On",                  "led_on.ogg");
SOUND_EFFECT_ENTRY(ledOffSound,               false, "Led Off",                 "led_off.ogg");
SOUND_EFFECT_ENTRY(cantMoveSound,             false, "Cant Move",               "cant_move.ogg");
SOUND_EFFECT_ENTRY(notificationSound,         false, "Notification",            "notification.ogg");
SOUND_EFFECT_ENTRY(personSpawnSound,          false, "Person Spawn",            "person_spawn.ogg");
SOUND_EFFECT_ENTRY(talkingSound1,             false, "Talking 1",               "talking_1.ogg");
SOUND_EFFECT_ENTRY(talkingSound2,             false, "Talking 2",               "talking_2.ogg");
SOUND_EFFECT_ENTRY(talkingSound3,             false, "Talking 3",               "talking_3.ogg");
SOUND_EFFECT_ENTRY(talkingSound4,             false, "Talking 4",               "talking_4.ogg");
SOUND_EFFECT_ENTRY(talkingSound5,             false, "Talking 5",               "talking_5.ogg");
SOUND_EFFECT_ENTRY(eatSound1,                 false, "Eat 1",                   "eat_1.ogg");
SOUND_EFFECT_ENTRY(yahooSound1,               false, "Yahoo 1",                  "yahoo_1.ogg");
SOUND_EFFECT_ENTRY(yahooSound2,               false, "Yahoo 2",                  "yahoo_1.ogg");
SOUND_EFFECT_ENTRY(yahooSound3,               false, "Yahoo 3",                  "yahoo_1.ogg");
SOUND_EFFECT_ENTRY(yahooSound4,               false, "Yahoo 4",                  "yahoo_1.ogg");
SOUND_EFFECT_ENTRY(applauseSound1,            false, "Applause 1",               "applause1.ogg");
SOUND_EFFECT_ENTRY(applauseSound2,            false, "Applause 2",               "applause2.ogg");
SOUND_EFFECT_ENTRY(applauseSound3,            false, "Applause 3",               "applause3.ogg");
SOUND_EFFECT_ENTRY(applauseSound4,            false, "Applause 4",               "applause4.ogg");
SOUND_EFFECT_ENTRY(applauseSound5,            false, "Applause 5",               "applause5.ogg");
SOUND_EFFECT_ENTRY(applauseSound6,            false, "Applause 6",               "applause6.ogg");
SOUND_EFFECT_ENTRY(spaceshipAppear,           false, "Spaceship Appear",         "spaceship_appear.ogg");
SOUND_EFFECT_ENTRY(spaceshipLeave,            false, "Spaceship Leave",          "spaceship_leave.ogg");
SOUND_EFFECT_ENTRY(spaceshipLoop,             true,  "Spaceship Loop",           "spaceship_loop.ogg");
SOUND_EFFECT_ENTRY(spaceshipTractorBeam,      false, "Spaceship Tractor Beam",   "spaceship_tractor_beam.ogg");
SOUND_EFFECT_ENTRY(stretchTimerSound,         true,  "Stretch Timer Sound",      "stretch_timer.ogg");

#undef SOUND_EFFECT_ENTRY
