/*
File:   res_bin_deserializer.cpp
Author: Taylor Robbins
Date:   03\23\2020
Description: 
	** Holds the BinDeserializer_t structure that helps us keep track of our progress
	** as we deserialize binary file formats as well as handles a few other pieces of
	** functionality to make our lives easier
*/

struct BinDeserializer_t
{
	bool printOutErrors;
	bool errorOccurred;
	const char* formatName;
	
	const u8* dataPntr;
	u32 dataSize;
	u32 index;
};

void CreateBinDeserializer(BinDeserializer_t* deser, const u8* dataPntr, u32 dataSize, const char* formatName, bool printOutErrors)
{
	Assert(deser != nullptr);
	Assert(dataPntr != nullptr || dataSize == 0);
	Assert(formatName != nullptr || !printOutErrors);
	ClearPointer(deser);
	deser->printOutErrors = printOutErrors;
	deser->errorOccurred = false;
	deser->formatName = formatName;
	deser->dataPntr = dataPntr;
	deser->dataSize = dataSize;
	deser->index = 0;
}

#define BIN_DESER_CONSUME_TYPE(deserPntr, variableToAssign, type) do                     \
{                                                                                        \
	if ((deserPntr)->index + sizeof(type) > (deserPntr)->dataSize)                       \
	{                                                                                    \
		if ((deserPntr)->printOutErrors)                                                 \
		{                                                                                \
			PrintLine_E("Expected %u more bytes in %s format deserialization for %s %s", \
				sizeof(type), (deserPntr)->formatName, #type, #variableToAssign          \
			);                                                                           \
		}                                                                                \
		(deserPntr)->errorOccurred = true;                                               \
		break;                                                                           \
	}                                                                                    \
	variableToAssign = *((const type*)(((deserPntr)->dataPntr) + ((deserPntr)->index))); \
	(deserPntr)->index += sizeof(type);                                                  \
} while(0)
#define BIN_DESER_CONSUME_BYTES(deserPntr, variablePntr, numBytes) do                             \
{                                                                                                 \
	if ((deserPntr)->index + (numBytes) > (deserPntr)->dataSize)                                  \
	{                                                                                             \
		if ((deserPntr)->printOutErrors)                                                          \
		{                                                                                         \
			PrintLine_E("Expected %u more bytes in %s format deserialization for %s",             \
				(numBytes), (deserPntr)->formatName, #variablePntr                                \
			);                                                                                    \
		}                                                                                         \
		(deserPntr)->errorOccurred = true;                                                        \
		break;                                                                                    \
	}                                                                                             \
	MyMemCopy((void*)(variablePntr), ((deserPntr)->dataPntr) + ((deserPntr)->index), (numBytes)); \
	(deserPntr)->index += (numBytes);                                                             \
} while(0)
#define BIN_DESER_CONSUME_STRUCT(deserPntr, pointerVarToAssign, type) do                 \
{                                                                                        \
	if ((deserPntr)->index + sizeof(type) > (deserPntr)->dataSize)                       \
	{                                                                                    \
		if ((deserPntr)->printOutErrors)                                                 \
		{                                                                                \
			PrintLine_E("Expected %u more bytes in %s format deserialization for %s %s", \
				sizeof(type), (deserPntr)->formatName, #type, #pointerVarToAssign        \
			);                                                                           \
		}                                                                                \
		(deserPntr)->errorOccurred = true;                                               \
		break;                                                                           \
	}                                                                                    \
	(pointerVarToAssign) = (const type*)((deserPntr)->dataPntr + (deserPntr)->index);    \
	(deserPntr)->index += sizeof(type);                                                  \
} while(0)
#define BIN_DESER_CONSUME_BYTES_PNTR(deserPntr, pointerVarToAssign, numBytes) do         \
{                                                                                        \
	if ((deserPntr)->index + (numBytes) > (deserPntr)->dataSize)                         \
	{                                                                                    \
		if ((deserPntr)->printOutErrors)                                                 \
		{                                                                                \
			PrintLine_E("Expected %u more bytes in %s format deserialization for %s %s", \
				(numBytes), (deserPntr)->formatName, #pointerVarToAssign                 \
			);                                                                           \
		}                                                                                \
		(deserPntr)->errorOccurred = true;                                               \
		break;                                                                           \
	}                                                                                    \
	(pointerVarToAssign) = (const u8*)((deserPntr)->dataPntr + (deserPntr)->index);      \
	(deserPntr)->index += (numBytes);                                                    \
} while(0)
