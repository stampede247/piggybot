/*
File:   res_sound_resource_list.h
Author: Taylor Robbins
Date:   11\21\2019
*/

#ifndef _RES_SOUND_RESOURCE_LIST_H
#define _RES_SOUND_RESOURCE_LIST_H

//get NUM_SOUND_EFFECT_RESOURCES defined
#define SOUND_EFFECT_ENTRY(internalName, looping, readableName, fileName) //nothing
#include "res_sounds_list.h"

struct SoundResource_t
{
	MemoryArena_t* allocArena;
	bool filePathChanged;
	char* filePath;
	char* readableName;
	char* internalName;
	Sound_t sound;
	r32 volume;
	bool looping;
};

struct SoundResourceList_t
{
	bool filled;
	Sound_t missingSound;
	Sound_t missingSoundLooping;
	
	union
	{
		SoundResource_t sounds[NUM_SOUND_EFFECT_RESOURCES];
		struct
		{
			#define SOUND_EFFECT_ENTRY(internalName, looping, readableName, fileName) SoundResource_t internalName
			#include "res_sounds_list.h"
		};
	};
};

#endif //  _RES_SOUND_RESOURCE_LIST_H
