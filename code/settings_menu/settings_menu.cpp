/*
File:   settings_menu.cpp
Author: Taylor Robbins
Date:   10\23\2020
Description: 
	** Holds the functions that run the Settings Menu App State
*/

void SettingsMenuUpdateRecs()
{
	settMenu->connectBtnRec.size = NewVec2(250, 100);
	settMenu->connectBtnRec.topLeft = RenderScreenSize/2 - settMenu->connectBtnRec.size/2;
}

void SettingsMenuCaptureMouse()
{
	if (IsMouseInside(settMenu->connectBtnRec)) { CaptureMouse("SettingsMenuConnectBtn"); }
}

// +--------------------------------------------------------------+
// |                   Initialize Settings Menu                   |
// +--------------------------------------------------------------+
void InitializeSettingsMenu()
{
	Assert(settMenu != nullptr);
	
	
	
	settMenu->initialized = true;
}

// +--------------------------------------------------------------+
// |                     Start Settings Menu                      |
// +--------------------------------------------------------------+
bool StartSettingsMenu(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	// SoftQueueMusicChange(GetMusic(menuMusic), SoundTransition_FadeIn);
	StopAllLoopingSounds();
	
	
	return true;
}

// +--------------------------------------------------------------+
// |                  Deinitialize Settings Menu                  |
// +--------------------------------------------------------------+
void DeinitializeSettingsMenu()
{
	Assert(settMenu != nullptr);
	Assert(settMenu->initialized == true);
	
	
	
	settMenu->initialized = false;
	ClearPointer(settMenu);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesSettingsMenuCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                     Update Settings Menu                     |
// +--------------------------------------------------------------+
void UpdateSettingsMenu(AppMenu_t appMenuAbove)
{
	Assert(settMenu != nullptr);
	Assert(settMenu->initialized);
	
	SettingsMenuUpdateRecs();
	SettingsMenuCaptureMouse();
	
	if (appMenuAbove == AppMenu_None)
	{
		bool connectBtnPressed = false;
		
		// +======================================+
		// | Handle Connect Btn Mouse Interaction |
		// +======================================+
		if (IsMouseOver("SettingsMenuConnectBtn"))
		{
			appOutput->cursorType = Cursor_Pointer;
			if (HandleMouseClickedOn(MouseButton_Left, "SettingsMenuConnectBtn"))
			{
				connectBtnPressed = true;
			}
		}
		if (ButtonReleased(Button_Enter))
		{
			HandleButton(Button_Enter);
			connectBtnPressed = true;
		}
		
		// +==============================+
		// |    Handle Connect Button     |
		// +==============================+
		if (connectBtnPressed)
		{
			if (!app->chat.created)
			{
				if (CreateChatHandler(&app->chat, false, false))
				{
					WriteLine_I("Chat Handler Connecting...");
				}
				else
				{
					WriteLine_E("Chat Handler failed to start connection!");
				}
			}
			else
			{
				WriteLine_W("Forcibly closing connection...");
				DestroyChatHandler(&app->chat);
				WriteLine_W("Connection Closed");
			}
		}
		
		// +==============================+
		// | Handle Switch States Hotkey  |
		// +==============================+
		if (ButtonPressed(Button_Tilde))
		{
			HandleButton(Button_Tilde);
			PushAppState(AppState_Visualizer);
		}
	}
}

// +--------------------------------------------------------------+
// |                     Render Settings Menu                     |
// +--------------------------------------------------------------+
void RenderSettingsMenu(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	// +==============================+
	// |         Render Setup         |
	// +==============================+
	RcBegin(GetShader(tileShader), NewRec(Vec2_Zero, RenderScreenSize), GetFont(pixelFont), renderBuffer, true);
	RcClear(ColorCornflowerBlue, 1.0f);
	SettingsMenuUpdateRecs();
	
	// +==============================+
	// |     Draw Connect Button      |
	// +==============================+
	{
		RcBindFont(GetFont(bigDebugFont));
		bool connected = app->chat.created;
		const char* connectBtnText = "Connect";
		if (connected) { connectBtnText = "Disconnect"; }
		v2 textPos = settMenu->connectBtnRec.topLeft + settMenu->connectBtnRec.size/2 + NewVec2(0, RcCenterOffset());
		Color_t buttonColor = connected ? PalOrange : PalGreen;
		Color_t textColor = White;
		bool isHovered = IsMouseOver("SettingsMenuConnectBtn");
		bool isPressed = IsMouseClickingOn(MouseButton_Left, "SettingsMenuConnectBtn");
		if (ButtonDownNoHandling(Button_Enter)) { isPressed = true; }
		
		if (isPressed) { buttonColor = connected ? PalOrangeDark : PalGreenDark; }
		else if (isHovered) { buttonColor = connected ? PalOrangeLight : PalGreenLight; }
		
		RcDrawRectangle(settMenu->connectBtnRec, buttonColor);
		RcDrawStringNt(connectBtnText, textPos, textColor, 1.0f, Alignment_Center);
	}
	
	// +==============================+
	// | Draw Chat Connection Status  |
	// +==============================+
	{
		RcBindFont(GetFont(debugFont));
		v2 textPos = NewVec2(settMenu->connectBtnRec.x + settMenu->connectBtnRec.width/2, settMenu->connectBtnRec.y - 5 - RcMaxExtendDown());
		const char* chatStatusStr = "[Disconnected]";
		if (app->chat.created)
		{
			if (app->chat.connection.state != IrcConnectionState_Connected)
			{
				chatStatusStr = GetIrcConnectionStateStr(app->chat.connection.state);
			}
			else
			{
				if (!app->chat.didLogin) { chatStatusStr = "Waiting to login"; }
				else if (!app->chat.gotJoinResponse) { chatStatusStr = "Logging in..."; }
				else
				{
					chatStatusStr = "Connected";
				}
			}
		}
		RcDrawStringNt(chatStatusStr, textPos, White, 1.0f, Alignment_Center);
	}
}

