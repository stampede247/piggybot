/*
File:   settings_menu.h
Author: Taylor Robbins
Date:   10\23\2020
*/

#ifndef _SETTINGS_MENU_H
#define _SETTINGS_MENU_H

struct SettingsMenuData_t
{
	bool initialized;
	
	rec connectBtnRec;
};

#endif //  _SETTINGS_MENU_H
