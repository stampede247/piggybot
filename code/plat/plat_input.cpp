/*
File:   plat_input.cpp
Author: Taylor Robbins
Date:   11\04\2017
Description: 
	** Holds a bunch of helper functions relating to processing the input into the
	** AppInput_t structure for the application to use
*/

InputEvent_t* Plat_PushInputEvent(AppInput_t* currentInput) //pre-declared in win32_func_defs.h (or equivelent file on other platforms)
{
	Assert(currentInput != nullptr);
	if (currentInput->numInputEvents < MAX_NUM_INPUT_EVENTS)
	{
		InputEvent_t* result = &currentInput->inputEvents[currentInput->numInputEvents];
		currentInput->numInputEvents++;
		ClearPointer(result);
		return result;
	}
	return nullptr;
}
InputEvent_t* Plat_PushInputEventSimple(AppInput_t* currentInput, InputEventType_t eventType) //pre-declared in win32_func_defs.h (or equivelent file on other platforms)
{
	Assert(currentInput != nullptr);
	if (currentInput->numInputEvents < MAX_NUM_INPUT_EVENTS)
	{
		InputEvent_t* result = &currentInput->inputEvents[currentInput->numInputEvents];
		currentInput->numInputEvents++;
		ClearPointer(result);
		result->type = eventType;
		return result;
	}
	return nullptr;
}

Buttons_t Plat_AppButtonForKey(i32 glfwKeyCode, bool upsideDownModeEnabled)
{
	switch (glfwKeyCode)
	{
		case GLFW_KEY_A:             return Button_A;
		case GLFW_KEY_B:             return Button_B;
		case GLFW_KEY_C:             return Button_C;
		case GLFW_KEY_D:             return Button_D;
		case GLFW_KEY_E:             return Button_E;
		case GLFW_KEY_F:             return Button_F;
		case GLFW_KEY_G:             return Button_G;
		case GLFW_KEY_H:             return Button_H;
		case GLFW_KEY_I:             return Button_I;
		case GLFW_KEY_J:             return Button_J;
		case GLFW_KEY_K:             return Button_K;
		case GLFW_KEY_L:             return Button_L;
		case GLFW_KEY_M:             return Button_M;
		case GLFW_KEY_N:             return Button_N;
		case GLFW_KEY_O:             return Button_O;
		case GLFW_KEY_P:             return Button_P;
		case GLFW_KEY_Q:             return Button_Q;
		case GLFW_KEY_R:             return Button_R;
		case GLFW_KEY_S:             return Button_S;
		case GLFW_KEY_T:             return Button_T;
		case GLFW_KEY_U:             return Button_U;
		case GLFW_KEY_V:             return Button_V;
		case GLFW_KEY_W:             return Button_W;
		case GLFW_KEY_X:             return Button_X;
		case GLFW_KEY_Y:             return Button_Y;
		case GLFW_KEY_Z:             return Button_Z;
		
		case GLFW_KEY_0:             return Button_0;
		case GLFW_KEY_1:             return Button_1;
		case GLFW_KEY_2:             return Button_2;
		case GLFW_KEY_3:             return Button_3;
		case GLFW_KEY_4:             return Button_4;
		case GLFW_KEY_5:             return Button_5;
		case GLFW_KEY_6:             return Button_6;
		case GLFW_KEY_7:             return Button_7;
		case GLFW_KEY_8:             return Button_8;
		case GLFW_KEY_9:             return Button_9;
		
		case GLFW_KEY_KP_0:          return Button_Num0;
		case GLFW_KEY_KP_1:          return Button_Num1;
		case GLFW_KEY_KP_2:          return Button_Num2;
		case GLFW_KEY_KP_3:          return Button_Num3;
		case GLFW_KEY_KP_4:          return Button_Num4;
		case GLFW_KEY_KP_5:          return Button_Num5;
		case GLFW_KEY_KP_6:          return Button_Num6;
		case GLFW_KEY_KP_7:          return Button_Num7;
		case GLFW_KEY_KP_8:          return Button_Num8;
		case GLFW_KEY_KP_9:          return Button_Num9;
		
		case GLFW_KEY_KP_DECIMAL:    return Button_NumPeriod;
		case GLFW_KEY_KP_DIVIDE:     return Button_NumDivide;
		case GLFW_KEY_KP_MULTIPLY:   return Button_NumMultiply;
		case GLFW_KEY_KP_SUBTRACT:   return Button_NumSubtract;
		case GLFW_KEY_KP_ADD:        return Button_NumAdd;
		case GLFW_KEY_KP_ENTER:      return Button_Enter;
		
		case GLFW_KEY_F1:            return Button_F1;
		case GLFW_KEY_F2:            return Button_F2;
		case GLFW_KEY_F3:            return Button_F3;
		case GLFW_KEY_F4:            return Button_F4;
		case GLFW_KEY_F5:            return Button_F5;
		case GLFW_KEY_F6:            return Button_F6;
		case GLFW_KEY_F7:            return Button_F7;
		case GLFW_KEY_F8:            return Button_F8;
		case GLFW_KEY_F9:            return Button_F9;
		case GLFW_KEY_F10:           return Button_F10;
		case GLFW_KEY_F11:           return Button_F11;
		case GLFW_KEY_F12:           return Button_F12;
		
		case GLFW_KEY_ENTER:         return Button_Enter;
		case GLFW_KEY_BACKSPACE:     return Button_Backspace;
		case GLFW_KEY_ESCAPE:        return Button_Escape;
		case GLFW_KEY_INSERT:        return Button_Insert;
		case GLFW_KEY_DELETE:        return Button_Delete;
		case GLFW_KEY_HOME:          return Button_Home;
		case GLFW_KEY_END:           return Button_End;
		case GLFW_KEY_PAGE_UP:       return Button_PageUp;
		case GLFW_KEY_PAGE_DOWN:     return Button_PageDown;
		case GLFW_KEY_TAB:           return Button_Tab;
		case GLFW_KEY_CAPS_LOCK:     return Button_CapsLock;
		case GLFW_KEY_NUM_LOCK:      return Button_NumLock;
		
		#if WINDOWS_COMPILATION
		
		case GLFW_KEY_LEFT_CONTROL:
		case GLFW_KEY_RIGHT_CONTROL: return Button_Control;
		case GLFW_KEY_LEFT_ALT:
		case GLFW_KEY_RIGHT_ALT:     return Button_Alt;
		case GLFW_KEY_LEFT_SHIFT:
		case GLFW_KEY_RIGHT_SHIFT:   return Button_Shift;
		
		#elif OSX_COMPILATION
		
		case GLFW_KEY_LEFT_CONTROL:
		case GLFW_KEY_RIGHT_CONTROL: return Button_Alt;
		case GLFW_KEY_LEFT_SUPER:
		case GLFW_KEY_RIGHT_SUPER:   return Button_Control;
		case GLFW_KEY_LEFT_SHIFT:
		case GLFW_KEY_RIGHT_SHIFT:   return Button_Shift;
		
		#endif
		
		case GLFW_KEY_RIGHT:         return Button_Right;
		case GLFW_KEY_LEFT:          return Button_Left;
		case GLFW_KEY_UP:            return upsideDownModeEnabled ? Button_Down : Button_Up;
		case GLFW_KEY_DOWN:          return upsideDownModeEnabled ? Button_Up : Button_Down;
		
		case GLFW_KEY_EQUAL:         return Button_Plus;
		case GLFW_KEY_MINUS:         return Button_Minus;
		case GLFW_KEY_BACKSLASH:     return Button_Pipe;
		case GLFW_KEY_LEFT_BRACKET:  return Button_OpenBracket;
		case GLFW_KEY_RIGHT_BRACKET: return Button_CloseBracket;
		case GLFW_KEY_SEMICOLON:     return Button_Colon;
		case GLFW_KEY_APOSTROPHE:    return Button_Quote;
		case GLFW_KEY_COMMA:         return Button_Comma;
		case GLFW_KEY_PERIOD:        return Button_Period;
		case GLFW_KEY_SLASH:         return Button_QuestionMark;
		case GLFW_KEY_GRAVE_ACCENT:  return Button_Tilde;
		case GLFW_KEY_SPACE:         return Button_Space;
		
		
		default:                     return Buttons_NumButtons;
	};
}

u8 Plat_GetKeysForButton(Buttons_t button, i32* keyBuffer)
{
	switch (button)
	{
		case Button_A:            keyBuffer[0] = GLFW_KEY_A; return 1;
		case Button_B:            keyBuffer[0] = GLFW_KEY_B; return 1;
		case Button_C:            keyBuffer[0] = GLFW_KEY_C; return 1;
		case Button_D:            keyBuffer[0] = GLFW_KEY_D; return 1;
		case Button_E:            keyBuffer[0] = GLFW_KEY_E; return 1;
		case Button_F:            keyBuffer[0] = GLFW_KEY_F; return 1;
		case Button_G:            keyBuffer[0] = GLFW_KEY_G; return 1;
		case Button_H:            keyBuffer[0] = GLFW_KEY_H; return 1;
		case Button_I:            keyBuffer[0] = GLFW_KEY_I; return 1;
		case Button_J:            keyBuffer[0] = GLFW_KEY_J; return 1;
		case Button_K:            keyBuffer[0] = GLFW_KEY_K; return 1;
		case Button_L:            keyBuffer[0] = GLFW_KEY_L; return 1;
		case Button_M:            keyBuffer[0] = GLFW_KEY_M; return 1;
		case Button_N:            keyBuffer[0] = GLFW_KEY_N; return 1;
		case Button_O:            keyBuffer[0] = GLFW_KEY_O; return 1;
		case Button_P:            keyBuffer[0] = GLFW_KEY_P; return 1;
		case Button_Q:            keyBuffer[0] = GLFW_KEY_Q; return 1;
		case Button_R:            keyBuffer[0] = GLFW_KEY_R; return 1;
		case Button_S:            keyBuffer[0] = GLFW_KEY_S; return 1;
		case Button_T:            keyBuffer[0] = GLFW_KEY_T; return 1;
		case Button_U:            keyBuffer[0] = GLFW_KEY_U; return 1;
		case Button_V:            keyBuffer[0] = GLFW_KEY_V; return 1;
		case Button_W:            keyBuffer[0] = GLFW_KEY_W; return 1;
		case Button_X:            keyBuffer[0] = GLFW_KEY_X; return 1;
		case Button_Y:            keyBuffer[0] = GLFW_KEY_Y; return 1;
		case Button_Z:            keyBuffer[0] = GLFW_KEY_Z; return 1;
		
		case Button_0:            keyBuffer[0] = GLFW_KEY_0; return 1;
		case Button_1:            keyBuffer[0] = GLFW_KEY_1; return 1;
		case Button_2:            keyBuffer[0] = GLFW_KEY_2; return 1;
		case Button_3:            keyBuffer[0] = GLFW_KEY_3; return 1;
		case Button_4:            keyBuffer[0] = GLFW_KEY_4; return 1;
		case Button_5:            keyBuffer[0] = GLFW_KEY_5; return 1;
		case Button_6:            keyBuffer[0] = GLFW_KEY_6; return 1;
		case Button_7:            keyBuffer[0] = GLFW_KEY_7; return 1;
		case Button_8:            keyBuffer[0] = GLFW_KEY_8; return 1;
		case Button_9:            keyBuffer[0] = GLFW_KEY_9; return 1;
		
		case Button_Num0:         keyBuffer[0] = GLFW_KEY_KP_0; return 1;
		case Button_Num1:         keyBuffer[0] = GLFW_KEY_KP_1; return 1;
		case Button_Num2:         keyBuffer[0] = GLFW_KEY_KP_2; return 1;
		case Button_Num3:         keyBuffer[0] = GLFW_KEY_KP_3; return 1;
		case Button_Num4:         keyBuffer[0] = GLFW_KEY_KP_4; return 1;
		case Button_Num5:         keyBuffer[0] = GLFW_KEY_KP_5; return 1;
		case Button_Num6:         keyBuffer[0] = GLFW_KEY_KP_6; return 1;
		case Button_Num7:         keyBuffer[0] = GLFW_KEY_KP_7; return 1;
		case Button_Num8:         keyBuffer[0] = GLFW_KEY_KP_8; return 1;
		case Button_Num9:         keyBuffer[0] = GLFW_KEY_KP_9; return 1;
		
		case Button_NumPeriod:    keyBuffer[0] = GLFW_KEY_KP_DECIMAL;  return 1;
		case Button_NumDivide:    keyBuffer[0] = GLFW_KEY_KP_DIVIDE;   return 1;
		case Button_NumMultiply:  keyBuffer[0] = GLFW_KEY_KP_MULTIPLY; return 1;
		case Button_NumSubtract:  keyBuffer[0] = GLFW_KEY_KP_SUBTRACT; return 1;
		case Button_NumAdd:       keyBuffer[0] = GLFW_KEY_KP_ADD;      return 1;
		
		case Button_F1:           keyBuffer[0] = GLFW_KEY_F1; return 1;
		case Button_F2:           keyBuffer[0] = GLFW_KEY_F2; return 1;
		case Button_F3:           keyBuffer[0] = GLFW_KEY_F3; return 1;
		case Button_F4:           keyBuffer[0] = GLFW_KEY_F4; return 1;
		case Button_F5:           keyBuffer[0] = GLFW_KEY_F5; return 1;
		case Button_F6:           keyBuffer[0] = GLFW_KEY_F6; return 1;
		case Button_F7:           keyBuffer[0] = GLFW_KEY_F7; return 1;
		case Button_F8:           keyBuffer[0] = GLFW_KEY_F8; return 1;
		case Button_F9:           keyBuffer[0] = GLFW_KEY_F9; return 1;
		case Button_F10:          keyBuffer[0] = GLFW_KEY_F10; return 1;
		case Button_F11:          keyBuffer[0] = GLFW_KEY_F11; return 1;
		case Button_F12:          keyBuffer[0] = GLFW_KEY_F12; return 1;
		
		case Button_Enter:        keyBuffer[0] = GLFW_KEY_ENTER; keyBuffer[1] = GLFW_KEY_KP_ENTER; return 2;
		case Button_Backspace:    keyBuffer[0] = GLFW_KEY_BACKSPACE; return 1;
		case Button_Escape:       keyBuffer[0] = GLFW_KEY_ESCAPE; return 1;
		case Button_Insert:       keyBuffer[0] = GLFW_KEY_INSERT; return 1;
		case Button_Delete:       keyBuffer[0] = GLFW_KEY_DELETE; return 1;
		case Button_Home:         keyBuffer[0] = GLFW_KEY_HOME; return 1;
		case Button_End:          keyBuffer[0] = GLFW_KEY_END; return 1;
		case Button_PageUp:       keyBuffer[0] = GLFW_KEY_PAGE_UP; return 1;
		case Button_PageDown:     keyBuffer[0] = GLFW_KEY_PAGE_DOWN; return 1;
		case Button_Tab:          keyBuffer[0] = GLFW_KEY_TAB; return 1;
		case Button_CapsLock:     keyBuffer[0] = GLFW_KEY_CAPS_LOCK; return 1;
		case Button_NumLock:      keyBuffer[0] = GLFW_KEY_NUM_LOCK;  return 1;
		
		case Button_Control:      keyBuffer[0] = GLFW_KEY_LEFT_CONTROL; keyBuffer[1] = GLFW_KEY_RIGHT_CONTROL; return 2;
		case Button_Alt:          keyBuffer[0] = GLFW_KEY_LEFT_ALT;     keyBuffer[1] = GLFW_KEY_RIGHT_ALT; return 2;  
		case Button_Shift:        keyBuffer[0] = GLFW_KEY_LEFT_SHIFT;   keyBuffer[1] = GLFW_KEY_RIGHT_SHIFT; return 2;
		
		case Button_Right:        keyBuffer[0] = GLFW_KEY_RIGHT; return 1;
		case Button_Left:         keyBuffer[0] = GLFW_KEY_LEFT; return 1;
		case Button_Up:           keyBuffer[0] = GLFW_KEY_UP; return 1;
		case Button_Down:         keyBuffer[0] = GLFW_KEY_DOWN; return 1;
		
		case Button_Plus:         keyBuffer[0] = GLFW_KEY_EQUAL; return 1;
		case Button_Minus:        keyBuffer[0] = GLFW_KEY_MINUS; return 1;
		case Button_Pipe:         keyBuffer[0] = GLFW_KEY_BACKSLASH; return 1;
		case Button_OpenBracket:  keyBuffer[0] = GLFW_KEY_LEFT_BRACKET; return 1;
		case Button_CloseBracket: keyBuffer[0] = GLFW_KEY_RIGHT_BRACKET; return 1;
		case Button_Colon:        keyBuffer[0] = GLFW_KEY_SEMICOLON; return 1;
		case Button_Quote:        keyBuffer[0] = GLFW_KEY_APOSTROPHE; return 1;
		case Button_Comma:        keyBuffer[0] = GLFW_KEY_COMMA; return 1;
		case Button_Period:       keyBuffer[0] = GLFW_KEY_PERIOD; return 1;
		case Button_QuestionMark: keyBuffer[0] = GLFW_KEY_SLASH; return 1;
		case Button_Tilde:        keyBuffer[0] = GLFW_KEY_GRAVE_ACCENT; return 1;
		case Button_Space:        keyBuffer[0] = GLFW_KEY_SPACE; return 1;
		
		
		default: return 0;
	};
}

Buttons_t Plat_AppButtonForMouse(i32 glfwButtonCode)
{
	switch (glfwButtonCode)
	{
		case GLFW_MOUSE_BUTTON_LEFT:   return MouseButton_Left;
		case GLFW_MOUSE_BUTTON_RIGHT:  return MouseButton_Right;
		case GLFW_MOUSE_BUTTON_MIDDLE: return MouseButton_Middle;
		
		
		default:                       return Buttons_NumButtons;
	};
}

u8 Plat_GetMouseBtnsForButton(Buttons_t button, i32* keyBuffer)
{
	switch (button)
	{
		case MouseButton_Left:   keyBuffer[0] = GLFW_MOUSE_BUTTON_LEFT; return 1;
		case MouseButton_Right:  keyBuffer[0] = GLFW_MOUSE_BUTTON_RIGHT; return 1;
		case MouseButton_Middle: keyBuffer[0] = GLFW_MOUSE_BUTTON_MIDDLE; return 1;
		
		
		default: return 0;
	};
}

void Plat_HandleMouseEvent(GLFWwindow* window, AppInput_t* currentInput, i32 glfwButtonCode, i32 action, i32 modifiers)
{
	Buttons_t button = Plat_AppButtonForMouse(glfwButtonCode);
	if (button >= Buttons_NumButtons) return; //No keymap
	
	ButtonState_t* buttonState = &currentInput->buttons[button];
	bool ctrl          = IsFlagSet(modifiers, GLFW_MOD_CONTROL);
	bool alt           = IsFlagSet(modifiers, GLFW_MOD_ALT);
	bool shift         = IsFlagSet(modifiers, GLFW_MOD_SHIFT);
	bool super         = IsFlagSet(modifiers, GLFW_MOD_SUPER);
	bool capsLock;
	#if WINDOWS_COMPILATION
	SHORT capsKeyState = GetKeyState(VK_CAPITAL); //TODO: This is win32 platform specific, implement this on other platforms
	capsLock      = IsFlagSet(capsKeyState, 0x01);
	#else
	capsLock      = IsFlagSet(modifiers, GLFW_MOD_CAPS_LOCK);
	#endif
	bool pressed       = (action == GLFW_PRESS);
	
	if (action == GLFW_REPEAT)
	{
		buttonState->repeatCount++;
		
		InputEvent_t* inputEvent = Plat_PushInputEvent(currentInput);
		if (inputEvent != nullptr)
		{
			inputEvent->type = InputEventType_ButtonTransition;
			inputEvent->button = button;
			inputEvent->flags |= BtnTransFlag_Pressed|BtnTransFlag_Repeat;
			if (ctrl)     { inputEvent->flags |= BtnTransFlag_Ctrl;  }
			if (alt)      { inputEvent->flags |= BtnTransFlag_Alt;   }
			if (shift)    { inputEvent->flags |= BtnTransFlag_Shift; }
			if (super)    { inputEvent->flags |= BtnTransFlag_Super; }
			if (capsLock) { inputEvent->flags |= BtnTransFlag_CapsLock; }
			inputEvent->mousePos = currentInput->mousePos;
		}
	}
	else if (buttonState->isDown != pressed)
	{
		if (pressed)
		{
			//NOTE: If the key is being pressed then all we care about is the 
			//		first key pressed when isDown is false
			buttonState->transCount++;
			buttonState->isDown = true;
			buttonState->lastTransTime = 0;
			
			if (button == MouseButton_Left)
			{
				currentInput->mouseStartPos[MouseButton_Left] = currentInput->mousePos;
				currentInput->mouseMaxDist[MouseButton_Left] = 0;
			}
			else if (button == MouseButton_Right)
			{
				currentInput->mouseStartPos[MouseButton_Right] = currentInput->mousePos;
				currentInput->mouseMaxDist[MouseButton_Right] = 0;
			}
			else if (button == MouseButton_Middle)
			{
				currentInput->mouseStartPos[MouseButton_Middle] = currentInput->mousePos;
				currentInput->mouseMaxDist[MouseButton_Middle] = 0;
			}
			
			InputEvent_t* inputEvent = Plat_PushInputEvent(currentInput);
			if (inputEvent != nullptr)
			{
				inputEvent->type = InputEventType_ButtonTransition;
				inputEvent->button = button;
				inputEvent->flags |= BtnTransFlag_Pressed;
				if (ctrl)     { inputEvent->flags |= BtnTransFlag_Ctrl;  }
				if (alt)      { inputEvent->flags |= BtnTransFlag_Alt;   }
				if (shift)    { inputEvent->flags |= BtnTransFlag_Shift; }
				if (super)    { inputEvent->flags |= BtnTransFlag_Super; }
				if (capsLock) { inputEvent->flags |= BtnTransFlag_CapsLock; }
				inputEvent->mousePos = currentInput->mousePos;
			}
		}
		else
		{
			//NOTE: If the key is being released we need to check for multiple key mappings
			//		and only trigger when the last one is released
			bool foundPressedKey = false;
			i32 keys[4] = {};
			
			u8 numKeys = Plat_GetMouseBtnsForButton(button, keys);
			for (u8 keyIndex = 0; keyIndex < numKeys; keyIndex++)
			{
				if (keys[keyIndex] != glfwButtonCode && glfwGetMouseButton(window, keys[keyIndex]) == GLFW_PRESS)
				{
					WriteLine_D("Other mouse button held, ignoring button release");
					
					foundPressedKey = true;
					break;
				}
			}
			if (!foundPressedKey)
			{
				ClearArray(keys);
				numKeys = Plat_GetKeysForButton(button, keys);
				for (u8 keyIndex = 0; keyIndex < numKeys; keyIndex++)
				{
					if (glfwGetKey(window, keys[keyIndex]) == GLFW_PRESS)
					{
						WriteLine_D("Other key held, ignoring button release");
							
						foundPressedKey = true;
						break;
					}
				}
			}
			
			if (!foundPressedKey)
			{
				buttonState->transCount++;
				buttonState->isDown = false;
				buttonState->lastTransTime = 0;
				
				InputEvent_t* inputEvent = Plat_PushInputEvent(currentInput);
				if (inputEvent != nullptr)
				{
					inputEvent->type = InputEventType_ButtonTransition;
					inputEvent->button = button;
					if (ctrl)     { inputEvent->flags |= BtnTransFlag_Ctrl;  }
					if (alt)      { inputEvent->flags |= BtnTransFlag_Alt;   }
					if (shift)    { inputEvent->flags |= BtnTransFlag_Shift; }
					if (super)    { inputEvent->flags |= BtnTransFlag_Super; }
					if (capsLock) { inputEvent->flags |= BtnTransFlag_CapsLock; }
					inputEvent->mousePos = currentInput->mousePos;
				}
			}
		}
	}
}

void Plat_HandleKeyEvent(GLFWwindow* window, AppInput_t* currentInput, i32 glfwKeyCode, i32 action, i32 modifiers)
{
	Buttons_t button = Plat_AppButtonForKey(glfwKeyCode, Plat_IsUpsideDownModeEnabled());
	if (button >= Buttons_NumButtons) { return; } //No keymap
	
	ButtonState_t* buttonState = &currentInput->buttons[button];
	bool ctrl          = IsFlagSet(modifiers, GLFW_MOD_CONTROL);
	bool alt           = IsFlagSet(modifiers, GLFW_MOD_ALT);
	bool shift         = IsFlagSet(modifiers, GLFW_MOD_SHIFT);
	bool super         = IsFlagSet(modifiers, GLFW_MOD_SUPER);
	bool capsLock;
	#if WINDOWS_COMPILATION
	SHORT capsKeyState = GetKeyState(VK_CAPITAL); //TODO: This is win32 platform specific, implement this on other platforms
	capsLock        = IsFlagSet(capsKeyState, 0x01);
	#else
	capsLock        = IsFlagSet(modifiers, GLFW_MOD_CAPS_LOCK);
	#endif
	bool pressed       = (action == GLFW_PRESS);
	
	char character = '\0';
	if (!ctrl && !alt && !super)
	{
		character = GetButtonChar(button, shift, capsLock);
	}
	
	if (action == GLFW_REPEAT)
	{
		buttonState->repeatCount++;
		
		InputEvent_t* inputEvent = Plat_PushInputEvent(currentInput);
		if (inputEvent != nullptr)
		{
			inputEvent->type = InputEventType_ButtonTransition;
			inputEvent->button = button;
			inputEvent->character = character;
			inputEvent->flags |= BtnTransFlag_Pressed|BtnTransFlag_Repeat;
			if (character != '\0') { inputEvent->flags |= BtnTransFlag_Char;     }
			if (ctrl)              { inputEvent->flags |= BtnTransFlag_Ctrl;     }
			if (alt)               { inputEvent->flags |= BtnTransFlag_Alt;      }
			if (shift)             { inputEvent->flags |= BtnTransFlag_Shift;    }
			if (super)             { inputEvent->flags |= BtnTransFlag_Super;    }
			if (capsLock)          { inputEvent->flags |= BtnTransFlag_CapsLock; }
			inputEvent->mousePos = currentInput->mousePos;
		}
	}
	else if (buttonState->isDown != pressed)
	{
		if (pressed)
		{
			//NOTE: If the key is being pressed then all we care about is the 
			//		first key pressed when isDown is false
			buttonState->transCount++;
			buttonState->isDown = true;
			buttonState->lastTransTime = 0;
			
			InputEvent_t* inputEvent = Plat_PushInputEvent(currentInput);
			if (inputEvent != nullptr)
			{
				inputEvent->type = InputEventType_ButtonTransition;
				inputEvent->button = button;
				inputEvent->character = character;
				inputEvent->flags |= BtnTransFlag_Pressed;
				if (character != '\0') { inputEvent->flags |= BtnTransFlag_Char;     }
				if (ctrl)              { inputEvent->flags |= BtnTransFlag_Ctrl;     }
				if (alt)               { inputEvent->flags |= BtnTransFlag_Alt;      }
				if (shift)             { inputEvent->flags |= BtnTransFlag_Shift;    }
				if (super)             { inputEvent->flags |= BtnTransFlag_Super;    }
				if (capsLock)          { inputEvent->flags |= BtnTransFlag_CapsLock; }
				inputEvent->mousePos = currentInput->mousePos;
			}
		}
		else
		{
			//NOTE: If the key is being released we need to check for multiple key mappings
			//		and only trigger when the last one is released
			bool foundPressedKey = false;
			i32 keys[4] = {};
			
			u8 numKeys = Plat_GetMouseBtnsForButton(button, keys);
			for (u8 keyIndex = 0; keyIndex < numKeys; keyIndex++)
			{
				if (glfwGetMouseButton(window, keys[keyIndex]) == GLFW_PRESS)
				{
					WriteLine_D("Other mouse button held, ignoring key release");
					
					foundPressedKey = true;
					break;
				}
			}
			if (!foundPressedKey)
			{
				ClearArray(keys);
				numKeys = Plat_GetKeysForButton(button, keys);
				for (u8 keyIndex = 0; keyIndex < numKeys; keyIndex++)
				{
					if (keys[keyIndex] != glfwKeyCode && glfwGetKey(window, keys[keyIndex]) == GLFW_PRESS)
					{
						WriteLine_D("Other key held, ignoring key release");
						
						foundPressedKey = true;
						break;
					}
				}
			}
			
			if (!foundPressedKey)
			{
				buttonState->transCount++;
				buttonState->isDown = false;
				buttonState->lastTransTime = 0;
				
				InputEvent_t* inputEvent = Plat_PushInputEvent(currentInput);
				if (inputEvent != nullptr)
				{
					inputEvent->type = InputEventType_ButtonTransition;
					inputEvent->button = button;
					inputEvent->character = character;
					if (character != '\0') { inputEvent->flags |= BtnTransFlag_Char;     }
					if (ctrl)              { inputEvent->flags |= BtnTransFlag_Ctrl;     }
					if (alt)               { inputEvent->flags |= BtnTransFlag_Alt;      }
					if (shift)             { inputEvent->flags |= BtnTransFlag_Shift;    }
					if (super)             { inputEvent->flags |= BtnTransFlag_Super;    }
					if (capsLock)          { inputEvent->flags |= BtnTransFlag_CapsLock; }
					inputEvent->mousePos = currentInput->mousePos;
				}
			}
		}
	}
}

void Plat_UpdateGamepadButton(AppInput_t* currentInput, u32 gamepadIndex, GamepadButtons_t button, bool isDown)
{
	Assert(currentInput != nullptr);
	if (gamepadIndex >= MAX_NUM_GAMEPADS) { return; }
	if (button >= Gamepad_NumButtons) { return; }
	ButtonState_t* btnState = &currentInput->gamepads[gamepadIndex].buttons[button];
	if (btnState->isDown != isDown)
	{
		btnState->transCount++;
		btnState->isDown = isDown;
		btnState->lastTransTime = 0;
		
		InputEvent_t* inputEvent = Plat_PushInputEvent(currentInput);
		if (inputEvent != nullptr)
		{
			inputEvent->type = InputEventType_GamepadTransition;
			inputEvent->gamepadBtn = button;
			inputEvent->gamepadIndex = gamepadIndex;
			if (isDown) { inputEvent->flags |= GamepadTransFlag_Pressed; }
		}
	}
}

void Plat_DestroyAppInput(AppInput_t* appInput)
{
	Assert(appInput != nullptr);
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		InputEvent_t* inputEvent = &appInput->inputEvents[eIndex];
		if (inputEvent->filePath != nullptr)
		{
			ArenaPop(&platform.mainHeap, inputEvent->filePath);
		}
	}
	appInput->numInputEvents = 0;
}

void Plat_SwapAppInputs(AppInput_t* currentInput, AppInput_t* newInput)
{
	Assert(currentInput != nullptr && newInput != nullptr && currentInput != newInput);
	
	MyMemCopy(newInput, currentInput, sizeof(AppInput_t));
	
	newInput->mouseInsideChanged = false;
	newInput->mouseMoved = false;
	newInput->scrollDelta = Vec2_Zero;
	newInput->scrollChanged = false;
	newInput->textInputLength = 0;
	
	newInput->numInputEvents = 0;
	
	newInput->windowMoved = false;
	newInput->windowResized = false;
	
	newInput->windowFocusChanged = false;
	newInput->fullscreenChanged = false;
	newInput->windowMinimizedChanged = false;
	newInput->windowInteractionHappened = false;
	
	newInput->currentMonitorChanged = false;
	newInput->currentDisplayModeChanged = false;
	newInput->targetFramerateChanged = false;
	
	for (u32 bIndex = 0; bIndex < ArrayCount(newInput->buttons); bIndex++)
	{
		ButtonState_t* btnState = &newInput->buttons[bIndex];
		btnState->transCount = 0;
		btnState->repeatCount = 0;
	}
	
	for (i32 gIndex = 0; gIndex < ArrayCount(newInput->gamepads); gIndex++)
	{
		GamepadState_t* gamepad = &newInput->gamepads[gIndex];
		gamepad->wasConnected = gamepad->isConnected;
		for (u32 bIndex = 0; bIndex < ArrayCount(gamepad->buttons); bIndex++)
		{
			gamepad->buttons[bIndex].transCount = 0;
			gamepad->buttons[bIndex].repeatCount = 0;
		}
	}
}

void Plat_UpdateAppInputGamepadInfo(AppInput_t* appInput)
{
	Assert(appInput != nullptr);
	
	for (u32 gIndex = 0; gIndex < MAX_NUM_GAMEPADS; gIndex++)
	{
		GamepadState_t* gamepad = &appInput->gamepads[gIndex];
		GLFWgamepadstate gpState;
		gamepad->isConnected = (glfwJoystickPresent(gIndex) && glfwJoystickIsGamepad(gIndex) && glfwGetGamepadState(gIndex, &gpState));
		if (gamepad->isConnected != gamepad->wasConnected)
		{
			PrintLine_R("Gamepad %u %s!", gIndex, gamepad->isConnected ? "Connected" : "Disconnected");
		}
		
		if (gamepad->isConnected)
		{
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_A,            (gpState.buttons[GLFW_GAMEPAD_BUTTON_A]            == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_B,            (gpState.buttons[GLFW_GAMEPAD_BUTTON_B]            == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_X,            (gpState.buttons[GLFW_GAMEPAD_BUTTON_X]            == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_Y,            (gpState.buttons[GLFW_GAMEPAD_BUTTON_Y]            == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_Right,        (gpState.buttons[GLFW_GAMEPAD_BUTTON_DPAD_RIGHT]   == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_Left,         (gpState.buttons[GLFW_GAMEPAD_BUTTON_DPAD_LEFT]    == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_Up,           (gpState.buttons[GLFW_GAMEPAD_BUTTON_DPAD_UP]      == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_Down,         (gpState.buttons[GLFW_GAMEPAD_BUTTON_DPAD_DOWN]    == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_LeftBumper,   (gpState.buttons[GLFW_GAMEPAD_BUTTON_LEFT_BUMPER]  == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_RightBumper,  (gpState.buttons[GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER] == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_LeftStick,    (gpState.buttons[GLFW_GAMEPAD_BUTTON_LEFT_THUMB]   == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_RightStick,   (gpState.buttons[GLFW_GAMEPAD_BUTTON_RIGHT_THUMB]  == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_Start,        (gpState.buttons[GLFW_GAMEPAD_BUTTON_START]        == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_Back,         (gpState.buttons[GLFW_GAMEPAD_BUTTON_BACK]         == GLFW_PRESS));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_Home,         (gpState.buttons[GLFW_GAMEPAD_BUTTON_GUIDE]        == GLFW_PRESS));
			
			gamepad->leftStickRaw.x  = gpState.axes[GLFW_GAMEPAD_AXIS_LEFT_X];
			gamepad->leftStickRaw.y  = gpState.axes[GLFW_GAMEPAD_AXIS_LEFT_Y];
			gamepad->rightStickRaw.x = gpState.axes[GLFW_GAMEPAD_AXIS_RIGHT_X];
			gamepad->rightStickRaw.y = gpState.axes[GLFW_GAMEPAD_AXIS_RIGHT_Y];
			gamepad->leftTrigger  = gpState.axes[GLFW_GAMEPAD_AXIS_LEFT_TRIGGER];
			gamepad->rightTrigger = gpState.axes[GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER];
			
			gamepad->leftStick  = (Vec2Length(gamepad->leftStickRaw)  > GAMEPAD_STICK_DEAD_ZONE) ? gamepad->leftStickRaw  : Vec2_Zero;
			gamepad->rightStick = (Vec2Length(gamepad->rightStickRaw) > GAMEPAD_STICK_DEAD_ZONE) ? gamepad->rightStickRaw : Vec2_Zero;
			
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_LeftTrigger,  (gamepad->leftTrigger  > -1.0f));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_RightTrigger, (gamepad->rightTrigger > -1.0f));
			
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_lsRight, (Vec2Length(gamepad->leftStick) >= GAMEPAD_STICK_DIRECTION_DEAD_ZONE && NewDir2(gamepad->leftStick) == Dir2_Right));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_lsDown,  (Vec2Length(gamepad->leftStick) >= GAMEPAD_STICK_DIRECTION_DEAD_ZONE && NewDir2(gamepad->leftStick) == Dir2_Down));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_lsLeft,  (Vec2Length(gamepad->leftStick) >= GAMEPAD_STICK_DIRECTION_DEAD_ZONE && NewDir2(gamepad->leftStick) == Dir2_Left));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_lsUp,    (Vec2Length(gamepad->leftStick) >= GAMEPAD_STICK_DIRECTION_DEAD_ZONE && NewDir2(gamepad->leftStick) == Dir2_Up));
			
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_rsRight, (Vec2Length(gamepad->rightStick) >= GAMEPAD_STICK_DIRECTION_DEAD_ZONE && NewDir2(gamepad->rightStick) == Dir2_Right));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_rsDown,  (Vec2Length(gamepad->rightStick) >= GAMEPAD_STICK_DIRECTION_DEAD_ZONE && NewDir2(gamepad->rightStick) == Dir2_Down));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_rsLeft,  (Vec2Length(gamepad->rightStick) >= GAMEPAD_STICK_DIRECTION_DEAD_ZONE && NewDir2(gamepad->rightStick) == Dir2_Left));
			Plat_UpdateGamepadButton(appInput, gIndex, Gamepad_rsUp,    (Vec2Length(gamepad->rightStick) >= GAMEPAD_STICK_DIRECTION_DEAD_ZONE && NewDir2(gamepad->rightStick) == Dir2_Up));
			
			gamepad->dPad = Vec2_Zero;
			if (gamepad->buttons[Gamepad_Right].isDown) { gamepad->dPad.x += 1.0f; }
			if (gamepad->buttons[Gamepad_Left].isDown)  { gamepad->dPad.x -= 1.0f; }
			if (gamepad->buttons[Gamepad_Down].isDown)  { gamepad->dPad.y -= 1.0f; }
			if (gamepad->buttons[Gamepad_Up].isDown)    { gamepad->dPad.y += 1.0f; }
			
			const char* gamepadName = glfwGetGamepadName(gIndex);
			if (gamepadName == nullptr) { gamepadName = "Unknown"; }
			if (gamepad->name == nullptr || !StrCompareIgnoreCaseNt(gamepad->name, gamepadName))
			{
				if (gamepad->name != nullptr) { ArenaPop(&platform.mainHeap, gamepad->name); }
				gamepad->name = ArenaNtString(&platform.mainHeap, gamepadName);
				Assert(gamepad->name != nullptr);
				gamepad->isPlaystation = StrFindSubstringIgnoreCaseNt(gamepad->name, "PS");
				#if 0
				if (gIndex > 0) { gamepad->isPlaystation = false; }
				#endif
			}
		}
		else
		{
			if (gamepad->name != nullptr)
			{
				ArenaPop(&platform.mainHeap, gamepad->name);
				gamepad->name = nullptr;
			}
		}
	}
}
